#!/usr/bin/python3
import sys, os, subprocess

from PySide2.QtWidgets import QLabel, QWidget, QDesktopWidget, QFileDialog, QApplication, QMainWindow
from PySide2.QtCore import Qt, QTimer
from PySide2.QtGui import QIcon, QPixmap
# from PySide2.QtWebEngineWidgets import QWebEngineView, QWebEnginePage, QWebEngineSettings

from modules import monitor_window as monitor
from modules.paths import *
from modules.utils import *
from modules import config

if not os.path.isdir(path_tmp):
    os.mkdir(path_tmp)


class main_window(QMainWindow):
    def __init__(self, parent=None):
        super(main_window, self).__init__(parent)
        self.setWindowIcon(QIcon(os.path.join(path_graphics, 'kihvim.png')))
        self.setFocusPolicy(Qt.StrongFocus)
        self.setGeometry(0, 0, QDesktopWidget().screenGeometry().width(), QDesktopWidget().screenGeometry().height())
        self.setWindowTitle('kihvim')

        self.main_background = QLabel(parent=self)
        self.main_background.setStyleSheet('QWidget {border-image: url(' + os.path.join(path_graphics, 'main_background.png').replace('\\', '/') + ') ; } ')

        self.settings = {}

        from modules import config
        config.load(self)

        self.monitor = monitor
        self.monitor.load(self)
        self.monitor_window.show()

        #self.screencopy_window = screencopy_window.screencopy_window(self)

        self.screen_width_proportion = float(self.monitor_window.height())/float(self.monitor_window.width())
        self.screen_height_proportion = float(self.monitor_window.width())/float(self.monitor_window.height())

        self.main_options_box_is_visible = False
        self.main_options_box_selected = False
        from modules import options_box
        self.options_box = options_box
        self.options_box.load(self)

        from modules import right_panel
        self.right_panel = right_panel
        self.right_panel.load(self)

        self.playlist_options_is_visible = False
        self.playlist = []
        self.last_item = False
        self.selected_media = False
        self.highlighted_media = 0
        from modules import playlist_panel
        self.playlist_panel = playlist_panel
        self.playlist_panel.load(self)

        playlist_panel.populate_playlist(self)

        self.selected_addition = False
        self.list_of_images_from_folder = {}
        self.list_of_images_added = {}
        self.list_of_videos_from_folder = {}
        self.list_of_videos_added = {}
        self.list_of_video_songs = {}
        self.list_of_local_videos = {}
        self.list_of_bible_videos = {}
        self.list_of_library_videos = {}
        self.list_of_cameras = {}
        self.list_of_songs = {}
        from modules import left_panel
        self.left_panel = left_panel
        self.left_panel.load(self)

        left_panel.populate_list_of_images_from_folder(self)
        left_panel.populate_image_list(self)

        self.left_panel_audio_icon.setVisible(False)
        self.left_panel_audio_panel.setVisible(False)

        left_panel.populate_list_of_videos_from_folder(self)

        left_panel.populate_video_song_list(self)
        left_panel.populate_video_local_list(self)
        left_panel.populate_video_bible_list(self)
        left_panel.populate_video_library_list(self)

        #self.left_panel_video_icon.setVisible(False)
        #self.left_panel_video_panel.setVisible(False)

        self.left_panel_web_icon.setVisible(False)
        self.left_panel_web_panel.setVisible(False)

        self.left_panel_clock_icon.setVisible(False)
        self.left_panel_clock_panel.setVisible(False)

        self.left_panel_screencopy_icon.setVisible(False)
        self.left_panel_screencopy_panel.setVisible(False)

        self.left_panel_camera_icon.setVisible(False)
        self.left_panel_camera_panel.setVisible(False)

        self.right_panel_monitor_player.setVisible(False)

        self.showMaximized()

        self.timer = QTimer(self)
        self.timer.setInterval(1000/self.settings['interface_framerate'])
        self.timer.timeout.connect(lambda: self.update_things())
        self.timer.start()

    def update_things(self):
        # if QDesktopWidget().screenCount() > 1:
        self.right_panel_monitor.setPixmap(QApplication.primaryScreen().grabWindow(self.monitor_window.winId()))#), self.monitor_window.x(), self.monitor_window.y(), self.monitor_window.width(), self.monitor_window.height()))

        if self.video_player.get_state() == 3:
            if self.monitor.current_media[0] in ['audio', 'video']:
                self.right_panel_monitor_player_timeline.update()
                if not self.video_player.audio_get_volume() == 100:
                    self.video_player.setVolume(100)
                if self.monitor.current_media[0] == 'video' and self.monitor.current_media[6]:
                    if self.video_player.get_position() > self.monitor.current_media[6]/self.monitor.current_media[7]:
                        self.video_player.stop()
            elif self.monitor.current_media[0] in ['clock']:
                self.right_panel_monitor_prelude_timeline.update()
                if self.right_panel_monitor_prelude.time_to_full_volume:
                    volume = (((datetime.datetime.now() - self.right_panel_monitor_prelude.time_to_full_volume[0]).total_seconds()) / self.right_panel_monitor_prelude.time_to_full_volume[1] ) * self.right_panel_monitor_prelude_volume.value()

                    if self.right_panel_monitor_prelude.time_to_full_volume[2]:
                        volume = self.right_panel_monitor_prelude_volume.value() - volume

                    if volume >= self.right_panel_monitor_prelude_volume.value():
                        volume = self.right_panel_monitor_prelude_volume.value()
                        self.right_panel_monitor_prelude.time_to_full_volume = False

                    elif volume <= 0:
                        if self.right_panel_monitor_prelude.time_to_full_volume[3]:
                            self.video_player.stop()
                        else:
                            volume = 0
                        self.right_panel_monitor_prelude.time_to_full_volume = False

                    if not (self.monitor.current_media[4] or (self.right_panel_monitor_prelude_pausebutton.isChecked() and not self.right_panel_monitor_prelude_nextbutton.isEnabled())):
                        self.video_player.audio_set_volume(int(volume))
                    if int(volume) == self.right_panel_monitor_prelude_volume.value():
                        self.right_panel_monitor_prelude_volume_label.setText('<font style="font-size:14px;"><b>' + str(self.right_panel_monitor_prelude_volume.value()) + '</b></font><br><font style="font-size:7px;">VOL</font>')
                    else:
                        self.right_panel_monitor_prelude_volume_label.setText('<font style="font-size:14px;"><b>' + str(self.right_panel_monitor_prelude_volume.value()) + '</b></font><br><font style="font-size:7px;">( ' + str(int(volume)) + ' )</font>')
                else:
                    if not self.right_panel_monitor_prelude_pausebutton.isChecked() and not self.video_player.audio_get_volume() == self.right_panel_monitor_prelude_volume.value() and not self.monitor.current_media[4]:
                        self.right_panel_monitor_prelude_volume_changing()

                if self.monitor.current_media[4]:
                    self.video_player.audio_set_volume(0)
        else:
            if self.monitor.current_media and self.monitor.current_media[0] in ['clock']:
                self.right_panel_monitor_player_sort_song()
                if self.right_panel_monitor_prelude.song:
                    self.monitor_window.media_open(self.right_panel_monitor_prelude.song[2])

                    if not self.right_panel_monitor_prelude_nextbutton.isEnabled():
                        self.right_panel_monitor_prelude_nextbutton.setEnabled(True)

                    if not self.right_panel_monitor_prelude_pausebutton.isChecked():
                        self.right_panel_monitor_prelude_crossfade_opacity()

                    self.video_player.play()

                    self.monitor_window.clock_widget_title_label.setText(u'<font color:silver><small>TOCANDO AGORA:</small></font><br><fony color:gray><big>' + str(self.right_panel_monitor_prelude.song[4]) + u' 🞍 <b>' + self.right_panel_monitor_prelude.song[0] + u'</b></big></font>')

        if self.monitor.current_media and self.monitor.current_media[0] in ['clock']:#if self.monitor_window.clock_widget_label_opacity.opacity() > 0.0:
            self.monitor_window.clock_widget_label.setText(str(datetime.datetime.now().time()).split('.')[0])
        if self.selected_media and self.selected_media[0] == 'clock':#if self.right_panel_shownext_preview_clock.isVisible() == True:
            self.right_panel_shownext_preview_clock.setText(str(datetime.datetime.now().time()).split('.')[0])
        if len(self.playlist) > 0 and self.selected_media[0] == 'camera':
            self.right_panel_shownext_preview_image.setPixmap(QPixmap.grabWindow(QApplication.desktop().winId(), self.playlist_nextarrow_preview.mapToGlobal(self.playlist_nextarrow_preview.pos()).x() - self.playlist_nextarrow_preview.x(), self.playlist_nextarrow_preview.mapToGlobal(self.playlist_nextarrow_preview.pos()).y() - self.playlist_nextarrow_preview.y(), self.playlist_nextarrow_preview.width(), self.playlist_nextarrow_preview.height()).scaled(self.right_panel_shownext_preview_image.width(), self.right_panel_shownext_preview_image.height(), Qt.KeepAspectRatio, Qt.SmoothTransformation))
        #
        # if self.selected_addition == 'screencopy' and app.screencopy.selected_area:
        #     self.left_panel_screencopy_preview.setPixmap(QPixmap.grabWindow(QApplication.desktop().winId(), app.screencopy.selected_area[0], app.screencopy.selected_area[1], app.screencopy.selected_area[2], app.screencopy.selected_area[3]).scaled(self.left_panel_screencopy_preview.width(), self.left_panel_screencopy_preview.height(), Qt.KeepAspectRatio, Qt.SmoothTransformation))
        # if len(self.playlist) > 0 and self.selected_media[0] == 'screencopy':
        #     self.right_panel_shownext_preview_image.setPixmap(QPixmap.grabWindow(QApplication.desktop().winId(), self.selected_media[2][0], self.selected_media[2][1], self.selected_media[2][2], self.selected_media[2][3]).scaled(self.right_panel_shownext_preview_image.width(), self.right_panel_shownext_preview_image.height(), Qt.KeepAspectRatio, Qt.SmoothTransformation))
        # if self.monitor.current_media and self.monitor.current_media[0] == 'screencopy':
        #     self.monitor_window.image_widget.setPixmap(QPixmap.grabWindow(QApplication.desktop().winId(), self.monitor.current_media[2][0], self.monitor.current_media[2][1], self.monitor.current_media[2][2], self.monitor.current_media[2][3]).scaled(self.monitor_window.image_widget.width(), self.monitor_window.image_widget.height(), Qt.KeepAspectRatio, Qt.SmoothTransformation))

    def resizeEvent(self, event):
        self.main_background.setGeometry(0, 0, self.width(), self.height())
        self.options_box.resize(self)
        self.right_panel.resize(self)
        self.playlist_panel.resize(self)
        self.left_panel.resize(self)

    def keyPressEvent(self, event):
        if event.key() == Qt.Key_Space:
            if self.right_panel.show_now_button.isVisible():
                self.show_now()
        if event.key() == Qt.Key_Escape:
            self.monitor_window.hide_widgets()
        if event.key() == Qt.Key_Backspace:
            self.monitor_window.hide_all()

    def closeEvent(self, event):
        config.save(self)
        self.monitor_window.close()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    # app.setDesktopSettingsAware(False)
    # app.setStyle("plastique")
    app.setApplicationName("kihvim")
    app.setAttribute(Qt.AA_UseSoftwareOpenGL)
    app.main = main_window()
    app.main.show()
    app.main.monitor.hide_all(app.main)

    sys.exit(app.exec_())
