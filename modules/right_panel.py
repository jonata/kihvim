import os
from PySide2.QtWidgets import QLabel, QMessageBox, QWidget, QDesktopWidget, QLabel, QGraphicsOpacityEffect, QFrame, QPushButton, QCheckBox, QDial, QComboBox, QFileDialog, QVBoxLayout, QScrollArea, QListWidget, QListView, QLineEdit, QPlainTextEdit, QInputDialog, QApplication, QTabWidget, QListWidgetItem
from PySide2.QtCore import Qt, QPropertyAnimation, QEasingCurve, QUrl, QRectF, QSize, QRect, QTimer#, Signal
from PySide2.QtGui import QIcon, QFont, QPalette, QFontMetrics, QPixmap, QPainter, QPen, QColor

from modules.paths import *
from modules.effect import generate_effect
from modules import utils
from modules import monitor_video
from modules import playlist_panel

def load(self):
    self.right_panel_widget = QWidget(parent=self)

    self.right_panel_monitor_background = QLabel(parent=self.right_panel_widget)
    self.right_panel_monitor_background.setObjectName('right_panel_monitor_background')
    self.right_panel_monitor_background.setStyleSheet('#right_panel_monitor_background { border-top: 4px; border-right: 4px; border-bottom: 4px; border-left: 4px; border-image: url("' + os.path.join(path_graphics, "black_box.png").replace('\\', '/') + '") 4 4 4 4 stretch stretch; }')

    self.right_panel_monitor_title = QLabel('EXIBINDO AGORA', parent=self.right_panel_monitor_background)
    self.right_panel_monitor_title.setObjectName('right_panel_monitor_title')
    self.right_panel_monitor_title.setStyleSheet('#right_panel_monitor_title { padding-left:3px; padding-bottom:3px; font-family: "Ubuntu"; font-size:11px; font-weight:bold; color:white; border-top: 3px; border-right: 3px; border-left: 3px; border-image: url("' + os.path.join(path_graphics, "monitor_title_background.png").replace('\\', '/') + '") 3 3 0 3 stretch stretch; }')

    class right_panel_monitor(QLabel):
        def enterEvent(widget, event):
            generate_effect(self.right_panel_monitor_withdrawbutton_opacity_animation, 'opacity', 200, self.right_panel_monitor_withdrawbutton_opacity.opacity(), 1.0)
        def leaveEvent(widget, event):
            generate_effect(self.right_panel_monitor_withdrawbutton_opacity_animation, 'opacity', 200, self.right_panel_monitor_withdrawbutton_opacity.opacity(), 0.0)

    self.right_panel_monitor = right_panel_monitor(parent=self.right_panel_monitor_background)
    self.right_panel_monitor.setScaledContents(True)
    self.right_panel_monitor.setStyleSheet('QLabel {font-family: "Ubuntu"; font-size:12pt; font-weight:bold; color:white}')

    self.right_panel_monitor_withdrawbutton = QPushButton(u'TIRAR', parent=self.right_panel_monitor);
    self.right_panel_monitor_withdrawbutton.setStyleSheet('QPushButton { font-family: "Ubuntu"; font-size:14px; color:white; font-weight:bold; border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
    self.right_panel_monitor_withdrawbutton.clicked.connect(lambda:self.monitor.hide_all(self))
    self.right_panel_monitor_withdrawbutton_opacity = QGraphicsOpacityEffect()
    self.right_panel_monitor_withdrawbutton.setGraphicsEffect(self.right_panel_monitor_withdrawbutton_opacity)
    self.right_panel_monitor_withdrawbutton_opacity.setOpacity(0.0)
    self.right_panel_monitor_withdrawbutton_opacity_animation = QPropertyAnimation(self.right_panel_monitor_withdrawbutton_opacity, b'opacity')

    self.right_panel_monitor_player = QWidget(self.right_panel_widget)

    self.right_panel_monitor_player_stopbutton = QPushButton(self.right_panel_monitor_player)
    self.right_panel_monitor_player_stopbutton.setStyleSheet('QPushButton { image: url(' + os.path.join(path_graphics, 'stop_icon.png').replace('\\', '/') + '); border-top: 5px; border-right: 0px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_dark_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 0px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_dark_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 0px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_dark_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(path_graphics, "button_dark_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
    self.right_panel_monitor_player_stopbutton.clicked.connect(lambda:right_panel_montiro_player_stopbutton_pressed(self))

    self.right_panel_monitor_player_playbutton = QPushButton(self.right_panel_monitor_player)
    self.right_panel_monitor_player_playbutton.setCheckable(True)
    self.right_panel_monitor_player_playbutton.setStyleSheet('QPushButton { image: url(' + os.path.join(path_graphics, 'play_icon.png').replace('\\', '/') + ');  border-top: 5px; border-right: 0px; border-bottom: 5px; border-left: 0px; border-image: url("' + os.path.join(path_graphics, "button_dark_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 0px; border-bottom: 5px; border-left: 0px; border-image: url("' + os.path.join(path_graphics, "button_dark_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 0px; border-bottom: 5px; border-left: 0px; border-image: url("' + os.path.join(path_graphics, "button_dark_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { image: url(' + os.path.join(path_graphics, 'pause_icon.png').replace('\\', '/') + '); border-image: url("' + os.path.join(path_graphics, "button_dark_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:checked { image: url(' + os.path.join(path_graphics, 'pause_icon.png').replace('\\', '/') + '); border-image: url("' + os.path.join(path_graphics, "button_dark_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
    self.right_panel_monitor_player_playbutton.clicked.connect(lambda:monitor_video.play_pause(self))

    self.right_panel_monitor_player_timelinebox = QLabel(parent=self.right_panel_monitor_player)
    self.right_panel_monitor_player_timelinebox.setObjectName('right_panel_monitor_player_timelinebox')
    self.right_panel_monitor_player_timelinebox.setStyleSheet('#right_panel_monitor_player_timelinebox { border-top: 4px; border-right: 4px; border-bottom: 4px; border-left: 0px; border-image: url("' + os.path.join(path_graphics, "black_box.png").replace('\\', '/') + '") 4 4 4 4 stretch stretch; }')

    class right_panel_monitor_player_timeline(QWidget):
        def paintEvent(widget, paintEvent):
            painter = QPainter(widget)
            if self.monitor.current_media[0] in ['audio', 'video']:
                painter.setRenderHint(QPainter.Antialiasing)

                painter.setPen(QColor.fromRgb(102,102,102))
                painter.setFont(QFont("Ubuntu", 8))
                rectangle = QRectF(2, 0, (widget.width()*.5)-2,widget.height()*.5)

                if self.monitor.current_media[0] == 'video' and self.monitor.current_media[6]:
                    total_time = str(utils.convert_to_timecode((self.monitor.current_media[6] - self.monitor.current_media[5]), True))
                else:
                    total_time = str(utils.convert_to_timecode(self.monitor.current_media[-2], True))
                painter.drawText(rectangle, Qt.AlignVCenter | Qt.AlignLeft, total_time)
                if self.monitor.current_media[0] == 'video' and self.monitor.current_media[6]:
                    time_to_stop = self.monitor.current_media[6]-(self.video_player.get_position()*self.monitor.current_media[7])
                else:
                    time_to_stop = self.monitor.current_media[-2]-(self.video_player.get_position()*self.monitor.current_media[7])

                if (int(time_to_stop) < 31 and int(time_to_stop) % 2 == 0) or int(time_to_stop) < 10:
                    painter.setPen(QColor.fromRgb(255,0,0))
                else:
                    painter.setPen(QColor.fromRgb(102,102,102))
                rectangle = QRectF((widget.width()*.5)+2, 0, (widget.width()*.5)-2,widget.height()*.5)
                if self.video_player.get_position() > 0 and self.video_player.get_state() == 3:
                    painter.drawText(rectangle, Qt.AlignVCenter | Qt.AlignRight, str(utils.convert_to_timecode(time_to_stop, True)))

                painter.setPen(Qt.NoPen)
                painter.setBrush(QColor.fromRgb(24,33,41))
                rectangle = QRectF(0, widget.height()*.5, widget.width(),widget.height()*.5)
                painter.drawRect(rectangle)

                painter.setBrush(QColor.fromRgb(48,66,81))

                rectangle = QRectF(0, widget.height()*.5, widget.width()*((self.video_player.get_position()*self.monitor.current_media[7])/self.monitor.current_media[-2]),widget.height()*.5)
                painter.drawRect(rectangle)
                painter.setPen(QColor.fromRgb(255,255,255))
                rectangle = QRectF(0, widget.height()*.5, (widget.width()*((self.video_player.get_position()*self.monitor.current_media[7])/self.monitor.current_media[-2]))-3,widget.height()*.5)
                painter.drawText(rectangle, Qt.AlignVCenter | Qt.AlignRight, str(utils.convert_to_timecode(self.monitor.current_media[-2]*((self.video_player.get_position()*self.monitor.current_media[7])/self.monitor.current_media[-2]), True)))

            painter.end()

    self.right_panel_monitor_player_timeline = right_panel_monitor_player_timeline(parent=self.right_panel_monitor_player_timelinebox)

    self.right_panel_monitor_prelude = QWidget(self.right_panel_widget)

    self.right_panel_monitor_prelude.time_to_full_volume = False
    self.right_panel_monitor_prelude.song = False

    self.right_panel_monitor_prelude_pausebutton = QPushButton(self.right_panel_monitor_prelude)
    self.right_panel_monitor_prelude_pausebutton.setCheckable(True)
    self.right_panel_monitor_prelude_pausebutton.setStyleSheet('QPushButton { image: url(' + os.path.join(path_graphics, 'prelude_normal.png').replace('\\', '/') + '); padding-right:18px;  border-top: 5px; border-right: 0px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_dark_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 0px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_dark_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 0px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_dark_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { image: url(' + os.path.join(path_graphics, 'prelude_pause.png').replace('\\', '/') + '); border-image: url("' + os.path.join(path_graphics, "button_dark_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:checked { image: url(' + os.path.join(path_graphics, 'prelude_pause.png').replace('\\', '/') + '); border-image: url("' + os.path.join(path_graphics, "button_dark_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
    self.right_panel_monitor_prelude_pausebutton.clicked.connect(lambda:self.right_panel_monitor_prelude_pausebutton_clicked())

    self.right_panel_monitor_prelude_timelinebox = QLabel(parent=self.right_panel_monitor_prelude)
    self.right_panel_monitor_prelude_timelinebox.setObjectName('right_panel_monitor_prelude_timelinebox')
    self.right_panel_monitor_prelude_timelinebox.setStyleSheet('#right_panel_monitor_prelude_timelinebox { border-top: 4px; border-right: 0px; border-bottom: 4px; border-left: 0px; border-image: url("' + os.path.join(path_graphics, "black_box.png").replace('\\', '/') + '") 4 4 4 4 stretch stretch; }')

    class right_panel_monitor_prelude_timeline(QWidget):
        def paintEvent(widget, paintEvent):
            '''painter = QPainter(widget)

            if self.monitor.current_media[0] in ['clock'] and self.right_panel_monitor_prelude.song:
                painter.setRenderHint(QPainter.Antialiasing)

                painter.setPen(QColor.fromRgb(102,102,102))
                painter.setFont(QFont("Ubuntu", 8, weight=75))
                rectangle = QRectF(32, 0, (widget.width()*.8)-2,widget.height()*.5)
                painter.drawText(rectangle, Qt.AlignVCenter | Qt.AlignLeft, u'CÂNTICO ' + str(self.right_panel_monitor_prelude.song[4]))

                painter.setFont(QFont("Ubuntu", 8))
                if (int(self.right_panel_monitor_prelude.song[3]*(1.0-self.video_player.position())) < 31 and int(self.right_panel_monitor_prelude.song[3]*(1.0-self.video_player.position())) % 2 == 0) or int(self.right_panel_monitor_prelude.song[3]*(1.0-self.video_player.position())) < 10:
                    painter.setPen(QColor.fromRgb(255,0,0))
                else:
                    painter.setPen(QColor.fromRgb(102,102,102))
                rectangle = QRectF((widget.width()*.5)+2, 0, (widget.width()*.5)-2,widget.height()*.5)
                if self.video_player.position() > 0.0 and self.video_player.state() == 1:
                    painter.drawText(rectangle, Qt.AlignVCenter | Qt.AlignRight, str(utils.convert_to_timecode(self.right_panel_monitor_prelude.song[3]*(1.0-self.video_player.position()), True)))

                painter.setPen(Qt.NoPen)
                painter.setBrush(QColor.fromRgb(24,33,41))
                rectangle = QRectF(0, widget.height()*.5, widget.width(),widget.height()*.5)
                painter.drawRect(rectangle)

                painter.setBrush(QColor.fromRgb(48,66,81))

                rectangle = QRectF(0, widget.height()*.5, widget.width()*self.video_player.position(),widget.height()*.5)
                painter.drawRect(rectangle)
                painter.setPen(QColor.fromRgb(255,255,255))
                rectangle = QRectF(0, widget.height()*.5, (widget.width()*self.video_player.position())-3,widget.height()*.5)
                painter.drawText(rectangle, Qt.AlignVCenter | Qt.AlignRight, str(utils.convert_to_timecode(self.right_panel_monitor_prelude.song[3]*self.video_player.position(), True)))

            painter.end()
            '''
            None

    self.right_panel_monitor_prelude_timeline = right_panel_monitor_prelude_timeline(parent=self.right_panel_monitor_prelude_timelinebox)

    self.right_panel_monitor_prelude_volume = QDial(parent=self.right_panel_monitor_prelude)
    self.right_panel_monitor_prelude_volume.setMaximum(100)
    self.right_panel_monitor_prelude_volume.setMinimum(0)
    self.right_panel_monitor_prelude_volume.setStyleSheet('QDial { border-color:QLinearGradient(  x1: 0, y1: 0, x2: 0, y2: 1.0, stop: 0 #6d7785, stop: 0.49 #546270, stop: 0.5 #4a5968, stop: 1 #304251 ); }')

    self.right_panel_monitor_prelude_volume.valueChanged.connect(lambda:self.right_panel_monitor_prelude_volume_changing())
    #self.right_panel_monitor_prelude_volume.sliderReleased.connect(lambda:right_panel_monitor_prelude_volume_changed(self))

    self.right_panel_monitor_prelude_volume_label = QLabel(parent=self.right_panel_monitor_prelude_volume)
    self.right_panel_monitor_prelude_volume_label.setStyleSheet('QLabel { image: url(' + os.path.join(path_graphics, 'prelude_volume.png').replace('\\', '/') + '); font-family: "Ubuntu"; qproperty-alignment: "AlignCenter"; color:white;}')
    self.right_panel_monitor_prelude_volume_label.setAttribute(Qt.WA_TransparentForMouseEvents)

    self.right_panel_monitor_prelude_nextbutton = QPushButton(self.right_panel_monitor_prelude)
    self.right_panel_monitor_prelude_nextbutton.setStyleSheet('QPushButton { image: url(' + os.path.join(path_graphics, 'prelude_next.png').replace('\\', '/') + '); padding-left:2px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 0px; border-image: url("' + os.path.join(path_graphics, "button_dark_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 0px; border-image: url("' + os.path.join(path_graphics, "button_dark_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 0px; border-image: url("' + os.path.join(path_graphics, "button_dark_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(path_graphics, "button_dark_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
    self.right_panel_monitor_prelude_nextbutton.clicked.connect(lambda:self.right_panel_monitor_prelude_nextbutton_clicked())



    self.right_panel_shownext_box = QLabel(parent=self.right_panel_widget)
    self.right_panel_shownext_box.setObjectName('right_panel_shownext_box')
    self.right_panel_shownext_box.setStyleSheet('#right_panel_shownext_box { border-top: 75px; border-right: 25px; border-left: 25px; border-image: url("' + os.path.join(path_graphics, "shownext_box_background.png").replace('\\', '/') + '") 75 25 0 25 stretch stretch; }')

    self.right_panel_shownext_preview_background = QLabel(parent=self.right_panel_shownext_box)
    self.right_panel_shownext_preview_background.setObjectName('right_panel_shownext_preview_background')
    self.right_panel_shownext_preview_background.setStyleSheet('#right_panel_shownext_preview_background { border-top: 4px; border-right: 4px; border-bottom: 4px; border-left: 4px; border-image: url("' + os.path.join(path_graphics, "black_box.png").replace('\\', '/') + '") 4 4 4 4 stretch stretch; }')

    self.right_panel_shownext_preview_title = QLabel(u'PRÓXIMA EXIBIÇÃO', parent=self.right_panel_shownext_preview_background)
    self.right_panel_shownext_preview_title.setObjectName('right_panel_shownext_preview_title')
    self.right_panel_shownext_preview_title.setStyleSheet('#right_panel_shownext_preview_title { padding-left:3px; padding-top:4px; font-family: "Ubuntu"; font-size:11px; font-weight:bold; color:white; border-bottom: 3px; border-right: 3px; border-left: 3px; border-image: url("' + os.path.join(path_graphics, "shownext_preview_title_background.png").replace('\\', '/') + '") 0 3 3 3 stretch stretch; }')

    self.right_panel_shownext_preview = QWidget(parent=self.right_panel_shownext_box)
    self.right_panel_shownext_preview_black_palette = self.right_panel_shownext_preview.palette()
    self.right_panel_shownext_preview_black_palette.setColor(QPalette.Window, QColor(0,0,0))
    self.right_panel_shownext_preview.setPalette(self.right_panel_shownext_preview_black_palette)
    self.right_panel_shownext_preview.setAutoFillBackground(True)

    self.right_panel_shownext_preview_audio = QWidget(parent=self.right_panel_shownext_preview)
    self.right_panel_shownext_preview_audio_artwork = QLabel(parent=self.right_panel_shownext_preview_audio)
    self.right_panel_shownext_preview_audio_artwork.setAlignment(Qt.AlignCenter)
    self.right_panel_shownext_preview_audio_title = QLabel(parent=self.right_panel_shownext_preview_audio)
    self.right_panel_shownext_preview_audio_title.setAlignment(Qt.AlignVCenter | Qt.AlignLeft)
    self.right_panel_shownext_preview_audio_title.setWordWrap(True)

    self.right_panel_shownext_preview_image = QLabel(parent=self.right_panel_shownext_preview)
    self.right_panel_shownext_preview_image.setAlignment(Qt.AlignCenter)

    self.right_panel_shownext_preview_clock = QLabel(parent=self.right_panel_shownext_preview)
    self.right_panel_shownext_preview_clock.setAlignment(Qt.AlignCenter)

    class right_panel_shownext_button(QLabel):
        def mouseReleaseEvent(widget, event):
            if len(self.playlist) > 0:
                self.right_panel_shownext_box.setStyleSheet('#right_panel_shownext_box { border-top: 75px; border-right: 25px; border-left: 25px; border-image: url("' + os.path.join(path_graphics, "shownext_box_background_hover.png").replace('\\', '/') + '") 75 25 0 25 stretch stretch; }')
            else:
                self.right_panel_shownext_box.setStyleSheet('#right_panel_shownext_box { border-top: 75px; border-right: 25px; border-left: 25px; border-image: url("' + os.path.join(path_graphics, "shownext_box_background_disabled.png").replace('\\', '/') + '") 75 25 0 25 stretch stretch; }')
        def mousePressEvent(widget, event):
            if len(self.playlist) > 0:
                show_now_clicked(self)
                self.right_panel_shownext_box.setStyleSheet('#right_panel_shownext_box { border-top: 75px; border-right: 25px; border-left: 25px; border-image: url("' + os.path.join(path_graphics, "shownext_box_background_pressed.png").replace('\\', '/') + '") 75 25 0 25 stretch stretch; }')
        def enterEvent(widget, event):
            if len(self.playlist) > 0:
                self.right_panel_shownext_box.setStyleSheet('#right_panel_shownext_box { border-top: 75px; border-right: 25px; border-left: 25px; border-image: url("' + os.path.join(path_graphics, "shownext_box_background_hover.png").replace('\\', '/') + '") 75 25 0 25 stretch stretch; }')
        def leaveEvent(widget, event):
            if len(self.playlist) > 0:
                self.right_panel_shownext_box.setStyleSheet('#right_panel_shownext_box { border-top: 75px; border-right: 25px; border-left: 25px; border-image: url("' + os.path.join(path_graphics, "shownext_box_background.png").replace('\\', '/') + '") 75 25 0 25 stretch stretch; }')
            else:
                self.right_panel_shownext_box.setStyleSheet('#right_panel_shownext_box { border-top: 75px; border-right: 25px; border-left: 25px; border-image: url("' + os.path.join(path_graphics, "shownext_box_background_disabled.png").replace('\\', '/') + '") 75 25 0 25 stretch stretch; }')

    self.right_panel_shownext_button = right_panel_shownext_button(parent=self.right_panel_shownext_box)
    self.right_panel_shownext_button.setScaledContents(True)
    self.right_panel_shownext_button.setStyleSheet('QLabel {font-family: "Ubuntu"; font-size:12pt; font-weight:bold; color:white; qproperty-alignment: "AlignCenter";}')

def resize(self):
    self.right_panel_widget.setGeometry(self.width()*.75,0,self.width()*.25,self.height())
    self.right_panel_monitor_background.setGeometry(20,20,self.right_panel_widget.width()-40,((self.right_panel_widget.width()-40)*self.screen_width_proportion)+25)
    self.right_panel_monitor_title.setGeometry(3,3,self.right_panel_monitor_background.width()-6,25)
    self.right_panel_monitor.setGeometry(3,28,self.right_panel_monitor_background.width()-6,(self.right_panel_widget.width()-46)*self.screen_width_proportion)
    self.right_panel_monitor_withdrawbutton.setGeometry(self.right_panel_monitor.width()*.3,self.right_panel_monitor.height()*.4,self.right_panel_monitor.width()*.4,self.right_panel_monitor.height()*.2)
    self.right_panel_monitor_player.setGeometry(20,20+self.right_panel_monitor_background.height()+10,self.right_panel_monitor_background.width(),40)
    self.right_panel_monitor_player_stopbutton.setGeometry(0,0,self.right_panel_monitor_player.height(),self.right_panel_monitor_player.height())
    self.right_panel_monitor_player_playbutton.setGeometry(self.right_panel_monitor_player.height()+1,0,self.right_panel_monitor_player.height(),self.right_panel_monitor_player.height())
    self.right_panel_monitor_player_timelinebox.setGeometry((self.right_panel_monitor_player.height()*2)+2,0,self.right_panel_monitor_player.width()-((self.right_panel_monitor_player.height()*2)+2),self.right_panel_monitor_player.height())
    self.right_panel_monitor_player_timeline.setGeometry(0,3,self.right_panel_monitor_player_timelinebox.width()-3,self.right_panel_monitor_player_timelinebox.height()-6)
    self.right_panel_monitor_prelude.setGeometry(20,20+self.right_panel_monitor_background.height()+2,self.right_panel_monitor_background.width(),56)
    self.right_panel_monitor_prelude_pausebutton.setGeometry(0,8,60,40)
    self.right_panel_monitor_prelude_volume.setGeometry(40,0,self.right_panel_monitor_prelude.height(),self.right_panel_monitor_prelude.height())
    self.right_panel_monitor_prelude_volume_label.setGeometry(0,0,self.right_panel_monitor_prelude_volume.width(),self.right_panel_monitor_prelude_volume.height())
    self.right_panel_monitor_prelude_timelinebox.setGeometry(61,8,self.right_panel_monitor_prelude.width()-102,40)
    self.right_panel_monitor_prelude_timeline.setGeometry(0,3,self.right_panel_monitor_prelude_timelinebox.width()-2,self.right_panel_monitor_prelude_timelinebox.height()-6)
    self.right_panel_monitor_prelude_nextbutton.setGeometry(self.right_panel_monitor_prelude.width()-40,8,40,40)
    self.right_panel_shownext_box.setGeometry(0,self.right_panel_widget.height()-80-((self.right_panel_widget.width()-46)*self.screen_width_proportion)-25-26,self.right_panel_widget.width(),80+((self.right_panel_widget.width()-46)*self.screen_width_proportion)+25+26)
    self.right_panel_shownext_preview_background.setGeometry(20,80,self.right_panel_widget.width()-40,((self.right_panel_widget.width()-40)*self.screen_width_proportion)+25)
    self.right_panel_shownext_preview_title.setGeometry(3,self.right_panel_monitor_background.height()-28,self.right_panel_monitor_background.width()-6,25)

    self.right_panel_shownext_preview.setGeometry(23,83,self.right_panel_monitor_background.width()-6,(self.right_panel_widget.width()-46)*self.screen_width_proportion)

    self.right_panel_shownext_preview_audio.setGeometry(0,0,self.right_panel_shownext_preview.width(), self.right_panel_shownext_preview.height())
    self.right_panel_shownext_preview_audio_artwork.setGeometry(self.right_panel_shownext_preview.width()*.1,(self.right_panel_shownext_preview.height()*.5)-(self.right_panel_shownext_preview.height()*.1),self.right_panel_shownext_preview.height()*.2, self.right_panel_shownext_preview.height()*.2)
    self.right_panel_shownext_preview_audio_title.setGeometry((self.right_panel_shownext_preview.width()*.11) + (self.right_panel_shownext_preview.width()*.2),0,self.right_panel_shownext_preview.width() - ((self.right_panel_shownext_preview.width()*.21) + (self.right_panel_shownext_preview.width()*.2)), self.right_panel_shownext_preview.height())
    self.right_panel_shownext_preview_audio_artwork.setStyleSheet('QLabel {background-color: #fff; font-family: "Ubuntu Condensed"; font-size:' + str(self.right_panel_shownext_preview.height()*.09) + 'pt; font-weight:bold; color:black;}')
    self.right_panel_shownext_preview_audio_title.setStyleSheet('QLabel {background-color:none; font-family: "Ubuntu"; font-size:' + str(self.right_panel_shownext_preview.height()*.05) + 'pt; font-weight:bold; color:white}')
    self.right_panel_shownext_preview_image.setGeometry(0,0,self.right_panel_shownext_preview.width(), self.right_panel_shownext_preview.height())
    self.right_panel_shownext_preview_clock.setGeometry(0,0,self.right_panel_shownext_preview.width(), self.right_panel_shownext_preview.height())
    self.right_panel_shownext_preview_clock.setStyleSheet('QLabel {background-color: black; font-family: "Ubuntu"; font-size:' + str(self.right_panel_shownext_preview.height()*.1) + 'pt; font-weight:bold; color:white}')

    self.right_panel_shownext_button.setGeometry(0,0,self.right_panel_shownext_box.width(),80)

def right_panel_montiro_player_stopbutton_pressed(self):
    monitor_video.stop(self)
    self.right_panel_monitor_player_playbutton.setChecked(False)

def right_panel_monitor_prelude_volume_changing(self):
    if not self.right_panel_monitor_prelude_pausebutton.isChecked():
        self.video_player.setVolume(self.right_panel_monitor_prelude_volume.value())
    self.right_panel_monitor_prelude_volume_label.setText('<font style="font-size:14px;"><b>' + str(self.right_panel_monitor_prelude_volume.value()) + '</b></font><br><font style="font-size:7px;">' + u'VOL' + '</font>')

def right_panel_monitor_prelude_pausebutton_clicked(self, stop=False):
    if self.monitor.current_media[4]:
        self.monitor.current_media[4] = False
    self.right_panel_monitor_prelude_crossfade()

def right_panel_monitor_prelude_crossfade(self,stop=False):
    if self.video_player.audio_get_volume() > 0 or stop:
        fadeout = True

    else:
        fadeout = False

    self.right_panel_monitor_prelude.time_to_full_volume = [datetime.datetime.now(), 3.0, fadeout, stop] #is for fade out, should stop
    self.right_panel_monitor_prelude_crossfade_opacity(fadeout)

def right_panel_monitor_prelude_crossfade_opacity(self, fadeout=False):
    if fadeout:
        generate_effect(self.monitor.clock_widget_title_label_opacity_animation, 'opacity', 3000, self.monitor.clock_widget_title_label_opacity.opacity(), 0.0)
    elif not self.monitor.current_media[4]:
        generate_effect(self.monitor.clock_widget_title_label_opacity_animation, 'opacity', 3000, self.monitor.clock_widget_title_label_opacity.opacity(), 1.0)

def right_panel_monitor_prelude_nextbutton_clicked(self):
    self.right_panel_monitor_prelude_nextbutton.setEnabled(False)
    #if not self.right_panel_monitor_prelude_pausebutton.isChecked():
    self.right_panel_monitor_prelude_crossfade(stop=True)

def right_panel_monitor_player_sort_song(self):
    number = self.list_of_songs.keys()[random.randint(0,len(self.list_of_songs.keys())-1)]
    self.right_panel_monitor_prelude.song = self.list_of_songs[number]
    self.right_panel_monitor_prelude.song.append(MP3(self.right_panel_monitor_prelude.song[2]).info.length)
    self.right_panel_monitor_prelude.song.append(number)

############################################################################
## PREVIEW

def update_preview(self):
    if len(self.playlist) > 0:

        self.selected_media = self.playlist[0]
        self.right_panel_shownext_preview.setVisible(True)
        if not self.selected_media[0] == 'audio':
            self.right_panel_shownext_preview_audio.setVisible(False)
        if not self.selected_media[0] in ['image', 'video', 'web', 'screencopy','camera']:
            self.right_panel_shownext_preview_image.setPixmap(None)
        if not self.selected_media[0] in ['clock']:
            self.right_panel_shownext_preview_clock.setVisible(False)
        if not self.selected_media[0] in ['camera']:
            None #TODO

        if self.selected_media[0] == 'audio':
            self.right_panel_shownext_preview_audio_artwork.setText(str(self.selected_media[2]))
            self.right_panel_shownext_preview_audio_title.setText(self.selected_media[3])
            self.right_panel_shownext_preview_audio.setVisible(True)
        elif self.selected_media[0] == 'image':
            self.right_panel_shownext_preview_image.setPixmap(QPixmap(self.selected_media[2]).scaled(self.right_panel_shownext_preview_image.width(), self.right_panel_shownext_preview_image.height(), Qt.KeepAspectRatio, Qt.SmoothTransformation))
        elif self.selected_media[0] == 'video':
            self.right_panel_shownext_preview_image.setPixmap(self.selected_media[3].scaled(self.right_panel_shownext_preview_image.width(), self.right_panel_shownext_preview_image.height(), Qt.KeepAspectRatio, Qt.SmoothTransformation))
        elif self.selected_media[0] == 'web':
            self.right_panel_shownext_preview_image.setPixmap(self.selected_media[3].scaled(self.right_panel_shownext_preview_image.width(), self.right_panel_shownext_preview_image.height(), Qt.KeepAspectRatio, Qt.SmoothTransformation))
        elif self.selected_media[0] == 'clock':
            self.right_panel_shownext_preview_clock.setVisible(True)
        elif self.selected_media[0] == 'screencopy':
            None
        elif self.selected_media[0] == 'camera':
            None #TODO

    else:
        self.right_panel_shownext_preview.setVisible(False)

        #self.right_panel_shownext_preview

def show_now_clicked(self):
    self.monitor.current_media = self.playlist[0]
    self.monitor.show_media(self)
    #if self.playlist[0][0] in ['audio']:
    #    self.right_panel_monitor_player.setVisible(False)
    if self.playlist[0][-1]:
        self.playlist.append(self.playlist.pop(0))
    else:
        del self.playlist[0]

    self.playlist_options_is_visible = False
    playlist_panel.playlist_options_panel_open_button_clicked(self)

    playlist_panel.populate_playlist(self)
    update_preview(self)
