from PySide2.QtCore import QRect


def generate_effect(widget, effect_type, duration, startValue, endValue):
    widget.setDuration(duration)
    if effect_type == 'geometry':
        widget.setStartValue(QRect(startValue[0],startValue[1],startValue[2],startValue[3]))
        widget.setEndValue(QRect(endValue[0],endValue[1],endValue[2],endValue[3]))
    elif effect_type == 'opacity':
        widget.setStartValue(startValue)
        widget.setEndValue(endValue)
    widget.start()
