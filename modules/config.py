import os

from modules.paths import *


def load(self):
    self.settings['enable_animations'] = True
    self.settings['interface_framerate'] = 40
    self.settings['yeartext'] = ''
    self.settings['show_yeartext'] = True
    self.settings['language_mnemonic'] = ''
    if os.path.isfile(os.path.join(path_user_config, 'settings')):
        settings_file = open(os.path.join(path_user_config, 'settings'), 'r', encoding='utf-8').read()
        if '[song ' in settings_file:
            for song in settings_file.split('[song '):
                if song.split(']')[0].isnumeric():
                    song_number = int(song.split(']')[0])
                    song_title = ''
                    song_lyrics = ''

                    song_content = song.split(']')[1].split('[')[0]
                    if 'title=' in song_content:
                        song_title = song_content.split('title=',1)[1].split('\n')[0]
                    if 'filepath=' in song_content:
                        filepath = song_content.split('filepath=',1)[1].split('\n')[0]
                    if 'lyrics=' in song_content:
                        song_lyrics = song_content.split('lyrics={',1)[1].split('}')[0]

                    self.list_of_songs[song_number] = [song_title, song_lyrics, filepath]
        if '[video song ' in settings_file:
            for song in settings_file.split('[video song '):
                if song.split(']')[0].isnumeric():
                    song_number = int(song.split(']')[0])
                    song_title = ''

                    song_content = song.split(']')[1].split('[')[0]
                    if 'title=' in song_content:
                        song_title = song_content.split('title=',1)[1].split('\n')[0]
                    if 'filepath=' in song_content:
                        filepath = song_content.split('filepath=',1)[1].split('\n')[0]

                    self.list_of_video_songs[song_number] = [song_title, filepath]

        if '[paths]' in settings_file:
            for line in settings_file.split('[paths]')[1].split('[')[0].split('\n'):
                if 'videos=' in line:
                    if not line.split('=',1)[-1] == '' and os.path.isdir(line.split('=',1)[-1]):
                        path_for_videos = line.split('=',1)[-1]
                if 'images=' in line:
                    if not line.split('=',1)[-1] == '' and os.path.isdir(line.split('=',1)[-1]):
                        path_for_images = line.split('=',1)[-1]

        if '[config]' in settings_file:
            for line in settings_file.split('[config]')[1].split('[')[0].split('\n'):
                if 'enable_animations=' in line:
                    self.settings['enable_animations'] = bool(line.split('enable_animations=',1)[1].split('\n')[0])
                if 'framerate=' in line:
                    self.settings['interface_framerate'] = int(line.split('framerate=',1)[1].split('\n')[0])
                if 'show_yeartext=' in line:
                    self.settings['show_yeartext'] = bool(line.split('show_yeartext=', 1)[1].split('\n')[0])
                if 'yeartext=' in line:
                    self.settings['yeartext'] = line.split('yeartext=', 1)[1].split('\n')[0]
                if 'language_mnemonic=' in line:
                    self.settings['language_mnemonic'] = line.split('language_mnemonic=', 1)[1].split('\n')[0]

    if os.path.isfile(os.path.join(path_user_config, 'bible')):
        bible_file = open(os.path.join(path_user_config, 'bible'), 'r', encoding='utf-8').read()

        if '[verse ' in bible_file:

            for verse_content in bible_file.split('[verse '):

                verse = verse_content.split(']')[0]
                filepath = False
                start = False
                end = False

                if 'filepath=' in verse_content:
                    filepath = verse_content.split('filepath=',1)[1].split('\n')[0]
                if 'start=' in verse_content:
                    start = float(verse_content.split('start=',1)[1].split('\n')[0])
                if 'end=' in verse_content:
                    end = float(verse_content.split('end=',1)[1].split('\n')[0])

                if not verse == '' and filepath and os.path.isfile(filepath) and start and end:
                    self.list_of_bible_videos[verse] = [filepath, start, end]

    if os.path.isfile(os.path.join(path_user_config, 'library')):
        library_file = open(os.path.join(path_user_config, 'library'), 'r', encoding='utf-8').read()

        if '[album ' in library_file:
            for album_content in library_file.split('[album '):
                if 'filepath=' in album_content:
                    album = album_content.rsplit(']',1)[0]
                    self.list_of_library_videos[album] = {}
                    for file_content in album_content.split('filepath='):
                        if 'title=' in file_content:

                            filepath = file_content.split('\n')[0]
                            title = file_content.split('title=')[1].split('\n')[0]
                            chapters = {}
                            for chapter in album_content.split('chapter='):
                                if len(chapter.split(';')) > 2:
                                    name = chapter.split(';')[0]
                                    start = float(chapter.split(';')[1])
                                    end = float(chapter.split(';')[2])
                                    chapters[name] = [start, end]

                            try:
                                video = MP4(filepath)
                                if 'covr' in  video.tags.keys():
                                    artwork = video.tags["covr"][0]
                                    open(os.path.join(path_tmp, 'thumbnail.png'), 'wb').write(artwork)
                            except: #remover quando mutagen
                                video_chapters_info = unicode(subprocess.Popen([ffprobe_bin,'-loglevel', 'error',  '-show_chapters', '-print_format', 'xml', filepath], startupinfo=startupinfo, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE).stdout.read(), encoding='utf-8')
                                stream = '0:' + str(int(video_chapters_info.split('nb_streams="')[1].split('"')[0])-1)
                                subprocess.call([ffmpeg_bin, '-loglevel', 'error', '-y', '-i', filepath, '-map', stream, '-c:v', 'copy', os.path.join(path_tmp, 'thumbnail.png')])

                            thumbnail = QPixmap(os.path.join(path_tmp, 'thumbnail.png'))

                            if os.path.isfile(filepath):
                                self.list_of_library_videos[album][filepath] = [title, chapters, thumbnail]


def save(self):
    settings_file = u''

    for song in self.list_of_songs.keys():
        settings_file += u'[song ' + str(song) + ']\n'
        settings_file += u'title=' + self.list_of_songs[song][0] + u'\n'
        settings_file += u'lyrics={' + self.list_of_songs[song][1] + u'}\n'
        settings_file += u'filepath=' + self.list_of_songs[song][2] + u'\n'
        settings_file += u'\n'

    for song in self.list_of_video_songs.keys():
        settings_file += u'[video song ' + str(song) + ']\n'
        settings_file += u'title=' + self.list_of_video_songs[song][0] + u'\n'
        settings_file += u'filepath=' + self.list_of_video_songs[song][1] + u'\n'
        settings_file += u'\n'

    settings_file += u'[paths]\n'
    settings_file += u'videos=' + path_for_videos + u'\n'
    settings_file += u'images=' + path_for_images + u'\n'

    settings_file += u'[config]\n'
    settings_file += u'enable_animations=' + str(self.settings['enable_animations']) + u'\n'
    settings_file += u'framerate=' + str(self.settings['interface_framerate']) + u'\n'
    settings_file += u'show_yeartext=' + str(self.settings['show_yeartext']) + u'\n'
    settings_file += u'yeartext=' + str(self.settings['yeartext']) + u'\n'
    settings_file += u'language_mnemonic=' + str(self.settings['language_mnemonic']) + u'\n'

    if not os.path.isdir(os.path.join(path_home, '.config')):
        os.mkdir(os.path.join(path_home, '.config'))

    if not os.path.isdir(path_user_config):
        os.mkdir(path_user_config)

    if os.path.isfile(os.path.join(path_user_config, 'settings')):
        os.remove(os.path.join(path_user_config, 'settings'))
    open(os.path.join(path_user_config, 'settings'), 'w', encoding='utf-8').write(settings_file)

    bible_file = u''

    for verse in self.list_of_bible_videos.keys():
        bible_file += u'[verse ' + verse + u']\n'
        bible_file += u'filepath=' + self.list_of_bible_videos[verse][0] + u'\n'
        bible_file += u'start=' + str(self.list_of_bible_videos[verse][1]) + u'\n'
        bible_file += u'end=' + str(self.list_of_bible_videos[verse][2]) + u'\n'

    if os.path.isfile(os.path.join(path_user_config, 'bible')):
        os.remove(os.path.join(path_user_config, 'bible'))
    open(os.path.join(path_user_config, 'bible'), 'w', encoding='utf-8').write(bible_file)

    library_file = u''

    for album in self.list_of_library_videos.keys():
        library_file += u'[album ' + album + u']\n'
        for filepath in self.list_of_library_videos[album].keys():
            library_file += u'filepath=' + filepath + u'\n'
            library_file += u'title=' + self.list_of_library_videos[album][filepath][0] + u'\n'
            for chapter in self.list_of_library_videos[album][filepath][1].keys():
                library_file += u'chapter=' + chapter + ';' + str(self.list_of_library_videos[album][filepath][1][chapter][0]) + ';' + str(self.list_of_library_videos[album][filepath][1][chapter][1]) + u'\n'

    if os.path.isfile(os.path.join(path_user_config, 'library')):
        os.remove(os.path.join(path_user_config, 'library'))
    open(os.path.join(path_user_config, 'library'), 'w', encoding='utf-8').write(library_file)
