import os
from PySide2.QtWidgets import QLabel, QWidget, QPushButton, QCheckBox, QVBoxLayout, QScrollArea
from PySide2.QtCore import Qt, QPropertyAnimation, QEasingCurve

from modules.paths import *
from modules.effect import generate_effect
from modules import right_panel


def load(self):
    self.playlist_shadow_left = QLabel(parent=self)
    self.playlist_shadow_left.setStyleSheet('QWidget {border-image: url(' + os.path.join(path_graphics, 'playlist_shadow_left.png').replace('\\', '/') + ') ; } ')

    self.playlist_options_panel = QLabel(parent=self)
    self.playlist_options_panel.setObjectName('playlist_options_panel')
    self.playlist_options_panel.setStyleSheet('#playlist_options_panel { border-bottom: 60px; border-left: 40px; border-image: url("' + os.path.join(path_graphics, "playlist_options_background.png").replace('\\', '/') + '") 0 0 60 40 stretch stretch; }')
    self.playlist_options_panel_geometry_animation = QPropertyAnimation(self.playlist_options_panel, b'geometry')
    self.playlist_options_panel_geometry_animation.setEasingCurve(QEasingCurve.OutCirc)

    self.playlist_options_panel_audio_panel = QWidget(parent=self.playlist_options_panel)

    self.playlist_options_panel_audio_panel_show_lyrics = QCheckBox(u'Mostrar letra na tela', parent=self.playlist_options_panel_audio_panel)
    self.playlist_options_panel_audio_panel_show_lyrics.clicked.connect(lambda:self.playlist_options_panel_audio_panel_show_lyrics_clicked())
    #  self.playlist_options_panel_audio_panel = QLabel(parent=self.playlist_options_panel)
    # self.playlist_options_panel_audio_panel.setStyleSheet('QLabel { font-family: "Ubuntu"; font-size:12pt; color:gray; qproperty-alignment: "AlignCenter"; }')
    # self.playlist_options_panel_audio_panel.setText(u'Sem opções\npara audio')

    # self.playlist_options_panel_image_panel = QWidget(parent=self.playlist_options_panel)
    self.playlist_options_panel_image_panel = QLabel(parent=self.playlist_options_panel)
    self.playlist_options_panel_image_panel.setStyleSheet('QLabel { font-family: "Ubuntu"; font-size:12pt; color:gray; qproperty-alignment: "AlignCenter"; }')
    self.playlist_options_panel_image_panel.setText(u'Sem opções\npara imagem')

    # self.playlist_options_panel_video_panel = QWidget(parent=self.playlist_options_panel)
    self.playlist_options_panel_video_panel = QLabel(parent=self.playlist_options_panel)
    self.playlist_options_panel_video_panel.setStyleSheet('QLabel { font-family: "Ubuntu"; font-size:12pt; color:gray; qproperty-alignment: "AlignCenter"; }')
    self.playlist_options_panel_video_panel.setText(u'Sem opções\npara video')

    # self.playlist_options_panel_web_panel = QWidget(parent=self.playlist_options_panel)
    self.playlist_options_panel_web_panel = QLabel(parent=self.playlist_options_panel)
    self.playlist_options_panel_web_panel.setStyleSheet('QLabel { font-family: "Ubuntu"; font-size:12pt; color:gray; qproperty-alignment: "AlignCenter"; }')
    self.playlist_options_panel_web_panel.setText(u'Sem opções\npara web')

    # self.playlist_options_panel_clock_panel = QWidget(parent=self.playlist_options_panel)
    self.playlist_options_panel_clock_panel = QLabel(parent=self.playlist_options_panel)
    self.playlist_options_panel_clock_panel.setStyleSheet('QLabel { font-family: "Ubuntu"; font-size:12pt; color:gray; qproperty-alignment: "AlignCenter"; }')
    self.playlist_options_panel_clock_panel.setText(u'Sem opções\npara clock')

    # self.playlist_options_panel_screencopy_panel = QWidget(parent=self.playlist_options_panel)
    self.playlist_options_panel_screencopy_panel = QLabel(parent=self.playlist_options_panel)
    self.playlist_options_panel_screencopy_panel.setStyleSheet('QLabel { font-family: "Ubuntu"; font-size:12pt; color:gray; qproperty-alignment: "AlignCenter"; }')
    self.playlist_options_panel_screencopy_panel.setText(u'Sem opções\npara screencopy')

    # self.playlist_options_panel_camera_panel = QWidget(parent=self.playlist_options_panel)
    self.playlist_options_panel_camera_panel = QLabel(parent=self.playlist_options_panel)
    self.playlist_options_panel_camera_panel.setStyleSheet('QLabel { font-family: "Ubuntu"; font-size:12pt; color:gray; qproperty-alignment: "AlignCenter"; }')
    self.playlist_options_panel_camera_panel.setText(u'Sem opções\npara camera')

    self.playlist_widget = QLabel(parent=self)
    self.playlist_widget.setObjectName('playlist_widget')
    self.playlist_widget.setStyleSheet('#playlist_widget { border-bottom: 80px; border-left: 32px; border-image: url("' + os.path.join(path_graphics, "playlist_background.png").replace('\\', '/') + '") 0 0 80 32 stretch stretch; }')

    class playlist_options_panel_open_button(QLabel):
        # def mouseReleaseEvent(widget, event):
        def mousePressEvent(widget, event):
            if len(self.playlist) > 0:
                self.playlist_options_is_visible = not self.playlist_options_is_visible
                self.playlist_options_panel_open_button_clicked()

        def enterEvent(widget, event):
            if self.playlist_options_is_visible:
                widget.setStyleSheet('#playlist_options_panel_open_button { image: url("' + os.path.join(path_graphics, "playlist_options_panel_open_button_back.png").replace('\\', '/') + '") }')
            elif len(self.playlist) > 0:
                widget.setStyleSheet('#playlist_options_panel_open_button { image: url("' + os.path.join(path_graphics, "playlist_options_panel_open_button_hover.png").replace('\\', '/') + '") }')

        def leaveEvent(widget, event):
            #if self.highlighted_media:
            widget.setStyleSheet('#playlist_options_panel_open_button { image: url("' + os.path.join(path_graphics, "playlist_options_panel_open_button_normal.png").replace('\\', '/') + '") }')

    self.playlist_options_panel_open_button = playlist_options_panel_open_button(parent=self)
    self.playlist_options_panel_open_button.setObjectName('playlist_options_panel_open_button')
    self.playlist_options_panel_open_button.setStyleSheet('#playlist_options_panel_open_button { image: url("' + os.path.join(path_graphics, "playlist_options_panel_open_button_normal.png").replace('\\', '/') + '") }')
    self.playlist_options_panel_open_button_geometry_animation = QPropertyAnimation(self.playlist_options_panel_open_button, b'geometry')
    self.playlist_options_panel_open_button_geometry_animation.setEasingCurve(QEasingCurve.OutCirc)

    self.playlist_widget_list = QWidget()
    self.playlist_widget_list.setStyleSheet('QWidget { background: transparent; }')
    self.playlist_widget_listgrid = QVBoxLayout(self.playlist_widget_list)
    self.playlist_widget_listgrid.setSpacing(4)
    self.playlist_widget_listgrid.setContentsMargins(5, 0, 17, 0)
    #self.playlist_widget_listgrid.setHorizontalSpacing(2)

    self.playlist_widget_scroll = QScrollArea(parent=self.playlist_widget)
    self.playlist_widget_scroll.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
    self.playlist_widget_scroll.setWidget(self.playlist_widget_list)
    self.playlist_widget_scroll.setFocusPolicy(Qt.NoFocus)
    self.playlist_widget_scroll.setStyleSheet('QScrollArea { background: transparent; }')
    #self.playlist_widget_scroll.horizontalScrollBar().valueChanged.connect(lambda:playlist_widget_scroll_updated(self))

    class playlist_nextarrow(QLabel):
        def mousePressEvent(widget, event):
            playlist_nextarrow_clicked(self)

    self.playlist_nextarrow = playlist_nextarrow(parent=self)
    self.playlist_nextarrow.setObjectName('playlist_nextarrow')
    self.playlist_nextarrow.setStyleSheet('#playlist_nextarrow { border-top: 3px; border-right: 18px; border-bottom: 3px; border-left: 3px; border-image: url("' + os.path.join(path_graphics, "playlist_nextarrow.png").replace('\\', '/') + '") 3 18 3 3 stretch stretch; }')

    self.playlist_nextarrow_preview = QLabel(parent=self.playlist_nextarrow)
    self.playlist_nextarrow_preview.setScaledContents(True)
    self.playlist_nextarrow_preview.setGeometry(13,10,50*self.screen_height_proportion,50)

    self.playlist_nextarrow_description = QLabel(parent=self.playlist_nextarrow)
    self.playlist_nextarrow_description.setStyleSheet('QLabel { padding: 5px; }')

    self.playlist_lastitem = QLabel(parent=self.playlist_widget)
    self.playlist_lastitem.setObjectName('playlist_lastitem')
    self.playlist_lastitem.setStyleSheet('#playlist_lastitem { border-top: 3px; border-right: 3px; border-bottom: 3px; border-left: 3px; border-image: url("' + os.path.join(path_graphics, "item_background.png").replace('\\', '/') + '") 3 3 3 3 stretch stretch; }')


def resize(self):
    self.playlist_shadow_left.setGeometry(self.width()*.75,0,10,self.height())
    if self.playlist_options_is_visible:
        self.playlist_options_panel.setGeometry(60,self.playlist_options_panel.y(),(self.width()*.5)-30,self.playlist_options_panel.height())
    else:
        self.playlist_options_panel.setGeometry((self.width()*.5)-20,0,(self.width()*.25)+20,self.height())
    self.playlist_options_panel_audio_panel.setGeometry(30,20,(self.width()*.5)-110,self.playlist_options_panel.height()-40)
    self.playlist_options_panel_audio_panel_show_lyrics.setGeometry(0,0,self.playlist_options_panel_audio_panel.width(),20)
    self.playlist_options_panel_image_panel.setGeometry(30,20,(self.width()*.5)-110,self.playlist_options_panel.height()-40)
    self.playlist_options_panel_video_panel.setGeometry(30,20,(self.width()*.5)-110,self.playlist_options_panel.height()-40)
    self.playlist_options_panel_web_panel.setGeometry(30,20,(self.width()*.5)-110,self.playlist_options_panel.height()-40)
    self.playlist_options_panel_clock_panel.setGeometry(30,20,(self.width()*.5)-110,self.playlist_options_panel.height()-40)
    self.playlist_options_panel_screencopy_panel.setGeometry(30,20,(self.width()*.5)-110,self.playlist_options_panel.height()-40)
    self.playlist_options_panel_camera_panel.setGeometry(30,20,(self.width()*.5)-110,self.playlist_options_panel.height()-40)
    self.playlist_options_panel_open_button.setGeometry(self.playlist_options_panel.x(),self.playlist_options_panel.height()-70,50,70)
    self.playlist_widget.setGeometry(self.width()*.5,0,self.width()*.25,self.height())
    self.playlist_nextarrow.setGeometry((self.width()*.5)-3,(self.height())-151,(self.width()*.25)+41,70)
    self.playlist_nextarrow_description.setGeometry(13+self.playlist_nextarrow_preview.width(),10,self.playlist_nextarrow.width()-20-13-(50*self.screen_height_proportion),50)
    self.playlist_widget_scroll.setGeometry(0,20,self.playlist_widget.width()-13,self.playlist_widget.height()-171)
    if self.playlist_widget_scroll.height() > self.playlist_widget_list.height():
        self.playlist_widget_scroll.setViewportMargins(0,self.playlist_widget_scroll.height()-self.playlist_widget_list.height()-4,0,0)
    else:
        self.playlist_widget_scroll.setViewportMargins(0,0,0,0)

    self.playlist_lastitem.setGeometry(37,self.playlist_widget.height()-20-56,self.playlist_widget.width()-57,56)


def populate_playlist(self):
    while self.playlist_widget_listgrid.count() > 0:
        child = self.playlist_widget_listgrid.takeAt(0)
        child.widget().deleteLater()

    if len(self.playlist) > 0:
        self.playlist_nextarrow.setVisible(True)
        self.right_panel_shownext_box.setStyleSheet('#right_panel_shownext_box { border-top: 75px; border-right: 25px; border-left: 25px; border-image: url("' + os.path.join(path_graphics, "shownext_box_background.png").replace('\\', '/') + '") 75 25 0 25 stretch stretch; }')
        self.right_panel_shownext_button.setText(u'MOSTRAR AGORA')

        active_list = list(reversed(self.playlist))
        first_item = active_list.pop()

        height = -4

        if first_item[0] == 'audio':
            item_description = u'<font style="font-size:14px;color:white;">' + first_item[3] + '</font><br><font style="font-size:10px;color:silver;">'
            if first_item[6]:
                item_description += u'COM LETRA NA TELA'
            else:
                item_description += u'SEM LETRA NA TELA'
            item_description += '</font>'
        elif first_item[0] in ['image']:
            item_description = u'<font style="font-size:14px;color:white;">' + first_item[2].split('/')[-1].rsplit('.', 1)[0] + '</font><br><font style="font-size:10px;color:silver;">' + first_item[2] + '</font>'
        elif first_item[0] in ['video']:
            item_description = u'<font style="font-size:14px;color:white;">' + first_item[4] + '</font><br><font style="font-size:10px;color:silver;">' + first_item[2] + '</font>'
        elif first_item[0] in ['web']:
            item_description = u'<font style="font-size:14px;color:white;">' + first_item[2] + '</font><br><font style="font-size:10px;color:silver;">' + first_item[2] + '</font>'
        elif first_item[0] in ['clock']:
            item_description = u'<font style="font-size:14px;color:white;">' + u'RELÓGIO' + '</font>'
        elif first_item[0] in ['screencopy']:
            item_description = u'<font style="font-size:14px;color:white;">' + u'CÓPIA DE TELA' + '</font>'
        elif first_item[0] in ['camera']:
            item_description = u'<font style="font-size:14px;color:white;">' + u'CAMERA' + '</font>'
            first_item[2].stop()
            first_item[2].set_xwindow(self.playlist_nextarrow_preview.winId())
            first_item[2].play()

        self.playlist_nextarrow_preview.setPixmap(first_item[1])
        self.playlist_nextarrow_description.setText(item_description)

        index = 0
        for item in active_list:
            if self.highlighted_media == self.playlist.index(item):
                subcolor = 'silver'
            else:
                subcolor = 'gray'

            if item[0] == 'audio':
                item_description = u'<font style="font-size:14px;color:black;">' + item[3] + '</font><br><font style="font-size:10px;color:' + subcolor + ';">'
                if item[6]:
                    item_description += u'COM LETRA NA TELA'
                else:
                    item_description += u'SEM LETRA NA TELA'
                item_description += '</font>'
            elif item[0] in ['image']:
                item_description = u'<font style="font-size:14px;color:black;">' + item[2].split('/')[-1].rsplit('.', 1)[0] + '</font><br><font style="font-size:10px;color:' + subcolor + ';">' + item[2] + '</font>'
            elif item[0] in ['video']:
                item_description = u'<font style="font-size:14px;color:black;">' + item[4] + '</font><br><font style="font-size:10px;color:' + subcolor + ';">' + item[2] + '</font>'
            elif item[0] in ['web']:
                item_description = u'<font style="font-size:14px;color:black;">' + item[2] + '</font><br><font style="font-size:10px;color:' + subcolor + ';">' + item[2] + '</font>'
            elif item[0] in ['clock']:
                item_description = u'<font style="font-size:14px;color:black;">' + u'RELÓGIO' + '</font>'
            elif item[0] in ['screencopy']:
                item_description = u'<font style="font-size:14px;color:black;">' + u'CÓPIA DE TELA' + '</font>'
            elif item[0] in ['camera']:
                item_description = u'<font style="font-size:14px;color:black;">' + u'CAMERA' + '</font>'

            class item_widget(QLabel):
                #def mouseReleaseEvent(widget, event):
                #    self.right_panel_shownext_box.setStyleSheet('#right_panel_shownext_box { border-top: 75px; border-right: 25px; border-left: 25px; border-image: url("' + os.path.join(path_graphics, "shownext_box_background_hover.png").replace('\\', '/') + '") 75 25 0 25 stretch stretch; }')
                #def mousePressEvent(widget, event):

                #    #self.highlight_media(lambda : self.playlist.index(item) )#, widget)
                #    self.highlight_media(self.playlist.index(item), widget)
                #    #    self.right_panel_shownext_box.setStyleSheet('#right_panel_shownext_box { border-top: 75px; border-right: 25px; border-left: 25px; border-image: url("' + os.path.join(path_graphics, "shownext_box_background_pressed.png").replace('\\', '/') + '") 75 25 0 25 stretch stretch; }')
                def enterEvent(widget, event):
                    widget.item_up_button.setVisible(True)
                    widget.item_delete_button.setVisible(True)
                    widget.item_down_button.setVisible(True)
                    widget.item_to_next_button.setVisible(True)
                    widget.item_to_back_button.setVisible(True)
                def leaveEvent(widget, event):
                    widget.item_up_button.setVisible(False)
                    widget.item_delete_button.setVisible(False)
                    widget.item_down_button.setVisible(False)
                    widget.item_to_next_button.setVisible(False)
                    widget.item_to_back_button.setVisible(False)

            item_widget = item_widget()
            # item_widget_index = self.playlist.index(item)
            item_widget.setText(item_description)
            item_widget.setObjectName('item_widget')
            if self.highlighted_media == self.playlist.index(item):
                item_widget.setStyleSheet('#item_widget { padding-left:' + str((50*self.screen_height_proportion)+5) + 'px; border-top: 3px; border-right: 3px; border-bottom: 3px; border-left: 3px; border-image: url("' + os.path.join(path_graphics, "item_background_highlighted.png").replace('\\', '/') + '") 3 3 3 3 stretch stretch; }')
            else:
                item_widget.setStyleSheet('#item_widget { padding-left:' + str((50*self.screen_height_proportion)+5) + 'px; border-top: 3px; border-right: 3px; border-bottom: 3px; border-left: 3px; border-image: url("' + os.path.join(path_graphics, "item_background.png").replace('\\', '/') + '") 3 3 3 3 stretch stretch; }')
            item_widget.mousePressEvent = lambda: highlight_media(self, index)
            # item_widget.setToolTip(str())

            item_widget.item_preview = QLabel(parent=item_widget)
            item_widget.item_preview.setScaledContents(True)
            item_widget.item_preview.setGeometry(3,3,50*self.screen_height_proportion,50)
            item_widget.item_preview.setPixmap(item[1])

            item_widget.item_up_button = QPushButton(parent=item_widget)
            item_widget.item_up_button.setStyleSheet('QPushButton { image: url(' + os.path.join(path_graphics, 'up_icon.png').replace('\\', '/') + '); border-top: 5px; border-right: 5px; border-bottom: 0px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_normal.png").replace('\\', '/') + '") 5 5 16 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 5px; border-bottom: 0px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 16 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 0px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_hover.png").replace('\\', '/') + '") 5 5 16 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 16 5 stretch stretch; outline: none; }')
            item_widget.item_up_button.setGeometry(self.playlist_widget_list.width()-60,6,30,15)
            item_widget.item_up_button.clicked.connect(lambda: playlist_item_up(self, index))
            item_widget.item_up_button.setVisible(False)

            item_widget.item_delete_button = QPushButton(parent=item_widget)
            item_widget.item_delete_button.setStyleSheet('QPushButton { image: url(' + os.path.join(path_graphics, 'delete_icon.png').replace('\\', '/') + '); border-top: 0px; border-right: 5px; border-bottom: 0px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_normal.png").replace('\\', '/') + '") 14 5 14 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 0px; border-right: 5px; border-bottom: 0px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 14 5 14 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 0px; border-right: 5px; border-bottom: 0px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_hover.png").replace('\\', '/') + '") 14 5 14 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 14 5 14 5 stretch stretch; outline: none; }')
            item_widget.item_delete_button.setGeometry(self.playlist_widget_list.width()-60,21,30,14)
            item_widget.item_delete_button.clicked.connect(lambda: playlist_item_delete(self, index))
            item_widget.item_delete_button.setVisible(False)

            item_widget.item_down_button = QPushButton(parent=item_widget)
            item_widget.item_down_button.setStyleSheet('QPushButton { image: url(' + os.path.join(path_graphics, 'down_icon.png').replace('\\', '/') + '); border-top: 0px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_normal.png").replace('\\', '/') + '") 16 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 0px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 16 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 0px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_hover.png").replace('\\', '/') + '") 16 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 16 5 5 5 stretch stretch; outline: none; }')
            item_widget.item_down_button.setGeometry(self.playlist_widget_list.width()-60,35,30,15)
            item_widget.item_down_button.clicked.connect(lambda: playlist_item_down(self, index))
            item_widget.item_down_button.setVisible(False)

            item_widget.item_to_next_button = QPushButton(parent=item_widget)
            item_widget.item_to_next_button.setStyleSheet('QPushButton { image: url(' + os.path.join(path_graphics, 'to_next_icon.png').replace('\\', '/') + '); border-top: 5px; border-right: 0px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_green_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 0px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_green_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 0px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_green_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(path_graphics, "button_green_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
            item_widget.item_to_next_button.setGeometry(((50*self.screen_height_proportion)*.5)-30+3,13,30,30)
            item_widget.item_to_next_button.clicked.connect(lambda: playlist_item_to_next(self, index))
            item_widget.item_to_next_button.setVisible(False)

            item_widget.item_to_back_button = QPushButton(parent=item_widget)
            item_widget.item_to_back_button.setStyleSheet('QPushButton { image: url(' + os.path.join(path_graphics, 'to_back_icon.png').replace('\\', '/') + '); border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 0px; border-image: url("' + os.path.join(path_graphics, "button_green_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 0px; border-image: url("' + os.path.join(path_graphics, "button_green_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 0px; border-image: url("' + os.path.join(path_graphics, "button_green_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(path_graphics, "button_green_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:checked { border-image: url("' + os.path.join(path_graphics, "button_green_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
            item_widget.item_to_back_button.setCheckable(True)
            item_widget.item_to_back_button.setGeometry(((50*self.screen_height_proportion)*.5)+3,13,30,30)
            item_widget.item_to_back_button.clicked.connect(lambda: playlist_item_to_back(self, index, item_widget.item_to_back_button.isChecked()))
            item_widget.item_to_back_button.setVisible(False)

            self.playlist_widget_listgrid.addWidget(item_widget)
            #height += 4

            if item[0] in ['camera']:
                item[2].stop()
                item[2].set_xwindow(item_widget.item_preview.winId())
                item[2].play()

            height += 60
            index += 1

        self.playlist_widget_list.setGeometry(0,0,self.playlist_widget.width(),height)

        if self.playlist_widget_scroll.height() > self.playlist_widget_list.height():
            self.playlist_widget_scroll.setViewportMargins(0,self.playlist_widget_scroll.height()-self.playlist_widget_list.height()-4,0,0)
        else:
            self.playlist_widget_scroll.setViewportMargins(0,0,0,0)



    else:
        self.playlist_nextarrow.setVisible(False)
        self.right_panel_shownext_box.setStyleSheet('#right_panel_shownext_box { border-top: 75px; border-right: 25px; border-left: 25px; border-image: url("' + os.path.join(path_graphics, "shownext_box_background_disabled.png").replace('\\', '/') + '") 75 25 0 25 stretch stretch; }')
        self.right_panel_shownext_button.setText(u'<font style="color:silver;">NADA PARA MOSTRAR</font>')

    if self.last_item:
        self.playlist_lastitem.setVisible(True)
    else:
        self.playlist_lastitem.setVisible(False)

    if len(self.playlist) > 0:
        playlist_options_panel_update(self)
    right_panel.update_preview(self)

def playlist_item_up(self, index):
    print(index)
    old = self.playlist[index + 1]
    self.playlist[index + 1] = self.playlist[index]
    self.playlist[index] = old
    populate_playlist(self)

def playlist_item_delete(self, index):
    del self.playlist[index]
    populate_playlist(self)

def playlist_item_down(self, index):
    old = self.playlist[index - 1]
    self.playlist[index - 1] = self.playlist[index]
    self.playlist[index] = old
    populate_playlist(self)

def playlist_item_to_next(self, index):
    self.playlist.insert(0, self.playlist.pop(index))
    populate_playlist(self)

def playlist_item_to_back(self, index, state):
    self.playlist[index][-1] = state
    #self.playlist_item_to_next(index)
    #self.show_now_clicked()
    #populate_playlist(self)

def highlight_media(self, index):
    self.highlighted_media = int(index)
    populate_playlist(self)

def playlist_nextarrow_clicked(self):
    self.highlighted_media = 0
    playlist_options_panel_update(self)
    populate_playlist(self)

def playlist_options_panel_open_button_clicked(self):
    if self.playlist_options_is_visible:
        generate_effect(self.playlist_options_panel_geometry_animation, 'geometry', 500, [self.playlist_options_panel.x(),self.playlist_options_panel.y(),self.playlist_options_panel.width(),self.playlist_options_panel.height()], [60,self.playlist_options_panel.y(),(self.width()*.5)-30,self.playlist_options_panel.height()])
        generate_effect(self.playlist_options_panel_open_button_geometry_animation, 'geometry', 500, [self.playlist_options_panel_open_button.x(),self.playlist_options_panel_open_button.y(),self.playlist_options_panel_open_button.width(),self.playlist_options_panel_open_button.height()], [60,self.playlist_options_panel_open_button.y(),self.playlist_options_panel_open_button.width(),self.playlist_options_panel_open_button.height()])
        playlist_options_panel_update(self)
    else:
        generate_effect(self.playlist_options_panel_geometry_animation, 'geometry', 500, [self.playlist_options_panel.x(),self.playlist_options_panel.y(),self.playlist_options_panel.width(),self.playlist_options_panel.height()], [(self.width()*.5)-20,0,(self.width()*.25)+20,self.height()])
        generate_effect(self.playlist_options_panel_open_button_geometry_animation, 'geometry', 500, [self.playlist_options_panel_open_button.x(),self.playlist_options_panel_open_button.y(),self.playlist_options_panel_open_button.width(),self.playlist_options_panel_open_button.height()], [(self.width()*.5)-20,self.playlist_options_panel_open_button.y(),self.playlist_options_panel_open_button.width(),self.playlist_options_panel_open_button.height()])

def playlist_options_panel_update(self):
    if self.highlighted_media + 1 >  len(self.playlist):
        self.highlighted_media = 0

    if not self.playlist[self.highlighted_media][0] == 'audio':
        self.playlist_options_panel_audio_panel.setVisible(False)

    if not self.playlist[self.highlighted_media][0] == 'image':
        self.playlist_options_panel_image_panel.setVisible(False)

    if not self.playlist[self.highlighted_media][0] == 'video':
        self.playlist_options_panel_video_panel.setVisible(False)

    if not self.playlist[self.highlighted_media][0] == 'web':
        self.playlist_options_panel_web_panel.setVisible(False)

    if not self.playlist[self.highlighted_media][0] == 'clock':
        self.playlist_options_panel_clock_panel.setVisible(False)

    if not self.playlist[self.highlighted_media][0] == 'screencopy':
        self.playlist_options_panel_screencopy_panel.setVisible(False)

    if not self.playlist[self.highlighted_media][0] == 'camera':
        self.playlist_options_panel_camera_panel.setVisible(False)

    if self.playlist[self.highlighted_media][0] == 'audio':
        self.playlist_options_panel_audio_panel.setVisible(True)
        self.playlist_options_panel_audio_panel_show_lyrics.setChecked(self.playlist[self.highlighted_media][6])

    if self.playlist[self.highlighted_media][0] == 'image':
        self.playlist_options_panel_image_panel.setVisible(True)

    if self.playlist[self.highlighted_media][0] == 'video':
        self.playlist_options_panel_video_panel.setVisible(True)

    if self.playlist[self.highlighted_media][0] == 'web':
        self.playlist_options_panel_web_panel.setVisible(True)

    if self.playlist[self.highlighted_media][0] == 'clock':
        self.playlist_options_panel_clock_panel.setVisible(True)

    if self.playlist[self.highlighted_media][0] == 'screencopy':
        self.playlist_options_panel_screencopy_panel.setVisible(True)

    if self.playlist[self.highlighted_media][0] == 'camera':
        self.playlist_options_panel_camera_panel.setVisible(True)

def playlist_options_panel_audio_panel_show_lyrics_clicked(self):
    self.playlist[self.highlighted_media][6] = self.playlist_options_panel_audio_panel_show_lyrics.isChecked()
    populate_playlist(self)
