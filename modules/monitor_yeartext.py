#!/usr/bin/python3

from PySide2.QtWidgets import QLabel, QGraphicsOpacityEffect, QWidget, QCheckBox, QLineEdit, QPushButton
from PySide2.QtCore import Qt, QPropertyAnimation
from PySide2.QtGui import QPixmap

import datetime
import urllib.request

from modules.paths import *
from modules.effect import generate_effect
from modules import options_box


def load(self):
    self.yeartext_widget = QLabel(parent=self.monitor_window)
    self.yeartext_widget.setGeometry(0, 0, self.monitor_window.width(), self.monitor_window.height())
    self.yeartext_widget.setStyleSheet('QLabel { color:white; font-size:' + str(int(self.monitor_window.height()*.05)) + 'px; padding:' + str(int(self.monitor_window.width()*.25)) + '; }')
    self.yeartext_widget.setWordWrap(True)
    self.yeartext_widget.setAlignment(Qt.AlignCenter)

    self.yeartext_widget_opacity = QGraphicsOpacityEffect()
    self.yeartext_widget.setGraphicsEffect(self.yeartext_widget_opacity)
    self.yeartext_widget_opacity_animation = QPropertyAnimation(self.yeartext_widget_opacity, b'opacity')
    self.yeartext_widget_opacity.setOpacity(0.0)

    update(self)

def load_options(self):
    class main_options_yeartext_icon(QLabel):
        def mousePressEvent(widget, event):
            if not self.main_options_box_selected == 'yeartext':
                self.main_options_box_selected = 'yeartext'
                options_box.show_main_options(self)
            else:
                self.main_options_box_selected = False
                options_box.hide_main_options(self)
            widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'main_options_yeartext_icon_pressed.png').replace('\\', '/') + '); } ')
        def enterEvent(widget, event):
            if not self.main_options_box_selected == 'yeartext':
                widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'main_options_yeartext_icon_hover.png').replace('\\', '/') + '); } ')
        def leaveEvent(widget, event):
            if not self.main_options_box_selected == 'yeartext':
                widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'main_options_yeartext_icon_normal.png').replace('\\', '/') + '); } ')

    self.main_options_yeartext_icon = main_options_yeartext_icon(parent=self.main_options_box)
    self.main_options_yeartext_icon.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'main_options_yeartext_icon_normal.png').replace('\\', '/') + '); } ')

    self.main_options_yeartext_panel = QWidget(self.main_options_box)

    self.main_options_yeartext_show_yeartext = QCheckBox(u'Mostrar texto do ano', parent=self.main_options_yeartext_panel)
    self.main_options_yeartext_show_yeartext.clicked.connect(lambda:main_options_yeartext_show_yeartext_clicked(self))
    self.main_options_yeartext_show_yeartext.setChecked(self.settings['show_yeartext'])

    self.main_options_yeartext_yeartext_label = QLabel(u'Texto do ano'.upper(), parent=self.main_options_yeartext_panel)

    self.main_options_yeartext_yeartext = QLineEdit(parent=self.main_options_yeartext_panel)
    self.main_options_yeartext_yeartext.editingFinished.connect(lambda:main_options_yeartext_yeartext_editingfinished(self))
    self.main_options_yeartext_yeartext.setText(self.settings['yeartext'])

    self.main_options_yeartext_update = QPushButton(u'', self.main_options_yeartext_panel)
    self.main_options_yeartext_update.setStyleSheet('QPushButton {  font-family: "Ubuntu"; font-size:14px; color:white; font-weight:bold; border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }') # image: url(' + os.path.join(path_graphics, 'main_options_yeartext_icon_normal.png').replace('\\', '/') + ');
    self.main_options_yeartext_update.clicked.connect(lambda:main_options_yeartext_update_clicked(self))



def resize_options(self):
    self.main_options_yeartext_icon.setGeometry(110,0,30,30)
    self.main_options_yeartext_panel.setGeometry(5,35,((self.width()*.5)-110)-10,(self.height()*.25)-40)
    self.main_options_yeartext_show_yeartext.setGeometry(5,5,self.main_options_yeartext_panel.width()-10,20)
    self.main_options_yeartext_yeartext_label.setGeometry(5,35,self.main_options_yeartext_panel.width()-10,20)
    self.main_options_yeartext_yeartext.setGeometry(5,55,self.main_options_yeartext_panel.width()-10-35,30)
    self.main_options_yeartext_update.setGeometry(self.main_options_yeartext_yeartext.x()+ self.main_options_yeartext_yeartext.width() + 5, 55, 30,30)


def show_options_panel(self):
    self.main_options_yeartext_panel.setVisible(True)


def hide_options_panel(self):
    self.main_options_yeartext_icon.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'main_options_yeartext_icon_normal.png').replace('\\', '/') + '); } ')
    self.main_options_yeartext_panel.setVisible(False)


def hide(self):
    generate_effect(self.yeartext_widget_opacity_animation, 'opacity', 100, self.yeartext_widget_opacity.opacity(), 0.0)


def show(self):
    generate_effect(self.yeartext_widget_opacity_animation, 'opacity', 10000, self.yeartext_widget_opacity.opacity(), 1.0)


def update(self):
    #if self.settings['yeartext']:
    self.yeartext_widget.setText(self.settings['yeartext'].split('—',1)[0] + '<br>—' + self.settings['yeartext'].split('—',1)[-1])


def main_options_yeartext_show_yeartext_clicked(self):
    self.settings['show_yeartext'] = self.main_options_yeartext_show_yeartext.isChecked()


def main_options_yeartext_yeartext_editingfinished(self):
    self.settings['yeartext'] = self.main_options_yeartext_yeartext.text()
    update(self)

def main_options_yeartext_update_clicked(self):
    language_mnemonic = 'E'
    if self.settings['language_mnemonic']:
        language_mnemonic = self.settings['language_mnemonic']
    epub_file = False
    from bs4 import BeautifulSoup
    import zipfile
    for filename in os.listdir(path_home):
        if filename.startswith('es') and filename.endswith('.epub'):
            epub_file = os.path.join(path_home, filename)
    else:
        with urllib.request.urlopen('https://www.jw.org/download/?output=html&pub=es' + str(datetime.datetime.now().year)[-2:] + '&fileformat=EPUB&langwritten=' + language_mnemonic + '&txtCMSLang=' + language_mnemonic) as response:
            html = BeautifulSoup(response.read().decode(), 'html5lib')
            link = html.find('iframe').get('src')
            try:
                request_epub = urllib.request.urlopen(link)
                epub_file = os.path.join(path_tmp, 'es.epub')
                open(epub_file, 'wb').write(request_epub.read())
            except urllib.error.HTTPError as e:
                None#break

    text = False
    if epub_file:
        html_found = False
        with zipfile.ZipFile(epub_file, 'r') as epub:
            for item in epub.infolist():
                if item.filename.endswith('.xhtml') and not text:
                    html = BeautifulSoup(epub.read(item.filename).decode(), 'html.parser')
                    for p in html.find_all('p'):
                        if '© ' in p.getText():
                            html_found = True
                        if html_found:
                            text = '“' + html.select('p#p3')[0].getText().split('“', 1)[-1]
                            break

    if text:
        self.main_options_yeartext_yeartext.setText(text)
        main_options_yeartext_yeartext_editingfinished(self)
