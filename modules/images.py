from PIL import Image


def create_thumbnail(source_file, target_file, width, height):
    im = Image.open(source_file)
    im.thumbnail((int(width), int(height)), Image.ANTIALIAS)
    imb = Image.new("RGB", (int(width), int(height)), (0, 0, 0))
    if imb.size[1] == im.size[1]:
        imb.paste(im, (int((imb.size[0]-im.size[0])*.5), 0))
    elif imb.size[0] == im.size[0]:
        imb.paste(im, (0, int((imb.size[1]-im.size[1])*.5)))
    imb.save(target_file)
