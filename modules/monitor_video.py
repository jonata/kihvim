#!/usr/bin/python3

import platform

from PySide2.QtWidgets import QLabel, QGraphicsOpacityEffect, QFrame

if platform.system() == "Darwin":
    from PySide2.QtWidgets import QMacCocoaViewContainer

from PySide2.QtCore import Qt, QPropertyAnimation, QUrl
from PySide2.QtGui import QPixmap, QPalette, QColor

from modules.paths import *
from modules.effect import generate_effect

import vlc
video_instance = vlc.Instance()

def load(self):
    self.video_player = video_instance.media_player_new()

    if platform.system() == "Darwin": # for MacOS
        self.video_widget = QMacCocoaViewContainer(0, parent=self.monitor_window)
    else:
        self.video_widget = QFrame(parent=self.monitor_window)

    self.palette = self.video_widget.palette()
    self.palette.setColor(QPalette.Window, QColor(0, 0, 0))
    self.video_widget.setPalette(self.palette)
    self.video_widget.setAutoFillBackground(True)

    if platform.system() == "Linux": # for Linux using the X Server
        self.video_player.set_xwindow(int(self.video_widget.winId()))
    elif platform.system() == "Windows": # for Windows
        self.video_player.set_hwnd(int(self.video_widget.winId()))
    elif platform.system() == "Darwin": # for MacOS
        self.video_player.set_nsobject(int(self.video_widget.winId()))

    #self.video_widget = QVideoWidget(parent=self.monitor_window)
    self.video_widget.setGeometry(0, 0, self.monitor_window.width(), self.monitor_window.height())
    self.video_widget.setVisible(False)
    #self.video_widget.setAlignment(Qt.AlignCenter)
    #self.video_player.setVideoOutput(self.video_widget)

    #self.video_widget_opacity = QGraphicsOpacityEffect()
    #self.video_widget.setGraphicsEffect(self.video_widget_opacity)
    #self.video_widget_opacity_animation = QPropertyAnimation(self.video_widget_opacity, b'opacity')
    #self.video_widget_opacity.setOpacity(0.0)


def hide(self):
    #generate_effect(self.video_widget_opacity_animation, 'opacity', 1000, self.video_widget_opacity.opacity(), 0.0)
    self.video_player.stop()
    self.video_widget.setVisible(False)

def show(self):
    self.video_widget.setVisible(True)
    #generate_effect(self.video_widget_opacity_animation, 'opacity', 1000, self.video_widget_opacity.opacity(), 1.0)

def load_media(self, media):

    self.video_player.set_media(video_instance.media_new(media))
    #self.video_mediaplayer.play()
    #if self.monitor.current_media[5]:
    #    self.video_mediaplayer.set_position(self.monitor.current_media[5]/self.monitor.current_media[7])

def stop(self):
    self.video_player.stop()

def play(self):
    self.video_player.play()

def play_pause(self):
    if self.video_player.get_state() == 3:
        self.video_player.pause()
        #self.is_paused = True
    else:
        #if self.video_mediaplayer.play() == -1:
        #    self.media_open(self.selected_media[1])
        #    return
        #

        self.video_player.play()
        #self.video_mediaplayer.audio_set_volume(100)
        if self.monitor.current_media[0] == 'audio' and self.monitor.current_media[6]:
            self.audio_lyrics_can_go_back = True
            self.audio_widget_title_geometry_animation.setEasingCurve(QEasingCurve.InOutQuart)
            self.audio_widget_artwork_geometry_animation.setEasingCurve(QEasingCurve.InOutQuart)
            generate_effect(self.audio_widget_lyrics_geometry_animation, 'geometry', (self.monitor.current_media[-2]-5)*1000, [self.audio_widget_lyrics.x(),self.monitor_window.height()-(self.monitor_window.height()*.2),self.audio_widget_lyrics.width(),self.audio_widget_lyrics.height()], [self.audio_widget_lyrics.x(),((-1)*self.audio_widget_lyrics.height())+(self.monitor_window.height()*.2),self.audio_widget_lyrics.width(),self.audio_widget_lyrics.height()])
            generate_effect(self.audio_widget_artwork_geometry_animation, 'geometry', 5000, [self.audio_widget_artwork.x(),self.audio_widget_artwork.y(),self.audio_widget_artwork.width(),self.audio_widget_artwork.height()], [self.audio_widget_artwork.x(),self.monitor_window.height()-self.audio_widget_artwork.height(),self.audio_widget_artwork.width(),self.audio_widget_artwork.height()])
            generate_effect(self.audio_widget_title_geometry_animation, 'geometry', 5000, [self.audio_widget_title.x(),self.audio_widget_title.y(),self.audio_widget_title.width(),self.audio_widget_title.height()], [self.audio_widget_title.x(),self.monitor_window.height()-self.audio_widget_artwork.height(),self.audio_widget_title.width(),self.monitor_window.height()*.2])


        #self.is_paused = False
