import os, sys, tempfile, random, subprocess


path_tmp = os.path.join(tempfile.gettempdir(), 'kihvim-' + str(random.randint(1000, 9999)))
path_kihvim = os.path.abspath(os.path.dirname(sys.argv[0]))
path_home = os.path.expanduser("~")
path_graphics = os.path.join(path_kihvim, 'graphics')
path_user_config = os.path.join(path_home, '.config', 'kihvim')

gs_executable = ['gs']
if sys.platform == 'darwin':
    gs_executable = [os.path.join(path_kihvim, 'resources', 'gs'), '-sGenericResourceDir=' + os.path.join(path_kihvim, 'resources', 'Resource') + '/', '-sICCProfilesDir=' +  os.path.join(path_kihvim, 'resources', 'iccprofiles') + '/', '-I' + os.path.join(path_kihvim, 'resources', 'Resource', 'Init')]
elif sys.platform == 'win32' or os.name == 'nt':
    gs_executable = [os.path.join(path_kihvim, 'gs927w32.exe')]
else:
    if 'SNAP_VERSION' in [*os.environ]:
        uid = subprocess.Popen(['id', '-u'], stdout=subprocess.PIPE).stdout.read().decode()[:-1]
        path_home = subprocess.Popen(['getent', 'passwd', uid], stdout=subprocess.PIPE).stdout.read().decode().split(':')[5]
gs_executable += ['-dNOPAUSE', '-dBATCH', '-q']

if sys.platform == 'darwin':
    ffmpeg_bin = os.path.join(path_kihvim, 'resources',  'ffmpeg')
    ffprobe_bin = os.path.join(path_kihvim, 'resources', 'ffprobe')
elif sys.platform == 'win32' or os.name == 'nt':
    ffmpeg_bin = os.path.join(path_kihvim, 'resources', 'ffmpeg.exe')
    ffprobe_bin = os.path.join(path_kihvim, 'resources', 'ffprobe.exe')
else:
    ffmpeg_bin = 'ffmpeg'
    ffprobe_bin = 'ffprobe'

if os.path.isdir(os.path.join(path_home, 'Videos', 'JWLibrary')):
    path_for_videos = os.path.join(path_home, 'Videos', 'JWLibrary')
elif os.path.isdir(os.path.join(path_home, 'Downloads')):
    path_for_videos = os.path.join(path_home, 'Downloads')
else:
    path_for_videos = path_home

if os.path.isdir(os.path.join(path_home, 'Downloads')):
    path_for_images = os.path.join(path_home, 'Downloads')
else:
    path_for_images = os.path.join(path_home)
