import os
from PySide2.QtWidgets import QLabel, QWidget, QPushButton, QCheckBox, QDial, QComboBox, QLineEdit
from PySide2.QtCore import QPropertyAnimation, QEasingCurve

from modules.paths import *
from modules.effect import generate_effect
from modules import monitor_yeartext
from modules import utils

language_list = utils.get_language_codes()


def load(self):
    self.main_options_box = QLabel(parent=self)
    self.main_options_box.setObjectName('main_options_box')
    self.main_options_box.setStyleSheet('QLabel {color:silver;} QCheckBox {color:silver;} #main_options_box { border-top: 34px; border-right: 5px; border-bottom: 0; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "background_options_box.png").replace('\\', '/') + '") 34 5 5 5 stretch stretch; }')
    self.main_options_box_geometry_animation = QPropertyAnimation(self.main_options_box, b'geometry')
    self.main_options_box_geometry_animation.setEasingCurve(QEasingCurve.OutCirc)

    class main_options_file_icon(QLabel):
        def mousePressEvent(widget, event):
            if not self.main_options_box_selected == 'file':
                self.main_options_box_selected = 'file'
                show_main_options(self)
            else:
                self.main_options_box_selected = False
                hide_main_options()
            widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'main_options_file_icon_pressed.png').replace('\\', '/') + '); } ')

        def enterEvent(widget, event):
            if not self.main_options_box_selected == 'file':
                widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'main_options_file_icon_hover.png').replace('\\', '/') + '); } ')

        def leaveEvent(widget, event):
            if not self.main_options_box_selected == 'file':
                widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'main_options_file_icon_normal.png').replace('\\', '/') + '); } ')

    self.main_options_file_icon = main_options_file_icon(parent=self.main_options_box)
    self.main_options_file_icon.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'main_options_file_icon_normal.png').replace('\\', '/') + '); } ')

    self.main_options_file_panel = QWidget(self.main_options_box)

    self.main_options_file_open_button = QPushButton(self.main_options_file_panel)
    self.main_options_file_open_button.setStyleSheet('QPushButton { image: url(' + os.path.join(path_graphics, 'open_playlist_icon.png').replace('\\', '/') + '); font-family: "Ubuntu"; font-size:14px; color:white; font-weight:bold; border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
    self.main_options_file_open_button.clicked.connect(lambda:self.main_options_file_open_button_clicked())

    self.main_options_file_open_label = QLabel(u'Abrir\nplaylist',self.main_options_file_panel)

    self.main_options_file_save_button = QPushButton(self.main_options_file_panel)
    self.main_options_file_save_button.setStyleSheet('QPushButton { image: url(' + os.path.join(path_graphics, 'save_playlist_icon.png').replace('\\', '/') + '); font-family: "Ubuntu"; font-size:14px; color:white; font-weight:bold; border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
    self.main_options_file_save_button.clicked.connect(lambda:self.main_options_file_save_button_clicked())

    self.main_options_file_save_label = QLabel(u'Guardar\nplaylist',self.main_options_file_panel)

    class main_options_settings_icon(QLabel):
        def mousePressEvent(widget, event):
            if not self.main_options_box_selected == 'settings':
                self.main_options_box_selected = 'settings'
                show_main_options(self)
            else:
                self.main_options_box_selected = False
                hide_main_options(self)
            widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'main_options_settings_icon_pressed.png').replace('\\', '/') + '); } ')
        def enterEvent(widget, event):
            if not self.main_options_box_selected == 'settings':
                widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'main_options_settings_icon_hover.png').replace('\\', '/') + '); } ')
        def leaveEvent(widget, event):
            if not self.main_options_box_selected == 'settings':
                widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'main_options_settings_icon_normal.png').replace('\\', '/') + '); } ')

    self.main_options_settings_icon = main_options_settings_icon(parent=self.main_options_box)
    self.main_options_settings_icon.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'main_options_settings_icon_normal.png').replace('\\', '/') + '); } ')

    self.main_options_settings_panel = QWidget(self.main_options_box)

    self.main_options_settings_enable_animations = QCheckBox(u'Animações na interface', parent=self.main_options_settings_panel)
    self.main_options_settings_enable_animations.clicked.connect(lambda:main_options_settings_enable_animations_clicked())
    self.main_options_settings_enable_animations.setChecked(self.settings['enable_animations'])

    self.main_options_settings_framerate_label = QLabel(parent=self.main_options_settings_panel)

    self.main_options_settings_framerate = QDial(parent=self.main_options_settings_panel)
    self.main_options_settings_framerate.setMaximum(60)
    self.main_options_settings_framerate.setMinimum(1)
    self.main_options_settings_framerate.valueChanged.connect(lambda:main_options_settings_framerate_changing(self))
    self.main_options_settings_framerate.sliderReleased.connect(lambda:main_options_settings_framerate_changed(self))
    self.main_options_settings_framerate.setValue(self.settings['interface_framerate'])
    main_options_settings_framerate_changing(self)


    self.main_options_settings_language_label = QLabel('IDIOMA', parent=self.main_options_settings_panel)

    self.main_options_settings_language = QComboBox(parent=self.main_options_settings_panel)
    self.main_options_settings_language.addItems(sorted([*language_list]))
    self.main_options_settings_language.activated.connect(lambda:main_options_settings_language_activated(self))
    if self.settings['language_mnemonic']:
        self.main_options_settings_language.setCurrentText(dict(map(reversed, language_list.items()))[self.settings['language_mnemonic']])

    class main_options_export_icon(QLabel):
        def mousePressEvent(widget, event):
            if not self.main_options_box_selected == 'export':
                self.main_options_box_selected = 'export'
                show_main_options(self)
            else:
                self.main_options_box_selected = False
                hide_main_options(self)
            widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'main_options_export_icon_pressed.png').replace('\\', '/') + '); } ')
        def enterEvent(widget, event):
            if not self.main_options_box_selected == 'export':
                widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'main_options_export_icon_hover.png').replace('\\', '/') + '); } ')
        def leaveEvent(widget, event):
            if not self.main_options_box_selected == 'export':
                widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'main_options_export_icon_normal.png').replace('\\', '/') + '); } ')

    self.main_options_export_icon = main_options_export_icon(parent=self.main_options_box)
    self.main_options_export_icon.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'main_options_export_icon_normal.png').replace('\\', '/') + '); } ')

    self.main_options_export_panel = QWidget(self.main_options_box)

    self.main_options_export_profile = QComboBox(self.main_options_export_panel)
    #self.main_options_export_profile.addItems(['320x180 (iPod/iPhone)', '320x240 (PSP)', '720x480 (DVD player)', '1280x720 (SmartTV)', '704x480 (SmartTV)'])
    self.main_options_export_profile.addItems(['1280x720 (SmartTV)'])

    self.main_options_export_button = QPushButton(u'GERAR VÍDEO', self.main_options_export_panel)
    self.main_options_export_button.setStyleSheet('QPushButton { font-family: "Ubuntu"; font-size:14px; color:white; font-weight:bold; border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }') #image: url(' + os.path.join(path_graphics, 'main_options_export_icon_normal.png').replace('\\', '/') + '); 
    self.main_options_export_button.clicked.connect(lambda:self.main_options_export_button_clicked())

    monitor_yeartext.load_options(self)

def resize(self):
    if self.main_options_box_is_visible:
        self.main_options_box.setGeometry(80,self.height()*.75,(self.width()*.5)-110,self.height()*.25)
    else:
        self.main_options_box.setGeometry(80,self.height()-30,(self.width()*.5)-110,30)

    self.main_options_file_icon.setGeometry(5,0,30,30)
    self.main_options_file_panel.setGeometry(5,35,((self.width()*.5)-110)-10,(self.height()*.25)-40)
    self.main_options_file_open_button.setGeometry(5,5,40,40)
    self.main_options_file_open_label.setGeometry(55,5,(self.main_options_file_panel.width()-60),40)
    self.main_options_file_save_button.setGeometry(5,55,40,40)
    self.main_options_file_save_label.setGeometry(55,55,(self.main_options_file_panel.width()-60),40)

    self.main_options_settings_icon.setGeometry(40,0,30,30)
    self.main_options_settings_panel.setGeometry(5,35,((self.width()*.5)-110)-10,(self.height()*.25)-40)
    self.main_options_settings_enable_animations.setGeometry(5,5,self.main_options_settings_panel.width()-10,20)
    self.main_options_settings_framerate.setGeometry(5,35,40,40)
    self.main_options_settings_framerate_label.setGeometry(50,35,self.main_options_settings_panel.width()-55,40)
    self.main_options_settings_language_label.setGeometry(5,85,self.main_options_settings_panel.width()-55,20)
    self.main_options_settings_language.setGeometry(5,105,self.main_options_settings_panel.width()-10,30)

    self.main_options_export_icon.setGeometry(75,0,30,30)
    self.main_options_export_panel.setGeometry(5,35,((self.width()*.5)-110)-10,(self.height()*.25)-40)
    self.main_options_export_profile.setGeometry(5,5,self.main_options_export_panel.width()-10,20)
    self.main_options_export_button.setGeometry(5,35,self.main_options_export_panel.width()-10,40)

    monitor_yeartext.resize_options(self)

def show_main_options(self):
    self.main_options_box_is_visible = True
    if self.settings['enable_animations']:
        generate_effect(self.main_options_box_geometry_animation, 'geometry', 500, [self.main_options_box.x(),self.main_options_box.y(),self.main_options_box.width(),self.main_options_box.height()], [80,self.height()*.75,(self.width()*.5)-110,self.height()*.25])
    else:
        self.main_options_box.setGeometry(80,self.height()*.75,(self.width()*.5)-110,self.height()*.25)

    if not self.main_options_box_selected == 'file':
        self.main_options_file_icon.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'main_options_file_icon_normal.png').replace('\\', '/') + '); } ')
        self.main_options_file_panel.setVisible(False)
    if not self.main_options_box_selected == 'settings':
        self.main_options_settings_icon.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'main_options_settings_icon_normal.png').replace('\\', '/') + '); } ')
        self.main_options_settings_panel.setVisible(False)
    if not self.main_options_box_selected == 'export':
        self.main_options_export_icon.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'main_options_export_icon_normal.png').replace('\\', '/') + '); } ')
        self.main_options_export_panel.setVisible(False)
    if not self.main_options_box_selected == 'yeartext':
        monitor_yeartext.hide_options_panel(self)


    if self.main_options_box_selected == 'file':
        self.main_options_file_panel.setVisible(True)
    if self.main_options_box_selected == 'settings':
        self.main_options_settings_panel.setVisible(True)
    if self.main_options_box_selected == 'export':
        self.main_options_export_panel.setVisible(True)
    if self.main_options_box_selected == 'yeartext':
        monitor_yeartext.show_options_panel(self)


def hide_main_options(self):
    self.main_options_box_is_visible = False
    if self.settings['enable_animations']:
        generate_effect(self.main_options_box_geometry_animation, 'geometry', 500, [self.main_options_box.x(),self.main_options_box.y(),self.main_options_box.width(),self.main_options_box.height()], [80,self.height()-30,(self.width()*.5)-110,30])
    else:
        self.main_options_box.setGeometry(80,self.height()-30,(self.width()*.5)-110,30)

def main_options_file_open_button_clicked(self):
    self.actual_playlist_file = QFileDialog.getOpenFileName(self, 'Selecione um arquivo de playlist para abrir', path_home, 'Playlists do Kihvim (*.kihvim)', options=self.dialog_options)[0]
    if self.actual_playlist_file:
        del self.playlist[:]
        playlist_content = codecs.open(os.path.join(self.actual_playlist_file), 'r', 'utf-8').read().split('<playlist')[1].split('>', 1)[1].split('</playlist>')[0]

        for media_line in playlist_content.split('<media '):
            if '</media>' in media_line:
                media_type = media_line.split('type="')[1].split('"')[0]
                final_media = [media_type]

                if media_type == 'audio':
                    media_line_audio = media_line.split('>')[0]
                    filepath = False
                    if ' filepath="' in media_line_audio:
                        filepath = media_line_audio.split(' filepath="')[1].split('"')[0]

                    if filepath and os.path.isfile(filepath):
                        title = False
                        if ' title="' in media_line_audio:
                            title = media_line_audio.split(' title="')[1].split('"')[0]
                        else:
                            song = ID3(filepath)
                            if 'TIT2' in song.keys():
                                title = song["TIT2"].text[0]
                        if not title:
                            title = filepath.split('/')[-1].rsplit('.')[0]

                        number = 0
                        if ' number="' in media_line_audio:
                            number = int(media_line_audio.split(' number="')[1].split('"')[0])
                        else:
                            song = ID3(filepath)
                            if 'TIT2' in song.keys():
                                number = get_number(song["TIT2"].text[0])

                            if not number and 'TRCK' in song.keys() and (song['TRCK'].text).isnumeric():
                                number = int(song['TRCK'].text)

                        if str(number) in title:
                            title = title.replace('_', ' ').split(str(number), 1)[1].split(' ', 1)[1]

                        show_lyrics = False
                        if ' show_lyrics="' in media_line_audio:
                            if media_line_audio.split(' show_lyrics="')[1].split('"')[0] == 'True':
                                show_lyrics = True

                        lyrics = media_line.split('>',1)[1].split('</media>')[0]

                        duration = 60.0
                        if ' duration="' in media_line_audio:
                            duration = float(media_line_audio.split(' duration="')[1].split('"')[0])
                        else:
                            duration = MP3(filepath).info.length

                        back_to_playlist = False
                        if ' back_to_playlist="' in media_line_audio:
                            if media_line_audio.split(' back_to_playlist="')[1].split('"')[0] == 'True':
                                back_to_playlist = True


                        icon_label = QLabel(str(number))
                        icon_label.setGeometry(0,0,50*self.screen_height_proportion,50)
                        icon_label.setAlignment(Qt.AlignCenter)
                        icon_label.setStyleSheet('QLabel {background-color:black; font-family: "Ubuntu Condensed"; font-size:28px; color:white;}')
                        preview = QPixmap(QPixmap().grabWidget(icon_label))

                        final_media += [preview, number, title, lyrics, filepath, show_lyrics, duration, back_to_playlist]
                        self.playlist.append(final_media)

                elif media_type == 'image':
                    filepath = False
                    if ' filepath="' in media_line_audio:
                        filepath = media_line.split(' filepath="')[1].split('"')[0]

                    if filepath and os.path.isfile(filepath):
                        back_to_playlist = False
                        if ' back_to_playlist="' in media_line:
                            if media_line.split(' back_to_playlist="')[1].split('"')[0] == 'True':
                                back_to_playlist = True

                        preview = QPixmap(os.path.join(path_tmp , filename_hash + '.png'))

                        final_media += [preview, filepath, back_to_playlist]
                        self.playlist.append(final_media)

                elif media_type == 'video':
                    filepath = False
                    if ' filepath="' in media_line_audio:
                        filepath = media_line.split(' filepath="')[1].split('"')[0]

                    if filepath and os.path.isfile(filepath):
                        md5 = hashlib.md5()
                        md5.update(open(filepath, 'rb').read())
                        filename_hash = md5.hexdigest()
                        video = MP4(filepath)
                        if 'covr' in  video.tags.keys():
                            artwork = video.tags["covr"][0]
                            open(os.path.join(path_tmp, filepath.split('/')[-1].rsplit('.',1)[0] + '.png'), 'wb').write(artwork)
                        else:
                            subprocess.call([ffmpeg_bin, '-loglevel', 'error', '-y', '-ss', '00:10.00', '-i', filepath, '-frames:v', '1', os.path.join(path_tmp, filepath.split('/')[-1].rsplit('.',1)[0] + '.png')], startupinfo=startupinfo)
                        create_thumbnail(os.path.join(path_tmp, filepath.split('/')[-1].rsplit('.',1)[0] + '.png'), os.path.join(path_tmp , filename_hash + '.png'), 56*self.screen_height_proportion, 56)

                        preview = QPixmap(os.path.join(path_tmp , filename_hash + '.png'))
                        image = QPixmap(os.path.join(path_tmp, filepath.split('/')[-1].rsplit('.', 1)[0] + '.png'))

                        title = ''
                        if ' title="' in media_line:
                            title = media_line.split(' title="')[1].split('"')[0]
                        elif '\xa9nam' in  video.tags.keys():
                            title = video.tags['\xa9nam'][0]
                        if not title:
                            title = filepath.split('/')[-1].rsplit('.')[0]

                        duration = 60.0
                        duration_output = subprocess.Popen([ffmpeg_bin, '-i', filepath], startupinfo=startupinfo, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT).stdout.read()
                        if 'Duration:' in duration_output:
                            duration = convert_timecode_to_seconds(duration_output.split('Duration: ')[1].split(',')[0])

                        back_to_playlist = False
                        if ' back_to_playlist="' in media_line:
                            if media_line.split(' back_to_playlist="')[1].split('"')[0] == 'True':
                                back_to_playlist = True

                        final_media += [preview, filepath, image, title, duration, back_to_playlist]
                        self.playlist.append(final_media)

                elif media_type == 'web':
                    address = ''
                    if ' address="' in media_line:
                        address = media_line.split(' address="')[1].split('"')[0]

                    if address:
                        icon_label = QLabel()
                        icon_label.setPixmap(os.path.join(path_graphics, 'web_icon_normal.png'))
                        icon_label.setGeometry(0,0,50*self.screen_height_proportion,50)
                        icon_label.setAlignment(Qt.AlignCenter)
                        icon_label.setStyleSheet('QLabel {background-color:black;}')
                        preview = QPixmap(QPixmap().grabWidget(icon_label))
                        image = QPixmap(QPixmap().grabWidget(icon_label))

                        back_to_playlist = False
                        if ' back_to_playlist="' in media_line:
                            if media_line.split(' back_to_playlist="')[1].split('"')[0] == 'True':
                                back_to_playlist = True

                        final_media += [preview, address, image, back_to_playlist]
                        self.playlist.append(final_media)

                elif media_type == 'clock':
                    icon_label = QLabel()
                    icon_label.setPixmap(os.path.join(path_graphics, 'clock_icon_normal.png'))
                    icon_label.setGeometry(0,0,50*self.screen_height_proportion,50)
                    icon_label.setAlignment(Qt.AlignCenter)
                    icon_label.setStyleSheet('QLabel {background-color:black;}')
                    preview = QPixmap(QPixmap().grabWidget(icon_label))

                    back_to_playlist = False
                    if ' back_to_playlist="' in media_line:
                        if media_line.split(' back_to_playlist="')[1].split('"')[0] == 'True':
                            back_to_playlist = True

                    final_media += [preview, back_to_playlist]
                    self.playlist.append(final_media)

                elif media_type == 'screencopy':
                    area = ''
                    if ' area="' in media_line:
                        area = media_line.split(' area="')[1].split('"')[0]

                    if area:
                        icon_label = QLabel()
                        icon_label.setPixmap(os.path.join(path_graphics, 'screencopy_icon_normal.png'))
                        icon_label.setGeometry(0,0,50*self.screen_height_proportion,50)
                        icon_label.setAlignment(Qt.AlignCenter)
                        icon_label.setStyleSheet('QLabel {background-color:black;}')
                        preview = QPixmap(QPixmap().grabWidget(icon_label))

                        back_to_playlist = False
                        if ' back_to_playlist="' in media_line:
                            if media_line.split(' back_to_playlist="')[1].split('"')[0] == 'True':
                                back_to_playlist = True

                        final_media += [preview, area, back_to_playlist]
                        self.playlist.append(final_media)

                elif media_type == 'camera':
                    icon_label = QLabel()
                    icon_label.setPixmap(os.path.join(path_graphics, 'camera_icon_normal.png'))
                    icon_label.setGeometry(0,0,50*self.screen_height_proportion,50)
                    icon_label.setAlignment(Qt.AlignCenter)
                    icon_label.setStyleSheet('QLabel {background-color:black;}')
                    preview = QPixmap(QPixmap().grabWidget(icon_label))

                    mediaplayer =  video_instance.media_player_new()

                    back_to_playlist = False
                    if ' back_to_playlist="' in media_line:
                        if media_line.split(' back_to_playlist="')[1].split('"')[0] == 'True':
                            back_to_playlist = True

                    final_media += [preview, mediaplayer, back_to_playlist]
                    self.playlist.append(final_media)

    self.populate_playlist()

def main_options_file_save_button_clicked(self):
    final_playlist_file = u'<?xml version="1.0" encoding="UTF-8"?>'
    final_playlist_file += '<playlist>'

    for media in self.playlist:
        final_playlist_file += '<media type="' + media[0] + '"'

        if media[0] == 'audio':
            final_playlist_file += ' number="' + str(media[2]) + '"'
            final_playlist_file += ' title="' + media[3] + '"'
            final_playlist_file += ' filepath="' + media[5] + '"'
            final_playlist_file += ' show_lyrics="' + str(media[6]) + '"'
            final_playlist_file += ' duration="' + str(media[7]) + '"'
            final_playlist_file += ' back_to_playlist="' + str(media[-1]) + '">'
            final_playlist_file += media[4] + '</media>'

        elif media[0] == 'image':
            final_playlist_file += ' filepath="' + media[2] + '"'
            final_playlist_file += ' back_to_playlist="' + str(media[-1]) + '">'
            final_playlist_file += '</media>'

        elif media[0] == 'video':
            final_playlist_file += ' filepath="' + media[2] + '"'
            final_playlist_file += ' title="' + media[4] + '">'
            final_playlist_file += ' duration="' + str(media[5]) + '">'
            final_playlist_file += ' back_to_playlist="' + str(media[-1]) + '">'
            final_playlist_file += '</media>'

        elif media[0] == 'web':
            final_playlist_file += ' address="' + media[2] + '"'
            final_playlist_file += ' back_to_playlist="' + str(media[-1]) + '">'
            final_playlist_file += '</media>'

        elif media[0] == 'clock':
            final_playlist_file += ' back_to_playlist="' + str(media[-1]) + '">'
            final_playlist_file += '</media>'

        elif media[0] == 'screencopy':
            final_playlist_file += ' area="' + str(media[2]) + '">'
            final_playlist_file += ' back_to_playlist="' + str(media[-1]) + '">'
            final_playlist_file += '</media>'

        elif media[0] == 'camera':
            final_playlist_file += ' back_to_playlist="' + str(media[-1]) + '">'
            final_playlist_file += '</media>'

    final_playlist_file += '</playlist>'

    if not self.actual_playlist_file:
        self.actual_playlist_file = QFileDialog.getSaveFileName(self, 'Selecione um nome de arquivo para a playlist', path_home, 'Playlists do Kihvim (*.kihvim)', options=self.dialog_options)[0]

    if not self.actual_playlist_file.endswith('.kihvim'):
        self.actual_playlist_file += 'kihvim'

    codecs.open(self.actual_playlist_file, 'w', 'utf-8').write(final_playlist_file)

def main_options_settings_enable_animations_clicked(self):
    self.settings['enable_animations'] = self.main_options_settings_enable_animations.isChecked()

def main_options_settings_framerate_changing(self):
    if self.main_options_settings_framerate.value() > 1 :
        self.main_options_settings_framerate_label.setText(u'<small>TAXA DE ATUALIZAÇÃO</small><br>' + str(self.main_options_settings_framerate.value()) + u' vezes a cada segundo')
    else:
        self.main_options_settings_framerate_label.setText(u'<small>TAXA DE ATUALIZAÇÃO</small><br>' + str(self.main_options_settings_framerate.value()) + u' vez a cada segundo')

def main_options_settings_language_activated(self):
    self.settings['language_mnemonic'] = language_list[self.main_options_settings_language.currentText()]

def main_options_settings_framerate_changed(self):
    main_options_settings_framerate_changing(self)
    self.settings['interface_framerate'] = self.main_options_settings_framerate.value()
    self.timer.setInterval(1000/self.settings['interface_framerate'])

def main_options_export_button_clicked(self):
    media_number = 0
    final_list = []

    final_command = ['ffmpeg', '-y']
    width = int(self.main_options_export_profile.currentText().split('x')[0])
    height = int(self.main_options_export_profile.currentText().split('x')[1].split(' ')[0])

    background = QLabel()
    background.setGeometry(0,0,width,height)
    background.setStyleSheet('QLabel {background-color:black;}')

    artwork = QLabel(u'⊡', parent=background)
    artwork.setGeometry(width*.45,height*.45,width*.1, height*.1)
    artwork.setStyleSheet('QLabel {font-family: "Ubuntu"; font-size:' + str(height*.04) + 'pt; color:white; qproperty-alignment: "AlignCenter";}')

    preview = QPixmap(QPixmap().grabWidget(background))
    preview.save(os.path.join(path_tmp , 'testmedia.png'))

    final_command.append('-loop')
    final_command.append('1')
    final_command.append('-i')
    final_command.append(os.path.join(path_tmp , 'testmedia.png'))
    final_command.append('-i')
    final_command.append(os.path.join(path_kihvim, 'resources',  'test_song.flac'))
    #final_command.append('-shortest')

    final_command.append('-vf')
    final_command.append('fade=out:0:30')

    final_command.append('-c:v')
    final_command.append('libx264')
    final_command.append('-c:a')
    final_command.append('copy')
    final_command.append('-preset')
    final_command.append('ultrafast')
    final_command.append('-qp')
    final_command.append('0')
    final_command.append('-t')
    final_command.append('2')
    final_command.append(os.path.join(path_tmp , 'media' + str('%03d'% media_number) + '.mkv'))
    subprocess.call(final_command)

    final_list.append(os.path.join(path_tmp , 'media' + str('%03d'% media_number) + '.mkv'))

    media_number += 1

    for media in self.playlist:
        final_command = ['ffmpeg', '-y']
        width = int(self.main_options_export_profile.currentText().split('x')[0])
        height = int(self.main_options_export_profile.currentText().split('x')[1].split(' ')[0])

        if media[0] == 'audio':

            background = QLabel()
            background.setGeometry(0,0,width,height)
            background.setStyleSheet('QLabel {background-color:black;}')

            artwork = QLabel(str(str(media[2])), parent=background)
            artwork.setGeometry(background.width()*.1,(background.height()*.5)-(background.height()*.1),background.height()*.2, background.height()*.2)
            artwork.setAlignment(Qt.AlignCenter)
            artwork.setStyleSheet('QLabel {background-color: #fff; font-family: "Ubuntu Condensed"; font-size:' + str(background.height()*.09) + 'pt; font-weight:bold; color:black;}')

            title = QLabel(media[3],parent=background)
            title.setAlignment(Qt.AlignVCenter | Qt.AlignLeft)
            title.setWordWrap(True)
            title.setGeometry((background.width()*.11) + (background.width()*.2),0,background.width() - ((background.width()*.21) + (background.width()*.2)), background.height())
            title.setStyleSheet('QLabel {background-color:none; font-family: "Ubuntu"; font-size:' + str(background.height()*.05) + 'pt; font-weight:bold; color:white}')

            preview = QPixmap(QPixmap().grabWidget(background))
            preview.save(os.path.join(path_tmp , 'media' + str('%03d'% media_number) + '.png'))

            final_command.append('-loop')
            final_command.append('1')
            final_command.append('-i')
            final_command.append(os.path.join(path_tmp , 'media' + str('%03d'% media_number) + '.png'))
            final_command.append('-i')
            final_command.append(media[5])
            #final_command.append('-shortest')
            final_command.append('-t')
            final_command.append(str(media[7]))
            final_command.append('-vf')
            final_command.append('fade=in:0:30')
            #final_command.append('-vf')
            #final_command.append('fade=out:' + str(int((media[7] - 1)*25)) + ':' + str(int((media[7])*25)))

            final_command.append('-c:v')
            final_command.append('libx264')
            final_command.append('-c:a')
            final_command.append('copy')
            final_command.append('-preset')
            final_command.append('ultrafast')

            final_command.append(os.path.join(path_tmp , 'media' + str('%03d'% media_number) + '.mkv'))

            final_list.append(os.path.join(path_tmp , 'media' + str('%03d'% media_number) + '.mkv'))

            subprocess.call(final_command)

        elif media[0] == 'image':
            create_thumbnail(media[2], os.path.join(path_tmp , 'media' + str('%03d'% media_number) + '.png'), width, height)

            final_command.append('-loop')
            final_command.append('1')
            final_command.append('-i')
            final_command.append(os.path.join(path_tmp , 'media' + str('%03d'% media_number) + '.png'))
            final_command.append('-i')
            final_command.append(os.path.join(path_kihvim, 'resources', 'silence.flac'))

            final_command.append('-c:v')
            final_command.append('copy')
            final_command.append('-c:a')
            final_command.append('copy')
            final_command.append('-t')
            final_command.append('5')
            final_command.append(os.path.join(path_tmp , 'media' + str('%03d'% media_number) + '.mkv'))

            final_list.append(os.path.join(path_tmp , 'media' + str('%03d'% media_number) + '.mkv'))

            subprocess.call(final_command)

        elif media[0] == 'video':
            final_command.append('-i')
            final_command.append(media[2])

            final_command.append('-c:v')
            final_command.append('copy')
            final_command.append('-c:a')
            final_command.append('copy')
            final_command.append(os.path.join(path_tmp , 'media' + str('%03d'% media_number) + '.mkv'))

            final_list.append(os.path.join(path_tmp , 'media' + str('%03d'% media_number) + '.mkv'))

            subprocess.call(final_command)

        elif media[0] == 'web':
            pass#final_playlist_file += ' address="' + media[2] + '"'
            #final_playlist_file += ' back_to_playlist="' + str(media[-1]) + '">'
            #final_playlist_file += '</media>'

        elif media[0] == 'clock':
            pass#final_playlist_file += ' back_to_playlist="' + str(media[-1]) + '">'
            #final_playlist_file += '</media>'

        elif media[0] == 'screencopy':
            pass#final_playlist_file += ' area="' + str(media[2]) + '">'
            #final_playlist_file += ' back_to_playlist="' + str(media[-1]) + '">'
            #final_playlist_file += '</media>'

        elif media[0] == 'camera':
            pass#final_playlist_file += ' back_to_playlist="' + str(media[-1]) + '">'
            #final_playlist_file += '</media>'

        #final_command.append('-c')
        #final_command.append('copy')
        #final_command.append('-bsf:v')
        #final_command.append('h264_mp4toannexb')
        #final_command.append('-f')
        #final_command.append('mpegts')
        #final_command.append(os.path.join(path_tmp , 'media' + str('%03d'% media_number) + '.ts'))

        #final_command.append('-c:v')
        #final_command.append('libx264')
        #final_command.append('-c:a')
        #final_command.append('aac')
        #final_command.append('-preset')
        #final_command.append('ultrafast')
        #final_command.append('-qp')
        #final_command.append('0')
        #final_command.append(os.path.join(path_tmp , 'media' + str('%03d'% media_number) + '.mkv'))#

        #final_list.append(os.path.join(path_tmp , 'media' + str('%03d'% media_number) + '.mkv'))

        #subprocess.call(final_command)

        media_number += 1


    concat_command = ['ffmpeg']
    concat_command.append('-y')
    for video in final_list:
        concat_command.append('-i')
        concat_command.append(video)
    concat_command.append('-filter_complex')
    filter_complex = ''
    for i in range(media_number):
        filter_complex += '[' + str(i) + ':v:0] [' + str(i) + ':a:0] '
    filter_complex += 'concat=n=' + str(media_number) + ':v=1:a=1 [v] [a]'
    concat_command.append(filter_complex)
    concat_command.append('-map')
    concat_command.append('[v]')
    concat_command.append('-map')
    concat_command.append('[a]')
    concat_command.append('-c:a')
    concat_command.append('aac')
    concat_command.append('-c:v')
    concat_command.append('libx264')
    concat_command.append('-pix_fmt')
    concat_command.append('yuv420p')
    concat_command.append(os.path.join(path_home, 'Playlist final do Kihvim.mp4'))

    subprocess.call(concat_command)


    '''if self.main_options_export_profile.currentText() == '320x180 (iPod/iPhone)':
        final_command.append('-c:a')
        final_command.append('aac')
        final_command.append('-b:a')
        final_command.append('128kb')
        final_command.append('-vcodec')
        final_command.append('mpeg4')
        final_command.append('-b:v')
        final_command.append('1200kb')
        final_command.append('-mbd')
        final_command.append('2')
        final_command.append('-flags')
        final_command.append('+trell+aic')
        final_command.append('-cmp')
        final_command.append('2')
        final_command.append('-subcmp')
        final_command.append('2')
        final_command.append('-s')
        final_command.append('320x180')
        final_command.append(os.path.join(path_tmp , 'media' + str('%03d'% media_number) + '.mp4'))
        final_list += "file '" + 'media' + str('%03d'% media_number) + '.mp4' + "'\n"

    elif self.main_options_export_profile.currentText() == '320x240 (PSP)':
        final_command.append('-b:v')
        final_command.append('300k')
        final_command.append('-s')
        final_command.append('320x240')
        final_command.append('-c:v')
        final_command.append('libxvid')
        final_command.append('-b:a')
        final_command.append('32k')
        final_command.append('-ar')
        final_command.append('24000')
        final_command.append('-acodec')
        final_command.append('aac')
        final_command.append(os.path.join(path_tmp , 'media' + str('%03d'% media_number) + '.mp4'))
        final_list += "file '" + 'media' + str('%03d'% media_number) + '.mp4' + "'\n"

    elif self.main_options_export_profile.currentText() == '720x480 (DVD player)':
        final_command.append('-target')
        final_command.append('ntsc-dvd')
        final_command.append('-ps')
        final_command.append('2000000000')
        final_command.append('-aspect')
        final_command.append('16:9')
        final_command.append(os.path.join(path_tmp , 'media' + str('%03d'% media_number) + '.mpeg'))
        final_list += "file '" + 'media' + str('%03d'% media_number) + '.mpeg' + "'\n"

    elif self.main_options_export_profile.currentText() == '1280x720 (SmartTV)':
        final_command.append('-s')
        final_command.append('hd720')
        final_command.append('-f')
        final_command.append('avi')
        final_command.append('-c:v')
        final_command.append('libx264')
        final_command.append('-crf')
        final_command.append('22')
        final_command.append('-b:v')
        final_command.append('1152k')
        final_command.append('-c:a')
        final_command.append('libmp3lame')
        final_command.append('-b:a')
        final_command.append('128k')
        final_command.append('-ac')
        final_command.append('2')
        final_command.append('-ar')
        final_command.append('44100')
        final_command.append('-pix_fmt')
        final_command.append('yuv420p')
        final_command.append(os.path.join(path_tmp , 'media' + str('%03d'% media_number) + '.mp4'))
        final_list += "file '" + 'media' + str('%03d'% media_number) + '.mp4' + "'\n"

    elif self.main_options_export_profile.currentText() == '704x480 (SmartTV)':
        final_command.append('-s')
        final_command.append('704x480')
        final_command.append('-f')
        final_command.append('avi')
        final_command.append('-c:v')
        final_command.append('libx264')
        final_command.append('-crf')
        final_command.append('22')
        final_command.append('-b:v')
        final_command.append('1152k')
        final_command.append('-c:a')
        final_command.append('libmp3lame')
        final_command.append('-b:a')
        final_command.append('128k')
        final_command.append('-ac')
        final_command.append('2')
        final_command.append('-ar')
        final_command.append('44100')
        final_command.append('-pix_fmt')
        final_command.append('yuv420p')
        final_command.append(os.path.join(path_tmp , 'media' + str('%03d'% media_number) + '.mp4'))
        final_list += "file '" + 'media' + str('%03d'% media_number) + '.mp4' + "'\n"

    final_command.append('-vf')
    final_command.append('setpts=PTS-STARTPTS, asetpts=PTS-STARTPTS')
    #final_command.append('-avoid_negative_ts')
    #final_command.append('make_zero')
    #final_command.append('-fflags')
    #final_command.append('+genpts')'''
