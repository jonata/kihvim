#!/usr/bin/python3

from PySide2.QtWidgets import QLabel, QGraphicsOpacityEffect
from PySide2.QtCore import Qt, QPropertyAnimation
from PySide2.QtGui import QPixmap

from modules.paths import *
from modules.effect import generate_effect


def load(self):
    self.image_widget = QLabel(parent=self.monitor_window)
    self.image_widget.setGeometry(0, 0, self.monitor_window.width(), self.monitor_window.height())
    self.image_widget.setAlignment(Qt.AlignCenter)

    self.image_widget_opacity = QGraphicsOpacityEffect()
    self.image_widget.setGraphicsEffect(self.image_widget_opacity)
    self.image_widget_opacity_animation = QPropertyAnimation(self.image_widget_opacity, b'opacity')
    self.image_widget_opacity.setOpacity(0.0)


def hide(self):
    if not self.monitor.current_media or not self.monitor.current_media[0] == 'image':
        generate_effect(self.image_widget_opacity_animation, 'opacity', 1000, self.image_widget_opacity.opacity(), 0.0)


def show(self):
    self.image_widget.setPixmap(QPixmap(self.monitor.current_media[2]).scaled(self.image_widget.width(), self.image_widget.height(), Qt.KeepAspectRatio, Qt.SmoothTransformation))
    generate_effect(self.image_widget_opacity_animation, 'opacity', 1000, self.image_widget_opacity.opacity(), 1.0)
