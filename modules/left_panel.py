import os
from PySide2.QtWidgets import QLabel, QWidget, QPushButton, QCheckBox, QComboBox, QFileDialog, QListWidget, QListView, QLineEdit, QPlainTextEdit, QInputDialog, QTabWidget, QListWidgetItem
from PySide2.QtCore import Qt, QPropertyAnimation, QEasingCurve, QSize
from PySide2.QtGui import QIcon, QPixmap
# from PySide2.QtWebEngineWidgets import QWebEngineView, QWebEnginePage, QWebEngineSettings

from modules.paths import *
from modules.effect import generate_effect
from modules.images import create_thumbnail
from modules import utils
from modules import playlist_panel

from mutagen.mp4 import MP4

#import vlc
#video_instance = vlc.Instance()

import hashlib


def load(self):
    self.left_panel_widget = QLabel(parent=self)
    self.left_panel_widget.setAttribute(Qt.WA_OpaquePaintEvent)
    # self.left_panel_widget.viewport().setAttribute(Qt.WA_OpaquePaintEvent)
    # .setAttribute(Qt.WA_OpaquePaintEvent)
    # self.left_panel_widget.setAttribute(Qt.WA_NoSystemBackground)
    self.left_panel_widget.setObjectName('left_panel')
    self.left_panel_widget.setStyleSheet('#left_panel { border-top: 75px; border-right: 60px; border-image: url("' + os.path.join(path_graphics, "left_panel_background.png").replace('\\', '/') + '") 75 60 0 0 stretch stretch; }')
    self.left_panel_geometry_animation = QPropertyAnimation(self.left_panel_widget, b'geometry')
    self.left_panel_geometry_animation.setEasingCurve(QEasingCurve.OutCirc)

    self.left_panel_add_title = QLabel(parent=self.left_panel_widget)
    self.left_panel_add_title.setObjectName('left_panel_add_title')
    self.left_panel_add_title.setStyleSheet('#left_panel_add_title { padding:5px; font-family: "Ubuntu"; font-size:12pt; font-weight:bold; color:white; qproperty-alignment: "AlignRight | AlignVCenter"; border-top: 2px; border-bottom: 2px;  border-image: url("' + os.path.join(path_graphics, "add_title_background.png").replace('\\', '/') + '") 2 0 2 0 stretch stretch; }')

    self.left_panel_addbutton = QPushButton(self)
    self.left_panel_addbutton.setStyleSheet('QPushButton { font-family: "Ubuntu"; font-size:12pt; font-weight:bold; color:white; border-right: 12px; border-image: url("' + os.path.join(path_graphics, "add_button_normal.png").replace('\\', '/') + '") 0 12 0 0 stretch stretch; outline: none; } QPushButton:hover:pressed {  border-right: 12px; border-image: url("' + os.path.join(path_graphics, "add_button_pressed.png").replace('\\', '/') + '") 0 12 0 0 stretch stretch; outline: none; } QPushButton:hover { border-right: 12px; border-image: url("' + os.path.join(path_graphics, "add_button_hover.png").replace('\\', '/') + '") 0 12 0 0 stretch stretch; outline: none; }')
    self.left_panel_addbutton_geometry_animation = QPropertyAnimation(self.left_panel_addbutton, b'geometry')
    self.left_panel_addbutton_geometry_animation.setEasingCurve(QEasingCurve.OutCirc)
    self.left_panel_addbutton.clicked.connect(lambda:left_panel_addbutton_clicked(self))

    self.left_panel_addbutton_label = QLabel(self.left_panel_addbutton)
    self.left_panel_addbutton_label.setStyleSheet('QLabel { padding:5px; font-family: "Ubuntu"; color:white; qproperty-alignment: "AlignLeft | AlignVCenter"; }')
    self.left_panel_addbutton_label.setAttribute(Qt.WA_TransparentForMouseEvents)

    self.left_panel_icons = QWidget(parent=self.left_panel_widget)

    #######################################################################
    ## ADDITION PANEL - AUDIO

    class left_panel_audio_icon(QLabel):
        # def mouseReleaseEvent(widget, event):
        #    widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'music_icon_hover.png').replace('\\', '/') + '); } ')
        def mousePressEvent(widget, event):
            if not self.selected_addition == 'audio':
                self.selected_addition = 'audio'
                show_additions(self)
            else:
                self.selected_addition = False
                hide_additions(self)
            widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'music_icon_pressed.png').replace('\\', '/') + '); } ')

        def enterEvent(widget, event):
            if not self.selected_addition == 'audio':
                widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'music_icon_hover.png').replace('\\', '/') + '); } ')

        def leaveEvent(widget, event):
            if not self.selected_addition == 'audio':
                widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'music_icon_normal.png').replace('\\', '/') + '); } ')

    self.left_panel_audio_icon = left_panel_audio_icon(parent=self.left_panel_icons)
    self.left_panel_audio_icon.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'music_icon_normal.png').replace('\\', '/') + '); } ')

    self.left_panel_audio_panel = QWidget(self.left_panel_widget)

    self.left_panel_audio_alert = QLabel(u'Parece que não há arquivos de cânticos importados no kihvim. Adicione os cânticos usando o botão abaixo.',parent=self.left_panel_audio_panel)
    self.left_panel_audio_alert.setWordWrap(True)
    self.left_panel_audio_alert.setStyleSheet('QLabel {padding: 50px; font-family: "Ubuntu"; font-size:12pt; color:gray; qproperty-alignment:"AlignCenter" ; }')

    self.left_panel_audio_list = QListWidget(parent=self.left_panel_audio_panel)
    self.left_panel_audio_list.setViewMode(QListView.IconMode)
    self.left_panel_audio_list.setIconSize(QSize(48, 48))
    self.left_panel_audio_list.setSpacing(5)
    self.left_panel_audio_list.currentItemChanged.connect(lambda:left_panel_audio_list_clicked(self))

    self.left_panel_audio_download_lyrics = QPushButton(parent=self.left_panel_audio_panel)
    self.left_panel_audio_download_lyrics.setIconSize(QSize(18, 18))
    self.left_panel_audio_download_lyrics.setIcon(QIcon(os.path.join(path_graphics, "get_web_icon.png")))
    self.left_panel_audio_download_lyrics.setStyleSheet('QPushButton { color:white; font-size:12px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
    self.left_panel_audio_download_lyrics.clicked.connect(lambda:left_panel_audio_download_lyrics_clicked(self))

    self.left_panel_audio_add = QPushButton(parent=self.left_panel_audio_panel)
    self.left_panel_audio_add.setIconSize(QSize(18, 18))
    self.left_panel_audio_add.setIcon(QIcon(os.path.join(path_graphics, "add_icon.png")))
    self.left_panel_audio_add.setStyleSheet('QPushButton { color:white; font-size:12px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
    self.left_panel_audio_add.clicked.connect(lambda:left_panel_audio_add_clicked(self))

    self.left_panel_audio_edit = QPushButton(parent=self.left_panel_audio_panel)
    self.left_panel_audio_edit.setCheckable(True)
    self.left_panel_audio_edit.setIconSize(QSize(18, 18))
    self.left_panel_audio_edit.setIcon(QIcon(os.path.join(path_graphics, "edit_icon.png")))
    self.left_panel_audio_edit.setStyleSheet('QPushButton { color:white; font-size:12px; border-top: 5px; border-right: 0px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 0px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 0px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:checked { border-top: 5px; border-right: 0px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
    self.left_panel_audio_edit.clicked.connect(lambda:left_panel_audio_edit_clicked(self))

    self.left_panel_audio_remove = QPushButton(parent=self.left_panel_audio_panel)
    self.left_panel_audio_remove.setIconSize(QSize(18, 18))
    self.left_panel_audio_remove.setIcon(QIcon(os.path.join(path_graphics, "remove_icon.png")))
    self.left_panel_audio_remove.setStyleSheet('QPushButton { color:white; font-size:12px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 0px; border-image: url("' + os.path.join(path_graphics, "button_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 0px; border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 0px; border-image: url("' + os.path.join(path_graphics, "button_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
    self.left_panel_audio_remove.clicked.connect(lambda:left_panel_audio_remove_clicked(self))

    self.left_panel_audio_edit_panel = QWidget(parent=self.left_panel_audio_panel)

    self.left_panel_audio_edit_number_label = QLabel(u'NÚMERO',parent=self.left_panel_audio_edit_panel)
    self.left_panel_audio_edit_number_label.setStyleSheet('QLabel {font-family: "Ubuntu"; font-size:10px; color:black; qproperty-alignment:"AlignLeft" ; }')

    self.left_panel_audio_edit_number = QLabel(parent=self.left_panel_audio_edit_panel)
    self.left_panel_audio_edit_number.setStyleSheet('QLabel {font-family: "Ubuntu"; font-weight:bold;  font-size:20px; color:black; qproperty-alignment:"AlignLeft" ; }')

    self.left_panel_audio_edit_title_label = QLabel(u'TÍTULO',parent=self.left_panel_audio_edit_panel)
    self.left_panel_audio_edit_title_label.setStyleSheet('QLabel {font-family: "Ubuntu"; font-size:10px; color:black; qproperty-alignment:"AlignLeft" ; }')

    self.left_panel_audio_edit_title = QLineEdit(parent=self.left_panel_audio_edit_panel)

    self.left_panel_audio_edit_filepath_label = QLabel(u'CAMINHO DO ARQUIVO',parent=self.left_panel_audio_edit_panel)
    self.left_panel_audio_edit_filepath_label.setStyleSheet('QLabel {font-family: "Ubuntu"; font-size:10px; color:black; qproperty-alignment:"AlignLeft" ; }')

    self.left_panel_audio_edit_filepath = QLabel(parent=self.left_panel_audio_edit_panel)
    self.left_panel_audio_edit_filepath.setStyleSheet('QLabel {font-family: "Ubuntu"; font-size:20px; color:black; qproperty-alignment:"AlignLeft" ; }')

    self.left_panel_audio_edit_lyrics_label = QLabel(u'LETRA',parent=self.left_panel_audio_edit_panel)
    self.left_panel_audio_edit_lyrics_label.setStyleSheet('QLabel {font-family: "Ubuntu"; font-size:10px; color:black; qproperty-alignment:"AlignLeft" ; }')

    class left_panel_audio_edit_lyrics(QPlainTextEdit):
        def enterEvent(widget, event):
            self.left_panel_audio_edit_lyrics_get_from_web.setVisible(True)
        def leaveEvent(widget, event):
            self.left_panel_audio_edit_lyrics_get_from_web.setVisible(False)

    self.left_panel_audio_edit_lyrics = left_panel_audio_edit_lyrics(parent=self.left_panel_audio_edit_panel)
    self.left_panel_audio_edit_lyrics.setStyleSheet('QLabel {font-family: "Ubuntu"; font-size:12px; color:black; qproperty-alignment:"AlignLeft" ; }')

    self.left_panel_audio_edit_lyrics_get_from_web = QPushButton(parent=self.left_panel_audio_edit_lyrics)
    self.left_panel_audio_edit_lyrics_get_from_web.setIconSize(QSize(18, 18))
    self.left_panel_audio_edit_lyrics_get_from_web.setIcon(QIcon(os.path.join(path_graphics, "get_web_icon.png")))
    self.left_panel_audio_edit_lyrics_get_from_web.setStyleSheet('QPushButton { color:white; font-size:12px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
    self.left_panel_audio_edit_lyrics_get_from_web.clicked.connect(lambda:left_panel_audio_edit_lyrics_get_from_web_clicked(self))
    self.left_panel_audio_edit_lyrics_get_from_web.setVisible(False)


    #######################################################################
    ## ADDITION PANEL - IMAGE

    class left_panel_image_icon(QLabel):
        #def mouseReleaseEvent(widget, event):
        #    widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'image_icon_hover.png').replace('\\', '/') + '); } ')
        def mousePressEvent(widget, event):
            if not self.selected_addition == 'image':
                self.selected_addition = 'image'
                show_additions(self)
            else:
                self.selected_addition = False
                hide_additions(self)
            widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'image_icon_pressed.png').replace('\\', '/') + '); } ')
        def enterEvent(widget, event):
            if not self.selected_addition == 'image':
                widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'image_icon_hover.png').replace('\\', '/') + '); } ')
        def leaveEvent(widget, event):
            if not self.selected_addition == 'image':
                widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'image_icon_normal.png').replace('\\', '/') + '); } ')

    self.left_panel_image_icon = left_panel_image_icon(parent=self.left_panel_icons)
    self.left_panel_image_icon.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'image_icon_normal.png').replace('\\', '/') + '); } ')

    self.left_panel_image_panel = QWidget(self.left_panel_widget)

    self.left_panel_image_tab = QTabWidget(parent=self.left_panel_image_panel)
    self.left_panel_image_tab.setTabPosition(QTabWidget.North)
    self.left_panel_image_tab.currentChanged.connect(lambda:left_panel_image_tab_changed(self))

    self.left_panel_image_tab_local = QWidget(parent=self)

    self.left_panel_image_alert = QLabel(u'Parece que não há arquivos de imagens disponíveis na pasta selecionada. Adicione imagens usando o botão abaixo.',parent=self.left_panel_image_tab_local)
    self.left_panel_image_alert.setWordWrap(True)
    self.left_panel_image_alert.setStyleSheet('QLabel {padding: 50px; font-family: "Ubuntu"; font-size:12pt; color:gray; qproperty-alignment:"AlignCenter" ; }')

    self.left_panel_image_list = QListWidget(parent=self.left_panel_image_tab_local)
    self.left_panel_image_list.setIconSize(QSize(56*self.screen_height_proportion, 56))
    self.left_panel_image_list.setSpacing(5)
    self.left_panel_image_list.currentItemChanged.connect(lambda:left_panel_image_list_clicked(self))

    self.left_panel_image_add = QPushButton(parent=self.left_panel_image_tab_local)
    self.left_panel_image_add.setIconSize(QSize(18, 18))
    self.left_panel_image_add.setIcon(QIcon(os.path.join(path_graphics, "add_icon.png")))
    self.left_panel_image_add.setStyleSheet('QPushButton { color:white; font-size:12px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
    self.left_panel_image_add.clicked.connect(lambda:left_panel_image_add_clicked(self))

    class left_panel_image_path_label(QLabel):
        def enterEvent(widget, event):
            self.left_panel_image_edit.setVisible(True)
        def leaveEvent(widget, event):
            self.left_panel_image_edit.setVisible(False)

    self.left_panel_image_path_label = left_panel_image_path_label(parent=self.left_panel_image_tab_local)
    self.left_panel_image_path_label.setAlignment(Qt.AlignVCenter | Qt.AlignRight)

    self.left_panel_image_edit = QPushButton(parent=self.left_panel_image_path_label)
    self.left_panel_image_edit.setIconSize(QSize(18, 18))
    self.left_panel_image_edit.setIcon(QIcon(os.path.join(path_graphics, "edit_icon.png")))
    self.left_panel_image_edit.setStyleSheet('QPushButton { color:white; font-size:12px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:checked { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
    self.left_panel_image_edit.clicked.connect(lambda:left_panel_image_edit_clicked(self))

    self.left_panel_image_remove = QPushButton(parent=self.left_panel_image_tab_local)
    self.left_panel_image_remove.setIconSize(QSize(18, 18))
    self.left_panel_image_remove.setIcon(QIcon(os.path.join(path_graphics, "remove_icon.png")))
    self.left_panel_image_remove.setStyleSheet('QPushButton { color:white; font-size:12px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
    self.left_panel_image_remove.clicked.connect(lambda:left_panel_image_remove_clicked(self))

    self.left_panel_image_tab.blockSignals(True)
    self.left_panel_image_tab.addTab(self.left_panel_image_tab_local, 'DO COMPUTADOR')
    self.left_panel_image_tab.blockSignals(False)

    self.left_panel_image_tab_web = QWidget(parent=self)

    self.left_panel_image_keyword = QLineEdit(parent=self.left_panel_image_tab_web)
    self.left_panel_image_keyword.setText('')
    self.left_panel_image_keyword.setStyleSheet('QLabel {font-family: "Ubuntu"; font-weight:bold;  font-size:20px; color:black; qproperty-alignment:"AlignLeft" ; }')
    self.left_panel_image_keyword.textEdited.connect(lambda:left_panel_image_keyword_changed(self))

    self.left_panel_image_keyword_go = QPushButton(u'PESQUISAR', parent=self.left_panel_image_tab_web)
    self.left_panel_image_keyword_go.setStyleSheet('QPushButton { color:white; font-size:12px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 0px; border-image: url("' + os.path.join(path_graphics, "button_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 0px; border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 0px; border-image: url("' + os.path.join(path_graphics, "button_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
    self.left_panel_image_keyword_go.clicked.connect(lambda:left_panel_image_keyword_go_clicked(self))

    #self.left_panel_image_webview = QWebEngineView(parent=self.left_panel_image_tab_web)
    self.left_panel_image_webview = QWidget(parent=self.left_panel_image_tab_web)

    #class LeftWebView(QWebEngineView):
    #    def createWindow(widget, windowType):

            #if windowType == QWebEnginePage.WebBrowserTab:
            #    self = LeftWebView()
            #    print('2')
            #    self.setAttribute(Qt.WA_DeleteOnClose, True)
            #    print('3')
            #    self.show()
            #    print('4')
            #    #return self.LeftWebView
    #        return widget
            #    print('5')
            #return super(LeftWebView, self).createWindow(windowType)
            #print('6')
            #return widget#self.left_panel_image_keyword_load_finished(widget.url())

    #self.left_panel_image_webview = LeftWebView(parent=self.left_panel_image_tab_web)




    #self.left_panel_image_webview.settings().setAttribute(QWebEngineSettings.JavascriptEnabled, True)
    #self.left_panel_image_webview.settings().setAttribute(QWebEngineSettings.JavascriptCanOpenWindows, True)
    #self.left_panel_image_webview.page().setLinkDelegationPolicy(QWebEnginePage.DelegateAllLinks)
    #ui->webView->settings()->setAttribute(QWebSettings::JavascriptCanOpenWindows, true);
    #self.left_panel_image_webview.loadFinished.connect(lambda:left_panel_image_keyword_load_finished(self))
    #self.left_panel_image_webview.linkClicked.connect(lambda:left_panel_image_keyword_load_finished(self))

    self.left_panel_image_webview_warning = QLabel(u'Faça uma pesquisa de imagens usando o campo acima. Ao encontrar a imagem desejada, clique no link que abrirá a imagem (geralmente algo como "Visualizar arquivo").',parent=self.left_panel_image_tab_web)
    self.left_panel_image_webview_warning.setWordWrap(True)
    self.left_panel_image_webview_warning.setStyleSheet('QLabel {padding: 50px; font-family: "Ubuntu"; font-size:12pt; color:gray; qproperty-alignment:"AlignCenter" ; }')

    class left_panel_image_webview_preview(QLabel):
        def enterEvent(widget, event):
            self.left_panel_image_webview_preview_cancel_button.setVisible(True)
        def leaveEvent(widget, event):
            self.left_panel_image_webview_preview_cancel_button.setVisible(False)

    self.left_panel_image_webview_preview = left_panel_image_webview_preview(parent=self.left_panel_image_tab_web)
    self.left_panel_image_webview_preview.setWordWrap(True)
    self.left_panel_image_webview_preview.setObjectName('left_panel_image_webview_preview')
    self.left_panel_image_webview_preview.setStyleSheet('#left_panel_image_webview_preview { border-top: 4px; border-right: 4px; border-bottom: 4px; border-left: 4px; border-image: url("' + os.path.join(path_graphics, "black_box.png").replace('\\', '/') + '") 4 4 4 4 stretch stretch; }')

    self.left_panel_image_webview_preview_cancel_button = QPushButton(u'CANCELAR', parent=self.left_panel_image_webview_preview)
    self.left_panel_image_webview_preview_cancel_button.setStyleSheet('QPushButton { color:white; font-size:12px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:checked { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
    self.left_panel_image_webview_preview_cancel_button.clicked.connect(lambda:left_panel_image_webview_preview_cancel_button_clicked(self))

    self.left_panel_image_tab.blockSignals(True)
    #self.left_panel_image_tab.addTab(self.left_panel_image_tab_web, 'DO JW.ORG')
    self.left_panel_image_tab.blockSignals(False)


    #######################################################################
    ## ADDITION PANEL - VIDEO

    class left_panel_video_icon(QLabel):
        #def mouseReleaseEvent(widget, event):
        #    widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'video_icon_hover.png').replace('\\', '/') + '); } ')
        def mousePressEvent(widget, event):
            if not self.selected_addition == 'video':
                self.selected_addition = 'video'
                show_additions(self)
            else:
                self.selected_addition = False
                hide_additions(self)
            widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'video_icon_pressed.png').replace('\\', '/') + '); } ')
        def enterEvent(widget, event):
            if not self.selected_addition == 'video':
                widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'video_icon_hover.png').replace('\\', '/') + '); } ')
        def leaveEvent(widget, event):
            if not self.selected_addition == 'video':
                widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'video_icon_normal.png').replace('\\', '/') + '); } ')

    self.left_panel_video_icon = left_panel_video_icon(parent=self.left_panel_icons)
    self.left_panel_video_icon.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'video_icon_normal.png').replace('\\', '/') + '); } ')

    self.left_panel_video_panel = QWidget(self.left_panel_widget)

    self.left_panel_video_tab = QTabWidget(parent=self.left_panel_video_panel)
    self.left_panel_video_tab.setTabPosition(QTabWidget.North)
    self.left_panel_video_tab.currentChanged.connect(lambda:left_panel_video_tab_changed(self))

    self.left_panel_video_tab_songs = QWidget(parent=self)

    self.left_panel_video_song_alert = QLabel(u'Parece que não há arquivos de vídeos de cânticos importados no kihvim. Adicione os cânticos usando o botão abaixo.',parent=self.left_panel_video_tab_songs)
    self.left_panel_video_song_alert.setWordWrap(True)
    self.left_panel_video_song_alert.setStyleSheet('QLabel {padding: 50px; font-family: "Ubuntu"; font-size:12pt; color:gray; qproperty-alignment:"AlignCenter" ; }')

    self.left_panel_video_song_list = QListWidget(parent=self.left_panel_video_tab_songs)
    self.left_panel_video_song_list.setViewMode(QListView.IconMode)
    self.left_panel_video_song_list.setIconSize(QSize(48, 48))
    self.left_panel_video_song_list.setSpacing(5)
    self.left_panel_video_song_list.currentItemChanged.connect(lambda:left_panel_video_song_list_clicked(self))

    self.left_panel_video_song_add = QPushButton(parent=self.left_panel_video_tab_songs)
    self.left_panel_video_song_add.setIconSize(QSize(18, 18))
    self.left_panel_video_song_add.setIcon(QIcon(os.path.join(path_graphics, "add_icon.png")))
    self.left_panel_video_song_add.setStyleSheet('QPushButton { color:white; font-size:12px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
    self.left_panel_video_song_add.clicked.connect(lambda:left_panel_video_song_add_clicked(self))

    self.left_panel_video_song_remove = QPushButton(parent=self.left_panel_video_tab_songs)
    self.left_panel_video_song_remove.setIconSize(QSize(18, 18))
    self.left_panel_video_song_remove.setIcon(QIcon(os.path.join(path_graphics, "remove_icon.png")))
    self.left_panel_video_song_remove.setStyleSheet('QPushButton { color:white; font-size:12px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
    self.left_panel_video_song_remove.clicked.connect(lambda:left_panel_video_song_remove_clicked(self))

    self.left_panel_video_tab.blockSignals(True)
    self.left_panel_video_tab.addTab(self.left_panel_video_tab_songs, u'CÂNTICOS')
    self.left_panel_video_tab.blockSignals(False)

    self.left_panel_video_tab_local = QWidget(parent=self)

    self.left_panel_video_local_alert = QLabel(u'Parece que não há arquivos de videos disponíveis na pasta selecionada. Adicione videos usando o botão abaixo.',parent=self.left_panel_video_tab_local)
    self.left_panel_video_local_alert.setWordWrap(True)
    self.left_panel_video_local_alert.setStyleSheet('QLabel {padding: 50px; font-family: "Ubuntu"; font-size:12pt; color:gray; qproperty-alignment:"AlignCenter" ; }')

    self.left_panel_video_local_list = QListWidget(parent=self.left_panel_video_tab_local)
    self.left_panel_video_local_list.setIconSize(QSize(56*self.screen_height_proportion, 56))
    self.left_panel_video_local_list.setSpacing(5)
    self.left_panel_video_local_list.currentItemChanged.connect(lambda:left_panel_video_local_list_clicked(self))

    self.left_panel_video_add = QPushButton(parent=self.left_panel_video_tab_local)
    self.left_panel_video_add.setIconSize(QSize(18, 18))
    self.left_panel_video_add.setIcon(QIcon(os.path.join(path_graphics, "add_icon.png")))
    self.left_panel_video_add.setStyleSheet('QPushButton { color:white; font-size:12px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
    self.left_panel_video_add.clicked.connect(lambda:left_panel_video_add_clicked(self))

    class left_panel_video_path_label(QLabel):
        def enterEvent(widget, event):
            self.left_panel_video_edit.setVisible(True)
        def leaveEvent(widget, event):
            self.left_panel_video_edit.setVisible(False)

    self.left_panel_video_path_label = left_panel_video_path_label(parent=self.left_panel_video_tab_local)
    self.left_panel_video_path_label.setAlignment(Qt.AlignVCenter | Qt.AlignRight)

    self.left_panel_video_edit = QPushButton(parent=self.left_panel_video_tab_local)
    self.left_panel_video_edit.setIconSize(QSize(18, 18))
    self.left_panel_video_edit.setIcon(QIcon(os.path.join(path_graphics, "edit_icon.png")))
    self.left_panel_video_edit.setStyleSheet('QPushButton { color:white; font-size:12px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:checked { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
    self.left_panel_video_edit.clicked.connect(lambda:left_panel_video_edit_clicked(self))

    self.left_panel_video_remove = QPushButton(parent=self.left_panel_video_tab_local)
    self.left_panel_video_remove.setIconSize(QSize(18, 18))
    self.left_panel_video_remove.setIcon(QIcon(os.path.join(path_graphics, "remove_icon.png")))
    self.left_panel_video_remove.setStyleSheet('QPushButton { color:white; font-size:12px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
    self.left_panel_video_remove.clicked.connect(lambda:left_panel_video_remove_clicked(self))

    self.left_panel_video_tab.blockSignals(True)
    self.left_panel_video_tab.addTab(self.left_panel_video_tab_local, 'DO COMPUTADOR')
    self.left_panel_video_tab.blockSignals(False)

    self.left_panel_video_tab_bible = QWidget(parent=self)

    self.left_panel_video_bible_alert = QLabel(u'Parece que não há arquivos de videos da Bíblia disponíveis.',parent=self.left_panel_video_tab_bible)
    self.left_panel_video_bible_alert.setWordWrap(True)
    self.left_panel_video_bible_alert.setStyleSheet('QLabel {padding: 50px; font-family: "Ubuntu"; font-size:12pt; color:gray; qproperty-alignment:"AlignCenter" ; }')

    self.left_panel_video_bible_search = QLineEdit(parent=self.left_panel_video_tab_bible)
    self.left_panel_video_bible_search.textEdited.connect(lambda:populate_video_bible_books_list(self))

    class left_panel_video_bible_book_label(QLabel):
        def mousePressEvent(widget, event):
            populate_video_bible_books_list(self)

    self.left_panel_video_bible_book_label = left_panel_video_bible_book_label(parent=self.left_panel_video_tab_bible)
    self.left_panel_video_bible_book_label.setStyleSheet('QLabel {background-color:gray; font-family: "Ubuntu Condensed"; font-size:28px; color:white; qproperty-alignment:"AlignCenter"; }')

    class left_panel_video_bible_chapter_label(QLabel):
        def mousePressEvent(widget, event):
            populate_video_bible_chapters_list(self)

    self.left_panel_video_bible_chapter_label = left_panel_video_bible_chapter_label(parent=self.left_panel_video_tab_bible)
    self.left_panel_video_bible_chapter_label.setStyleSheet('QLabel {background-color:silver; font-family: "Ubuntu Condensed"; font-size:28px; color:white; qproperty-alignment:"AlignCenter"; }')

    class left_panel_video_bible_verse_label(QLabel):
        def mousePressEvent(widget, event):
            populate_video_bible_verses_list(self)

    self.left_panel_video_bible_verse_label = left_panel_video_bible_verse_label(parent=self.left_panel_video_tab_bible)
    self.left_panel_video_bible_verse_label.setStyleSheet('QLabel {background-color:silver; font-family: "Ubuntu Condensed"; font-size:28px; color:white; qproperty-alignment:"AlignCenter"; }')

    self.left_panel_video_bible_selected_book = False
    self.left_panel_video_bible_selected_chapter = False
    self.left_panel_video_bible_selected_verse = False

    self.left_panel_video_bible_list = QListWidget(parent=self.left_panel_video_tab_bible)
    self.left_panel_video_bible_list.setViewMode(QListView.IconMode)
    self.left_panel_video_bible_list.setSpacing(5)
    self.left_panel_video_bible_list.currentItemChanged.connect(lambda:left_panel_video_bible_list_clicked(self))

    self.left_panel_video_tab.blockSignals(True)
    self.left_panel_video_tab.addTab(self.left_panel_video_tab_bible, u'BÍBLIA')
    self.left_panel_video_tab.blockSignals(False)

    self.left_panel_video_tab_library = QWidget(parent=self)

    self.left_panel_video_library_alert = QLabel(u'Parece que não há arquivos de videos disponíveis como biblioteca.',parent=self.left_panel_video_tab_library)
    self.left_panel_video_library_alert.setWordWrap(True)
    self.left_panel_video_library_alert.setStyleSheet('QLabel {padding: 50px; font-family: "Ubuntu"; font-size:12pt; color:gray; qproperty-alignment:"AlignCenter" ; }')

    self.left_panel_video_library_search = QLineEdit(parent=self.left_panel_video_tab_library)
    self.left_panel_video_library_search.textEdited.connect(lambda:populate_video_library_albums_list(self))

    class left_panel_video_library_album_label(QLabel):
        def mousePressEvent(widget, event):
            populate_video_library_albums_list(self)

    self.left_panel_video_library_album_label = left_panel_video_library_album_label(parent=self.left_panel_video_tab_library)
    #self.left_panel_video_library_album_label.setStyleSheet('QLabel {background-color:gray; font-family: "Ubuntu Condensed"; font-size:28px; color:white; qproperty-alignment:"AlignCenter"; }')
    self.left_panel_video_library_album_label.setScaledContents(True)

    class left_panel_video_library_title_label(QLabel):
        def mousePressEvent(widget, event):
            populate_video_library_titles_list(self)

    self.left_panel_video_library_title_label = left_panel_video_library_title_label(parent=self.left_panel_video_tab_library)
    #self.left_panel_video_library_title_label.setStyleSheet('QLabel {background-color:silver; font-family: "Ubuntu Condensed"; font-size:28px; color:white; qproperty-alignment:"AlignCenter"; }')
    self.left_panel_video_library_title_label.setScaledContents(True)

    class left_panel_video_library_chapter_label(QLabel):
        def mousePressEvent(widget, event):
            populate_video_library_chapters_list(self)

    self.left_panel_video_library_chapter_label = left_panel_video_library_chapter_label(parent=self.left_panel_video_tab_library)
    self.left_panel_video_library_chapter_label.setStyleSheet('QLabel {padding:8px; background-color:silver; font-family: "Ubuntu Condensed"; font-size:16px; color:white; qproperty-alignment:"AlignLeft | AlignVCenter"; }')
    self.left_panel_video_library_chapter_label.setScaledContents(True)

    self.left_panel_video_library_selected_album = False
    self.left_panel_video_library_selected_title = False
    self.left_panel_video_library_selected_chapter = False

    self.left_panel_video_library_list = QListWidget(parent=self.left_panel_video_tab_library)
    #self.left_panel_video_library_list.setViewMode(QListView.IconMode)
    self.left_panel_video_library_list.setSpacing(5)
    self.left_panel_video_library_list.currentItemChanged.connect(lambda:left_panel_video_library_list_clicked(self))

    self.left_panel_video_tab.blockSignals(True)
    self.left_panel_video_tab.addTab(self.left_panel_video_tab_library, 'BIBLIOTECA')
    self.left_panel_video_tab.blockSignals(False)

    #######################################################################
    ## ADDITION PANEL - WEB

    class left_panel_web_icon(QLabel):
        #def mouseReleaseEvent(widget, event):
        #    widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'web_icon_hover.png').replace('\\', '/') + '); } ')
        def mousePressEvent(widget, event):
            if not self.selected_addition == 'web':
                self.selected_addition = 'web'
                show_additions(self)
            else:
                self.selected_addition = False
                hide_additions(self)
            widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'web_icon_pressed.png').replace('\\', '/') + '); } ')
        def enterEvent(widget, event):
            if not self.selected_addition == 'web':
                widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'web_icon_hover.png').replace('\\', '/') + '); } ')
        def leaveEvent(widget, event):
            if not self.selected_addition == 'web':
                widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'web_icon_normal.png').replace('\\', '/') + '); } ')

    self.left_panel_web_icon = left_panel_web_icon(parent=self.left_panel_icons)
    self.left_panel_web_icon.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'web_icon_normal.png').replace('\\', '/') + '); } ')

    self.left_panel_web_panel = QWidget(self.left_panel_widget)

    self.left_panel_web_address_label = QLabel(u'ENDEREÇO',parent=self.left_panel_web_panel)
    self.left_panel_web_address_label.setStyleSheet('QLabel {font-family: "Ubuntu"; font-size:10px; color:black; qproperty-alignment:"AlignLeft" ; }')

    self.left_panel_web_address = QLineEdit(parent=self.left_panel_web_panel)
    self.left_panel_web_address.setStyleSheet('QLabel {font-family: "Ubuntu"; font-weight:bold;  font-size:20px; color:black; qproperty-alignment:"AlignLeft" ; }')

    self.left_panel_web_address_go = QPushButton(u'TESTAR', parent=self.left_panel_web_panel)
    self.left_panel_web_address_go.setStyleSheet('QPushButton { color:white; font-size:12px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 0px; border-image: url("' + os.path.join(path_graphics, "button_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 0px; border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 0px; border-image: url("' + os.path.join(path_graphics, "button_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
    self.left_panel_web_address_go.clicked.connect(lambda:left_panel_web_address_go_clicked(self))

    #self.left_panel_web_webview = QWebEngineView(parent=self.left_panel_web_panel)

    #######################################################################
    ## ADDITION PANEL - CLOCK

    class left_panel_clock_icon(QLabel):
        #def mouseReleaseEvent(widget, event):
        #    widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'clock_icon_hover.png').replace('\\', '/') + '); } ')
        def mousePressEvent(widget, event):
            if not self.selected_addition == 'clock':
                self.selected_addition = 'clock'
                show_additions(self)
            else:
                self.selected_addition = False
                hide_additions(self)
            widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'clock_icon_pressed.png').replace('\\', '/') + '); } ')
        def enterEvent(widget, event):
            if not self.selected_addition == 'clock':
                widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'clock_icon_hover.png').replace('\\', '/') + '); } ')
        def leaveEvent(widget, event):
            if not self.selected_addition == 'clock':
                widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'clock_icon_normal.png').replace('\\', '/') + '); } ')

    self.left_panel_clock_icon = left_panel_clock_icon(parent=self.left_panel_icons)
    self.left_panel_clock_icon.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'clock_icon_normal.png').replace('\\', '/') + '); } ')

    self.left_panel_clock_panel = QWidget(self.left_panel_widget)

    self.left_panel_clock_show_clock = QCheckBox(u'Mostrar relógio', parent=self.left_panel_clock_panel)
    #self.left_panel_clock_show_clock.clicked.connect(lambda:self.main_options_settings_enable_animations_clicked())

    self.left_panel_clock_show_song = QCheckBox(u'Mostrar título do cântico', parent=self.left_panel_clock_panel)
    #self.left_panel_clock_show_song.clicked.connect(lambda:self.main_options_settings_enable_animations_clicked())

    self.left_panel_clock_start_silent = QCheckBox(u'Iniciar mudo', parent=self.left_panel_clock_panel)
    #self.left_panel_clock_show_song.clicked.connect(lambda:self.main_options_settings_enable_animations_clicked())


    #######################################################################
    ## ADDITION PANEL - SCREENCOPY

    class left_panel_screencopy_icon(QLabel):
        def mousePressEvent(widget, event):
            if not self.selected_addition == 'screencopy':
                self.selected_addition = 'screencopy'
                show_additions(self)
            else:
                self.selected_addition = False
                hide_additions(self)
            widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'screencopy_icon_pressed.png').replace('\\', '/') + '); } ')
        def enterEvent(widget, event):
            if not self.selected_addition == 'screencopy':
                widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'screencopy_icon_hover.png').replace('\\', '/') + '); } ')
        def leaveEvent(widget, event):
            if not self.selected_addition == 'screencopy':
                widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'screencopy_icon_normal.png').replace('\\', '/') + '); } ')

    self.left_panel_screencopy_icon = left_panel_screencopy_icon(parent=self.left_panel_icons)
    self.left_panel_screencopy_icon.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'screencopy_icon_normal.png').replace('\\', '/') + '); } ')

    self.left_panel_screencopy_panel = QWidget(self.left_panel_widget)

    self.left_panel_screencopy_preview_background = QLabel(self.left_panel_screencopy_panel)
    self.left_panel_screencopy_preview_background.setObjectName('left_panel_screencopy_preview_background')
    self.left_panel_screencopy_preview_background.setStyleSheet('#left_panel_screencopy_preview_background { border-top: 4px; border-right: 4px; border-bottom: 4px; border-left: 4px; border-image: url("' + os.path.join(path_graphics, "black_box.png").replace('\\', '/') + '") 4 4 4 4 stretch stretch; }')


    self.left_panel_screencopy_preview = QLabel(self.left_panel_screencopy_preview_background)
    self.left_panel_screencopy_preview.setStyleSheet('QLabel {font-family: "Ubuntu"; font-size:12pt; color:gray; qproperty-alignment:"AlignCenter" ; }')

    self.left_panel_screencopy_description = QLabel(self.left_panel_screencopy_panel)

    self.left_panel_screencopy_take_button = QPushButton(u'DEFINIR CÓPIA', self.left_panel_screencopy_panel)
    self.left_panel_screencopy_take_button.setStyleSheet('QPushButton { color:white; font-size:12px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
    self.left_panel_screencopy_take_button.clicked.connect(lambda:left_panel_screencopy_take_button_clicked(self))

    #######################################################################
    ## ADDITION PANEL - CAMERA

    class left_panel_camera_icon(QLabel):
        def mousePressEvent(widget, event):
            if not self.selected_addition == 'camera':
                self.selected_addition = 'camera'
                show_additions(self)
            else:
                self.selected_addition = False
                hide_additions(self)
            widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'camera_icon_pressed.png').replace('\\', '/') + '); } ')
        def enterEvent(widget, event):
            if not self.selected_addition == 'camera':
                widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'camera_icon_hover.png').replace('\\', '/') + '); } ')
        def leaveEvent(widget, event):
            if not self.selected_addition == 'camera':
                widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'camera_icon_normal.png').replace('\\', '/') + '); } ')

    self.left_panel_camera_icon = left_panel_camera_icon(parent=self.left_panel_icons)
    self.left_panel_camera_icon.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'camera_icon_normal.png').replace('\\', '/') + '); } ')

    self.left_panel_camera_panel = QWidget(self.left_panel_widget)

    self.left_panel_camera_select = QComboBox(self.left_panel_camera_panel)
    self.left_panel_camera_select.activated.connect(lambda:left_panel_camera_select_selected(self))

    self.left_panel_camera_test_button = QPushButton(u'TESTAR', self.left_panel_camera_panel)
    self.left_panel_camera_test_button.setStyleSheet('QPushButton { color:white; font-size:12px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
    self.left_panel_camera_test_button.clicked.connect(lambda:left_panel_camera_test_button_clicked(self))

    self.left_panel_camera_preview = QLabel(self.left_panel_camera_panel)
    self.left_panel_camera_preview.setStyleSheet('QLabel {font-family: "Ubuntu"; font-size:12pt; color:gray; qproperty-alignment:"AlignCenter" ; }')

    #self.camera_instance = vlc.Instance()
    #self.left_panel_camera_preview_mediaplayer = False
    #self.left_panel_camera_preview_mediaplayer = video_instance.media_player_new()

    #self.left_panel_camera_preview_mediaplayer_widget = QFrame(parent=self)
    #self.left_panel_camera_preview_mediaplayer_widget.setGeometry(0,0,self.width(),self.height())
    #self.left_panel_camera_preview_mediaplayer.set_xwindow(self.left_panel_camera_preview_mediaplayer_widget.winId())
    #self.left_panel_camera_preview_mediaplayer.set_xwindow(self.left_panel_camera_preview.winId())

    self.left_panel_logo = QLabel(parent=self)
    self.left_panel_logo.setObjectName('left_panel_logo')
    self.left_panel_logo.setStyleSheet('#left_panel_logo { border-bottom: 70px; border-right: 55px; border-image: url("' + os.path.join(path_graphics, "left_panel_logo.png").replace('\\', '/') + '") 0 55 70 0 stretch stretch; }')
    self.left_panel_logo.setAttribute(Qt.WA_TransparentForMouseEvents)
    #self.left_panel_logo.setVisible(False)

    #self.left_panel_logo_test_button_player = video_instance.media_player_new()
    #self.left_panel_logo_test_button_player.set_media(video_instance.media_new(os.path.join(path_kihvim, 'resources',  'test_song.flac')))

    class left_panel_logo_test_button(QLabel):
        def mousePressEvent(widget, event):
            self.left_panel_logo_test_button_player.stop()
            generate_effect(app.monitor.test_widget_opacity_animation, 'opacity', 500, 1.0, 0.0)
            self.left_panel_logo_test_button_player.play()


    self.left_panel_logo_test_button = left_panel_logo_test_button(parent=self)
    #self.left_panel_logo_test_button.setObjectName('left_panel_logo_test_button')
    self.left_panel_logo_test_button.setPixmap(QPixmap(os.path.join(path_graphics, "kihvim_corner_test_logo.png")))
    #self.left_panel_logo_test_button.setStyleSheet('#left_panel_logo_test_button { image: url("' + .replace('\\', '/') + '") }')
    #self.left_panel_logo_test_button.setMask(self.left_panel_logo_test_button.pixmap().mask())

def resize(self):
    if self.selected_addition:
        self.left_panel_addbutton.setGeometry((self.width()*.5)-183,20,196,50)
        self.left_panel_widget.setGeometry(0,0,self.width()*.5,self.height())
    else:
        self.left_panel_addbutton.setGeometry(-196,20,196,50)
        self.left_panel_widget.setGeometry(67-(self.width()*.5),0,self.width()*.5,self.height())
    self.left_panel_add_title.setGeometry(0,20,self.left_panel_widget.width()-183,50)
    self.left_panel_addbutton_label.setGeometry(0,0,self.left_panel_addbutton.width(),self.left_panel_addbutton.height())

    self.left_panel_logo.setGeometry(0,0,60,self.height())
    self.left_panel_logo_test_button.setGeometry(0,self.height()-60,60,60)
    self.left_panel_icons.setGeometry(self.left_panel_widget.width()-55,70,45,self.left_panel_widget.height()-70)
    self.left_panel_audio_icon.setGeometry(0,0,45,45)
    self.left_panel_audio_panel.setGeometry(12,70,self.left_panel_widget.width()-12-55,self.left_panel_widget.height()-70)
    self.left_panel_audio_list.setGeometry(10,10,self.left_panel_audio_panel.width()-20,self.left_panel_audio_panel.height()-80)
    self.left_panel_audio_alert.setGeometry(10,10,self.left_panel_audio_panel.width()-20,self.left_panel_audio_panel.height()-80)
    self.left_panel_audio_edit.setGeometry(10+self.left_panel_audio_list.width()-80,10+self.left_panel_audio_list.height()+5,40,40)
    self.left_panel_audio_remove.setGeometry(10+self.left_panel_audio_list.width()-40,10+self.left_panel_audio_list.height()+5,40,40)
    self.left_panel_audio_download_lyrics.setGeometry(10+self.left_panel_audio_list.width()-125,10+self.left_panel_audio_list.height()+5,40,40)

    if self.left_panel_audio_list.count() > 0:
        self.left_panel_audio_add.setGeometry(10+self.left_panel_audio_list.width()-170,10+self.left_panel_audio_list.height()+5,40,40)
        #self.left_panel_audio_remove.setVisible(True)
    else:
        self.left_panel_audio_add.setGeometry(self.left_panel_audio_panel.width()*.4,self.left_panel_audio_panel.height()*.6,self.left_panel_audio_panel.width()*.2,self.left_panel_audio_panel.height()*.1)
        #self.left_panel_audio_remove.setVisible(False)

    self.left_panel_audio_edit_panel.setGeometry(10,10,self.left_panel_audio_panel.width()-20,self.left_panel_audio_panel.height()-80)
    self.left_panel_audio_edit_number_label.setGeometry(0,0,self.left_panel_audio_edit_panel.width()*.2,20)
    self.left_panel_audio_edit_number.setGeometry(0,20,self.left_panel_audio_edit_panel.width()*.2,30)
    self.left_panel_audio_edit_title_label.setGeometry(self.left_panel_audio_edit_panel.width()*.2,0,self.left_panel_audio_edit_panel.width()*.8,20)
    self.left_panel_audio_edit_title.setGeometry(self.left_panel_audio_edit_panel.width()*.2,20,self.left_panel_audio_edit_panel.width()*.8,30)
    self.left_panel_audio_edit_filepath_label.setGeometry(0,60,self.left_panel_audio_edit_panel.width(),20)
    self.left_panel_audio_edit_filepath.setGeometry(0,80,self.left_panel_audio_edit_panel.width(),30)
    self.left_panel_audio_edit_lyrics_label.setGeometry(0,120,self.left_panel_audio_edit_panel.width(),20)
    self.left_panel_audio_edit_lyrics.setGeometry(0,140,self.left_panel_audio_edit_panel.width(),self.left_panel_audio_edit_panel.height()-160)
    self.left_panel_audio_edit_lyrics_get_from_web.setGeometry(self.left_panel_audio_edit_lyrics.width()-50,self.left_panel_audio_edit_lyrics.height()-50,40,40)

    self.left_panel_image_icon.setGeometry(0,45,45,45)
    self.left_panel_image_panel.setGeometry(12,70,self.left_panel_widget.width()-12-55,self.left_panel_widget.height()-70)
    self.left_panel_image_tab.setGeometry(10,10,self.left_panel_image_panel.width()-20,self.left_panel_image_panel.height()-20)
    self.left_panel_image_list.setGeometry(10,10,self.left_panel_image_tab_local.width()-20,self.left_panel_image_tab_local.height()-65)
    self.left_panel_image_alert.setGeometry(10,10,self.left_panel_image_tab_local.width()-20,self.left_panel_image_tab_local.height()-80)

    self.left_panel_image_remove.setGeometry(10+self.left_panel_image_list.width()-40,10+self.left_panel_image_list.height()+5,40,40)

    if self.left_panel_image_list.count() > 0:
        self.left_panel_image_path_label.setGeometry(10,10+self.left_panel_image_list.height()+5,self.left_panel_image_list.width()-90,40)
        self.left_panel_image_add.setGeometry(10+self.left_panel_image_list.width()-85,10+self.left_panel_image_list.height()+5,40,40)
    else:
        self.left_panel_image_path_label.setGeometry(10,10+self.left_panel_image_list.height()+5,self.left_panel_image_list.width(),40)
        self.left_panel_image_add.setGeometry(self.left_panel_image_list.width()*.4,self.left_panel_image_list.height()*.6,self.left_panel_image_list.width()*.2,self.left_panel_image_list.height()*.1)
    self.left_panel_image_edit.setGeometry(self.left_panel_image_path_label.width()-40,0,40,40)
    self.left_panel_image_keyword.setGeometry(10,30,(self.left_panel_image_tab_local.width()-20)*.8,30)
    self.left_panel_image_keyword_go.setGeometry(10+((self.left_panel_image_tab_local.width()-20)*.8),30,(self.left_panel_image_tab_local.width()-20)*.2,30)
    self.left_panel_image_webview.setGeometry(10,70,self.left_panel_image_tab_local.width()-20, self.left_panel_image_tab_local.height()-80)
    self.left_panel_image_webview_warning.setGeometry(self.left_panel_image_webview.x(),self.left_panel_image_webview.y(),self.left_panel_image_webview.width(),self.left_panel_image_webview.height())
    self.left_panel_image_webview_preview.setGeometry(self.left_panel_image_webview.width()*.1,(self.left_panel_image_webview.height()-((self.left_panel_image_webview.width()*.8)*self.screen_width_proportion))*.5,self.left_panel_image_webview.width()*.8,(self.left_panel_image_webview.width()*.8)*self.screen_width_proportion)
    self.left_panel_image_webview_preview_cancel_button.setGeometry(self.left_panel_image_webview_preview.width()*.15,(self.left_panel_image_webview_preview.height()-30)*.5,self.left_panel_image_webview_preview.width()*.7,30)

    self.left_panel_video_icon.setGeometry(0,90,45,45)
    self.left_panel_video_panel.setGeometry(12,70,self.left_panel_widget.width()-12-55,self.left_panel_widget.height()-70)
    self.left_panel_video_tab.setGeometry(10,10,self.left_panel_video_panel.width()-20,self.left_panel_video_panel.height()-20)

    self.left_panel_video_song_list.setGeometry(10,10,self.left_panel_video_tab.width()-4-20,self.left_panel_video_tab_songs.height()-80)
    self.left_panel_video_song_alert.setGeometry(10,10,self.left_panel_video_tab.width()-4-20,self.left_panel_video_tab_songs.height()-80)
    self.left_panel_video_song_remove.setGeometry(10+self.left_panel_video_song_list.width()-40,10+self.left_panel_video_song_list.height()+5,40,40)

    if self.left_panel_video_song_list.count() > 0:
        self.left_panel_video_song_add.setGeometry(10+self.left_panel_video_song_list.width()-85,10+self.left_panel_video_song_list.height()+5,40,40)
    else:
        self.left_panel_video_song_add.setGeometry(self.left_panel_video_tab_songs.width()*.4,self.left_panel_video_tab_songs.height()*.6,self.left_panel_video_tab_songs.width()*.2,self.left_panel_video_tab_songs.height()*.1)

    self.left_panel_video_local_list.setGeometry(10,10,self.left_panel_video_tab_songs.width()-20,self.left_panel_video_tab_songs.height()-80)
    self.left_panel_video_local_alert.setGeometry(10,10,self.left_panel_video_tab_songs.width()-20,self.left_panel_video_tab_songs.height()-20)

    self.left_panel_video_remove.setGeometry(10+self.left_panel_video_local_list.width()-40,10+self.left_panel_video_local_list.height()+5,40,40)
    if self.left_panel_video_local_list.count() > 0:
        self.left_panel_video_path_label.setGeometry(10,10+self.left_panel_video_local_list.height()+5,self.left_panel_video_local_list.width()-90,40)
        self.left_panel_video_add.setGeometry(10+self.left_panel_video_local_list.width()-85,10+self.left_panel_video_local_list.height()+5,40,40)
    else:
        self.left_panel_video_path_label.setGeometry(10,10+self.left_panel_video_local_list.height()+5,self.left_panel_video_local_list.width(),40)
        self.left_panel_video_add.setGeometry(self.left_panel_video_local_list.width()*.4,self.left_panel_video_local_list.height()*.7,self.left_panel_video_local_list.width()*.2,self.left_panel_video_local_list.height()*.1)
    self.left_panel_video_edit.setGeometry(self.left_panel_video_path_label.width()-40,self.left_panel_video_path_label.y(),40,40)

    self.left_panel_video_bible_alert.setGeometry(10,10,self.left_panel_video_tab_bible.width()-20,self.left_panel_video_tab_bible.height()-20)
    self.left_panel_video_bible_search.setGeometry(10,10,self.left_panel_video_tab_songs.width()-20,48)
    self.left_panel_video_bible_book_label.setGeometry(10,10,96,48)
    self.left_panel_video_bible_chapter_label.setGeometry(111,10,48,48)
    self.left_panel_video_bible_verse_label.setGeometry(164,10,48,48)
    self.left_panel_video_bible_list.setGeometry(10,68,self.left_panel_video_tab_songs.width()-20,self.left_panel_video_tab_songs.height()-78)

    self.left_panel_video_library_alert.setGeometry(10,10,self.left_panel_video_tab_library.width()-20,self.left_panel_video_tab_library.height()-20)
    self.left_panel_video_library_search.setGeometry(10,10,self.left_panel_video_tab_songs.width()-20,48)
    self.left_panel_video_library_album_label.setGeometry(10,10,48,48)
    self.left_panel_video_library_title_label.setGeometry(68,10,48,48)
    self.left_panel_video_library_chapter_label.setGeometry(126,10,self.left_panel_video_tab_songs.width()-146,48)
    self.left_panel_video_library_list.setGeometry(10,68,self.left_panel_video_tab_songs.width()-20,self.left_panel_video_tab_songs.height()-78)

    self.left_panel_web_icon.setGeometry(0,135,45,45)
    self.left_panel_web_panel.setGeometry(12,70,self.left_panel_widget.width()-12-55,self.left_panel_widget.height()-70)
    self.left_panel_web_address_label.setGeometry(10,10,(self.left_panel_web_panel.width()-20)*.8,20)
    self.left_panel_web_address.setGeometry(10,30,(self.left_panel_web_panel.width()-20)*.8,30)
    self.left_panel_web_address_go.setGeometry(10+((self.left_panel_web_panel.width()-20)*.8),30,(self.left_panel_web_panel.width()-20)*.2,30)
    #self.left_panel_web_webview.setGeometry(10,70,self.left_panel_web_panel.width()-20, self.left_panel_web_panel.height()-80)

    self.left_panel_clock_icon.setGeometry(0,180,45,45)
    self.left_panel_clock_panel.setGeometry(12,70,self.left_panel_widget.width()-12-55,self.left_panel_widget.height()-70)
    self.left_panel_clock_show_clock.setGeometry(10,10,self.left_panel_web_panel.width()-20,20)
    self.left_panel_clock_show_song.setGeometry(10,30,self.left_panel_web_panel.width()-20,20)
    self.left_panel_clock_start_silent.setGeometry(10,50,self.left_panel_web_panel.width()-20,20)

    self.left_panel_screencopy_icon.setGeometry(0,225,45,45)
    self.left_panel_screencopy_panel.setGeometry(12,70,self.left_panel_widget.width()-12-55,self.left_panel_widget.height()-70)
    self.left_panel_screencopy_preview_background.setGeometry(10,10,self.left_panel_screencopy_panel.width()-20,(self.left_panel_screencopy_panel.width()-20)*self.screen_width_proportion)
    self.left_panel_screencopy_preview.setGeometry(3,3,self.left_panel_screencopy_preview_background.width()-6,(self.left_panel_screencopy_preview_background.width()-6)*self.screen_width_proportion)
    self.left_panel_screencopy_description.setGeometry(10,20+self.left_panel_screencopy_preview.height(),self.left_panel_screencopy_preview.width()*.75,30)
    self.left_panel_screencopy_take_button.setGeometry(10+(self.left_panel_screencopy_preview_background.width()*.75),20+self.left_panel_screencopy_preview_background.height(),self.left_panel_screencopy_preview_background.width()*.25,30)

    self.left_panel_camera_icon.setGeometry(0,270,45,45)
    self.left_panel_camera_panel.setGeometry(12,70,self.left_panel_widget.width()-12-55,self.left_panel_widget.height()-70)
    self.left_panel_camera_select.setGeometry(10,10,(self.left_panel_camera_panel.width()-20)*.75,30)
    self.left_panel_camera_test_button.setGeometry(10+((self.left_panel_camera_panel.width()-20)*.75),10,(self.left_panel_camera_panel.width()-20)*.25,30)
    self.left_panel_camera_preview.setGeometry(10,50,self.left_panel_camera_panel.width()-20,(self.left_panel_camera_panel.width()-20)*self.screen_width_proportion)


def show_additions(self):
    self.left_panel_addbutton.setEnabled(False)
    self.left_panel_addbutton_label.setText(u'<font style="font-size:9px;">' + u'SELECIONE ALGO PARA' + '</font><br><font style="font-size:14px;"><b>' + u'ADICIONAR' + '</b></font>')

    if self.settings['enable_animations']:
        generate_effect(self.left_panel_geometry_animation, 'geometry', 500, [self.left_panel_widget.x(),0,self.width()*.5,self.height()], [0,0,self.width()*.5,self.height()])
        generate_effect(self.left_panel_addbutton_geometry_animation, 'geometry', 500, [self.left_panel_addbutton.x(),self.left_panel_addbutton.y(),self.left_panel_addbutton.width(),self.left_panel_addbutton.height()], [(self.width()*.5)-183,self.left_panel_addbutton.y(),self.left_panel_addbutton.width(),self.left_panel_addbutton.height()])
    else:
        self.left_panel_widget.setGeometry(0,0,self.width()*.5,self.height())
        self.left_panel_addbutton.setGeometry((self.width()*.5)-183,self.left_panel_addbutton.y(),self.left_panel_addbutton.width(),self.left_panel_addbutton.height())

    if not self.selected_addition == 'audio':
        self.left_panel_audio_icon.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'music_icon_normal.png').replace('\\', '/') + '); } ')
        self.left_panel_audio_panel.setVisible(False)
    if not self.selected_addition == 'image':
        self.left_panel_image_icon.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'image_icon_normal.png').replace('\\', '/') + '); } ')
        self.left_panel_image_panel.setVisible(False)
    if not self.selected_addition == 'video':
        self.left_panel_video_icon.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'video_icon_normal.png').replace('\\', '/') + '); } ')
        self.left_panel_video_panel.setVisible(False)
    if not self.selected_addition == 'clock':
        self.left_panel_clock_icon.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'clock_icon_normal.png').replace('\\', '/') + '); } ')
        self.left_panel_clock_panel.setVisible(False)
    if not self.selected_addition == 'web':
        self.left_panel_web_icon.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'web_icon_normal.png').replace('\\', '/') + '); } ')
        self.left_panel_web_panel.setVisible(False)
    if not self.selected_addition == 'screencopy':
        self.left_panel_screencopy_icon.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'screencopy_icon_normal.png').replace('\\', '/') + '); } ')
        self.left_panel_screencopy_panel.setVisible(False)
    if not self.selected_addition == 'camera':
        self.left_panel_camera_icon.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'camera_icon_normal.png').replace('\\', '/') + '); } ')
        self.left_panel_camera_panel.setVisible(False)
        #if not self.left_panel_camera_preview_mediaplayer.get_media() == None:
        #    self.left_panel_camera_preview_mediaplayer.stop()
        #    self.left_panel_camera_preview_mediaplayer.get_media().release()
        #if self.left_panel_camera_preview_mediaplayer:
        #    #print self.left_panel_camera_preview_mediaplayer
        #    #self.left_panel_camera_preview_mediaplayer.get_media().release()
        #    del self.left_panel_camera_preview_mediaplayer
        #    self.left_panel_camera_preview_mediaplayer = False
        #    #self.camera_instance.release()


    if self.selected_addition == 'audio':
        self.left_panel_add_title.setText(u'CÂNTICO')
        self.left_panel_audio_panel.setVisible(True)
    self.left_panel_audio_list.setCurrentItem(None)
    if self.selected_addition == 'image':
        self.left_panel_add_title.setText(u'IMAGEM')
        self.left_panel_image_panel.setVisible(True)
        self.left_panel_image_keyword.setText('')
        left_panel_image_tab_changed(self)
    self.left_panel_image_list.setCurrentItem(None)
    if self.selected_addition == 'video':
        self.left_panel_add_title.setText(u'VÍDEO')
        self.left_panel_video_panel.setVisible(True)
        left_panel_video_tab_changed(self)
    self.left_panel_video_local_list.setCurrentItem(None)
    if self.selected_addition == 'screencopy':
        self.left_panel_add_title.setText(u'CÓPIA DE TELA')
        self.left_panel_screencopy_panel.setVisible(True)
        self.left_panel_screencopy_preview.setPixmap(None)
        self.left_panel_screencopy_preview.setText(u'Não há área para ser exibida.\nDefina uma área no botão abaixo.')
        self.left_panel_screencopy_description.setText(u'<font style="font-size:9px;color:gray;">' + u'SEM ÁREA PARA MOSTRAR' + '</font>')
        app.screencopy.selected_area = False
    if self.selected_addition == 'web':
        self.left_panel_web_address.setText('http://www.jw.org')
        self.left_panel_add_title.setText(u'PÁGINA DO JW.ORG')
        self.left_panel_web_panel.setVisible(True)
    if self.selected_addition == 'clock':
        self.left_panel_add_title.setText(u'PRELÚDIO')
        self.left_panel_clock_panel.setVisible(True)
        self.left_panel_addbutton.setEnabled(True)
        self.left_panel_addbutton_label.setText(u'<font style="font-size:14px;"><b>' + u'ADICIONAR' + '</b></font><br><font style="font-size:9px;">' + u'NA LISTA DE EXECUÇÃO' + '</font>')
    if self.selected_addition == 'camera':
        self.left_panel_add_title.setText(u'CAMERA')
        self.left_panel_camera_panel.setVisible(True)
        populate_camera_list(self)
        self.left_panel_camera_select.setCurrentIndex(-1)
        self.left_panel_camera_test_button.setEnabled(False)
        #self.left_panel_camera_preview_mediaplayer = video_instance.media_player_new()
        #self.left_panel_camera_preview_mediaplayer.set_xwindow(self.left_panel_camera_preview.winId())
        self.left_panel_camera_preview.setText(u'Não há camera selecionada.\nDefina uma camera para testar  .')
    #self.left_panel_video_local_list.setCurrentItem(None)

def hide_additions(self):
    if self.settings['enable_animations']:
        generate_effect(self.left_panel_geometry_animation, 'geometry', 500, [self.left_panel_widget.x(),0,self.width()*.5,self.height()], [67-(self.width()*.5),0,self.width()*.5,self.height()])
        generate_effect(self.left_panel_addbutton_geometry_animation, 'geometry', 500, [self.left_panel_addbutton.x(),self.left_panel_addbutton.y(),self.left_panel_addbutton.width(),self.left_panel_addbutton.height()], [-196,self.left_panel_addbutton.y(),self.left_panel_addbutton.width(),self.left_panel_addbutton.height()])
    else:
        self.left_panel_widget.setGeometry(67-(self.width()*.5),0,self.width()*.5,self.height())
        self.left_panel_addbutton.setGeometry(-196,self.left_panel_addbutton.y(),self.left_panel_addbutton.width(),self.left_panel_addbutton.height())

def left_panel_addbutton_clicked(self):
    back_to_playlist = False
    if self.selected_addition == 'audio':
        icon_label = QLabel(self.left_panel_audio_list.currentItem().toolTip())
        icon_label.setGeometry(0,0,50*self.screen_height_proportion,50)
        icon_label.setAlignment(Qt.AlignCenter)
        icon_label.setStyleSheet('QLabel {background-color:black; font-family: "Ubuntu Condensed"; font-size:28px; color:white;}')
        preview = QPixmap(QPixmap().grabWidget(icon_label))

        duration = MP3(self.list_of_songs[int(self.left_panel_audio_list.currentItem().toolTip())][2]).info.length

        self.playlist.append([  'audio',                                                                             # [0] kind
                                preview,                                                                             # [1] preview image
                                int(self.left_panel_audio_list.currentItem().toolTip()),                             # [2] number
                                self.list_of_songs[int(self.left_panel_audio_list.currentItem().toolTip())][0],      # [3] title
                                self.list_of_songs[int(self.left_panel_audio_list.currentItem().toolTip())][1],      # [4] lyrics
                                self.list_of_songs[int(self.left_panel_audio_list.currentItem().toolTip())][2],      # [5] file path
                                False,                                                                               # [6] show lyrics ?
                                duration,                                                                            # [7] duration in sec
                                back_to_playlist])                                                                   # [8] back to playlist

    elif self.selected_addition == 'image':
        if self.left_panel_image_tab.currentIndex() == 0:
            filepath = self.left_panel_image_list.currentItem().toolTip()
            md5 = hashlib.md5()
            md5.update(open(filepath, 'rb').read())
            filename_hash = md5.hexdigest()
        elif self.left_panel_image_tab.currentIndex() == 1:
            filename_hash = self.left_panel_image_webview_preview.toolTip()
            filepath = os.path.join(path_tmp , filename_hash + '.png')
        preview = QPixmap(os.path.join(path_tmp , filename_hash + '.png'))
        self.playlist.append([ 'image',                                                                              # [0] kind
                                preview,                                                                             # [1] preview image
                                filepath,                                                                            # [2] file path
                                back_to_playlist])                                                                   # [3] back to playlist

    elif self.selected_addition == 'video':
        start = False
        end = False

        if self.left_panel_video_tab.currentIndex() == 0:
            filepath = self.list_of_video_songs[int(self.left_panel_video_song_list.currentItem().toolTip())][1]
            icon_label = QLabel(self.left_panel_video_song_list.currentItem().toolTip())
            icon_label.setGeometry(0,0,50*self.screen_height_proportion,50)
            icon_label.setAlignment(Qt.AlignCenter)
            icon_label.setStyleSheet('QLabel {background-color:black; font-family: "Ubuntu Condensed"; font-size:28px; color:white;}')
            preview = QPixmap(QPixmap().grabWidget(icon_label))
            title = self.list_of_video_songs[int(self.left_panel_video_song_list.currentItem().toolTip())][0]

        elif self.left_panel_video_tab.currentIndex() == 1:
            filepath = self.list_of_local_videos[self.left_panel_video_local_list.currentItem().toolTip()][1]
            md5 = hashlib.md5()
            md5.update(open(filepath, 'rb').read())
            filename_hash = md5.hexdigest()
            preview = QPixmap(os.path.join(path_tmp , filename_hash + '.png'))

            if self.list_of_local_videos[self.left_panel_video_local_list.currentItem().toolTip()][2]:
                title = self.list_of_local_videos[self.left_panel_video_local_list.currentItem().toolTip()][2]
            else:
                title = filepath.split('/')[-1].rsplit('.', 1)[0]

        elif self.left_panel_video_tab.currentIndex() == 2:
            verse = self.left_panel_video_bible_selected_book + ' ' + self.left_panel_video_bible_selected_chapter + ':' + self.left_panel_video_bible_selected_verse
            filepath = self.list_of_bible_videos[verse][0]

            icon_label = QLabel(verse)
            icon_label.setGeometry(0,0,50*self.screen_height_proportion,50)
            icon_label.setAlignment(Qt.AlignCenter)
            icon_label.setStyleSheet('QLabel {background-color:gray; font-family: "Ubuntu Condensed"; font-size:16px; color:white;}')
            preview = QPixmap(QPixmap().grabWidget(icon_label))

            title = verse

            start = self.list_of_bible_videos[verse][1]
            end = self.list_of_bible_videos[verse][2]
        elif self.left_panel_video_tab.currentIndex() == 3:
            filepath = self.left_panel_video_library_selected_title
            icon_label = QLabel()
            icon_label.setScaledContents(True)
            icon_label.setPixmap(self.list_of_library_videos[self.left_panel_video_library_selected_album][self.left_panel_video_library_selected_title][-1])
            icon_label.setGeometry(0,0,50*self.screen_height_proportion,50)

            preview = QPixmap(self.list_of_library_videos[self.left_panel_video_library_selected_album][self.left_panel_video_library_selected_title][-1])

            title = self.list_of_library_videos[self.left_panel_video_library_selected_album][self.left_panel_video_library_selected_title][0]

            if not self.left_panel_video_library_selected_chapter == u'all':
                start = self.list_of_library_videos[self.left_panel_video_library_selected_album][self.left_panel_video_library_selected_title][1][self.left_panel_video_library_selected_chapter][0]
                end = self.list_of_library_videos[self.left_panel_video_library_selected_album][self.left_panel_video_library_selected_title][1][self.left_panel_video_library_selected_chapter][1] - 1.0

        duration_output = subprocess.Popen([ffmpeg_bin, '-i', filepath], stdout=subprocess.PIPE, stderr=subprocess.STDOUT).stdout.read().decode()
        if 'Duration:' in duration_output:
            duration = utils.convert_timecode_to_seconds(duration_output.split('Duration: ')[1].split(',')[0])

        subprocess.call([ffmpeg_bin, '-loglevel', 'error', '-y', '-ss', '00:10.00', '-i', filepath, '-frames:v', '1', os.path.join(path_tmp, filepath.split('/')[-1].rsplit('.', 1)[0] + '.png')])
        image = QPixmap(os.path.join(path_tmp, filepath.split('/')[-1].rsplit('.', 1)[0] + '.png'))

        self.playlist.append([ 'video',                                                                              # [0] kind
                                preview,                                                                             # [1] preview image
                                filepath,                                                                            # [2] file path
                                image,                                                                               # [3] preview
                                title,                                                                               # [4] title
                                start,                                                                               # [5] start
                                end,                                                                                 # [6] end
                                duration,                                                                            # [7] duration
                                back_to_playlist])                                                                   # [8] back to playlist

    #elif self.selected_addition == 'web':
    #    preview = QPixmap().grabWidget(self.left_panel_web_webview, h=self.left_panel_web_webview.width()*self.screen_width_proportion).scaled(56*self.screen_height_proportion, 56)
    #    image = QPixmap().grabWidget(self.left_panel_web_webview, h=self.left_panel_web_webview.width()*self.screen_width_proportion)
    #
    #    self.playlist.append([ 'web',                                                                                # [0] kind
    #                            preview,                                                                             # [1] preview image
    #                            self.left_panel_web_address.text(),                                                  # [2] address
    #                            image,                                                                               # [3] preview
    #                            back_to_playlist])                                                                   # [4] back to playlist
    elif self.selected_addition == 'clock':
        icon_label = QLabel()
        icon_label.setPixmap(os.path.join(path_graphics, 'clock_icon_normal.png'))
        icon_label.setGeometry(0,0,50*self.screen_height_proportion,50)
        icon_label.setAlignment(Qt.AlignCenter)
        icon_label.setStyleSheet('QLabel {background-color:black;}')
        preview = QPixmap(QPixmap().grabWidget(icon_label))

        self.playlist.append([ 'clock',                                                                              # [0] kind
                                preview,                                                                             # [1] preview
                                self.left_panel_clock_show_clock.isChecked(),                                        # [2] show clock
                                self.left_panel_clock_show_song.isChecked(),                                        # [3] show song
                                self.left_panel_clock_start_silent.isChecked(),                                      # [4] start silent
                                back_to_playlist])                                                                   # [5] back to playlist
    elif self.selected_addition == 'screencopy':
        icon_label = QLabel()
        icon_label.setPixmap(os.path.join(path_graphics, 'screencopy_icon_normal.png'))
        icon_label.setGeometry(0,0,50*self.screen_height_proportion,50)
        icon_label.setAlignment(Qt.AlignCenter)
        icon_label.setStyleSheet('QLabel {background-color:black;}')
        preview = QPixmap(QPixmap().grabWidget(icon_label))

        self.playlist.append([ 'screencopy',                                                                         # [0] kind
                                preview,                                                                             # [1] preview
                                app.screencopy.selected_area,                                                        # [2] selected area
                                back_to_playlist])                                                                   # [3] back to playlist

    elif self.selected_addition == 'camera':
        self.left_panel_camera_preview_mediaplayer.stop()
        self.left_panel_camera_preview_mediaplayer.get_media().release()
        icon_label = QLabel()
        icon_label.setPixmap(os.path.join(path_graphics, 'camera_icon_normal.png'))
        icon_label.setGeometry(0,0,50*self.screen_height_proportion,50)
        icon_label.setAlignment(Qt.AlignCenter)
        icon_label.setStyleSheet('QLabel {background-color:black;}')
        preview = QPixmap(QPixmap().grabWidget(icon_label))
        populate_camera_list(self)
        for camera in self.list_of_cameras.keys():
            if self.left_panel_camera_select.currentText() == self.list_of_cameras[camera][0]:
                selected_camera = camera
                break

        if selected_camera:
            mediaplayer =  video_instance.media_player_new()
            mediaplayer.set_media(video_instance.media_new('v4l2://' + selected_camera))

            self.playlist.append([ 'camera',                                                                             # [0] kind
                                    preview,                                                                             # [1] preview
                                    mediaplayer,                                                                         # [2] media player
                                    back_to_playlist])                                                                   # [3] back to playlist

    playlist_panel.populate_playlist(self)

############################################################################
## ADDITION PANEL - AUDIO

def populate_audio_list(self):
    self.left_panel_audio_list.clear()
    self.left_panel_audio_alert.setVisible(False)
    self.left_panel_audio_list.setVisible(False)
    self.left_panel_audio_edit.setVisible(False)
    self.left_panel_audio_remove.setVisible(False)
    self.left_panel_audio_download_lyrics.setVisible(False)
    self.left_panel_audio_edit_panel.setVisible(False)

    if len(self.list_of_songs.keys()) > 0:
        for key in sorted(self.list_of_songs.keys()):
            if os.path.isfile(self.list_of_songs[key][2]):
                label = QLabel(str(key))
                label.setGeometry(0,0,48,48)
                label.setAlignment(Qt.AlignCenter)
                label.setStyleSheet('QLabel {background-color:silver; font-family: "Ubuntu Condensed"; font-size:28px; color:white;}')
                item = QListWidgetItem(QIcon(QPixmap().grabWidget(label)), None)
                item.setToolTip(str(key))
                self.left_panel_audio_list.addItem(item)

    if self.left_panel_audio_list.count() > 0:
        self.left_panel_audio_list.setVisible(True)
        self.left_panel_audio_add.setGeometry(10+self.left_panel_audio_list.width()-170,10+self.left_panel_audio_list.height()+5,40,40)
        self.left_panel_audio_edit.setVisible(True)
        self.left_panel_audio_remove.setVisible(True)
        self.left_panel_audio_download_lyrics.setVisible(True)
    else:
        self.left_panel_audio_add.setGeometry(self.left_panel_audio_panel.width()*.4,self.left_panel_audio_panel.height()*.6,self.left_panel_audio_panel.width()*.2,self.left_panel_audio_panel.height()*.1)
        self.left_panel_audio_alert.setVisible(True)

    self.left_panel_audio_list_clear_selection()

def left_panel_audio_list_clear_selection(self):
    self.left_panel_audio_list.setCurrentRow(-1)
    self.left_panel_audio_edit.setEnabled(False)
    self.left_panel_audio_remove.setEnabled(False)

def left_panel_audio_download_lyrics_clicked(self):
    for key in self.list_of_songs.keys():
        self.list_of_songs[key][1] = self.download_lyrics(key)

def left_panel_audio_add_clicked(self):
    song_path_list = QFileDialog.getOpenFileNames(self, "Selecione os arquivos de audio", path_home, "Arquivos de audio (*.mp3)")[0]
    for song_path_file in song_path_list:
        if os.path.isfile(song_path_file):
            song = ID3(song_path_file)
            title = song["TIT2"].text[0]
            number = utils.get_number(title)
            lyrics = ''
            if u'USLT::XXX' in song.keys():
                lyrics = song[u'USLT::XXX'].text

            if not number and 'TRCK' in song.keys() and (song['TRCK'].text).isnumeric():
                number = song['TRCK'].text

            if number in title:
                title = title.replace('_', ' ').split(number, 1)[1].split(' ', 1)[1]

            if number:
                self.list_of_songs[int(number)] = [title, lyrics, song_path_file]

    populate_audio_list(self)

def left_panel_audio_remove_clicked(self):
    del self.list_of_songs[int(self.left_panel_audio_list.currentItem().toolTip())]
    populate_audio_list(self)

def left_panel_audio_edit_clicked(self):
    if self.left_panel_audio_edit.isChecked():
        self.left_panel_audio_edit_panel.setVisible(True)
        self.left_panel_audio_list.setVisible(False)
        self.left_panel_audio_add.setVisible(False)
        self.left_panel_audio_edit_number.setText(self.left_panel_audio_list.currentItem().toolTip())
        self.left_panel_audio_edit_title.setText(self.list_of_songs[int(self.left_panel_audio_list.currentItem().toolTip())][0])
        self.left_panel_audio_edit_filepath.setText(self.list_of_songs[int(self.left_panel_audio_list.currentItem().toolTip())][2])
        self.left_panel_audio_edit_lyrics.setPlainText(self.list_of_songs[int(self.left_panel_audio_list.currentItem().toolTip())][1])
    else:
        self.left_panel_audio_edit_panel.setVisible(False)
        self.left_panel_audio_list.setVisible(True)
        self.left_panel_audio_add.setVisible(True)

        self.list_of_songs[int(self.left_panel_audio_list.currentItem().toolTip())][0] = self.left_panel_audio_edit_title.text()
        self.list_of_songs[int(self.left_panel_audio_list.currentItem().toolTip())][1] = self.left_panel_audio_edit_lyrics.toPlainText()

def left_panel_audio_list_clicked(self):
    if self.left_panel_audio_list.currentItem():
        self.left_panel_add_title.setText(u'<font style="font-size:14px;"><b>' + u'CÂNTICO '+ str(self.left_panel_audio_list.currentItem().toolTip()) + '</b></font><br><font style="font-size:9px;">' + self.list_of_songs[int(self.left_panel_audio_list.currentItem().toolTip())][0] + '</font>')
        self.left_panel_addbutton_label.setText(u'<font style="font-size:14px;"><b>' + u'ADICIONAR' + '</b></font><br><font style="font-size:9px;">' + u'NA LISTA DE EXECUÇÃO' + '</font>')
        self.left_panel_addbutton.setEnabled(True)
        self.left_panel_audio_edit.setEnabled(True)
        self.left_panel_audio_remove.setEnabled(True)
        #self.playlist_options_panel_audio_panel_show_lyrics.setChecked(False)

def left_panel_audio_edit_lyrics_get_from_web_clicked(self):
    self.left_panel_audio_edit_lyrics.setPlainText(self.download_lyrics(int(self.left_panel_audio_list.currentItem().toolTip())))

def download_lyrics(self, song):
    media_file = MP3(self.list_of_songs[song][2])
    locale = self.list_of_songs[song][2].split('/')[-1].split('_')[1]
    if not locale in ['T']:
        locale = 'T'
    if media_file.tags and 'UFID:Default' in media_file.tags.keys():
        docid = media_file.tags['UFID:Default'].data
        lyrics_page = urllib.request.urlopen('http://wol.jw.org/wol/finder?wtlocale=' + locale + '&docid=' + docid)
        html = unicode(lyrics_page.read(), 'utf-8')
        if '"pGroup"' in html and '"closingContent"' in html:
            counter = 1
            final_text = u''
            for line in html.split('<div class="pGroup">')[1].split('<div class="closingContent">')[0].split('</p>'):
                if '<div class="chorus">' in line:
                    final_text += '\n'

                if 'class="sl"' in line:
                    if counter > 1:
                        final_text += '\n'
                    if '<li>' in line:
                        line = line.replace('<li>', str(counter) + '. ')
                    counter += 1
                else:
                    final_text += '    '

                final_text += clean_spaces(clean_xml(line)) + '\n'
    return final_text

############################################################################
## ADDITION PANEL - IMAGE

def populate_image_list(self):
    self.left_panel_image_list.clear()
    self.left_panel_image_alert.setVisible(False)
    self.left_panel_image_list.setVisible(False)
    self.left_panel_image_remove.setVisible(False)
    self.left_panel_image_path_label.setText(u'<font style="font-size:9px;color:gray;">' + u'EXIBINDO IMAGENS DA PASTA' + '</font><br><font style="font-size:14px;color:gray;">' + path_for_images + '</font>')
    self.left_panel_image_edit.setVisible(False)

    list_of_images = []

    for key in self.list_of_images_from_folder.keys():
        if os.path.isfile(self.list_of_images_from_folder[key][1]):
            list_of_images.append(self.list_of_images_from_folder[key])

    for key in self.list_of_images_added.keys():
        if os.path.isfile(self.list_of_images_added[key][1]):
            list_of_images.append(self.list_of_images_added[key])

    if len(list_of_images) > 0:
        self.left_panel_image_list.setVisible(True)
        for image in list_of_images:
            item = QListWidgetItem(QIcon(image[0]), None)
            item.setToolTip(image[1])
            label = QLabel('<font style="font-size:12pt; color:black; line-height:50px;">' + image[1].split('/')[-1].rsplit('.', 1)[0] + '</font><br><font style="font-size:8pt; color:silver">' + image[1] + '</font>')
            self.left_panel_image_list.addItem(item)
            self.left_panel_image_list.setItemWidget(item, label)
        self.left_panel_image_path_label.setGeometry(10,10+self.left_panel_image_list.height()+5,self.left_panel_image_list.width()-90,40)
        self.left_panel_image_add.setGeometry(10+self.left_panel_image_list.width()-85,10+self.left_panel_image_list.height()+5,40,40)
        self.left_panel_image_remove.setVisible(True)
    else:
        self.left_panel_image_path_label.setGeometry(10,10+self.left_panel_image_list.height()+5,self.left_panel_image_list.width(),40)
        self.left_panel_image_add.setGeometry(self.left_panel_image_panel.width()*.4,self.left_panel_image_panel.height()*.6,self.left_panel_image_panel.width()*.2,self.left_panel_image_panel.height()*.1)
        self.left_panel_image_alert.setVisible(True)
    self.left_panel_image_edit.setGeometry(self.left_panel_image_path_label.width()-40,0,40,40)

    left_panel_image_list_clear_selection(self)

def left_panel_image_list_clear_selection(self):
    self.left_panel_image_list.setCurrentRow(-1)
    self.left_panel_image_remove.setEnabled(False)

def populate_list_of_images_from_folder(self):
    self.list_of_images_from_folder = {}
    for filename in os.listdir(path_for_images):
        if filename.split('.')[-1] in ['jpg', 'jpeg', 'png', 'JPG', 'PNG', 'JPEG']:
            md5 = hashlib.md5()
            md5.update(open(os.path.join(path_for_images, filename), 'rb').read())
            filename_hash = md5.hexdigest()
            if not os.path.isfile(os.path.join(path_tmp , filename_hash + '.png')):
                create_thumbnail(os.path.join(path_for_images, filename), os.path.join(path_tmp , filename_hash + '.png'),int(56*self.screen_height_proportion), 56)
            icon = os.path.join(path_tmp , filename_hash + '.png')
            #label.setStyleSheet('QLabel { padding-left:5px; } ')
            self.list_of_images_from_folder[filename_hash] = [icon, os.path.join(path_for_images, filename)]

def left_panel_image_edit_clicked(self):
    newpath = QFileDialog.getExistingDirectory(self, "Selecione uma pasta de images", path_home)
    if not newpath == '':
        path_for_images = newpath
        populate_list_of_images_from_folder(self)
        populate_image_list(self)

def left_panel_image_add_clicked(self):
    image_path_list = QFileDialog.getOpenFileNames(self, "Selecione os arquivos de imagem", path_home, "Arquivos de imagem (*.jpg *.jpeg *.png *.pdf)")[0]
    for filename in image_path_list:
        if os.path.isfile(filename):
            icon = False

            filename_hash = False
            if filename.split('.')[-1] in ['jpg', 'jpeg', 'png', 'JPG', 'JPEG', 'PNG']:
                md5 = hashlib.md5()
                md5.update(open(filename, 'rb').read())
                filename_hash = md5.hexdigest()
                if not os.path.isfile(os.path.join(path_tmp , filename_hash + '.png')):
                    create_thumbnail(filename, os.path.join(path_tmp , filename_hash + '.png'), 56*self.screen_height_proportion, 56)
                icon = os.path.join(path_tmp , filename_hash + '.png')
            elif filename.split('.')[-1] in ['pdf']:
                args =  list.copy(gs_executable)
                args += ['-c', '(' + filename + ') (r) file runpdfbegin pdfpagecount = quit']
                gs_count_pages = subprocess.Popen(args, stdout=subprocess.PIPE)
                number_of_pages = int(clean_spaces(gs_count_pages.stdout.read()))
                page_number = QInputDialog.getInteger(self, u'Página do PDF', u'Indique o número da página', value=1, minValue=1, maxValue=number_of_pages)
                if page_number[1] == True:
                    args =  list.copy(gs_executable)
                    args += ['-dTextAlphaBits=4','-dGraphicsAlphaBits=4','-dDOINTERPOLATE']
                    args += ['-sDEVICE=pngalpha']

                    args += ['-dFirstPage=' + str(page_number[0])]
                    args += ['-dLastPage=' + str(page_number[0])]

                    args += ['-g' + str(app.monitor.width()) + 'x' + str(app.monitor.height())]
                    #args += ['-r' + str(target_ratio_width) + 'x' + str(target_ratio_height)]
                    args += ['-sOutputFile=' + os.path.join(path_tmp, filename.split('/')[-1] + '_' + str(page_number[0])  + '.png')]
                    args += [os.path.join(destination_folder,'.' + destination_filename + '-' + str(current_page) + '.ps')]

                    subprocess.call(args)

                    if not transparency:
                        png_page = Image.open(os.path.join(path_tmp, filename.split('/')[-1] + '_' + str(page_number[0])  + '.png'))
                        alpha = png_page.convert('RGBA').split()[-1]

                        png_page_with_background = Image.new("RGBA", (int(app.monitor.width()), int(app.monitor.height())), (255, 255, 255) + (255,))
                        png_page_with_background.paste(png_page, mask=alpha)
                        png_page_with_background.save(os.path.join(path_tmp, filename.split('/')[-1] + '_' + str(page_number[0])  + '.png'))

                    md5 = hashlib.md5()
                    md5.update(open(os.path.join(path_tmp, filename.split('/')[-1] + '_' + str(page_number[0])  + '.png'), 'rb').read())
                    filename_hash = md5.hexdigest()
                    if not os.path.isfile(os.path.join(path_tmp , filename_hash + '.png')):
                        create_thumbnail(os.path.join(path_tmp, filename.split('/')[-1] + '_' + str(page_number[0])  + '.png'), os.path.join(path_tmp , filename_hash + '.png'), 56*self.screen_height_proportion, 56)
                    icon = os.path.join(path_tmp , filename_hash + '.png')

            if icon and filename_hash:
                self.list_of_images_added[filename_hash] = [icon, filename]

    populate_image_list(self)

def left_panel_image_list_clicked(self):
    if self.left_panel_image_list.currentItem():
        self.left_panel_add_title.setText(u'<font style="font-size:14px;"><b>' + u'IMAGEM' + '</b></font><br><font style="font-size:9px;">' + self.left_panel_image_list.currentItem().toolTip() + '</font>')
        self.left_panel_addbutton_label.setText(u'<font style="font-size:14px;"><b>' + u'ADICIONAR' + '</b></font><br><font style="font-size:9px;">' + u'NA LISTA DE EXECUÇÃO' + '</font>')
        self.left_panel_addbutton.setEnabled(True)
        self.left_panel_image_remove.setEnabled(True)

def left_panel_image_tab_changed(self):
    if self.left_panel_image_keyword.text() == '':
        self.left_panel_image_keyword_go.setEnabled(False)
        #self.left_panel_image_webview.setVisible(False)
        self.left_panel_image_webview_warning.setVisible(True)
        self.left_panel_image_webview_preview.setVisible(False)
        self.left_panel_image_webview_preview_cancel_button.setVisible(False)
    else:
        self.left_panel_image_webview_preview_cancel_button_clicked()

def left_panel_image_keyword_go_clicked(self):
    #self.left_panel_image_webview.load(QUrl('https://www.google.com.br/search?q=' + str(self.left_panel_image_keyword.text()).replace(' ', '+') + '&as_sitesearch=jw.org&tbm=isch'))
    #self.left_panel_image_webview.load(QUrl('https://duckduckgo.com/?q=' + self.left_panel_image_keyword.text().replace(' ', '+') + '&sites=jw.org&iar=images'))
    self.left_panel_image_webview_preview_cancel_button_clicked()
    #self.left_panel_image_webview.setVisible(True)
    #self.left_panel_image_webview_warning.setVisible(False)
    #self.left_panel_image_webview.show()
    #self.left_panel_add_title.setText(u'<font style="font-size:14px;"><b>' + u'IMAGEM DO JW.ORG' + '</b></font><br><font style="font-size:9px;">' + self.left_panel_web_address.text() + '</font>')
    #self.left_panel_addbutton_label.setText(u'<font style="font-size:14px;"><b>' + u'ADICIONAR' + '</b></font><br><font style="font-size:9px;">' + u'NA LISTA DE EXECUÇÃO' + '</font>')
    #self.left_panel_addbutton.setEnabled(True)

    #self.left_panel_web_webview.setZoomFactor(zoom)

def left_panel_image_keyword_changed(self):
    if not self.left_panel_image_keyword.text() == '':
        self.left_panel_image_keyword_go.setEnabled(True)
    else:
        self.left_panel_image_keyword_go.setEnabled(False)


def left_panel_image_keyword_load_finished(self):
    print('loading...')
    image_address = False
    page_html = False

    #self.left_panel_image_webview.page().runJavaScript("var links = document.getElementsByTagName('a');for (var i=0, len=links.length; i < len; i ++) {  links[i].target = '_self';}")

    #print(self.left_panel_image_webview.page().title())
    #print(self.left_panel_image_webview.page().url())
    #page_html = self.left_panel_image_webview.toHtml()

    if page_html:
        if '<title>Access Denied</title>' in page_html.toHtml():
            image_address = page_html.toHtml().split('"')[1].split('"')[0]
        elif len(page_html.toHtml().split('<img ')) < 3  and ' src="' in page_html.toHtml():
            image_address = page_html.toHtml().split('<img ')[1].split(' src="')[1].split('"')[0]

    if image_address:
        image = urllib.request.urlopen(image_address)
        open(os.path.join(path_tmp, 'image_from_jworg.jpg'), 'wb').write(image.read())
        md5 = hashlib.md5()
        md5.update(open(os.path.join(path_tmp, 'image_from_jworg.jpg'), 'rb').read())
        filename_hash = md5.hexdigest()

        create_thumbnail(os.path.join(path_tmp, 'image_from_jworg.jpg'), os.path.join(path_tmp, filename_hash + '.png'), int(app.monitor.width()), int(app.monitor.height()))

        #self.left_panel_image_webview.setVisible(False)
        self.left_panel_image_webview_preview.setPixmap(QPixmap(os.path.join(path_tmp, filename_hash + '.png')).scaled(self.left_panel_image_webview_preview.width(), self.left_panel_image_webview_preview.height(), Qt.KeepAspectRatio, Qt.SmoothTransformation))
        self.left_panel_image_webview_preview.setToolTip(filename_hash)
        self.left_panel_add_title.setText(u'<font style="font-size:14px;"><b>' + u'IMAGEM' + '</b></font><br><font style="font-size:9px;">do jw.org (' + self.left_panel_image_keyword.text().lower() + ')</font>')
        self.left_panel_addbutton_label.setText(u'<font style="font-size:14px;"><b>' + u'ADICIONAR' + '</b></font><br><font style="font-size:9px;">' + u'NA LISTA DE EXECUÇÃO' + '</font>')
        self.left_panel_addbutton.setEnabled(True)
        self.left_panel_image_webview_preview.setVisible(True)

def left_panel_image_webview_preview_cancel_button_clicked(self):
    self.left_panel_add_title.setText(u'IMAGEM')
    self.left_panel_addbutton.setEnabled(False)
    #if self.left_panel_image_webview.page().history().canGoBack():
    #    self.left_panel_image_webview.back()
    #self.left_panel_image_webview.setVisible(True)
    self.left_panel_image_webview_preview.setVisible(False)
    self.left_panel_image_webview_warning.setVisible(False)

############################################################################
## ADDITION PANEL - VIDEO

def populate_video_song_list(self):
    self.left_panel_video_song_list.clear()
    self.left_panel_video_song_alert.setVisible(False)
    self.left_panel_video_song_list.setVisible(False)
    self.left_panel_video_song_remove.setVisible(False)

    existing_files = False
    if len(self.list_of_video_songs.keys()) > 0:
        for key in sorted(self.list_of_video_songs.keys()):
            if os.path.isfile(self.list_of_video_songs[key][1]):
                if not existing_files:
                    existing_files = True
                label = QLabel(str(key))
                label.setGeometry(0,0,48,48)
                label.setAlignment(Qt.AlignCenter)
                label.setStyleSheet('QLabel {background-color:silver; font-family: "Ubuntu Condensed"; font-size:28px; color:white;}')
                item = QListWidgetItem(QIcon(QPixmap().grabWidget(label)), None)
                item.setToolTip(str(key))
                self.left_panel_video_song_list.addItem(item)

    if existing_files:
        self.left_panel_video_song_list.setVisible(True)
        self.left_panel_video_song_add.setGeometry(10+self.left_panel_video_song_list.width()-85,10+self.left_panel_video_song_list.height()+5,40,40)
        self.left_panel_video_song_remove.setVisible(True)
    else:
        self.left_panel_video_song_alert.setVisible(True)
        self.left_panel_video_song_add.setGeometry(self.left_panel_video_tab_songs.width()*.4,self.left_panel_video_tab_songs.height()*.6,self.left_panel_video_tab_songs.width()*.2,self.left_panel_video_tab_songs.height()*.1)


def left_panel_video_song_list_clicked(self):
    if self.left_panel_video_song_list.currentItem():
        self.left_panel_add_title.setText(u'<font style="font-size:14px;"><b>' + u'CÂNTICO '+ str(self.left_panel_video_song_list.currentItem().toolTip()) + '</b></font><br><font style="font-size:9px;">' + self.list_of_video_songs[int(self.left_panel_video_song_list.currentItem().toolTip())][0] + '</font>')
        self.left_panel_addbutton_label.setText(u'<font style="font-size:14px;"><b>' + u'ADICIONAR' + '</b></font><br><font style="font-size:9px;">' + u'NA LISTA DE EXECUÇÃO' + '</font>')
        self.left_panel_addbutton.setEnabled(True)
        self.left_panel_video_song_remove.setEnabled(True)

def left_panel_video_song_add_clicked(self):
    song_path_list = QFileDialog.getOpenFileNames(self, u"Selecione os arquivos de vídeo", path_home, "Arquivos de audio (*.mp4)")[0]
    for song_path_file in song_path_list:
        if os.path.isfile(song_path_file):
            song = MP4(song_path_file)
            title = song_path_file.split('/')[-1]
            if '\xa9nam' in  song.tags.keys():
                title = song.tags['\xa9nam'][0]

            number = get_number(title)

            if number:
                if number in title:
                    title = title.replace('_', ' ').split(number, 1)[1].split(' ', 1)[1]
                self.list_of_video_songs[int(number)] = [title, song_path_file]


    populate_video_song_list(self)

def left_panel_video_song_remove_clicked(self):
    del self.list_of_video_songs[int(self.left_panel_video_song_list.currentItem().toolTip())]
    populate_video_song_list(self)


def populate_video_local_list(self):
    self.left_panel_video_local_list.clear()
    self.left_panel_video_local_alert.setVisible(False)
    self.left_panel_video_local_list.setVisible(False)
    self.left_panel_video_remove.setVisible(False)
    self.left_panel_video_path_label.setText(u'<font style="font-size:9px;color:gray;">' + u'EXIBINDO VÍDEOS DA PASTA' + '</font><br><font style="font-size:14px;color:gray;">' + path_for_videos + '</font>')
    self.left_panel_video_edit.setVisible(False)

    self.list_of_local_videos = {}

    for key in self.list_of_videos_from_folder.keys():
        if os.path.isfile(self.list_of_videos_from_folder[key][1]):
            self.list_of_local_videos[key] = self.list_of_videos_from_folder[key]

    for key in self.list_of_videos_added.keys():
        if os.path.isfile(self.list_of_videos_added[key][1]):
            self.list_of_local_videos[key] = self.list_of_videos_added[key]

    if len(self.list_of_local_videos.keys()) > 0:
        self.left_panel_video_local_list.setVisible(True)
        for hashcode in self.list_of_local_videos.keys():
            item = QListWidgetItem(QIcon(self.list_of_local_videos[hashcode][0]), None)
            item.setToolTip(hashcode)

            if self.list_of_local_videos[hashcode][2]:
                title = self.list_of_local_videos[hashcode][2]
            else:
                title = self.list_of_local_videos[hashcode][1].split('/')[-1].rsplit('.', 1)[0]

            label = QLabel('<font style="font-size:12pt; color:black; line-height:50px;">' + title + '</font><br><font style="font-size:8pt; color:silver">' + self.list_of_local_videos[hashcode][1] + '</font>')
            self.left_panel_video_local_list.addItem(item)
            self.left_panel_video_local_list.setItemWidget(item, label)
        self.left_panel_video_path_label.setGeometry(10,10+self.left_panel_video_local_list.height()+5,self.left_panel_video_local_list.width()-90,40)
        self.left_panel_video_add.setGeometry(10+self.left_panel_video_local_list.width()-85,10+self.left_panel_video_local_list.height()+5,40,40)
        self.left_panel_video_remove.setVisible(True)
    else:
        self.left_panel_video_path_label.setGeometry(10,10+self.left_panel_video_local_list.height()+5,self.left_panel_video_local_list.width(),40)
        self.left_panel_video_add.setGeometry(self.left_panel_video_panel.width()*.4,self.left_panel_video_panel.height()*.6,self.left_panel_video_panel.width()*.2,self.left_panel_video_panel.height()*.1)
        self.left_panel_video_local_alert.setVisible(True)
    self.left_panel_video_edit.setGeometry(self.left_panel_video_path_label.width()-40,0,40,40)

    left_panel_video_local_list_clear_selection(self)

def left_panel_video_tab_changed(self):
    None






def populate_video_bible_list(self):
    self.left_panel_video_bible_alert.setVisible(False)
    self.left_panel_video_bible_list.setVisible(False)

    path_for_bible_videos = path_home
    #if os.path.isfile(os.path.join(path_home, '.config', 'user-dirs.dirs')):
    #    filecontent = open(os.path.join(os.path.join(path_home, '.config', 'user-dirs.dirs'))).read()
    #    if 'XDG_VIDEOS_DIR=' in filecontent:
    #        if os.path.isdir(os.path.join(filecontent.split('XDG_VIDEOS_DIR=',1)[1].split('"')[1].replace('$HOME', path_home))):
    #            path_for_bible_videos = os.path.join(filecontent.split('XDG_VIDEOS_DIR=',1)[1].split('"')[1].replace('$HOME', path_home))

    #self.list_of_bible_videos = {}
    list_of_bible_videos_filenames = []

    for verse in self.list_of_bible_videos.values():
        list_of_bible_videos_filenames.append(verse[0])

    for root, dirs, files in os.walk(path_for_bible_videos):
        for name in files:
            if name.endswith('.m4v'):
                if not unicode(os.path.join(root, name), 'utf-8') in list_of_bible_videos_filenames:
                    video_chapters_info = unicode(subprocess.Popen([ffprobe_bin,'-loglevel', 'error',  '-show_chapters', '-print_format', 'xml', os.path.join(root, name)], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE).stdout.read(), 'utf-8')
                    for chapter in video_chapters_info.split('<chapter '):
                        chapter_name = False
                        start = False
                        end = False
                        if 'key="title"' in chapter and 'value="' in chapter.split('key="title"')[1]:
                            chapter_name_from_source = chapter.split('key="title"')[1].split('value="')[1].split('"')[0]
                            if ' ' in chapter_name_from_source and ':' in chapter_name_from_source:
                                chapter_name = chapter_name_from_source
                        if 'start_time="' in chapter:
                            start = float(chapter.split('start_time="')[1].split('"')[0])
                        if 'end_time="' in chapter:
                            end = float(chapter.split('end_time="')[1].split('"')[0])
                        if chapter_name and start and end:
                            self.list_of_bible_videos[chapter_name] = [os.path.join(root, name), start, end]

    populate_video_bible_books_list(self)

    if self.left_panel_video_bible_list.count() > 0:
        self.left_panel_video_bible_list.setVisible(True)
    else:
        self.left_panel_video_bible_alert.setVisible(True)

    left_panel_video_bible_list_clear_selection(self)

def populate_video_bible_books_list(self):
    self.left_panel_video_bible_list.clear()
    self.left_panel_video_bible_list.setIconSize(QSize(96, 48))

    self.left_panel_video_bible_selected_book = False
    self.left_panel_video_bible_selected_chapter = False
    self.left_panel_video_bible_selected_verse = False
    self.left_panel_video_bible_search.setVisible(True)
    self.left_panel_video_bible_book_label.setVisible(False)
    self.left_panel_video_bible_chapter_label.setVisible(False)
    self.left_panel_video_bible_verse_label.setVisible(False)

    list_of_books = []
    for key in self.list_of_bible_videos.keys():
        book = key.split(' ')[0]
        if not book in list_of_books:
            list_of_books.append(book)

    for book in list_of_books:
        label = QLabel(book)
        label.setGeometry(0,0,96,48)
        label.setStyleSheet('QLabel {background-color:gray; font-family: "Ubuntu Condensed"; font-size:28px; color:white;  qproperty-alignment:"AlignCenter";}')
        item = QListWidgetItem(QIcon(QPixmap().grabWidget(label)), None)
        item.setToolTip(book)
        if self.left_panel_video_bible_search.text().lower() == '' or self.left_panel_video_bible_search.text().lower() in book.lower():
            self.left_panel_video_bible_list.addItem(item)

    #self.left_panel_video_bible_list_clear_selection()

def populate_video_bible_chapters_list(self):
    self.left_panel_video_bible_list.clear()
    self.left_panel_video_bible_list.setIconSize(QSize(48, 48))

    self.left_panel_video_bible_selected_chapter = False
    self.left_panel_video_bible_selected_verse = False

    self.left_panel_video_bible_book_label.setVisible(True)
    self.left_panel_video_bible_book_label.setText(self.left_panel_video_bible_selected_book)
    self.left_panel_video_bible_chapter_label.setVisible(False)
    self.left_panel_video_bible_verse_label.setVisible(False)

    list_of_chapters = []
    for key in self.list_of_bible_videos.keys():
        if self.left_panel_video_bible_selected_book in key:
            chapter = int(key.split(' ')[1].split(':')[0])
            if not chapter in list_of_chapters:
                list_of_chapters.append(chapter)

    for chapter in sorted(list_of_chapters):
        label = QLabel(str(chapter))
        label.setGeometry(0,0,48,48)
        label.setAlignment(Qt.AlignCenter)
        label.setStyleSheet('QLabel {background-color:silver; font-family: "Ubuntu Condensed"; font-size:28px; color:white;}')
        item = QListWidgetItem(QIcon(QPixmap().grabWidget(label)), None)
        item.setToolTip(str(chapter))
        self.left_panel_video_bible_list.addItem(item)

def populate_video_bible_verses_list(self):
    self.left_panel_video_bible_list.clear()
    self.left_panel_video_bible_list.setIconSize(QSize(48, 48))

    self.left_panel_video_bible_selected_verse = False

    self.left_panel_video_bible_chapter_label.setVisible(True)
    self.left_panel_video_bible_chapter_label.setText(self.left_panel_video_bible_selected_chapter)
    self.left_panel_video_bible_verse_label.setVisible(False)

    list_of_verses = []
    for key in self.list_of_bible_videos.keys():
        if self.left_panel_video_bible_selected_book in key:
            if ' ' + self.left_panel_video_bible_selected_chapter + ':' in key:
                verse = int(key.split(':')[-1])
                if not verse in list_of_verses:
                    list_of_verses.append(verse)

    for verse in sorted(list_of_verses):
        label = QLabel(str(verse))
        label.setGeometry(0,0,48,48)
        label.setStyleSheet('QLabel {background-color:silver; font-family: "Ubuntu Condensed"; font-size:28px; color:white; qproperty-alignment:"AlignCenter"; }')
        item = QListWidgetItem(QIcon(QPixmap().grabWidget(label)), None)
        item.setToolTip(str(verse))
        self.left_panel_video_bible_list.addItem(item)

    #self.left_panel_video_bible_list_clear_selection()

def left_panel_video_bible_list_clicked(self):
    self.left_panel_video_library_search.setVisible(False)
    self.left_panel_add_title.setText(u'<font style="font-size:14px;"><b>' + u'VÍDEO' + '</b></font>')
    self.left_panel_addbutton_label.setText(u'<font style="font-size:9px;">' + u'SELECIONE ALGO PARA' + '</font><br><font style="font-size:14px;"><b>' + u'ADICIONAR' + '</b></font>')
    self.left_panel_addbutton.setEnabled(True)

    self.left_panel_video_bible_search.setVisible(False)
    if self.left_panel_video_bible_list.currentItem():
        if not self.left_panel_video_bible_selected_book:
            self.left_panel_video_bible_selected_book = self.left_panel_video_bible_list.currentItem().toolTip()
            populate_video_bible_chapters_list(self)
        elif not self.left_panel_video_bible_selected_chapter:
            self.left_panel_video_bible_selected_chapter = self.left_panel_video_bible_list.currentItem().toolTip()
            populate_video_bible_verses_list(self)
        else:#if not self.left_panel_video_bible_selected_verse:
            self.left_panel_video_bible_selected_verse = self.left_panel_video_bible_list.currentItem().toolTip()
            self.left_panel_video_bible_verse_label.setVisible(True)
            self.left_panel_video_bible_verse_label.setText(self.left_panel_video_bible_selected_verse)

            self.left_panel_add_title.setText(u'<font style="font-size:14px;"><b>' + u'VÍDEO' + '</b></font><br><font style="font-size:9px;">' + self.left_panel_video_bible_selected_book + u' ' + self.left_panel_video_bible_selected_chapter + u':' + self.left_panel_video_bible_selected_verse + '</font>')
            self.left_panel_addbutton_label.setText(u'<font style="font-size:14px;"><b>' + u'ADICIONAR' + '</b></font><br><font style="font-size:9px;">' + u'NA LISTA DE EXECUÇÃO' + '</font>')
            self.left_panel_addbutton.setEnabled(True)

    self.left_panel_video_bible_list_clear_selection()

def left_panel_video_bible_list_clear_selection(self):
    #self.left_panel_video_bible_list.setCurrentRow(-1)
    self.left_panel_video_bible_list.clearSelection()

def populate_video_library_list(self):
    self.left_panel_video_library_alert.setVisible(False)
    self.left_panel_video_library_list.setVisible(False)

    path_for_library_videos = path_home
    #if os.path.isfile(os.path.join(path_home, '.config', 'user-dirs.dirs')):
    #    filecontent = open(os.path.join(os.path.join(path_home, '.config', 'user-dirs.dirs'))).read()
    #    if 'XDG_VIDEOS_DIR=' in filecontent:
    #        if os.path.isdir(os.path.join(filecontent.split('XDG_VIDEOS_DIR=',1)[1].split('"')[1].replace('$HOME', path_home))):
    #            path_for_library_videos = os.path.join(filecontent.split('XDG_VIDEOS_DIR=',1)[1].split('"')[1].replace('$HOME', path_home))

    #self.list_of_library_videos = {}
    list_of_library_videos_filenames = []

    for album in self.list_of_library_videos.keys():
        for filename in self.list_of_library_videos[album].keys():
            list_of_library_videos_filenames.append(filename)


    for root, dirs, files in os.walk(path_for_library_videos):
        for name in files:
            if name.endswith('.m4v'):
                if not unicode(os.path.join(root, name), 'utf-8') in list_of_library_videos_filenames:
                    video_chapters_info = unicode(subprocess.Popen([ffprobe_bin,'-loglevel', 'error', '-show_format', '-show_chapters', '-print_format', 'xml', os.path.join(root, name)], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE).stdout.read(), 'utf-8')

                    title = False
                    album = False

                    try:
                        video = MP4(os.path.join(root, name))
                        if '\xa9nam' in  [*video.tags]:
                            title = video.tags['\xa9nam'][0]
                        if '\xa9alb' in  [*video.tags]:
                            album = video.tags['\xa9alb'][0]
                        if 'covr' in  [*video.tags]:
                            artwork = video.tags["covr"][0]
                            open(os.path.join(path_tmp, 'thumbnail.png'), 'wb').write(artwork)
                    except:
                        if 'key="title"' in video_chapters_info:
                            title = video_chapters_info.split('key="title"')[1].split('value="')[1].split('"')[0]
                        if 'key="album"' in video_chapters_info:
                            album = video_chapters_info.split('key="album"')[1].split('value="')[1].split('"')[0]
                        if 'nb_streams="' in video_chapters_info:
                            stream = '0:' + str(int(video_chapters_info.split('nb_streams="')[1].split('"')[0])-1)
                            subprocess.call([ffmpeg_bin, '-loglevel', 'error', '-y', '-i', os.path.join(root, name), '-map', stream, '-c:v', 'copy', os.path.join(path_tmp, 'thumbnail.png')])

                    thumbnail = False
                    if os.path.isfile(os.path.join(path_tmp, 'thumbnail.png')):
                        thumbnail = QPixmap(os.path.join(path_tmp, 'thumbnail.png'))

                    if album and not album in self.list_of_library_videos.keys():
                        self.list_of_library_videos[album] = {}

                    chapters = {}
                    for chapter in video_chapters_info.split('<chapter '):

                        chapter_name = False
                        start = False
                        end = False

                        if 'key="title"' in chapter and 'value="' in chapter.split('key="title"')[1]:
                            chapter_name = chapter.split('key="title"')[1].split('value="')[1].split('"')[0]
                        if 'start_time="' in chapter:
                            start = float(chapter.split('start_time="')[1].split('"')[0])
                        if 'end_time="' in chapter:
                            end = float(chapter.split('end_time="')[1].split('"')[0])

                        if chapter_name and start and end:
                            chapters[chapter_name] = [start, end]

                    if title and thumbnail:
                        self.list_of_library_videos[album][unicode(os.path.join(root, name), 'utf-8')] = [title,chapters,thumbnail]

    populate_video_library_albums_list(self)

    if self.left_panel_video_library_list.count() > 0:
        self.left_panel_video_library_list.setVisible(True)
    else:
        self.left_panel_video_library_alert.setVisible(True)

    left_panel_video_library_list_clear_selection(self)

def populate_video_library_albums_list(self):
    self.left_panel_video_library_list.clear()
    self.left_panel_video_library_list.setIconSize(QSize(96, 96))

    self.left_panel_video_library_selected_album = False
    self.left_panel_video_library_selected_title = False
    self.left_panel_video_library_selected_chapter = False
    self.left_panel_video_library_search.setVisible(True)
    self.left_panel_video_library_album_label.setVisible(False)
    self.left_panel_video_library_title_label.setVisible(False)
    self.left_panel_video_library_chapter_label.setVisible(False)

    for album in sorted(self.list_of_library_videos.keys()):
        list_filenames = []
        for filename in self.list_of_library_videos[album].keys():
            list_filenames.append([filename, self.list_of_library_videos[album][filename][-1]])
        label = QLabel()
        label.setScaledContents(True)
        label.setPixmap(sorted(list_filenames)[0][1])
        label.setGeometry(0,0,96,96)
        #label.setStyleSheet('QLabel {background-color:gray; font-family: "Ubuntu Condensed"; font-size:28px; color:white;  qproperty-alignment:"AlignCenter";}')
        item = QListWidgetItem(QIcon(QPixmap().grabWidget(label)), album)
        item.setToolTip(album)
        if self.left_panel_video_library_search.text().lower() == '' or self.left_panel_video_library_search.text().lower() in album.lower():
            self.left_panel_video_library_list.addItem(item)

    #self.left_panel_video_library_list_clear_selection()

def populate_video_library_titles_list(self):
    self.left_panel_video_library_list.clear()
    self.left_panel_video_library_list.setIconSize(QSize(64, 64))

    self.left_panel_video_library_selected_title = False
    self.left_panel_video_library_selected_chapter = False

    self.left_panel_video_library_album_label.setVisible(True)
    self.left_panel_video_library_album_label.setPixmap(self.list_of_library_videos[self.left_panel_video_library_selected_album][sorted(self.list_of_library_videos[self.left_panel_video_library_selected_album].keys())[0]][-1])
    self.left_panel_video_library_title_label.setVisible(False)
    self.left_panel_video_library_chapter_label.setVisible(False)

    for title in sorted(self.list_of_library_videos[self.left_panel_video_library_selected_album].keys()):
        label = QLabel()
        label.setScaledContents(True)
        label.setPixmap(self.list_of_library_videos[self.left_panel_video_library_selected_album][title][-1])
        label.setGeometry(0,0,64,64)
        item = QListWidgetItem(QIcon(QPixmap().grabWidget(label)), self.list_of_library_videos[self.left_panel_video_library_selected_album][title][0])
        item.setToolTip(title)
        self.left_panel_video_library_list.addItem(item)

def populate_video_library_chapters_list(self):
    self.left_panel_video_library_list.clear()
    self.left_panel_video_library_list.setIconSize(QSize(96, 48))

    self.left_panel_video_library_selected_chapter = False

    self.left_panel_video_library_title_label.setVisible(True)

    self.left_panel_video_library_title_label.setPixmap(self.list_of_library_videos[self.left_panel_video_library_selected_album][self.left_panel_video_library_selected_title][-1])
    self.left_panel_video_library_chapter_label.setVisible(False)

    label = QLabel(u'▬')
    label.setGeometry(0,0,48,48)
    label.setStyleSheet('QLabel {background-color:silver; font-family: "Ubuntu Condensed"; font-size:28px; color:white; qproperty-alignment:"AlignCenter"; }')
    first_item = QListWidgetItem(QIcon(QPixmap().grabWidget(label)), u'Todo o vídeo')
    first_item.setToolTip(u'all')
    self.left_panel_video_library_list.addItem(first_item)

    for value in sorted(self.list_of_library_videos[self.left_panel_video_library_selected_album][self.left_panel_video_library_selected_title][1].values()):
        for chapter in self.list_of_library_videos[self.left_panel_video_library_selected_album][self.left_panel_video_library_selected_title][1].keys():
            if value == self.list_of_library_videos[self.left_panel_video_library_selected_album][self.left_panel_video_library_selected_title][1][chapter]:
                label = QLabel(u'🠷')
                label.setGeometry(0,0,48,48)
                label.setStyleSheet('QLabel {background-color:silver; font-family: "Ubuntu Condensed"; font-size:28px; color:white; qproperty-alignment:"AlignCenter"; }')
                item = QListWidgetItem(QIcon(QPixmap().grabWidget(label)), chapter)
                item.setToolTip(chapter)
                self.left_panel_video_library_list.addItem(item)
                break

def left_panel_video_library_list_clicked(self):
    self.left_panel_video_library_search.setVisible(False)
    self.left_panel_add_title.setText(u'<font style="font-size:14px;"><b>' + u'VÍDEO' + '</b></font>')
    self.left_panel_addbutton_label.setText(u'<font style="font-size:9px;">' + u'SELECIONE ALGO PARA' + '</font><br><font style="font-size:14px;"><b>' + u'ADICIONAR' + '</b></font>')
    self.left_panel_addbutton.setEnabled(True)

    if self.left_panel_video_library_list.currentItem():
        if not self.left_panel_video_library_selected_album:
            self.left_panel_video_library_selected_album = self.left_panel_video_library_list.currentItem().toolTip()
            populate_video_library_titles_list(self)
        elif not self.left_panel_video_library_selected_title:
            self.left_panel_video_library_selected_title = self.left_panel_video_library_list.currentItem().toolTip()
            populate_video_library_chapters_list(self)
        else:#if not self.left_panel_video_library_selected_chapter:
            self.left_panel_video_library_selected_chapter = self.left_panel_video_library_list.currentItem().toolTip()
            self.left_panel_video_library_chapter_label.setVisible(True)
            self.left_panel_video_library_chapter_label.setText(self.left_panel_video_library_selected_chapter)

            self.left_panel_add_title.setText(u'<font style="font-size:14px;"><b>' + u'VÍDEO' + '</b></font><br><font style="font-size:9px;">' + self.left_panel_video_library_selected_title + '</font>')
            self.left_panel_addbutton_label.setText(u'<font style="font-size:14px;"><b>' + u'ADICIONAR' + '</b></font><br><font style="font-size:9px;">' + u'NA LISTA DE EXECUÇÃO' + '</font>')
            self.left_panel_addbutton.setEnabled(True)

    self.left_panel_video_library_list_clear_selection()

def left_panel_video_library_list_clear_selection(self):
    #self.left_panel_video_library_list.setCurrentRow(-1)
    self.left_panel_video_library_list.clearSelection()

def left_panel_video_local_list_clear_selection(self):
    self.left_panel_video_local_list.setCurrentRow(-1)
    self.left_panel_video_remove.setEnabled(False)

def populate_list_of_videos_from_folder(self):
    self.list_of_videos_from_folder = {}
    for filename in os.listdir(path_for_videos):

        if os.path.isfile(os.path.join(path_for_videos, filename)) and  filename.split('.')[-1] in ['m4v', 'mp4', 'M4V', 'MP4']:
            md5 = hashlib.md5()
            md5.update(open(os.path.join(path_for_videos, filename), 'rb').read())
            filename_hash = md5.hexdigest()
            title = filename
            if not os.path.isfile(os.path.join(path_tmp , filename_hash + '.png')):
                video = MP4(os.path.join(path_for_videos, filename))

                if video.tags and 'covr' in [*video.tags]:
                    artwork = video.tags["covr"][0]
                    open(os.path.join(path_tmp, filename + '.png'), 'wb').write(artwork)
                else:
                    subprocess.call([ffmpeg_bin, '-loglevel', 'error', '-y', '-ss', '00:10.00', '-i', os.path.join(path_for_videos, filename), '-frames:v', '1', os.path.join(path_tmp, filename + '.png')])

                create_thumbnail(os.path.join(path_tmp, filename + '.png'), os.path.join(path_tmp , filename_hash + '.png'), int(56*self.screen_height_proportion), 56)

                if not video.tags == None and '\xa9nam' in  [*video.tags]:
                    title = video.tags['\xa9nam'][0]

            icon = os.path.join(path_tmp , filename_hash + '.png')
            #label.setStyleSheet('QLabel { padding-left:5px; } ')
            self.list_of_videos_from_folder[filename_hash] = [icon, os.path.join(path_for_videos, filename), title]

def left_panel_video_edit_clicked(self):
    newpath = QFileDialog.getExistingDirectory(self, "Selecione uma pasta de images", path_home)
    if not newpath == '':
        path_for_videos = newpath
        populate_list_of_videos_from_folder(self)
        populate_video_local_list(self)

def left_panel_video_add_clicked(self):
    image_path_list = QFileDialog.getOpenFileNames(self, "Selecione os arquivos de vídeo", path_home, "Arquivos de vídeo (*.m4v *.mp4 *.M4V *.MP4)")[0]
    for filename in image_path_list:
        if os.path.isfile(filename):
            icon = False
            filename_hash = False
            title = False
            if filename.split('.')[-1] in ['m4v', 'mp4', 'M4V', 'MP4']:
                md5 = hashlib.md5()
                md5.update(open(filename, 'rb').read())
                filename_hash = md5.hexdigest()
                if not os.path.isfile(os.path.join(path_tmp , filename_hash + '.png')):
                    video = MP4(filename)
                    print(video)

                    if video.tags and 'covr' in [*video.tags]:
                        artwork = video.tags["covr"][0]
                        open(os.path.join(path_tmp, filename.split('/')[-1] + '.png'), 'wb').write(artwork)
                    else:
                        subprocess.call([ffmpeg_bin, '-loglevel', 'error', '-y', '-ss', '00:10.00', '-i', filename, '-frames:v', '1', os.path.join(path_tmp, filename.split('/')[-1] + '.png')])

                    create_thumbnail(os.path.join(path_tmp, filename.split('/')[-1] + '.png'), os.path.join(path_tmp , filename_hash + '.png'),int(56*self.screen_height_proportion), 56)

                    if '\xa9nam' in  [*video.tags]:
                        title = video.tags['\xa9nam'][0]

                icon = os.path.join(path_tmp , filename_hash + '.png')

            if icon and filename_hash:
                self.list_of_videos_added[filename_hash] = [icon, filename, title]

    populate_video_local_list(self)

def left_panel_video_local_list_clicked(self):
    if self.left_panel_video_local_list.currentItem():
        self.left_panel_add_title.setText(u'<font style="font-size:14px;"><b>' + u'VÍDEO' + '</b></font><br><font style="font-size:9px;">' + self.list_of_local_videos[self.left_panel_video_local_list.currentItem().toolTip()][1] + '</font>')
        self.left_panel_addbutton_label.setText(u'<font style="font-size:14px;"><b>' + u'ADICIONAR' + '</b></font><br><font style="font-size:9px;">' + u'NA LISTA DE EXECUÇÃO' + '</font>')
        self.left_panel_addbutton.setEnabled(True)
        self.left_panel_video_remove.setEnabled(True)




############################################################################
## ADDITION PANEL - WEB

def left_panel_web_address_go_clicked(self):
    #self.left_panel_web_webview.load(QUrl(self.left_panel_web_address.text()))
    #self.left_panel_web_webview.show()
    self.left_panel_add_title.setText(u'<font style="font-size:14px;"><b>' + u'PÁGINA DO JW.ORG' + '</b></font><br><font style="font-size:9px;">' + self.left_panel_web_address.text() + '</font>')
    self.left_panel_addbutton_label.setText(u'<font style="font-size:14px;"><b>' + u'ADICIONAR' + '</b></font><br><font style="font-size:9px;">' + u'NA LISTA DE EXECUÇÃO' + '</font>')
    self.left_panel_addbutton.setEnabled(True)

    #self.left_panel_web_webview.setZoomFactor(zoom)


############################################################################
## ADDITION PANEL - SCREENCOPY

def left_panel_screencopy_take_button_clicked(self):
    app.screencopy.show()

############################################################################
## ADDITION PANEL - CAMERA

def populate_camera_list(self):
    self.list_of_cameras = {}
    cameras_output = subprocess.Popen(['v4l2-ctl', '--list-devices'], stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT).stdout.read()

    for camera in cameras_output.replace(':\n\t', ':').split('\n'):
        if '):/' in camera:
            device = camera.split('):',1)[1].split('\n',1)[0]
            if len(self.playlist) > 0:
                for item in self.playlist:
                    if item[0] == 'camera' and item[2] == device:
                        device = False
                        break
            if device:
                name = camera.split('(',1)[0]
                self.list_of_cameras[device] = [name]

    self.left_panel_camera_select.clear()

    for camera in self.list_of_cameras.keys():
        self.left_panel_camera_select.addItem(self.list_of_cameras[camera][0])

def left_panel_camera_select_selected(self):
    self.left_panel_camera_test_button.setEnabled(True)

def left_panel_camera_test_button_clicked(self):
    selected_camera = False

    for camera in self.list_of_cameras.keys():
        if self.left_panel_camera_select.currentText() == self.list_of_cameras[camera][0]:
            selected_camera = camera
            break

    if selected_camera:
        self.left_panel_camera_preview_mediaplayer.stop()
        self.left_panel_camera_preview_mediaplayer.set_media(video_instance.media_new('v4l2://' + selected_camera))
        self.left_panel_camera_preview_mediaplayer.play()

        self.left_panel_add_title.setText(u'<font style="font-size:14px;"><b>' + u'CAMERA' + '</b></font><br><font style="font-size:9px;">' + self.left_panel_camera_select.currentText() + '</font>')
        self.left_panel_addbutton_label.setText(u'<font style="font-size:14px;"><b>' + u'ADICIONAR' + '</b></font><br><font style="font-size:9px;">' + u'NA LISTA DE EXECUÇÃO' + '</font>')
        self.left_panel_addbutton.setEnabled(True)
