#!/usr/bin/python3

import os

from PySide2.QtWidgets import QWidget, QDesktopWidget, QLabel, QGraphicsOpacityEffect, QFrame
from PySide2.QtCore import Qt, QPropertyAnimation, QEasingCurve, QUrl, QCoreApplication
from PySide2.QtGui import QIcon, QPalette, QFontMetrics, QPixmap, QColor

from modules.paths import *
from modules.effect import generate_effect
from modules import monitor_image
from modules import monitor_yeartext
from modules import monitor_video

def load(self):
    class monitor_window(QWidget):
        def closeEvent(self, event):
            QCoreApplication.instance().quit()

    self.monitor_window = monitor_window(parent=None)
    self.monitor_window.setWindowFlags(Qt.Window)
    self.monitor_window.setWindowIcon(QIcon(os.path.join(path_graphics, 'kihvim.png')))

    if QDesktopWidget().screenCount() > 1:
        window_width = QDesktopWidget().screenGeometry(1).width()
        window_height = QDesktopWidget().screenGeometry(1).height()
        self.monitor_window.setWindowFlags(self.monitor_window.windowFlags() | Qt.WindowStaysOnTopHint | Qt.FramelessWindowHint | Qt.X11BypassWindowManagerHint)
        self.monitor_window.move(QDesktopWidget().screenGeometry(1).left(), QDesktopWidget().screenGeometry(1).top())

    else:
        window_width = 640
        window_height = 480
        self.monitor_window.setWindowFlags(self.monitor_window.windowFlags() | Qt.WindowStaysOnTopHint )

    self.monitor_window.resize(window_width, window_height)
    self.monitor_window.setMaximumSize(self.monitor_window.width(), self.monitor_window.height())
    self.monitor_window.setMinimumSize(self.monitor_window.width(), self.monitor_window.height())

    self.monitor_window.setCursor(Qt.BlankCursor)

    self.monitor_window.black_palette = self.palette()
    self.monitor_window.black_palette.setColor(QPalette.Window, QColor(0, 0, 0))
    self.monitor_window.setPalette(self.monitor_window.black_palette)
    self.monitor_window.setAutoFillBackground(True)

    self.monitor_window.setWindowTitle('kihvim monitor')

    # self.video_mediaplayer = video_instance.media_player_new()

    self.test_widget = QLabel(u'⊡', parent=self.monitor_window)
    self.test_widget.setGeometry(0, 0, self.monitor_window.width(), self.monitor_window.height())
    self.test_widget.setStyleSheet('QLabel {font-family: "Ubuntu"; font-size:' + str(self.monitor_window.height()*.04) + 'pt; color:white; qproperty-alignment: "AlignCenter";}')
    self.test_widget_opacity = QGraphicsOpacityEffect()
    self.test_widget.setGraphicsEffect(self.test_widget_opacity)
    self.test_widget_opacity_animation = QPropertyAnimation(self.test_widget_opacity, b'opacity')
    self.test_widget_opacity.setOpacity(0.0)

    monitor_yeartext.load(self)

    monitor_image.load(self)

    self.audio_widget = QWidget(parent=self.monitor_window)
    self.audio_widget.setGeometry(0, 0, self.monitor_window.width(), self.monitor_window.height())

    self.audio_widget_lyrics = QLabel(parent=self.audio_widget)
    self.audio_widget_lyrics.setGeometry(self.audio_widget.width()*.1, self.audio_widget.height(),self.audio_widget.width()*.8, self.audio_widget.height())
    self.audio_widget_lyrics.setAlignment(Qt.AlignLeft)
    self.audio_widget_lyrics.setWordWrap(True)
    self.audio_widget_lyrics.setStyleSheet('QLabel {font-family: "Ubuntu"; font-size:' + str(self.monitor_window.height()*.04) + 'pt; color:white;}') #font-weight:bold;

    self.audio_widget_lyrics_geometry_animation = QPropertyAnimation(self.audio_widget_lyrics, b'geometry')
    self.audio_widget_lyrics_geometry_animation.setEasingCurve(QEasingCurve.Linear)
    self.audio_widget_lyrics_geometry_animation.finished.connect(lambda:audio_widget_lyrics_geometry_animation_finished(self))

    self.audio_widget_background_shadow = QLabel(self.audio_widget)
    self.audio_widget_background_shadow.setGeometry(0, self.audio_widget.height()*.7, self.audio_widget.width(), (self.audio_widget.height()*.3)+5)
    self.audio_widget_background_shadow.setStyleSheet('QLabel { border-top: ' + str(self.audio_widget.height()*.1) + 'px; border-image: url("' + os.path.join(path_graphics, "audio_background_shadow.png").replace('\\', '/') + '") 10 0 0 0 stretch stretch; }')

    self.audio_widget_artwork = QLabel(parent=self.audio_widget)
    self.audio_widget_artwork.setGeometry(self.monitor_window.width()*.1,(self.monitor_window.height()*.5)-(self.monitor_window.height()*.1),self.monitor_window.height()*.2, self.monitor_window.height()*.2)
    self.audio_widget_artwork.setAlignment(Qt.AlignCenter)
    self.audio_widget_artwork.setStyleSheet('QLabel {background-color: #fff; font-family: "Ubuntu Condensed"; font-size:' + str(self.monitor_window.height()*.09) + 'pt; font-weight:bold; color:black;}')

    self.audio_widget_artwork_geometry_animation = QPropertyAnimation(self.audio_widget_artwork, b'geometry')
    self.audio_widget_artwork_geometry_animation.setEasingCurve(QEasingCurve.OutCirc)

    self.audio_widget_artwork_opacity = QGraphicsOpacityEffect()
    self.audio_widget_artwork.setGraphicsEffect(self.audio_widget_artwork_opacity)
    self.audio_widget_artwork_opacity_animation = QPropertyAnimation(self.audio_widget_artwork_opacity, b'opacity')
    self.audio_widget_artwork_opacity.setOpacity(0.0)

    self.audio_widget_title = QLabel(parent=self.audio_widget)
    self.audio_widget_title.setAlignment(Qt.AlignVCenter | Qt.AlignLeft)
    self.audio_widget_title.setWordWrap(True)
    self.audio_widget_title.setGeometry((self.monitor_window.width()*.11) + (self.monitor_window.width()*.2),0,self.monitor_window.width() - ((self.monitor_window.width()*.21) + (self.monitor_window.width()*.2)), self.monitor_window.height())
    self.audio_widget_title.setStyleSheet('QLabel {background-color:none; font-family: "Ubuntu"; font-size:' + str(self.monitor_window.height()*.05) + 'pt; font-weight:bold; color:white}')

    self.audio_widget_title_geometry_animation = QPropertyAnimation(self.audio_widget_title, b'geometry')
    self.audio_widget_title_geometry_animation.setEasingCurve(QEasingCurve.OutCirc)

    self.audio_widget_title_opacity = QGraphicsOpacityEffect()
    self.audio_widget_title.setGraphicsEffect(self.audio_widget_title_opacity)
    self.audio_widget_title_opacity_animation = QPropertyAnimation(self.audio_widget_title_opacity, b'opacity')
    self.audio_widget_title_opacity.setOpacity(0.0)

    self.clock_widget = QWidget(parent=self.monitor_window)
    self.clock_widget.setGeometry(0,0,self.monitor_window.width(), self.monitor_window.height())

    self.clock_widget_label = QLabel(parent=self.clock_widget)
    self.clock_widget_label.setGeometry(0,0,self.clock_widget.width(), self.clock_widget.height())
    self.clock_widget_label.setStyleSheet('QLabel {background-color: black; font-family: "Ubuntu"; font-size:' + str(self.monitor_window.height()*.1) + 'pt; font-weight:bold; color:white; qproperty-alignment: "AlignCenter";}')

    self.clock_widget_label_opacity = QGraphicsOpacityEffect()
    self.clock_widget_label.setGraphicsEffect(self.clock_widget_label_opacity)
    self.clock_widget_label_opacity.setOpacity(0.0)
    self.clock_widget_label_opacity_animation = QPropertyAnimation(self.clock_widget_label_opacity, b'opacity')

    self.clock_widget_title_label = QLabel(parent=self.clock_widget)
    self.clock_widget_title_label.setGeometry(0,self.clock_widget.height()*.8,self.clock_widget.width(), self.clock_widget.height()*.2)
    self.clock_widget_title_label.setStyleSheet('QLabel {font-family: "Ubuntu"; font-size:' + str(self.monitor_window.height()*.025) + 'pt; qproperty-alignment: "AlignCenter";}')

    self.clock_widget_title_label_opacity = QGraphicsOpacityEffect()
    self.clock_widget_title_label.setGraphicsEffect(self.clock_widget_title_label_opacity)
    self.clock_widget_title_label_opacity.setOpacity(0.0)
    self.clock_widget_title_label_opacity_animation = QPropertyAnimation(self.clock_widget_title_label_opacity, b'opacity')

    self.web_widget = QWidget(parent=self.monitor_window)
    self.web_widget.setGeometry(0,0,self.monitor_window.width(), self.monitor_window.height())

    #self.web_widget_webview = QWebEngineView(parent=self.web_widget)
    #self.web_widget_webview.setGeometry(0,0,self.web_widget.width(), self.web_widget.height())

    self.camera_widget = QFrame(parent=self.monitor_window)
    self.camera_widget.setGeometry(0,0,self.monitor_window.width(),self.monitor_window.height())

    monitor_video.load(self)

    self.monitor.current_media = False


def hide_all(self):
    self.monitor.current_media = False
    if self.settings['show_yeartext']:
        monitor_yeartext.show(self)
    hide_widgets(self)

def hide_widgets(self):
    if not self.monitor.current_media or not (self.monitor.current_media[0] in ['audio', 'video']):
        self.right_panel_monitor_player.setVisible(False)

    if not self.monitor.current_media or not self.monitor.current_media[0] == 'audio':
        generate_effect(self.audio_widget_artwork_opacity_animation, 'opacity', 1000, self.audio_widget_artwork_opacity.opacity(), 0.0)
        generate_effect(self.audio_widget_title_opacity_animation, 'opacity', 1000, self.audio_widget_title_opacity.opacity(), 0.0)
        self.audio_widget_background_shadow.setVisible(False)
        self.audio_lyrics_can_go_back = True
        audio_widget_lyrics_geometry_animation_finished(self)
        monitor_video.hide(self)
    if not self.monitor.current_media or not self.monitor.current_media[0] == 'image':
        monitor_image.hide(self)
    if not self.monitor.current_media or not self.monitor.current_media[0] == 'video':
        monitor_video.hide(self)
    if not self.monitor.current_media or not self.monitor.current_media[0] == 'web':
        self.web_widget.setVisible(False)
        self.monitor_window.setCursor(Qt.BlankCursor)
    if not self.monitor.current_media or not self.monitor.current_media[0] == 'clock':
        monitor_video.hide(self)
        self.right_panel_monitor_prelude.setVisible(False)
        #self.video_mediaplayer.audio_set_volume(100)
        generate_effect(self.clock_widget_label_opacity_animation, 'opacity', 2000, self.clock_widget_label_opacity.opacity(), 0.0)
        generate_effect(self.clock_widget_title_label_opacity_animation, 'opacity', 2000, self.clock_widget_title_label_opacity.opacity(), 0.0)
    if not self.monitor.current_media or not self.monitor.current_media[0] == 'screencopy':
        generate_effect(self.image_widget_opacity_animation, 'opacity', 1000, self.image_widget_opacity.opacity(), 0.0)
    if not self.monitor.current_media or not self.monitor.current_media[0] == 'camera':
        self.camera_widget.setVisible(False)

def show_media(self):
    if self.settings['show_yeartext']:
        monitor_yeartext.hide(self)
    hide_widgets(self)
    if self.monitor.current_media:
        if self.monitor.current_media[0] in ['audio', 'video']:
            self.right_panel_monitor_player.setVisible(True)
            self.right_panel_monitor_player_timeline.update()
            self.right_panel_monitor_player_playbutton.setChecked(False)

        if self.monitor.current_media[0] == 'audio':
            self.audio_widget_artwork.setText(str(self.monitor.current_media[2]))
            self.audio_widget_title.setText(self.monitor.current_media[3])

            if self.monitor.current_media[6]:
                self.audio_widget_lyrics.setVisible(True)
                number_of_lines = len(self.monitor.current_media[4].split('\n')) + 2
                self.audio_widget_lyrics.setGeometry(self.monitor_window.width()*.1,self.monitor_window.height(),self.monitor_window.width()*.8, number_of_lines*(QFontMetrics(self.audio_widget_lyrics.font()).height()))
                self.audio_widget_lyrics.setText(self.monitor.current_media[4])
            self.audio_widget_title_geometry_animation.setEasingCurve(QEasingCurve.OutCirc)
            self.audio_widget_artwork_geometry_animation.setEasingCurve(QEasingCurve.OutCirc)
            self.audio_widget_background_shadow.setVisible(True)
            generate_effect(self.audio_widget_artwork_geometry_animation, 'geometry', 500, [(self.monitor_window.width()*.5)-(self.audio_widget_artwork.width()*.5),(self.monitor_window.height()*.5)-(self.audio_widget_artwork.height()*.5),self.audio_widget_artwork.width(),self.audio_widget_artwork.height()], [self.monitor_window.width()*.1,(self.monitor_window.height()*.5)-(self.audio_widget_artwork.height()*.5),self.audio_widget_artwork.width(),self.audio_widget_artwork.height()])
            generate_effect(self.audio_widget_artwork_opacity_animation, 'opacity', 500, 0.0, 1.0)
            generate_effect(self.audio_widget_title_geometry_animation, 'geometry', 1000, [(self.monitor_window.width()*.5)-(self.audio_widget_title.width()*.5),(self.monitor_window.height()*.5)-(self.audio_widget_title.height()*.5),self.audio_widget_title.width(),self.audio_widget_title.height()], [(self.monitor_window.width()*.11) + (self.monitor_window.width()*.2),(self.monitor_window.height()*.5)-(self.audio_widget_title.height()*.5),self.audio_widget_title.width(),self.audio_widget_title.height()])
            generate_effect(self.audio_widget_title_opacity_animation, 'opacity', 1000, 0.0, 1.0)

            self.media_open(self.monitor.current_media[5])
        elif self.monitor.current_media[0] == 'image':
            monitor_image.show(self)
        elif self.monitor.current_media[0] == 'video':
            monitor_video.show(self)
            monitor_video.load_media(self, self.monitor.current_media[2])
            monitor_video.play(self)
            self.right_panel_monitor_player_playbutton.setChecked(True)
        elif self.monitor.current_media[0] == 'web':
            self.web_widget.setVisible(True)
            self.web_widget_webview.load(QUrl(self.monitor.current_media[2]))
            self.web_widget_webview.show()
            self.monitor_window.setCursor(Qt.ArrowCursor)
        elif self.monitor.current_media[0] == 'clock':
            self.right_panel_monitor_prelude.setVisible(True)
            self.right_panel_monitor_prelude_volume.setValue(35)
            self.right_panel_monitor_prelude_timeline.update()

            if self.monitor.current_media[4]:
                self.right_panel_monitor_prelude_pausebutton.setChecked(True)
            else:
                self.right_panel_monitor_prelude_pausebutton.setChecked(False)

            if self.monitor.current_media[2]:                                                        # [2] show clock
                generate_effect(self.clock_widget_label_opacity_animation, 'opacity', 2000, 0.0, 1.0)
            if self.monitor.current_media[3] and not self.monitor.current_media[4]:                                                        # [3] show song
                generate_effect(self.clock_widget_title_label_opacity_animation, 'opacity', 3000, 0.0, 1.0)

        elif self.monitor.current_media[0] == 'screencopy':
            generate_effect(self.image_widget_opacity_animation, 'opacity', 1000, self.image_widget_opacity.opacity(), 1.0)
        elif self.monitor.current_media[0] == 'camera':
            self.monitor.current_media[2].stop()
            self.monitor.current_media[2].set_xwindow(self.camera_widget.winId())
            self.monitor.current_media[2].play()

def media_play_pause(self):
    monitor_video.play_pause(self)

def media_stop(self):
    #self.video_mediaplayer.stop()
    if 'main' in dir(app):
        self.right_panel_monitor_player_playbutton.setChecked(False)
        self.right_panel_monitor_player_timeline.update()
    if self.monitor.current_media[0] == 'audio' and self.monitor.current_media[6]:
        self.audio_widget_artwork_geometry_animation.stop()
        #self.audio_widget_lyrics.setGeometry(self.monitor_window.width()*.1,self.monitor_window.height(),self.monitor_window.width()*.8, self.monitor_window.height())
        audio_widget_lyrics_geometry_animation_finished(self)


def media_open(self, selected_media):
    if sys.version < '3':
        selected_media = unicode(selected_media)
    self.media = video_instance.media_new(selected_media)
    #self.video_mediaplayer.set_media(self.media)

def audio_widget_lyrics_geometry_animation_finished(self):
    if self.audio_lyrics_can_go_back:
        generate_effect(self.audio_widget_lyrics_geometry_animation, 'geometry', 1000, [self.audio_widget_lyrics.x(),self.audio_widget_lyrics.y(),self.audio_widget_lyrics.width(),self.audio_widget_lyrics.height()], [self.audio_widget_lyrics.x(),((-1)*self.audio_widget_lyrics.height()),self.audio_widget_lyrics.width(),self.audio_widget_lyrics.height()])
        self.audio_widget_title_geometry_animation.setEasingCurve(QEasingCurve.OutCirc)
        self.audio_widget_artwork_geometry_animation.setEasingCurve(QEasingCurve.OutCirc)
        generate_effect(self.audio_widget_artwork_geometry_animation, 'geometry', 1000, [self.audio_widget_artwork.x(),self.audio_widget_artwork.y(),self.audio_widget_artwork.width(),self.audio_widget_artwork.height()], [self.monitor_window.width()*.1,(self.monitor_window.height()*.5)-(self.audio_widget_artwork.height()*.5),self.audio_widget_artwork.width(),self.audio_widget_artwork.height()])
        generate_effect(self.audio_widget_title_geometry_animation, 'geometry', 1000, [self.audio_widget_title.x(),self.audio_widget_title.y(),self.audio_widget_title.width(),self.audio_widget_title.height()], [(self.monitor_window.width()*.11) + (self.monitor_window.width()*.2),(self.monitor_window.height()*.5)-(self.audio_widget_title.height()*.5),self.audio_widget_title.width(),self.audio_widget_title.height()])
        self.audio_lyrics_can_go_back = False
