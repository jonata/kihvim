import os

from modules.paths import *

from PySide2.QtWidgets import QLabel, QMessageBox, QWidget, QDesktopWidget, QLabel, QGraphicsOpacityEffect, QFrame, QPushButton, QCheckBox, QDial, QComboBox, QFileDialog, QVBoxLayout, QScrollArea, QListWidget, QListView, QLineEdit, QPlainTextEdit, QInputDialog, QApplication, QTabWidget, QListWidgetItem
from PySide2.QtCore import Qt, QPropertyAnimation, QEasingCurve, QUrl, QRectF, QSize, QRect, QTimer#, Signal
from PySide2.QtGui import QIcon, QFont, QPalette, QFontMetrics, QPixmap, QPainter, QPen, QColor

class screencopy_window(QWidget):
    def __init__(self, parent=None):
        super(screencopy_window, self).__init__(parent)
        self.setWindowIcon(QIcon(os.path.join(path_graphics, 'kihvim.png')))
        self.setAttribute(Qt.WA_TranslucentBackground)

        self.distance = [0,0,False]
        self.setGeometry(50, 50, 640, 480)
        self.keep_proportion = False
        self.selected_area = False

        self.point_fixed_normal = QPixmap(os.path.join(path_graphics, 'screencopy_point_fixed_normal.png'))

        self.setWindowFlags(self.windowFlags() | Qt.WindowStaysOnTopHint | Qt.FramelessWindowHint)

        class point_resize(QPushButton):
            def mouseReleaseEvent(widget, event):
                self.point_resize.setStyleSheet('QPushButton { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "screencopy_point_resize_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "screencopy_point_resize_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
            def mousePressEvent(widget, event):
                self.distance = [(self.x()+self.width())-self.cursor().pos().x(),(self.y()+self.height())-self.cursor().pos().y()]
                self.point_resize.setStyleSheet('QPushButton { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "screencopy_point_resize_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
            def mouseMoveEvent(widget, event):
                if self.keep_proportion:
                    self.resize(self.cursor().pos().x()-self.x()+self.distance[0],(self.cursor().pos().x()-self.x()+self.distance[0])*app.main.screen_width_proportion)
                else:
                    self.resize(self.cursor().pos().x()-self.x()+self.distance[0],self.cursor().pos().y()-self.y()+self.distance[1])

        self.point_resize = point_resize(parent=self);
        self.point_resize.setGeometry(self.width()-30, self.height()-30, 30,30)
        self.point_resize.setStyleSheet('QPushButton { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "screencopy_point_resize_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "screencopy_point_resize_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')

        self.proportion_button = QPushButton(u'MANTER PROPORÇÃO', parent=self);
        self.proportion_button.setCheckable(True)
        self.proportion_button.setStyleSheet('QPushButton { font-family: "Ubuntu"; font-size:9px; color:white; border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_dark_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_dark_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(path_graphics, "button_dark_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_dark_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:checked { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_dark_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
        self.proportion_button.clicked.connect(lambda:self.proportion_button_clicked())

        self.confirm_button = QPushButton(u'CONFIRMAR', parent=self);
        self.confirm_button.setStyleSheet('QPushButton { font-family: "Ubuntu"; font-size:10px; color:white; font-weight:bold; border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
        self.confirm_button.clicked.connect(lambda:self.confirm_button_clicked())

        self.cancel_button = QPushButton(u'CANCELAR', parent=self);
        self.cancel_button.setStyleSheet('QPushButton { font-family: "Ubuntu"; font-size:9px; color:white; border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_dark_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_dark_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_dark_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(path_graphics, "button_dark_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
        self.cancel_button.clicked.connect(lambda:self.cancel_button_clicked())

    def resizeEvent(self, event):
        self.point_resize.move(self.width()-30, self.height()-30)
        self.proportion_button.setGeometry((self.width()*.5)-60,(self.height()*.5)-45,120,30)
        self.confirm_button.setGeometry((self.width()*.5)-60,(self.height()*.5)-15,120,30)
        self.cancel_button.setGeometry((self.width()*.5)-60,(self.height()*.5)+15,120,30)
    '''
    def paintEvent(self, paintEvent):
        painter = QPainter(self)
        painter.setRenderHint(QPainter.Antialiasing)

        rectangle = QRectF(15.0, 15.0, self.width()-30, self.height()-30)

        painter.setPen(QPen(QColor.fromRgb(62,83,99), 4))
        painter.setBrush(QColor.fromRgb(106,116,131,a=50))
        painter.drawRect(rectangle)

        painter.drawPixmap(0,0,self.point_fixed_normal)

        painter.end()'''

    def mouseReleaseEvent(self, event):
        self.distance = [0,0,False]

    def mousePressEvent(self, event):
        self.distance = [self.cursor().pos().x()-self.x(),self.cursor().pos().y()-self.y()]

    def mouseMoveEvent(self, event):
        if (self.distance[0] < 30 and self.distance[1] < 30) or ((self.distance[0] > 15 and self.distance[1] > 15) and (self.distance[0] < (self.width()-15) and self.distance[1] < (self.height()-15))):
            self.move(self.cursor().pos().x()-self.distance[0],self.cursor().pos().y()-self.distance[1])

    def proportion_button_clicked(self):
        self.keep_proportion = self.proportion_button.isChecked()

    def confirm_button_clicked(self):
        self.cancel_button_clicked()
        self.selected_area = [self.x(), self.y(), self.width(), self.height()]
        app.main.left_panel_add_title.setText(u'<font style="font-size:14px;"><b>' + u'CÓPIA DE TELA' + '</b></font><br><font style="font-size:9px;">' + 'x: ' + str(self.selected_area[0]) + ', y: ' + str(self.selected_area[1]) + ', largura: ' + str(self.selected_area[2]) + ', altura: ' + str(self.selected_area[3]) + '</font>')
        app.main.left_panel_screencopy_preview.setText('')
        app.main.left_panel_screencopy_description.setText(u'<font style="font-size:9px;color:gray;">' + u'ÁREA A SER MOSTADA' + '</font><br><font style="font-size:14px;color:gray;">' + 'x: ' + str(self.selected_area[0]) + ', y: ' + str(self.selected_area[1]) + ', largura: ' + str(self.selected_area[2]) + ', altura: ' + str(self.selected_area[3]) + '</font>')
        app.main.left_panel_addbutton.setEnabled(True)
        app.main.left_panel_addbutton_label.setText(u'<font style="font-size:14px;"><b>' + u'ADICIONAR' + '</b></font><br><font style="font-size:9px;">' + u'NA LISTA DE EXECUÇÃO' + '</font>')


    def cancel_button_clicked(self):
        self.selected_area = False
        self.close()
