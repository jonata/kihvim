# -*- mode: python -*-

block_cipher = None

a = Analysis(['kihvim.py'],
             pathex=[],
             binaries=[],
             datas=[
                      ( 'graphics/*.png', 'graphics' ),
                 ],
             hiddenimports=['numpy.core.multiarray', 'bs4', 'urllib', 'python-vlc'],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          exclude_binaries=True,
          name='kihvim',
          debug=False,
          strip=False,
          upx=True,
          console=True )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               name='kihvim')
