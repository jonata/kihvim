#!/usr/bin/env python3

from setuptools import setup
import os

desktop_file = '[Desktop Entry]'
desktop_file += '\nEncoding=UTF-8'
desktop_file += '\nName=Kihvim'
desktop_file += '\nExec=python3 /usr/local/share/kihvim/kihvim.py'
desktop_file += '\nIcon=/usr/local/share/kihvim/graphics/kihvim.png'
desktop_file += '\nInfo=Kihvim'
desktop_file += '\nCategories=Application;Multimedia'
desktop_file += '\nComment=Kingdom Hall Video Mixer'
desktop_file += '\nTerminal=false'
desktop_file += '\nType=Application'
desktop_file += '\nStartupNotify=true'
open('/tmp/' + 'kihvim.desktop', 'w').write(desktop_file)

data_files = []
for filename in os.listdir('graphics'):
    list_of_files = []
    if filename.endswith(('.png')):
        list_of_files.append('graphics/' + filename)
    data_files.append(('share/kihvim/graphics', list_of_files))
for filename in os.listdir('resources'):
    list_of_files = []
    if filename.endswith(('.ttf')):
        list_of_files.append('resources/' + filename)
    data_files.append(('share/kihvim/resources', list_of_files))
#for filename in os.listdir('modules'):
#    list_of_files = []
#    if filename.endswith(('.py')):
#        list_of_files.append('modules/' + filename)
#    data_files.append(('share/kihvim/modules', list_of_files))

data_files.append(('share/kihvim', ['kihvim.py']))
#data_files.append(('share/applications', ['snap/gui/kihvim.desktop']))
data_files.append(('share/applications', ['/tmp/kihvim.desktop']))

setup(name='Kihvim',
      version='19.05',
      description='Kingdom Hall Video Mixer',
      author='Jonatã Bolzan Loss',
      author_email='jonata@jonata.org',
      url='https://kihvim.jonata.org/',
      #packages=['opendvdproducer'],
      data_files = data_files,
      install_requires=[
                        ]
     )
