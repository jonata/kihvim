#!/usr/bin/env python3

import os, sys, subprocess, random, tempfile, datetime, time, codecs, shutil, hashlib, copy, threading
import urllib.request

from mutagen.id3 import ID3
from mutagen.mp3 import MP3
from mutagen.mp4 import MP4

from PyQt5.QtWidgets import QLabel, QMessageBox, QWidget, QDesktopWidget, QLabel, QGraphicsOpacityEffect, QFrame, QPushButton, QCheckBox, QDial, QComboBox, QFileDialog, QVBoxLayout, QScrollArea, QListWidget, QListView, QLineEdit, QPlainTextEdit, QInputDialog, QApplication, QTabWidget, QListWidgetItem
from PyQt5.QtCore import Qt, QPropertyAnimation, QEasingCurve, QUrl, QRectF, QSize, QRect, QTimer#, Signal
from PyQt5.QtGui import QIcon, QFont, QPalette, QFontMetrics, QPixmap, QPainter, QPen, QColor
from PyQt5.QtWebEngineWidgets import QWebEngineView, QWebEnginePage, QWebEngineSettings

from PIL import Image

# if (sys.platform == 'darwin' and not os.path.isdir('/Applications/VLC.app')) or (sys.platform.startswith('win32') and not (os.path.isdir('C:\\Program Files\\VideoLAN\\VLC') or os.path.isdir('C:\\Program Files (x86)\\VideoLAN\\VLC'))) or (sys.platform.startswith('linux') and not subprocess.call("type vlc", shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE) == 0):
#     vlc_message = QMessageBox(QMessageBox.Critical, u'VLC não foi encontrado', u'O kihvim necessita do VLC para poder executar arquivos de mídia, mas infelizmente parece que ele não está instalado no seu sistema. Por favor, providencie a instalação e abra novamente o kihvim.')
#     vlc_message.exec_()
#     exit()
# else:
import vlc

path_kihvim = os.path.abspath(os.path.dirname(sys.argv[0]))

gs_executable =  ['gs']
if sys.platform == 'darwin':
    gs_executable = [os.path.join(path_kihvim, 'resources', 'gs'), '-sGenericResourceDir=' +  os.path.join(path_kihvim, 'resources', 'Resource') + '/',  '-sICCProfilesDir=' +  os.path.join(path_kihvim, 'resources', 'iccprofiles') + '/', '-I' + os.path.join(path_kihvim, 'resources', 'Resource', 'Init')]
elif sys.platform == 'win32' or os.name == 'nt':
    gs_executable = [os.path.join(path_kihvim, 'gs927w32.exe')]
gs_executable += ['-dNOPAUSE', '-dBATCH', '-q']

video_instance = vlc.Instance()

startupinfo = None

if subprocess.call("type ffmpeg", shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE) == 0:
    ffmpeg_bin = 'ffmpeg'
    ffprobe_bin = 'ffprobe'
else:
    ffmpeg_bin = 'avconv'
    ffprobe_bin = 'avprobe'

path_home = os.path.expanduser("~")
path_tmp = os.path.join(tempfile.gettempdir(), 'kihvim-' + str(random.randint(1000,9999)))
path_graphics = os.path.join(path_kihvim, 'graphics')
path_user_config = os.path.join(path_home, '.config', 'kihvim')

os.mkdir(path_tmp)

def get_number(text):
    number_found = False
    final_number = ''
    for c in text:
        if not number_found and c.isnumeric():
            number_found = True
        if number_found and c.isnumeric():
            final_number += c
        elif number_found and not c.isnumeric():
            break

    return final_number

def convert_to_timecode(value, simple=False):
    fr = int(str('%.3f'% value).split('.')[-1])
    mm, ss = divmod(value, 60)
    hh, mm = divmod(mm, 60)

    if simple:
        if hh == 0 and mm == 0:
            return '%02d' % (ss)
        elif hh == 0:
            return '%02d:%02d' % (mm, ss)
        else:
            return '%02d:%02d:%02d' % (hh, mm, ss)
    else:
        return '%02d:%02d:%02d.%03d' % (hh, mm, ss, fr)

def convert_timecode_to_seconds(timecode):
    final_value = float('0.' + timecode.split('.')[-1])
    final_value += int(timecode.split(':')[2].split('.')[0])
    final_value += int(timecode.split(':')[1])*60.0
    final_value += int(timecode.split(':')[0])*3600.0
    return float("%.3f" % final_value)

def clean_xml(text):
    final_text = u''
    is_text = False
    for character in range(len(text)):
        if text[character] == '>':
            is_text = True
            continue
        elif text[character] == '<':
            is_text = False
            continue

        if is_text:
            final_text += text[character]

    return final_text

def clean_spaces(text):
    final_text = text
    while '  ' in final_text:
        final_text = final_text.replace('  ', ' ')

    while '\n' in final_text:
        final_text = final_text.replace('\n', '')
    while '\r' in final_text:

        final_text = final_text.replace('\r', '')

    while final_text[:1] == ' ':
        final_text = final_text[1:]

    #while final_text[:1] == ':':
    #    final_text = final_text[1:]

    while final_text[-1:] == ' ':
        final_text = final_text[:-1]

    #while final_text[-1:] == ':':
    #    final_text = final_text[:-1]

    return final_text

def create_thumbnail(source_file, target_file, width, height):
    im = Image.open(source_file)
    im.thumbnail((int(width), int(height)), Image.ANTIALIAS)
    imb = Image.new("RGB", (int(width), int(height)), (0,0,0))
    imb.paste(im, (int((imb.size[0]-im.size[0])*.5),0))
    imb.save(target_file)

class monitorWindow(QWidget):
    def __init__(self, parent=None):
        super(monitorWindow, self).__init__(parent)

        self.setWindowIcon(QIcon(os.path.join(path_graphics, 'kihvim.png')))

        if QDesktopWidget().screenCount() > 1:
            monitor = 1
            window_width = QDesktopWidget().screenGeometry(monitor).width()
            window_height = QDesktopWidget().screenGeometry(monitor).height()
        else:
            monitor = 0
            window_width = 640
            window_height = 480

        self.setCursor(Qt.BlankCursor)

        self.black_palette = self.palette()
        self.black_palette.setColor (QPalette.Window, QColor(0,0,0))
        self.setPalette(self.black_palette)
        self.setAutoFillBackground(True)

        self.showFullScreen()
        self.resize(window_width, window_height)
        self.move(QDesktopWidget().screenGeometry(monitor).left(), QDesktopWidget().screenGeometry(monitor).top())
        self.setMaximumSize(self.width(), self.height())
        self.setMinimumSize(self.width(), self.height())
        self.setWindowFlags(self.windowFlags() | Qt.WindowStaysOnTopHint | Qt.FramelessWindowHint | Qt.X11BypassWindowManagerHint)

        self.setWindowTitle('kihvim')

        self.video_mediaplayer = video_instance.media_player_new()

        self.test_widget = QLabel(u'⊡', parent=self)
        self.test_widget.setGeometry(0,0,self.width(), self.height())
        self.test_widget.setStyleSheet('QLabel {font-family: "Ubuntu"; font-size:' + str(self.height()*.04) + 'pt; color:white; qproperty-alignment: "AlignCenter";}')
        self.test_widget_opacity = QGraphicsOpacityEffect()
        self.test_widget.setGraphicsEffect(self.test_widget_opacity)
        self.test_widget_opacity_animation = QPropertyAnimation(self.test_widget_opacity, b'opacity')
        self.test_widget_opacity.setOpacity(0.0)

        self.image_widget = QLabel(parent=self)
        self.image_widget.setGeometry(0,0,self.width(), self.height())
        self.image_widget.setAlignment(Qt.AlignCenter)

        self.image_widget_opacity = QGraphicsOpacityEffect()
        self.image_widget.setGraphicsEffect(self.image_widget_opacity)
        self.image_widget_opacity_animation = QPropertyAnimation(self.image_widget_opacity, b'opacity')
        self.image_widget_opacity.setOpacity(0.0)

        self.audio_widget = QWidget(parent=self)
        self.audio_widget.setGeometry(0,0,self.width(), self.height())

        self.audio_widget_lyrics = QLabel(parent=self.audio_widget)
        self.audio_widget_lyrics.setGeometry(self.audio_widget.width()*.1,self.audio_widget.height(),self.audio_widget.width()*.8, self.audio_widget.height())
        self.audio_widget_lyrics.setAlignment(Qt.AlignLeft)
        self.audio_widget_lyrics.setWordWrap(True)
        self.audio_widget_lyrics.setStyleSheet('QLabel {font-family: "Ubuntu"; font-size:' + str(self.height()*.04) + 'pt; color:white;}') #font-weight:bold;

        self.audio_widget_lyrics_geometry_animation = QPropertyAnimation(self.audio_widget_lyrics, b'geometry')
        self.audio_widget_lyrics_geometry_animation.setEasingCurve(QEasingCurve.Linear)
        self.audio_widget_lyrics_geometry_animation.finished.connect(lambda:self.audio_widget_lyrics_geometry_animation_finished())

        self.audio_widget_background_shadow = QLabel(self.audio_widget)
        self.audio_widget_background_shadow.setGeometry(0,self.audio_widget.height()*.7,self.audio_widget.width(), (self.audio_widget.height()*.3)+5)
        self.audio_widget_background_shadow.setStyleSheet('QLabel { border-top: ' + str(self.audio_widget.height()*.1) + 'px; border-image: url("' + os.path.join(path_graphics, "audio_background_shadow.png").replace('\\', '/') + '") 10 0 0 0 stretch stretch; }')

        self.audio_widget_artwork = QLabel(parent=self.audio_widget)
        self.audio_widget_artwork.setGeometry(self.width()*.1,(self.height()*.5)-(self.height()*.1),self.height()*.2, self.height()*.2)
        self.audio_widget_artwork.setAlignment(Qt.AlignCenter)
        self.audio_widget_artwork.setStyleSheet('QLabel {background-color: #fff; font-family: "Ubuntu Condensed"; font-size:' + str(self.height()*.09) + 'pt; font-weight:bold; color:black;}')

        self.audio_widget_artwork_geometry_animation = QPropertyAnimation(self.audio_widget_artwork, b'geometry')
        self.audio_widget_artwork_geometry_animation.setEasingCurve(QEasingCurve.OutCirc)

        self.audio_widget_artwork_opacity = QGraphicsOpacityEffect()
        self.audio_widget_artwork.setGraphicsEffect(self.audio_widget_artwork_opacity)
        self.audio_widget_artwork_opacity_animation = QPropertyAnimation(self.audio_widget_artwork_opacity, b'opacity')
        self.audio_widget_artwork_opacity.setOpacity(0.0)

        self.audio_widget_title = QLabel(parent=self.audio_widget)
        self.audio_widget_title.setAlignment(Qt.AlignVCenter | Qt.AlignLeft)
        self.audio_widget_title.setWordWrap(True)
        self.audio_widget_title.setGeometry((self.width()*.11) + (self.width()*.2),0,self.width() - ((self.width()*.21) + (self.width()*.2)), self.height())
        self.audio_widget_title.setStyleSheet('QLabel {background-color:none; font-family: "Ubuntu"; font-size:' + str(self.height()*.05) + 'pt; font-weight:bold; color:white}')

        self.audio_widget_title_geometry_animation = QPropertyAnimation(self.audio_widget_title, b'geometry')
        self.audio_widget_title_geometry_animation.setEasingCurve(QEasingCurve.OutCirc)

        self.audio_widget_title_opacity = QGraphicsOpacityEffect()
        self.audio_widget_title.setGraphicsEffect(self.audio_widget_title_opacity)
        self.audio_widget_title_opacity_animation = QPropertyAnimation(self.audio_widget_title_opacity, b'opacity')
        self.audio_widget_title_opacity.setOpacity(0.0)

        self.clock_widget = QWidget(parent=self)
        self.clock_widget.setGeometry(0,0,self.width(), self.height())

        self.clock_widget_label = QLabel(parent=self.clock_widget)
        self.clock_widget_label.setGeometry(0,0,self.clock_widget.width(), self.clock_widget.height())
        self.clock_widget_label.setStyleSheet('QLabel {background-color: black; font-family: "Ubuntu"; font-size:' + str(self.height()*.1) + 'pt; font-weight:bold; color:white; qproperty-alignment: "AlignCenter";}')

        self.clock_widget_label_opacity = QGraphicsOpacityEffect()
        self.clock_widget_label.setGraphicsEffect(self.clock_widget_label_opacity)
        self.clock_widget_label_opacity.setOpacity(0.0)
        self.clock_widget_label_opacity_animation = QPropertyAnimation(self.clock_widget_label_opacity, b'opacity')

        self.clock_widget_title_label = QLabel(parent=self.clock_widget)
        self.clock_widget_title_label.setGeometry(0,self.clock_widget.height()*.8,self.clock_widget.width(), self.clock_widget.height()*.2)
        self.clock_widget_title_label.setStyleSheet('QLabel {font-family: "Ubuntu"; font-size:' + str(self.height()*.025) + 'pt; qproperty-alignment: "AlignCenter";}')

        self.clock_widget_title_label_opacity = QGraphicsOpacityEffect()
        self.clock_widget_title_label.setGraphicsEffect(self.clock_widget_title_label_opacity)
        self.clock_widget_title_label_opacity.setOpacity(0.0)
        self.clock_widget_title_label_opacity_animation = QPropertyAnimation(self.clock_widget_title_label_opacity, b'opacity')

        self.web_widget = QWidget(parent=self)
        self.web_widget.setGeometry(0,0,self.width(), self.height())

        self.web_widget_webview = QWebEngineView(parent=self.web_widget)
        self.web_widget_webview.setGeometry(0,0,self.web_widget.width(), self.web_widget.height())

        self.camera_widget = QFrame(parent=self)
        self.camera_widget.setGeometry(0,0,self.width(),self.height())

        self.video_widget = QFrame(parent=self)
        self.video_widget.setGeometry(0,0,self.width(),self.height())
        self.video_mediaplayer.set_xwindow(self.video_widget.winId())

        self.current_media = False

    def hide_all(self):
        self.current_media = False
        self.hide_widgets()

    def hide_widgets(self):
        if not self.current_media or not (self.current_media[0] in ['audio', 'video']):
            app.main.right_panel_monitor_player.setVisible(False)

        if not self.current_media or not self.current_media[0] == 'audio':
            generate_effect(self.audio_widget_artwork_opacity_animation, 'opacity', 1000, self.audio_widget_artwork_opacity.opacity(), 0.0)
            generate_effect(self.audio_widget_title_opacity_animation, 'opacity', 1000, self.audio_widget_title_opacity.opacity(), 0.0)
            self.audio_widget_background_shadow.setVisible(False)
            self.audio_lyrics_can_go_back = True
            self.audio_widget_lyrics_geometry_animation_finished()
            self.video_mediaplayer.stop()
        if not self.current_media or not self.current_media[0] == 'image':
            generate_effect(self.image_widget_opacity_animation, 'opacity', 1000, self.image_widget_opacity.opacity(), 0.0)
        if not self.current_media or not self.current_media[0] == 'video':
            self.video_mediaplayer.stop()
            self.video_widget.setVisible(False)
        if not self.current_media or not self.current_media[0] == 'web':
            self.web_widget.setVisible(False)
            self.setCursor(Qt.BlankCursor)
        if not self.current_media or not self.current_media[0] == 'clock':
            self.video_mediaplayer.stop()
            app.main.right_panel_monitor_prelude.setVisible(False)
            #self.video_mediaplayer.audio_set_volume(100)
            generate_effect(self.clock_widget_label_opacity_animation, 'opacity', 2000, self.clock_widget_label_opacity.opacity(), 0.0)
            generate_effect(self.clock_widget_title_label_opacity_animation, 'opacity', 2000, self.clock_widget_title_label_opacity.opacity(), 0.0)
        if not self.current_media or not self.current_media[0] == 'screencopy':
            generate_effect(self.image_widget_opacity_animation, 'opacity', 1000, self.image_widget_opacity.opacity(), 0.0)
        if not self.current_media or not self.current_media[0] == 'camera':
            self.camera_widget.setVisible(False)

    def show_media(self):
        self.hide_widgets()
        if self.current_media:
            if self.current_media[0] in ['audio', 'video']:
                app.main.right_panel_monitor_player.setVisible(True)
                app.main.right_panel_monitor_player_timeline.update()
                app.main.right_panel_monitor_player_playbutton.setChecked(False)

            if self.current_media[0] == 'audio':
                self.audio_widget_artwork.setText(str(self.current_media[2]))
                self.audio_widget_title.setText(self.current_media[3])

                if self.current_media[6]:
                    self.audio_widget_lyrics.setVisible(True)
                    number_of_lines = len(self.current_media[4].split('\n')) + 2
                    self.audio_widget_lyrics.setGeometry(self.width()*.1,self.height(),self.width()*.8, number_of_lines*(QFontMetrics(self.audio_widget_lyrics.font()).height()))
                    self.audio_widget_lyrics.setText(self.current_media[4])
                self.audio_widget_title_geometry_animation.setEasingCurve(QEasingCurve.OutCirc)
                self.audio_widget_artwork_geometry_animation.setEasingCurve(QEasingCurve.OutCirc)
                self.audio_widget_background_shadow.setVisible(True)
                generate_effect(self.audio_widget_artwork_geometry_animation, 'geometry', 500, [(self.width()*.5)-(self.audio_widget_artwork.width()*.5),(self.height()*.5)-(self.audio_widget_artwork.height()*.5),self.audio_widget_artwork.width(),self.audio_widget_artwork.height()], [self.width()*.1,(self.height()*.5)-(self.audio_widget_artwork.height()*.5),self.audio_widget_artwork.width(),self.audio_widget_artwork.height()])
                generate_effect(self.audio_widget_artwork_opacity_animation, 'opacity', 500, 0.0, 1.0)
                generate_effect(self.audio_widget_title_geometry_animation, 'geometry', 1000, [(self.width()*.5)-(self.audio_widget_title.width()*.5),(self.height()*.5)-(self.audio_widget_title.height()*.5),self.audio_widget_title.width(),self.audio_widget_title.height()], [(self.width()*.11) + (self.width()*.2),(self.height()*.5)-(self.audio_widget_title.height()*.5),self.audio_widget_title.width(),self.audio_widget_title.height()])
                generate_effect(self.audio_widget_title_opacity_animation, 'opacity', 1000, 0.0, 1.0)

                self.media_open(self.current_media[5])
            elif self.current_media[0] == 'image':
                self.image_widget.setPixmap(QPixmap(self.current_media[2]).scaled(self.image_widget.width(), self.image_widget.height(), Qt.KeepAspectRatio, Qt.SmoothTransformation))
                generate_effect(self.image_widget_opacity_animation, 'opacity', 1000, self.image_widget_opacity.opacity(), 1.0)
            elif self.current_media[0] == 'video':
                self.video_widget.setVisible(True)
                self.media_open(self.current_media[2])
                self.video_mediaplayer.play()
                if self.current_media[5]:
                    self.video_mediaplayer.set_position(self.current_media[5]/self.current_media[7])
                app.main.right_panel_monitor_player_playbutton.setChecked(True)
            elif self.current_media[0] == 'web':
                self.web_widget.setVisible(True)
                self.web_widget_webview.load(QUrl(self.current_media[2]))
                self.web_widget_webview.show()
                self.setCursor(Qt.ArrowCursor)
            elif self.current_media[0] == 'clock':
                app.main.right_panel_monitor_prelude.setVisible(True)
                app.main.right_panel_monitor_prelude_volume.setValue(35)
                app.main.right_panel_monitor_prelude_timeline.update()

                if self.current_media[4]:
                    app.main.right_panel_monitor_prelude_pausebutton.setChecked(True)
                else:
                    app.main.right_panel_monitor_prelude_pausebutton.setChecked(False)

                if self.current_media[2]:                                                        # [2] show clock
                    generate_effect(self.clock_widget_label_opacity_animation, 'opacity', 2000, 0.0, 1.0)
                if self.current_media[3] and not self.current_media[4]:                                                        # [3] show song
                    generate_effect(self.clock_widget_title_label_opacity_animation, 'opacity', 3000, 0.0, 1.0)

            elif self.current_media[0] == 'screencopy':
                generate_effect(self.image_widget_opacity_animation, 'opacity', 1000, self.image_widget_opacity.opacity(), 1.0)
            elif self.current_media[0] == 'camera':
                self.current_media[2].stop()
                self.current_media[2].set_xwindow(self.camera_widget.winId())
                self.current_media[2].play()

    def media_play_pause(self):
        if self.video_mediaplayer.is_playing():
            self.video_mediaplayer.pause()
            #self.is_paused = True
        else:
            #if self.video_mediaplayer.play() == -1:
            #    self.media_open(app.main.selected_media[1])
            #    return
            #

            self.video_mediaplayer.play()
            #self.video_mediaplayer.audio_set_volume(100)
            if self.current_media[0] == 'audio' and self.current_media[6]:
                self.audio_lyrics_can_go_back = True
                self.audio_widget_title_geometry_animation.setEasingCurve(QEasingCurve.InOutQuart)
                self.audio_widget_artwork_geometry_animation.setEasingCurve(QEasingCurve.InOutQuart)
                generate_effect(self.audio_widget_lyrics_geometry_animation, 'geometry', (self.current_media[-2]-5)*1000, [self.audio_widget_lyrics.x(),self.height()-(self.height()*.2),self.audio_widget_lyrics.width(),self.audio_widget_lyrics.height()], [self.audio_widget_lyrics.x(),((-1)*self.audio_widget_lyrics.height())+(self.height()*.2),self.audio_widget_lyrics.width(),self.audio_widget_lyrics.height()])
                generate_effect(self.audio_widget_artwork_geometry_animation, 'geometry', 5000, [self.audio_widget_artwork.x(),self.audio_widget_artwork.y(),self.audio_widget_artwork.width(),self.audio_widget_artwork.height()], [self.audio_widget_artwork.x(),self.height()-self.audio_widget_artwork.height(),self.audio_widget_artwork.width(),self.audio_widget_artwork.height()])
                generate_effect(self.audio_widget_title_geometry_animation, 'geometry', 5000, [self.audio_widget_title.x(),self.audio_widget_title.y(),self.audio_widget_title.width(),self.audio_widget_title.height()], [self.audio_widget_title.x(),self.height()-self.audio_widget_artwork.height(),self.audio_widget_title.width(),self.height()*.2])


            #self.is_paused = False

    def media_stop(self):
        self.video_mediaplayer.stop()
        if 'main' in dir(app):
            app.main.right_panel_monitor_player_playbutton.setChecked(False)
            app.main.right_panel_monitor_player_timeline.update()
        if self.current_media[0] == 'audio' and self.current_media[6]:
            self.audio_widget_artwork_geometry_animation.stop()
            #self.audio_widget_lyrics.setGeometry(self.width()*.1,self.height(),self.width()*.8, self.height())
            self.audio_widget_lyrics_geometry_animation_finished()


    def media_open(self, selected_media):
        if sys.version < '3':
            selected_media = unicode(selected_media)
        self.media = video_instance.media_new(selected_media)
        self.video_mediaplayer.set_media(self.media)

    def audio_widget_lyrics_geometry_animation_finished(self):
        if self.audio_lyrics_can_go_back:
            generate_effect(self.audio_widget_lyrics_geometry_animation, 'geometry', 1000, [self.audio_widget_lyrics.x(),self.audio_widget_lyrics.y(),self.audio_widget_lyrics.width(),self.audio_widget_lyrics.height()], [self.audio_widget_lyrics.x(),((-1)*self.audio_widget_lyrics.height()),self.audio_widget_lyrics.width(),self.audio_widget_lyrics.height()])
            self.audio_widget_title_geometry_animation.setEasingCurve(QEasingCurve.OutCirc)
            self.audio_widget_artwork_geometry_animation.setEasingCurve(QEasingCurve.OutCirc)
            generate_effect(self.audio_widget_artwork_geometry_animation, 'geometry', 1000, [self.audio_widget_artwork.x(),self.audio_widget_artwork.y(),self.audio_widget_artwork.width(),self.audio_widget_artwork.height()], [self.width()*.1,(self.height()*.5)-(self.audio_widget_artwork.height()*.5),self.audio_widget_artwork.width(),self.audio_widget_artwork.height()])
            generate_effect(self.audio_widget_title_geometry_animation, 'geometry', 1000, [self.audio_widget_title.x(),self.audio_widget_title.y(),self.audio_widget_title.width(),self.audio_widget_title.height()], [(self.width()*.11) + (self.width()*.2),(self.height()*.5)-(self.audio_widget_title.height()*.5),self.audio_widget_title.width(),self.audio_widget_title.height()])
            self.audio_lyrics_can_go_back = False

class screencopyWindow(QWidget):
    def __init__(self, parent=None):
        super(screencopyWindow, self).__init__(parent)
        self.setWindowIcon(QIcon(os.path.join(path_graphics, 'kihvim.png')))
        self.setAttribute(Qt.WA_TranslucentBackground)

        self.distance = [0,0,False]
        self.setGeometry(50, 50, 640, 480)
        self.keep_proportion = False
        self.selected_area = False

        self.point_fixed_normal = QPixmap(os.path.join(path_graphics, 'screencopy_point_fixed_normal.png'))

        self.setWindowFlags(self.windowFlags() | Qt.WindowStaysOnTopHint | Qt.FramelessWindowHint)

        class point_resize(QPushButton):
            def mouseReleaseEvent(widget, event):
                self.point_resize.setStyleSheet('QPushButton { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "screencopy_point_resize_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "screencopy_point_resize_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
            def mousePressEvent(widget, event):
                self.distance = [(self.x()+self.width())-self.cursor().pos().x(),(self.y()+self.height())-self.cursor().pos().y()]
                self.point_resize.setStyleSheet('QPushButton { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "screencopy_point_resize_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
            def mouseMoveEvent(widget, event):
                if self.keep_proportion:
                    self.resize(self.cursor().pos().x()-self.x()+self.distance[0],(self.cursor().pos().x()-self.x()+self.distance[0])*app.main.screen_width_proportion)
                else:
                    self.resize(self.cursor().pos().x()-self.x()+self.distance[0],self.cursor().pos().y()-self.y()+self.distance[1])

        self.point_resize = point_resize(parent=self);
        self.point_resize.setGeometry(self.width()-30, self.height()-30, 30,30)
        self.point_resize.setStyleSheet('QPushButton { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "screencopy_point_resize_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "screencopy_point_resize_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')

        self.proportion_button = QPushButton(u'MANTER PROPORÇÃO', parent=self);
        self.proportion_button.setCheckable(True)
        self.proportion_button.setStyleSheet('QPushButton { font-family: "Ubuntu"; font-size:9px; color:white; border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_dark_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_dark_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(path_graphics, "button_dark_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_dark_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:checked { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_dark_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
        self.proportion_button.clicked.connect(lambda:self.proportion_button_clicked())

        self.confirm_button = QPushButton(u'CONFIRMAR', parent=self);
        self.confirm_button.setStyleSheet('QPushButton { font-family: "Ubuntu"; font-size:10px; color:white; font-weight:bold; border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
        self.confirm_button.clicked.connect(lambda:self.confirm_button_clicked())

        self.cancel_button = QPushButton(u'CANCELAR', parent=self);
        self.cancel_button.setStyleSheet('QPushButton { font-family: "Ubuntu"; font-size:9px; color:white; border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_dark_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_dark_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_dark_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(path_graphics, "button_dark_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
        self.cancel_button.clicked.connect(lambda:self.cancel_button_clicked())

    def resizeEvent(self, event):
        self.point_resize.move(self.width()-30, self.height()-30)
        self.proportion_button.setGeometry((self.width()*.5)-60,(self.height()*.5)-45,120,30)
        self.confirm_button.setGeometry((self.width()*.5)-60,(self.height()*.5)-15,120,30)
        self.cancel_button.setGeometry((self.width()*.5)-60,(self.height()*.5)+15,120,30)
    '''
    def paintEvent(self, paintEvent):
        painter = QPainter(self)
        painter.setRenderHint(QPainter.Antialiasing)

        rectangle = QRectF(15.0, 15.0, self.width()-30, self.height()-30)

        painter.setPen(QPen(QColor.fromRgb(62,83,99), 4))
        painter.setBrush(QColor.fromRgb(106,116,131,a=50))
        painter.drawRect(rectangle)

        painter.drawPixmap(0,0,self.point_fixed_normal)

        painter.end()'''

    def mouseReleaseEvent(self, event):
        self.distance = [0,0,False]

    def mousePressEvent(self, event):
        self.distance = [self.cursor().pos().x()-self.x(),self.cursor().pos().y()-self.y()]

    def mouseMoveEvent(self, event):
        if (self.distance[0] < 30 and self.distance[1] < 30) or ((self.distance[0] > 15 and self.distance[1] > 15) and (self.distance[0] < (self.width()-15) and self.distance[1] < (self.height()-15))):
            self.move(self.cursor().pos().x()-self.distance[0],self.cursor().pos().y()-self.distance[1])

    def proportion_button_clicked(self):
        self.keep_proportion = self.proportion_button.isChecked()

    def confirm_button_clicked(self):
        self.cancel_button_clicked()
        self.selected_area = [self.x(), self.y(), self.width(), self.height()]
        app.main.left_panel_add_title.setText(u'<font style="font-size:14px;"><b>' + u'CÓPIA DE TELA' + '</b></font><br><font style="font-size:9px;">' + 'x: ' + str(self.selected_area[0]) + ', y: ' + str(self.selected_area[1]) + ', largura: ' + str(self.selected_area[2]) + ', altura: ' + str(self.selected_area[3]) + '</font>')
        app.main.left_panel_screencopy_preview.setText('')
        app.main.left_panel_screencopy_description.setText(u'<font style="font-size:9px;color:gray;">' + u'ÁREA A SER MOSTADA' + '</font><br><font style="font-size:14px;color:gray;">' + 'x: ' + str(self.selected_area[0]) + ', y: ' + str(self.selected_area[1]) + ', largura: ' + str(self.selected_area[2]) + ', altura: ' + str(self.selected_area[3]) + '</font>')
        app.main.left_panel_addbutton.setEnabled(True)
        app.main.left_panel_addbutton_label.setText(u'<font style="font-size:14px;"><b>' + u'ADICIONAR' + '</b></font><br><font style="font-size:9px;">' + u'NA LISTA DE EXECUÇÃO' + '</font>')


    def cancel_button_clicked(self):
        self.selected_area = False
        self.close()

class mainWindow(QWidget):
    def __init__(self, parent=None):
        super(mainWindow, self).__init__(parent)

        self.actual_playlist_file = False

        self.selected_media = False
        self.highlighted_media = 0
        self.playlist_options_is_visible = False
        self.main_options_box_is_visible = False
        self.main_options_box_selected = False

        #self.setAttribute(Qt.WA_OpaquePaintEvent)
        #self.setAttribute(Qt.WA_NoSystemBackground)
        #self.setWindowFlags(self.windowFlags() | Qt.FramelessWindowHint)
        self.setWindowIcon(QIcon(os.path.join(path_graphics, 'kihvim.png')))
        self.setFocusPolicy(Qt.StrongFocus)

        self.setGeometry(0, 0, QDesktopWidget().screenGeometry().width(), QDesktopWidget().screenGeometry().height())
        self.setWindowTitle('kihvim')

        self.main_background = QLabel(parent=self)
        self.main_background.setStyleSheet('QWidget {border-image: url(' + os.path.join(path_graphics, 'main_background.png').replace('\\', '/') + ') ; } ')

        self.main_options_box = QLabel(parent=self)
        self.main_options_box.setObjectName('main_options_box')
        self.main_options_box.setStyleSheet('QLabel {color:silver;} QCheckBox {color:silver;} #main_options_box { border-top: 34px; border-right: 5px; border-bottom: 0; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "background_options_box.png").replace('\\', '/') + '") 34 5 5 5 stretch stretch; }')
        self.main_options_box_geometry_animation = QPropertyAnimation(self.main_options_box, b'geometry')
        self.main_options_box_geometry_animation.setEasingCurve(QEasingCurve.OutCirc)

        class main_options_file_icon(QLabel):
            def mousePressEvent(widget, event):
                if not self.main_options_box_selected == 'file':
                    self.main_options_box_selected = 'file'
                    self.show_main_options()
                else:
                    self.main_options_box_selected = False
                    self.hide_main_options()
                widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'main_options_file_icon_pressed.png').replace('\\', '/') + '); } ')
            def enterEvent(widget, event):
                if not self.main_options_box_selected == 'file':
                    widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'main_options_file_icon_hover.png').replace('\\', '/') + '); } ')
            def leaveEvent(widget, event):
                if not self.main_options_box_selected == 'file':
                    widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'main_options_file_icon_normal.png').replace('\\', '/') + '); } ')

        self.main_options_file_icon = main_options_file_icon(parent=self.main_options_box)
        self.main_options_file_icon.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'main_options_file_icon_normal.png').replace('\\', '/') + '); } ')

        self.main_options_file_panel = QWidget(self.main_options_box)

        self.main_options_file_open_button = QPushButton(self.main_options_file_panel)
        self.main_options_file_open_button.setStyleSheet('QPushButton { image: url(' + os.path.join(path_graphics, 'open_playlist_icon.png').replace('\\', '/') + '); font-family: "Ubuntu"; font-size:14px; color:white; font-weight:bold; border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
        self.main_options_file_open_button.clicked.connect(lambda:self.main_options_file_open_button_clicked())

        self.main_options_file_open_label = QLabel(u'Abrir\nplaylist',self.main_options_file_panel)

        self.main_options_file_save_button = QPushButton(self.main_options_file_panel)
        self.main_options_file_save_button.setStyleSheet('QPushButton { image: url(' + os.path.join(path_graphics, 'save_playlist_icon.png').replace('\\', '/') + '); font-family: "Ubuntu"; font-size:14px; color:white; font-weight:bold; border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
        self.main_options_file_save_button.clicked.connect(lambda:self.main_options_file_save_button_clicked())

        self.main_options_file_save_label = QLabel(u'Guardar\nplaylist',self.main_options_file_panel)

        class main_options_settings_icon(QLabel):
            def mousePressEvent(widget, event):
                if not self.main_options_box_selected == 'settings':
                    self.main_options_box_selected = 'settings'
                    self.show_main_options()
                else:
                    self.main_options_box_selected = False
                    self.hide_main_options()
                widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'main_options_settings_icon_pressed.png').replace('\\', '/') + '); } ')
            def enterEvent(widget, event):
                if not self.main_options_box_selected == 'settings':
                    widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'main_options_settings_icon_hover.png').replace('\\', '/') + '); } ')
            def leaveEvent(widget, event):
                if not self.main_options_box_selected == 'settings':
                    widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'main_options_settings_icon_normal.png').replace('\\', '/') + '); } ')

        self.main_options_settings_icon = main_options_settings_icon(parent=self.main_options_box)
        self.main_options_settings_icon.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'main_options_settings_icon_normal.png').replace('\\', '/') + '); } ')

        self.main_options_settings_panel = QWidget(self.main_options_box)

        self.main_options_settings_enable_animations = QCheckBox(u'Animações na interface', parent=self.main_options_settings_panel)
        self.main_options_settings_enable_animations.clicked.connect(lambda:self.main_options_settings_enable_animations_clicked())

        self.main_options_settings_framerate = QDial(parent=self.main_options_settings_panel)
        self.main_options_settings_framerate.setMaximum(60)
        self.main_options_settings_framerate.setMinimum(1)
        self.main_options_settings_framerate.valueChanged.connect(lambda:self.main_options_settings_framerate_changing())
        self.main_options_settings_framerate.sliderReleased.connect(lambda:self.main_options_settings_framerate_changed())

        self.main_options_settings_framerate_label = QLabel(parent=self.main_options_settings_panel)

        class main_options_export_icon(QLabel):
            def mousePressEvent(widget, event):
                if not self.main_options_box_selected == 'export':
                    self.main_options_box_selected = 'export'
                    self.show_main_options()
                else:
                    self.main_options_box_selected = False
                    self.hide_main_options()
                widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'main_options_export_icon_pressed.png').replace('\\', '/') + '); } ')
            def enterEvent(widget, event):
                if not self.main_options_box_selected == 'export':
                    widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'main_options_export_icon_hover.png').replace('\\', '/') + '); } ')
            def leaveEvent(widget, event):
                if not self.main_options_box_selected == 'export':
                    widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'main_options_export_icon_normal.png').replace('\\', '/') + '); } ')

        self.main_options_export_icon = main_options_export_icon(parent=self.main_options_box)
        self.main_options_export_icon.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'main_options_export_icon_normal.png').replace('\\', '/') + '); } ')

        self.main_options_export_panel = QWidget(self.main_options_box)

        self.main_options_export_profile = QComboBox(self.main_options_export_panel)
        #self.main_options_export_profile.addItems(['320x180 (iPod/iPhone)', '320x240 (PSP)', '720x480 (DVD player)', '1280x720 (SmartTV)', '704x480 (SmartTV)'])
        self.main_options_export_profile.addItems(['1280x720 (SmartTV)'])

        self.main_options_export_button = QPushButton(u'GERAR VÍDEO', self.main_options_export_panel)
        self.main_options_export_button.setStyleSheet('QPushButton { image: url(' + os.path.join(path_graphics, 'main_options_export_icon_normal.png').replace('\\', '/') + '); font-family: "Ubuntu"; font-size:14px; color:white; font-weight:bold; border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
        self.main_options_export_button.clicked.connect(lambda:self.main_options_export_button_clicked())

        self.playlist = []
        self.last_item = False
        self.selected_addition = False
        self.list_of_songs = {}
        self.settings = {}
        self.list_of_images_from_folder = {}
        self.list_of_images_added = {}
        self.list_of_videos_from_folder = {}
        self.list_of_videos_added = {}
        self.list_of_video_songs = {}
        self.list_of_local_videos = {}
        self.list_of_bible_videos = {}
        self.list_of_library_videos = {}
        self.list_of_cameras = {}

        if os.path.isdir(os.path.join(path_home, 'Downloads')):
            self.path_for_videos = os.path.join(path_home, 'Downloads')
        else:
            self.path_for_videos = path_home

        if os.path.isdir(os.path.join(path_home, 'Downloads')):
            self.path_for_images = os.path.join(path_home, 'Downloads')
        else:
            self.path_for_images = path_home

        self.dialog_options = QFileDialog.DontUseNativeDialog#0 QFileDialog.ReadOnly

        self.screen_width_proportion = float(app.monitor.height())/float(app.monitor.width())
        self.screen_height_proportion = float(app.monitor.width())/float(app.monitor.height())

        #######################################################################
        ## RIGHT PANEL, with monitor

        self.right_panel = QWidget(parent=self)

        self.right_panel_monitor_background = QLabel(parent=self.right_panel)
        self.right_panel_monitor_background.setObjectName('right_panel_monitor_background')
        self.right_panel_monitor_background.setStyleSheet('#right_panel_monitor_background { border-top: 4px; border-right: 4px; border-bottom: 4px; border-left: 4px; border-image: url("' + os.path.join(path_graphics, "black_box.png").replace('\\', '/') + '") 4 4 4 4 stretch stretch; }')

        self.right_panel_monitor_title = QLabel('EXIBINDO AGORA', parent=self.right_panel_monitor_background)
        self.right_panel_monitor_title.setObjectName('right_panel_monitor_title')
        self.right_panel_monitor_title.setStyleSheet('#right_panel_monitor_title { padding-left:3px; padding-bottom:3px; font-family: "Ubuntu"; font-size:11px; font-weight:bold; color:white; border-top: 3px; border-right: 3px; border-left: 3px; border-image: url("' + os.path.join(path_graphics, "monitor_title_background.png").replace('\\', '/') + '") 3 3 0 3 stretch stretch; }')

        class right_panel_monitor(QLabel):
            def enterEvent(widget, event):
                generate_effect(self.right_panel_monitor_withdrawbutton_opacity_animation, 'opacity', 200, self.right_panel_monitor_withdrawbutton_opacity.opacity(), 1.0)
            def leaveEvent(widget, event):
                generate_effect(self.right_panel_monitor_withdrawbutton_opacity_animation, 'opacity', 200, self.right_panel_monitor_withdrawbutton_opacity.opacity(), 0.0)

        self.right_panel_monitor = right_panel_monitor(parent=self.right_panel_monitor_background)
        self.right_panel_monitor.setScaledContents(True)
        self.right_panel_monitor.setStyleSheet('QLabel {font-family: "Ubuntu"; font-size:12pt; font-weight:bold; color:white}')

        self.right_panel_monitor_withdrawbutton = QPushButton(u'TIRAR', parent=self.right_panel_monitor);
        self.right_panel_monitor_withdrawbutton.setStyleSheet('QPushButton { font-family: "Ubuntu"; font-size:14px; color:white; font-weight:bold; border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
        self.right_panel_monitor_withdrawbutton.clicked.connect(lambda:app.monitor.hide_all())
        self.right_panel_monitor_withdrawbutton_opacity = QGraphicsOpacityEffect()
        self.right_panel_monitor_withdrawbutton.setGraphicsEffect(self.right_panel_monitor_withdrawbutton_opacity)
        self.right_panel_monitor_withdrawbutton_opacity.setOpacity(0.0)
        self.right_panel_monitor_withdrawbutton_opacity_animation = QPropertyAnimation(self.right_panel_monitor_withdrawbutton_opacity, b'opacity')

        self.right_panel_monitor_player = QWidget(self.right_panel)

        self.right_panel_monitor_player_stopbutton = QPushButton(self.right_panel_monitor_player)
        self.right_panel_monitor_player_stopbutton.setStyleSheet('QPushButton { image: url(' + os.path.join(path_graphics, 'stop_icon.png').replace('\\', '/') + '); border-top: 5px; border-right: 0px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_dark_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 0px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_dark_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 0px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_dark_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(path_graphics, "button_dark_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
        self.right_panel_monitor_player_stopbutton.clicked.connect(lambda:app.monitor.media_stop())

        self.right_panel_monitor_player_playbutton = QPushButton(self.right_panel_monitor_player)
        self.right_panel_monitor_player_playbutton.setCheckable(True)
        self.right_panel_monitor_player_playbutton.setStyleSheet('QPushButton { image: url(' + os.path.join(path_graphics, 'play_icon.png').replace('\\', '/') + ');  border-top: 5px; border-right: 0px; border-bottom: 5px; border-left: 0px; border-image: url("' + os.path.join(path_graphics, "button_dark_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 0px; border-bottom: 5px; border-left: 0px; border-image: url("' + os.path.join(path_graphics, "button_dark_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 0px; border-bottom: 5px; border-left: 0px; border-image: url("' + os.path.join(path_graphics, "button_dark_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { image: url(' + os.path.join(path_graphics, 'pause_icon.png').replace('\\', '/') + '); border-image: url("' + os.path.join(path_graphics, "button_dark_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:checked { image: url(' + os.path.join(path_graphics, 'pause_icon.png').replace('\\', '/') + '); border-image: url("' + os.path.join(path_graphics, "button_dark_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
        self.right_panel_monitor_player_playbutton.clicked.connect(lambda:app.monitor.media_play_pause())

        self.right_panel_monitor_player_timelinebox = QLabel(parent=self.right_panel_monitor_player)
        self.right_panel_monitor_player_timelinebox.setObjectName('right_panel_monitor_player_timelinebox')
        self.right_panel_monitor_player_timelinebox.setStyleSheet('#right_panel_monitor_player_timelinebox { border-top: 4px; border-right: 4px; border-bottom: 4px; border-left: 0px; border-image: url("' + os.path.join(path_graphics, "black_box.png").replace('\\', '/') + '") 4 4 4 4 stretch stretch; }')

        class right_panel_monitor_player_timeline(QWidget):
            def paintEvent(widget, paintEvent):
                '''painter = QPainter(widget)
                if app.monitor.current_media[0] in ['audio', 'video']:
                    painter.setRenderHint(QPainter.Antialiasing)

                    painter.setPen(QColor.fromRgb(102,102,102))
                    painter.setFont(QFont("Ubuntu", 8))
                    rectangle = QRectF(2, 0, (widget.width()*.5)-2,widget.height()*.5)

                    if app.monitor.current_media[0] == 'video' and app.monitor.current_media[6]:
                        total_time = str(convert_to_timecode((app.monitor.current_media[6] - app.monitor.current_media[5]), True))
                    else:
                        total_time = str(convert_to_timecode(app.monitor.current_media[-2], True))
                    painter.drawText(rectangle, Qt.AlignVCenter | Qt.AlignLeft, total_time)

                    if app.monitor.current_media[0] == 'video' and app.monitor.current_media[6]:
                        time_to_stop = app.monitor.current_media[6]-(app.monitor.video_mediaplayer.get_position()*app.monitor.current_media[-2])
                    else:
                        time_to_stop = app.monitor.current_media[-2]*(1.0-app.monitor.video_mediaplayer.get_position())

                    #if (int(app.monitor.current_media[-2]*(1.0-app.monitor.video_mediaplayer.get_position())) < 31 and int(app.monitor.current_media[-2]*(1.0-app.monitor.video_mediaplayer.get_position())) % 2 == 0) or int(app.monitor.current_media[-2]*(1.0-app.monitor.video_mediaplayer.get_position())) < 10:
                    if (int(time_to_stop) < 31 and int(time_to_stop) % 2 == 0) or int(time_to_stop) < 10:
                        painter.setPen(QColor.fromRgb(255,0,0))
                    else:
                        painter.setPen(QColor.fromRgb(102,102,102))
                    rectangle = QRectF((widget.width()*.5)+2, 0, (widget.width()*.5)-2,widget.height()*.5)
                    if app.monitor.video_mediaplayer.get_position() > 0.0 and app.monitor.video_mediaplayer.is_playing():
                        painter.drawText(rectangle, Qt.AlignVCenter | Qt.AlignRight, str(convert_to_timecode(time_to_stop, True)))



                    painter.setPen(Qt.NoPen)
                    painter.setBrush(QColor.fromRgb(24,33,41))
                    rectangle = QRectF(0, widget.height()*.5, widget.width(),widget.height()*.5)
                    painter.drawRect(rectangle)

                    painter.setBrush(QColor.fromRgb(48,66,81))

                    rectangle = QRectF(0, widget.height()*.5, widget.width()*app.monitor.video_mediaplayer.get_position(),widget.height()*.5)
                    painter.drawRect(rectangle)
                    painter.setPen(QColor.fromRgb(255,255,255))
                    rectangle = QRectF(0, widget.height()*.5, (widget.width()*app.monitor.video_mediaplayer.get_position())-3,widget.height()*.5)
                    painter.drawText(rectangle, Qt.AlignVCenter | Qt.AlignRight, str(convert_to_timecode(app.monitor.current_media[-2]*app.monitor.video_mediaplayer.get_position(), True)))

                painter.end()
                '''
                None

        self.right_panel_monitor_player_timeline = right_panel_monitor_player_timeline(parent=self.right_panel_monitor_player_timelinebox)

        self.right_panel_monitor_prelude = QWidget(self.right_panel)

        self.right_panel_monitor_prelude.time_to_full_volume = False
        self.right_panel_monitor_prelude.song = False

        self.right_panel_monitor_prelude_pausebutton = QPushButton(self.right_panel_monitor_prelude)
        self.right_panel_monitor_prelude_pausebutton.setCheckable(True)
        self.right_panel_monitor_prelude_pausebutton.setStyleSheet('QPushButton { image: url(' + os.path.join(path_graphics, 'prelude_normal.png').replace('\\', '/') + '); padding-right:18px;  border-top: 5px; border-right: 0px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_dark_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 0px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_dark_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 0px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_dark_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { image: url(' + os.path.join(path_graphics, 'prelude_pause.png').replace('\\', '/') + '); border-image: url("' + os.path.join(path_graphics, "button_dark_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:checked { image: url(' + os.path.join(path_graphics, 'prelude_pause.png').replace('\\', '/') + '); border-image: url("' + os.path.join(path_graphics, "button_dark_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
        self.right_panel_monitor_prelude_pausebutton.clicked.connect(lambda:self.right_panel_monitor_prelude_pausebutton_clicked())

        self.right_panel_monitor_prelude_timelinebox = QLabel(parent=self.right_panel_monitor_prelude)
        self.right_panel_monitor_prelude_timelinebox.setObjectName('right_panel_monitor_prelude_timelinebox')
        self.right_panel_monitor_prelude_timelinebox.setStyleSheet('#right_panel_monitor_prelude_timelinebox { border-top: 4px; border-right: 0px; border-bottom: 4px; border-left: 0px; border-image: url("' + os.path.join(path_graphics, "black_box.png").replace('\\', '/') + '") 4 4 4 4 stretch stretch; }')

        class right_panel_monitor_prelude_timeline(QWidget):
            def paintEvent(widget, paintEvent):
                '''painter = QPainter(widget)

                if app.monitor.current_media[0] in ['clock'] and self.right_panel_monitor_prelude.song:
                    painter.setRenderHint(QPainter.Antialiasing)

                    painter.setPen(QColor.fromRgb(102,102,102))
                    painter.setFont(QFont("Ubuntu", 8, weight=75))
                    rectangle = QRectF(32, 0, (widget.width()*.8)-2,widget.height()*.5)
                    painter.drawText(rectangle, Qt.AlignVCenter | Qt.AlignLeft, u'CÂNTICO ' + str(self.right_panel_monitor_prelude.song[4]))

                    painter.setFont(QFont("Ubuntu", 8))
                    if (int(self.right_panel_monitor_prelude.song[3]*(1.0-app.monitor.video_mediaplayer.get_position())) < 31 and int(self.right_panel_monitor_prelude.song[3]*(1.0-app.monitor.video_mediaplayer.get_position())) % 2 == 0) or int(self.right_panel_monitor_prelude.song[3]*(1.0-app.monitor.video_mediaplayer.get_position())) < 10:
                        painter.setPen(QColor.fromRgb(255,0,0))
                    else:
                        painter.setPen(QColor.fromRgb(102,102,102))
                    rectangle = QRectF((widget.width()*.5)+2, 0, (widget.width()*.5)-2,widget.height()*.5)
                    if app.monitor.video_mediaplayer.get_position() > 0.0 and app.monitor.video_mediaplayer.is_playing():
                        painter.drawText(rectangle, Qt.AlignVCenter | Qt.AlignRight, str(convert_to_timecode(self.right_panel_monitor_prelude.song[3]*(1.0-app.monitor.video_mediaplayer.get_position()), True)))

                    painter.setPen(Qt.NoPen)
                    painter.setBrush(QColor.fromRgb(24,33,41))
                    rectangle = QRectF(0, widget.height()*.5, widget.width(),widget.height()*.5)
                    painter.drawRect(rectangle)

                    painter.setBrush(QColor.fromRgb(48,66,81))

                    rectangle = QRectF(0, widget.height()*.5, widget.width()*app.monitor.video_mediaplayer.get_position(),widget.height()*.5)
                    painter.drawRect(rectangle)
                    painter.setPen(QColor.fromRgb(255,255,255))
                    rectangle = QRectF(0, widget.height()*.5, (widget.width()*app.monitor.video_mediaplayer.get_position())-3,widget.height()*.5)
                    painter.drawText(rectangle, Qt.AlignVCenter | Qt.AlignRight, str(convert_to_timecode(self.right_panel_monitor_prelude.song[3]*app.monitor.video_mediaplayer.get_position(), True)))

                painter.end()
                '''
                None

        self.right_panel_monitor_prelude_timeline = right_panel_monitor_prelude_timeline(parent=self.right_panel_monitor_prelude_timelinebox)

        self.right_panel_monitor_prelude_volume = QDial(parent=self.right_panel_monitor_prelude)
        self.right_panel_monitor_prelude_volume.setMaximum(100)
        self.right_panel_monitor_prelude_volume.setMinimum(0)
        self.right_panel_monitor_prelude_volume.setStyleSheet('QDial { border-color:QLinearGradient(  x1: 0, y1: 0, x2: 0, y2: 1.0, stop: 0 #6d7785, stop: 0.49 #546270, stop: 0.5 #4a5968, stop: 1 #304251 ); }')

        self.right_panel_monitor_prelude_volume.valueChanged.connect(lambda:self.right_panel_monitor_prelude_volume_changing())
        #self.right_panel_monitor_prelude_volume.sliderReleased.connect(lambda:right_panel_monitor_prelude_volume_changed(self))

        self.right_panel_monitor_prelude_volume_label = QLabel(parent=self.right_panel_monitor_prelude_volume)
        self.right_panel_monitor_prelude_volume_label.setStyleSheet('QLabel { image: url(' + os.path.join(path_graphics, 'prelude_volume.png').replace('\\', '/') + '); font-family: "Ubuntu"; qproperty-alignment: "AlignCenter"; color:white;}')
        self.right_panel_monitor_prelude_volume_label.setAttribute(Qt.WA_TransparentForMouseEvents)

        self.right_panel_monitor_prelude_nextbutton = QPushButton(self.right_panel_monitor_prelude)
        self.right_panel_monitor_prelude_nextbutton.setStyleSheet('QPushButton { image: url(' + os.path.join(path_graphics, 'prelude_next.png').replace('\\', '/') + '); padding-left:2px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 0px; border-image: url("' + os.path.join(path_graphics, "button_dark_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 0px; border-image: url("' + os.path.join(path_graphics, "button_dark_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 0px; border-image: url("' + os.path.join(path_graphics, "button_dark_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(path_graphics, "button_dark_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
        self.right_panel_monitor_prelude_nextbutton.clicked.connect(lambda:self.right_panel_monitor_prelude_nextbutton_clicked())



        self.right_panel_shownext_box = QLabel(parent=self.right_panel)
        self.right_panel_shownext_box.setObjectName('right_panel_shownext_box')
        self.right_panel_shownext_box.setStyleSheet('#right_panel_shownext_box { border-top: 75px; border-right: 25px; border-left: 25px; border-image: url("' + os.path.join(path_graphics, "shownext_box_background.png").replace('\\', '/') + '") 75 25 0 25 stretch stretch; }')

        self.right_panel_shownext_preview_background = QLabel(parent=self.right_panel_shownext_box)
        self.right_panel_shownext_preview_background.setObjectName('right_panel_shownext_preview_background')
        self.right_panel_shownext_preview_background.setStyleSheet('#right_panel_shownext_preview_background { border-top: 4px; border-right: 4px; border-bottom: 4px; border-left: 4px; border-image: url("' + os.path.join(path_graphics, "black_box.png").replace('\\', '/') + '") 4 4 4 4 stretch stretch; }')

        self.right_panel_shownext_preview_title = QLabel(u'PRÓXIMA EXIBIÇÃO', parent=self.right_panel_shownext_preview_background)
        self.right_panel_shownext_preview_title.setObjectName('right_panel_shownext_preview_title')
        self.right_panel_shownext_preview_title.setStyleSheet('#right_panel_shownext_preview_title { padding-left:3px; padding-top:4px; font-family: "Ubuntu"; font-size:11px; font-weight:bold; color:white; border-bottom: 3px; border-right: 3px; border-left: 3px; border-image: url("' + os.path.join(path_graphics, "shownext_preview_title_background.png").replace('\\', '/') + '") 0 3 3 3 stretch stretch; }')

        self.right_panel_shownext_preview = QWidget(parent=self.right_panel_shownext_box)
        self.right_panel_shownext_preview_black_palette = self.right_panel_shownext_preview.palette()
        self.right_panel_shownext_preview_black_palette.setColor(QPalette.Window, QColor(0,0,0))
        self.right_panel_shownext_preview.setPalette(self.right_panel_shownext_preview_black_palette)
        self.right_panel_shownext_preview.setAutoFillBackground(True)

        self.right_panel_shownext_preview_audio = QWidget(parent=self.right_panel_shownext_preview)
        self.right_panel_shownext_preview_audio_artwork = QLabel(parent=self.right_panel_shownext_preview_audio)
        self.right_panel_shownext_preview_audio_artwork.setAlignment(Qt.AlignCenter)
        self.right_panel_shownext_preview_audio_title = QLabel(parent=self.right_panel_shownext_preview_audio)
        self.right_panel_shownext_preview_audio_title.setAlignment(Qt.AlignVCenter | Qt.AlignLeft)
        self.right_panel_shownext_preview_audio_title.setWordWrap(True)

        self.right_panel_shownext_preview_image = QLabel(parent=self.right_panel_shownext_preview)
        self.right_panel_shownext_preview_image.setAlignment(Qt.AlignCenter)

        self.right_panel_shownext_preview_clock = QLabel(parent=self.right_panel_shownext_preview)
        self.right_panel_shownext_preview_clock.setAlignment(Qt.AlignCenter)

        class right_panel_shownext_button(QLabel):
            def mouseReleaseEvent(widget, event):
                if len(self.playlist) > 0:
                    self.right_panel_shownext_box.setStyleSheet('#right_panel_shownext_box { border-top: 75px; border-right: 25px; border-left: 25px; border-image: url("' + os.path.join(path_graphics, "shownext_box_background_hover.png").replace('\\', '/') + '") 75 25 0 25 stretch stretch; }')
                else:
                    self.right_panel_shownext_box.setStyleSheet('#right_panel_shownext_box { border-top: 75px; border-right: 25px; border-left: 25px; border-image: url("' + os.path.join(path_graphics, "shownext_box_background_disabled.png").replace('\\', '/') + '") 75 25 0 25 stretch stretch; }')
            def mousePressEvent(widget, event):
                if len(self.playlist) > 0:
                    self.show_now_clicked()
                    self.right_panel_shownext_box.setStyleSheet('#right_panel_shownext_box { border-top: 75px; border-right: 25px; border-left: 25px; border-image: url("' + os.path.join(path_graphics, "shownext_box_background_pressed.png").replace('\\', '/') + '") 75 25 0 25 stretch stretch; }')
            def enterEvent(widget, event):
                if len(self.playlist) > 0:
                    self.right_panel_shownext_box.setStyleSheet('#right_panel_shownext_box { border-top: 75px; border-right: 25px; border-left: 25px; border-image: url("' + os.path.join(path_graphics, "shownext_box_background_hover.png").replace('\\', '/') + '") 75 25 0 25 stretch stretch; }')
            def leaveEvent(widget, event):
                if len(self.playlist) > 0:
                    self.right_panel_shownext_box.setStyleSheet('#right_panel_shownext_box { border-top: 75px; border-right: 25px; border-left: 25px; border-image: url("' + os.path.join(path_graphics, "shownext_box_background.png").replace('\\', '/') + '") 75 25 0 25 stretch stretch; }')
                else:
                    self.right_panel_shownext_box.setStyleSheet('#right_panel_shownext_box { border-top: 75px; border-right: 25px; border-left: 25px; border-image: url("' + os.path.join(path_graphics, "shownext_box_background_disabled.png").replace('\\', '/') + '") 75 25 0 25 stretch stretch; }')

        self.right_panel_shownext_button = right_panel_shownext_button(parent=self.right_panel_shownext_box)
        self.right_panel_shownext_button.setScaledContents(True)
        self.right_panel_shownext_button.setStyleSheet('QLabel {font-family: "Ubuntu"; font-size:12pt; font-weight:bold; color:white; qproperty-alignment: "AlignCenter";}')

        #######################################################################
        ## PLAYLIST

        self.playlist_shadow_left = QLabel(parent=self)
        self.playlist_shadow_left.setStyleSheet('QWidget {border-image: url(' + os.path.join(path_graphics, 'playlist_shadow_left.png').replace('\\', '/') + ') ; } ')


        self.playlist_options_panel = QLabel(parent=self)
        self.playlist_options_panel.setObjectName('playlist_options_panel')
        self.playlist_options_panel.setStyleSheet('#playlist_options_panel { border-bottom: 60px; border-left: 40px; border-image: url("' + os.path.join(path_graphics, "playlist_options_background.png").replace('\\', '/') + '") 0 0 60 40 stretch stretch; }')
        self.playlist_options_panel_geometry_animation = QPropertyAnimation(self.playlist_options_panel, b'geometry')
        self.playlist_options_panel_geometry_animation.setEasingCurve(QEasingCurve.OutCirc)


        self.playlist_options_panel_audio_panel = QWidget(parent=self.playlist_options_panel)

        self.playlist_options_panel_audio_panel_show_lyrics = QCheckBox(u'Mostrar letra na tela', parent=self.playlist_options_panel_audio_panel)
        self.playlist_options_panel_audio_panel_show_lyrics.clicked.connect(lambda:self.playlist_options_panel_audio_panel_show_lyrics_clicked())
        #self.playlist_options_panel_audio_panel = QLabel(parent=self.playlist_options_panel)
        #self.playlist_options_panel_audio_panel.setStyleSheet('QLabel { font-family: "Ubuntu"; font-size:12pt; color:gray; qproperty-alignment: "AlignCenter"; }')
        #self.playlist_options_panel_audio_panel.setText(u'Sem opções\npara audio')

        #self.playlist_options_panel_image_panel = QWidget(parent=self.playlist_options_panel)
        self.playlist_options_panel_image_panel = QLabel(parent=self.playlist_options_panel)
        self.playlist_options_panel_image_panel.setStyleSheet('QLabel { font-family: "Ubuntu"; font-size:12pt; color:gray; qproperty-alignment: "AlignCenter"; }')
        self.playlist_options_panel_image_panel.setText(u'Sem opções\npara imagem')

        #self.playlist_options_panel_video_panel = QWidget(parent=self.playlist_options_panel)
        self.playlist_options_panel_video_panel = QLabel(parent=self.playlist_options_panel)
        self.playlist_options_panel_video_panel.setStyleSheet('QLabel { font-family: "Ubuntu"; font-size:12pt; color:gray; qproperty-alignment: "AlignCenter"; }')
        self.playlist_options_panel_video_panel.setText(u'Sem opções\npara video')

        #self.playlist_options_panel_web_panel = QWidget(parent=self.playlist_options_panel)
        self.playlist_options_panel_web_panel = QLabel(parent=self.playlist_options_panel)
        self.playlist_options_panel_web_panel.setStyleSheet('QLabel { font-family: "Ubuntu"; font-size:12pt; color:gray; qproperty-alignment: "AlignCenter"; }')
        self.playlist_options_panel_web_panel.setText(u'Sem opções\npara web')

        #self.playlist_options_panel_clock_panel = QWidget(parent=self.playlist_options_panel)
        self.playlist_options_panel_clock_panel = QLabel(parent=self.playlist_options_panel)
        self.playlist_options_panel_clock_panel.setStyleSheet('QLabel { font-family: "Ubuntu"; font-size:12pt; color:gray; qproperty-alignment: "AlignCenter"; }')
        self.playlist_options_panel_clock_panel.setText(u'Sem opções\npara clock')

        #self.playlist_options_panel_screencopy_panel = QWidget(parent=self.playlist_options_panel)
        self.playlist_options_panel_screencopy_panel = QLabel(parent=self.playlist_options_panel)
        self.playlist_options_panel_screencopy_panel.setStyleSheet('QLabel { font-family: "Ubuntu"; font-size:12pt; color:gray; qproperty-alignment: "AlignCenter"; }')
        self.playlist_options_panel_screencopy_panel.setText(u'Sem opções\npara screencopy')

        #self.playlist_options_panel_camera_panel = QWidget(parent=self.playlist_options_panel)
        self.playlist_options_panel_camera_panel = QLabel(parent=self.playlist_options_panel)
        self.playlist_options_panel_camera_panel.setStyleSheet('QLabel { font-family: "Ubuntu"; font-size:12pt; color:gray; qproperty-alignment: "AlignCenter"; }')
        self.playlist_options_panel_camera_panel.setText(u'Sem opções\npara camera')

        self.playlist_panel = QLabel(parent=self)
        self.playlist_panel.setObjectName('playlist_panel')
        self.playlist_panel.setStyleSheet('#playlist_panel { border-bottom: 80px; border-left: 32px; border-image: url("' + os.path.join(path_graphics, "playlist_background.png").replace('\\', '/') + '") 0 0 80 32 stretch stretch; }')

        class playlist_options_panel_open_button(QLabel):
            #def mouseReleaseEvent(widget, event):
            def mousePressEvent(widget, event):
                if len(self.playlist) > 0:
                    self.playlist_options_is_visible = not self.playlist_options_is_visible
                    self.playlist_options_panel_open_button_clicked()
            def enterEvent(widget, event):
                if self.playlist_options_is_visible:
                    widget.setStyleSheet('#playlist_options_panel_open_button { image: url("' + os.path.join(path_graphics, "playlist_options_panel_open_button_back.png").replace('\\', '/') + '") }')
                elif len(self.playlist) > 0:
                    widget.setStyleSheet('#playlist_options_panel_open_button { image: url("' + os.path.join(path_graphics, "playlist_options_panel_open_button_hover.png").replace('\\', '/') + '") }')
            def leaveEvent(widget, event):
                #if self.highlighted_media:
                widget.setStyleSheet('#playlist_options_panel_open_button { image: url("' + os.path.join(path_graphics, "playlist_options_panel_open_button_normal.png").replace('\\', '/') + '") }')

        self.playlist_options_panel_open_button = playlist_options_panel_open_button(parent=self)
        self.playlist_options_panel_open_button.setObjectName('playlist_options_panel_open_button')
        self.playlist_options_panel_open_button.setStyleSheet('#playlist_options_panel_open_button { image: url("' + os.path.join(path_graphics, "playlist_options_panel_open_button_normal.png").replace('\\', '/') + '") }')
        self.playlist_options_panel_open_button_geometry_animation = QPropertyAnimation(self.playlist_options_panel_open_button, b'geometry')
        self.playlist_options_panel_open_button_geometry_animation.setEasingCurve(QEasingCurve.OutCirc)

        self.playlist_panel_list = QWidget()
        self.playlist_panel_list.setStyleSheet('QWidget { background: transparent; }')
        self.playlist_panel_listgrid = QVBoxLayout(self.playlist_panel_list)
        self.playlist_panel_listgrid.setSpacing(4)
        self.playlist_panel_listgrid.setContentsMargins(5,0,17,0)
        #self.playlist_panel_listgrid.setHorizontalSpacing(2)

        self.playlist_panel_scroll = QScrollArea(parent=self.playlist_panel)
        self.playlist_panel_scroll.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.playlist_panel_scroll.setWidget(self.playlist_panel_list)
        self.playlist_panel_scroll.setFocusPolicy(Qt.NoFocus)
        self.playlist_panel_scroll.setStyleSheet('QScrollArea { background: transparent; }')
        #self.playlist_panel_scroll.horizontalScrollBar().valueChanged.connect(lambda:playlist_panel_scroll_updated(self))

        class playlist_nextarrow(QLabel):
            def mousePressEvent(widget, event):
                self.playlist_nextarrow_clicked()

        self.playlist_nextarrow = playlist_nextarrow(parent=self)
        self.playlist_nextarrow.setObjectName('playlist_nextarrow')
        self.playlist_nextarrow.setStyleSheet('#playlist_nextarrow { border-top: 3px; border-right: 18px; border-bottom: 3px; border-left: 3px; border-image: url("' + os.path.join(path_graphics, "playlist_nextarrow.png").replace('\\', '/') + '") 3 18 3 3 stretch stretch; }')

        self.playlist_nextarrow_preview = QLabel(parent=self.playlist_nextarrow)
        self.playlist_nextarrow_preview.setScaledContents(True)
        self.playlist_nextarrow_preview.setGeometry(13,10,50*self.screen_height_proportion,50)

        self.playlist_nextarrow_description = QLabel(parent=self.playlist_nextarrow)
        self.playlist_nextarrow_description.setStyleSheet('QLabel { padding: 5px; }')

        self.playlist_lastitem = QLabel(parent=self.playlist_panel)
        self.playlist_lastitem.setObjectName('playlist_lastitem')
        self.playlist_lastitem.setStyleSheet('#playlist_lastitem { border-top: 3px; border-right: 3px; border-bottom: 3px; border-left: 3px; border-image: url("' + os.path.join(path_graphics, "item_background.png").replace('\\', '/') + '") 3 3 3 3 stretch stretch; }')

        #######################################################################
        ## ADDITION PANEL

        self.left_panel = QLabel(parent=self)
        self.left_panel.setAttribute(Qt.WA_OpaquePaintEvent)
        #self.left_panel.viewport().setAttribute(Qt.WA_OpaquePaintEvent)
        #.setAttribute(Qt.WA_OpaquePaintEvent)
        #self.left_panel.setAttribute(Qt.WA_NoSystemBackground)
        self.left_panel.setObjectName('left_panel')
        self.left_panel.setStyleSheet('#left_panel { border-top: 75px; border-right: 60px; border-image: url("' + os.path.join(path_graphics, "left_panel_background.png").replace('\\', '/') + '") 75 60 0 0 stretch stretch; }')
        self.left_panel_geometry_animation = QPropertyAnimation(self.left_panel, b'geometry')
        self.left_panel_geometry_animation.setEasingCurve(QEasingCurve.OutCirc)

        self.left_panel_add_title = QLabel(parent=self.left_panel)
        self.left_panel_add_title.setObjectName('left_panel_add_title')
        self.left_panel_add_title.setStyleSheet('#left_panel_add_title { padding:5px; font-family: "Ubuntu"; font-size:12pt; font-weight:bold; color:white; qproperty-alignment: "AlignRight | AlignVCenter"; border-top: 2px; border-bottom: 2px;  border-image: url("' + os.path.join(path_graphics, "add_title_background.png").replace('\\', '/') + '") 2 0 2 0 stretch stretch; }')

        self.left_panel_addbutton = QPushButton(self)
        self.left_panel_addbutton.setStyleSheet('QPushButton { font-family: "Ubuntu"; font-size:12pt; font-weight:bold; color:white; border-right: 12px; border-image: url("' + os.path.join(path_graphics, "add_button_normal.png").replace('\\', '/') + '") 0 12 0 0 stretch stretch; outline: none; } QPushButton:hover:pressed {  border-right: 12px; border-image: url("' + os.path.join(path_graphics, "add_button_pressed.png").replace('\\', '/') + '") 0 12 0 0 stretch stretch; outline: none; } QPushButton:hover { border-right: 12px; border-image: url("' + os.path.join(path_graphics, "add_button_hover.png").replace('\\', '/') + '") 0 12 0 0 stretch stretch; outline: none; }')
        self.left_panel_addbutton_geometry_animation = QPropertyAnimation(self.left_panel_addbutton, b'geometry')
        self.left_panel_addbutton_geometry_animation.setEasingCurve(QEasingCurve.OutCirc)
        self.left_panel_addbutton.clicked.connect(lambda:self.left_panel_addbutton_clicked())

        self.left_panel_addbutton_label = QLabel(self.left_panel_addbutton)
        self.left_panel_addbutton_label.setStyleSheet('QLabel { padding:5px; font-family: "Ubuntu"; color:white; qproperty-alignment: "AlignLeft | AlignVCenter"; }')
        self.left_panel_addbutton_label.setAttribute(Qt.WA_TransparentForMouseEvents)

        self.left_panel_icons = QWidget(parent=self.left_panel)

        #######################################################################
        ## ADDITION PANEL - AUDIO

        class left_panel_audio_icon(QLabel):
            #def mouseReleaseEvent(widget, event):
            #    widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'music_icon_hover.png').replace('\\', '/') + '); } ')
            def mousePressEvent(widget, event):
                if not self.selected_addition == 'audio':
                    self.selected_addition = 'audio'
                    self.show_additions()
                else:
                    self.selected_addition = False
                    self.hide_additions()
                widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'music_icon_pressed.png').replace('\\', '/') + '); } ')
            def enterEvent(widget, event):
                if not self.selected_addition == 'audio':
                    widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'music_icon_hover.png').replace('\\', '/') + '); } ')
            def leaveEvent(widget, event):
                if not self.selected_addition == 'audio':
                    widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'music_icon_normal.png').replace('\\', '/') + '); } ')

        self.left_panel_audio_icon = left_panel_audio_icon(parent=self.left_panel_icons)
        self.left_panel_audio_icon.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'music_icon_normal.png').replace('\\', '/') + '); } ')

        self.left_panel_audio_panel = QWidget(self.left_panel)

        self.left_panel_audio_alert = QLabel(u'Parece que não há arquivos de cânticos importados no kihvim. Adicione os cânticos usando o botão abaixo.',parent=self.left_panel_audio_panel)
        self.left_panel_audio_alert.setWordWrap(True)
        self.left_panel_audio_alert.setStyleSheet('QLabel {padding: 50px; font-family: "Ubuntu"; font-size:12pt; color:gray; qproperty-alignment:"AlignCenter" ; }')

        self.left_panel_audio_list = QListWidget(parent=self.left_panel_audio_panel)
        self.left_panel_audio_list.setViewMode(QListView.IconMode)
        self.left_panel_audio_list.setIconSize(QSize(48, 48))
        self.left_panel_audio_list.setSpacing(5)
        self.left_panel_audio_list.currentItemChanged.connect(lambda:self.left_panel_audio_list_clicked())

        self.left_panel_audio_download_lyrics = QPushButton(parent=self.left_panel_audio_panel)
        self.left_panel_audio_download_lyrics.setIconSize(QSize(18, 18))
        self.left_panel_audio_download_lyrics.setIcon(QIcon(os.path.join(path_graphics, "get_web_icon.png")))
        self.left_panel_audio_download_lyrics.setStyleSheet('QPushButton { color:white; font-size:12px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
        self.left_panel_audio_download_lyrics.clicked.connect(lambda:self.left_panel_audio_download_lyrics_clicked())

        self.left_panel_audio_add = QPushButton(parent=self.left_panel_audio_panel)
        self.left_panel_audio_add.setIconSize(QSize(18, 18))
        self.left_panel_audio_add.setIcon(QIcon(os.path.join(path_graphics, "add_icon.png")))
        self.left_panel_audio_add.setStyleSheet('QPushButton { color:white; font-size:12px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
        self.left_panel_audio_add.clicked.connect(lambda:self.left_panel_audio_add_clicked())

        self.left_panel_audio_edit = QPushButton(parent=self.left_panel_audio_panel)
        self.left_panel_audio_edit.setCheckable(True)
        self.left_panel_audio_edit.setIconSize(QSize(18, 18))
        self.left_panel_audio_edit.setIcon(QIcon(os.path.join(path_graphics, "edit_icon.png")))
        self.left_panel_audio_edit.setStyleSheet('QPushButton { color:white; font-size:12px; border-top: 5px; border-right: 0px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 0px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 0px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:checked { border-top: 5px; border-right: 0px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
        self.left_panel_audio_edit.clicked.connect(lambda:self.left_panel_audio_edit_clicked())

        self.left_panel_audio_remove = QPushButton(parent=self.left_panel_audio_panel)
        self.left_panel_audio_remove.setIconSize(QSize(18, 18))
        self.left_panel_audio_remove.setIcon(QIcon(os.path.join(path_graphics, "remove_icon.png")))
        self.left_panel_audio_remove.setStyleSheet('QPushButton { color:white; font-size:12px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 0px; border-image: url("' + os.path.join(path_graphics, "button_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 0px; border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 0px; border-image: url("' + os.path.join(path_graphics, "button_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
        self.left_panel_audio_remove.clicked.connect(lambda:self.left_panel_audio_remove_clicked())

        self.left_panel_audio_edit_panel = QWidget(parent=self.left_panel_audio_panel)

        self.left_panel_audio_edit_number_label = QLabel(u'NÚMERO',parent=self.left_panel_audio_edit_panel)
        self.left_panel_audio_edit_number_label.setStyleSheet('QLabel {font-family: "Ubuntu"; font-size:10px; color:black; qproperty-alignment:"AlignLeft" ; }')

        self.left_panel_audio_edit_number = QLabel(parent=self.left_panel_audio_edit_panel)
        self.left_panel_audio_edit_number.setStyleSheet('QLabel {font-family: "Ubuntu"; font-weight:bold;  font-size:20px; color:black; qproperty-alignment:"AlignLeft" ; }')

        self.left_panel_audio_edit_title_label = QLabel(u'TÍTULO',parent=self.left_panel_audio_edit_panel)
        self.left_panel_audio_edit_title_label.setStyleSheet('QLabel {font-family: "Ubuntu"; font-size:10px; color:black; qproperty-alignment:"AlignLeft" ; }')

        self.left_panel_audio_edit_title = QLineEdit(parent=self.left_panel_audio_edit_panel)

        self.left_panel_audio_edit_filepath_label = QLabel(u'CAMINHO DO ARQUIVO',parent=self.left_panel_audio_edit_panel)
        self.left_panel_audio_edit_filepath_label.setStyleSheet('QLabel {font-family: "Ubuntu"; font-size:10px; color:black; qproperty-alignment:"AlignLeft" ; }')

        self.left_panel_audio_edit_filepath = QLabel(parent=self.left_panel_audio_edit_panel)
        self.left_panel_audio_edit_filepath.setStyleSheet('QLabel {font-family: "Ubuntu"; font-size:20px; color:black; qproperty-alignment:"AlignLeft" ; }')

        self.left_panel_audio_edit_lyrics_label = QLabel(u'LETRA',parent=self.left_panel_audio_edit_panel)
        self.left_panel_audio_edit_lyrics_label.setStyleSheet('QLabel {font-family: "Ubuntu"; font-size:10px; color:black; qproperty-alignment:"AlignLeft" ; }')

        class left_panel_audio_edit_lyrics(QPlainTextEdit):
            def enterEvent(widget, event):
                self.left_panel_audio_edit_lyrics_get_from_web.setVisible(True)
            def leaveEvent(widget, event):
                self.left_panel_audio_edit_lyrics_get_from_web.setVisible(False)

        self.left_panel_audio_edit_lyrics = left_panel_audio_edit_lyrics(parent=self.left_panel_audio_edit_panel)
        self.left_panel_audio_edit_lyrics.setStyleSheet('QLabel {font-family: "Ubuntu"; font-size:12px; color:black; qproperty-alignment:"AlignLeft" ; }')

        self.left_panel_audio_edit_lyrics_get_from_web = QPushButton(parent=self.left_panel_audio_edit_lyrics)
        self.left_panel_audio_edit_lyrics_get_from_web.setIconSize(QSize(18, 18))
        self.left_panel_audio_edit_lyrics_get_from_web.setIcon(QIcon(os.path.join(path_graphics, "get_web_icon.png")))
        self.left_panel_audio_edit_lyrics_get_from_web.setStyleSheet('QPushButton { color:white; font-size:12px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
        self.left_panel_audio_edit_lyrics_get_from_web.clicked.connect(lambda:self.left_panel_audio_edit_lyrics_get_from_web_clicked())
        self.left_panel_audio_edit_lyrics_get_from_web.setVisible(False)


        #######################################################################
        ## ADDITION PANEL - IMAGE

        class left_panel_image_icon(QLabel):
            #def mouseReleaseEvent(widget, event):
            #    widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'image_icon_hover.png').replace('\\', '/') + '); } ')
            def mousePressEvent(widget, event):
                if not self.selected_addition == 'image':
                    self.selected_addition = 'image'
                    self.show_additions()
                else:
                    self.selected_addition = False
                    self.hide_additions()
                widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'image_icon_pressed.png').replace('\\', '/') + '); } ')
            def enterEvent(widget, event):
                if not self.selected_addition == 'image':
                    widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'image_icon_hover.png').replace('\\', '/') + '); } ')
            def leaveEvent(widget, event):
                if not self.selected_addition == 'image':
                    widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'image_icon_normal.png').replace('\\', '/') + '); } ')

        self.left_panel_image_icon = left_panel_image_icon(parent=self.left_panel_icons)
        self.left_panel_image_icon.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'image_icon_normal.png').replace('\\', '/') + '); } ')

        self.left_panel_image_panel = QWidget(self.left_panel)

        self.left_panel_image_tab = QTabWidget(parent=self.left_panel_image_panel)
        self.left_panel_image_tab.setTabPosition(QTabWidget.North)
        self.left_panel_image_tab.currentChanged.connect(lambda:self.left_panel_image_tab_changed())

        self.left_panel_image_tab_local = QWidget(parent=self)

        self.left_panel_image_alert = QLabel(u'Parece que não há arquivos de imagens disponíveis na pasta selecionada. Adicione imagens usando o botão abaixo.',parent=self.left_panel_image_tab_local)
        self.left_panel_image_alert.setWordWrap(True)
        self.left_panel_image_alert.setStyleSheet('QLabel {padding: 50px; font-family: "Ubuntu"; font-size:12pt; color:gray; qproperty-alignment:"AlignCenter" ; }')

        self.left_panel_image_list = QListWidget(parent=self.left_panel_image_tab_local)
        self.left_panel_image_list.setIconSize(QSize(56*self.screen_height_proportion, 56))
        self.left_panel_image_list.setSpacing(5)
        self.left_panel_image_list.currentItemChanged.connect(lambda:self.left_panel_image_list_clicked())

        self.left_panel_image_add = QPushButton(parent=self.left_panel_image_tab_local)
        self.left_panel_image_add.setIconSize(QSize(18, 18))
        self.left_panel_image_add.setIcon(QIcon(os.path.join(path_graphics, "add_icon.png")))
        self.left_panel_image_add.setStyleSheet('QPushButton { color:white; font-size:12px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
        self.left_panel_image_add.clicked.connect(lambda:self.left_panel_image_add_clicked())

        class left_panel_image_path_label(QLabel):
            def enterEvent(widget, event):
                self.left_panel_image_edit.setVisible(True)
            def leaveEvent(widget, event):
                self.left_panel_image_edit.setVisible(False)

        self.left_panel_image_path_label = left_panel_image_path_label(parent=self.left_panel_image_tab_local)
        self.left_panel_image_path_label.setAlignment(Qt.AlignVCenter | Qt.AlignRight)

        self.left_panel_image_edit = QPushButton(parent=self.left_panel_image_path_label)
        self.left_panel_image_edit.setIconSize(QSize(18, 18))
        self.left_panel_image_edit.setIcon(QIcon(os.path.join(path_graphics, "edit_icon.png")))
        self.left_panel_image_edit.setStyleSheet('QPushButton { color:white; font-size:12px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:checked { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
        self.left_panel_image_edit.clicked.connect(lambda:self.left_panel_image_edit_clicked())

        self.left_panel_image_remove = QPushButton(parent=self.left_panel_image_tab_local)
        self.left_panel_image_remove.setIconSize(QSize(18, 18))
        self.left_panel_image_remove.setIcon(QIcon(os.path.join(path_graphics, "remove_icon.png")))
        self.left_panel_image_remove.setStyleSheet('QPushButton { color:white; font-size:12px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
        self.left_panel_image_remove.clicked.connect(lambda:self.left_panel_image_remove_clicked())

        self.left_panel_image_tab.blockSignals(True)
        self.left_panel_image_tab.addTab(self.left_panel_image_tab_local, 'DO COMPUTADOR')
        self.left_panel_image_tab.blockSignals(False)

        self.left_panel_image_tab_web = QWidget(parent=self)

        self.left_panel_image_keyword = QLineEdit(parent=self.left_panel_image_tab_web)
        self.left_panel_image_keyword.setText('')
        self.left_panel_image_keyword.setStyleSheet('QLabel {font-family: "Ubuntu"; font-weight:bold;  font-size:20px; color:black; qproperty-alignment:"AlignLeft" ; }')
        self.left_panel_image_keyword.textEdited.connect(lambda:self.left_panel_image_keyword_changed())

        self.left_panel_image_keyword_go = QPushButton(u'PESQUISAR', parent=self.left_panel_image_tab_web)
        self.left_panel_image_keyword_go.setStyleSheet('QPushButton { color:white; font-size:12px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 0px; border-image: url("' + os.path.join(path_graphics, "button_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 0px; border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 0px; border-image: url("' + os.path.join(path_graphics, "button_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
        self.left_panel_image_keyword_go.clicked.connect(lambda:self.left_panel_image_keyword_go_clicked())

        #self.left_panel_image_webview = QWebEngineView(parent=self.left_panel_image_tab_web)

        class LeftWebView(QWebEngineView):
            def createWindow(widget, windowType):

                #if windowType == QWebEnginePage.WebBrowserTab:
                #    self = LeftWebView()
                #    print('2')
                #    self.setAttribute(Qt.WA_DeleteOnClose, True)
                #    print('3')
                #    self.show()
                #    print('4')
                #    #return self.LeftWebView
                return widget
                #    print('5')
                #return super(LeftWebView, self).createWindow(windowType)
                #print('6')
                #return widget#self.left_panel_image_keyword_load_finished(widget.url())

        self.left_panel_image_webview = LeftWebView(parent=self.left_panel_image_tab_web)




        self.left_panel_image_webview.settings().setAttribute(QWebEngineSettings.JavascriptEnabled, True)
        self.left_panel_image_webview.settings().setAttribute(QWebEngineSettings.JavascriptCanOpenWindows, True)
        #self.left_panel_image_webview.page().setLinkDelegationPolicy(QWebEnginePage.DelegateAllLinks)
        #ui->webView->settings()->setAttribute(QWebSettings::JavascriptCanOpenWindows, true);
        self.left_panel_image_webview.loadFinished.connect(lambda:self.left_panel_image_keyword_load_finished())
        #self.left_panel_image_webview.linkClicked.connect(lambda:self.left_panel_image_keyword_load_finished())

        self.left_panel_image_webview_warning = QLabel(u'Faça uma pesquisa de imagens usando o campo acima. Ao encontrar a imagem desejada, clique no link que abrirá a imagem (geralmente algo como "Visualizar arquivo").',parent=self.left_panel_image_tab_web)
        self.left_panel_image_webview_warning.setWordWrap(True)
        self.left_panel_image_webview_warning.setStyleSheet('QLabel {padding: 50px; font-family: "Ubuntu"; font-size:12pt; color:gray; qproperty-alignment:"AlignCenter" ; }')

        class left_panel_image_webview_preview(QLabel):
            def enterEvent(widget, event):
                self.left_panel_image_webview_preview_cancel_button.setVisible(True)
            def leaveEvent(widget, event):
                self.left_panel_image_webview_preview_cancel_button.setVisible(False)

        self.left_panel_image_webview_preview = left_panel_image_webview_preview(parent=self.left_panel_image_tab_web)
        self.left_panel_image_webview_preview.setWordWrap(True)
        self.left_panel_image_webview_preview.setObjectName('left_panel_image_webview_preview')
        self.left_panel_image_webview_preview.setStyleSheet('#left_panel_image_webview_preview { border-top: 4px; border-right: 4px; border-bottom: 4px; border-left: 4px; border-image: url("' + os.path.join(path_graphics, "black_box.png").replace('\\', '/') + '") 4 4 4 4 stretch stretch; }')

        self.left_panel_image_webview_preview_cancel_button = QPushButton(u'CANCELAR', parent=self.left_panel_image_webview_preview)
        self.left_panel_image_webview_preview_cancel_button.setStyleSheet('QPushButton { color:white; font-size:12px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:checked { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
        self.left_panel_image_webview_preview_cancel_button.clicked.connect(lambda:self.left_panel_image_webview_preview_cancel_button_clicked())

        self.left_panel_image_tab.blockSignals(True)
        self.left_panel_image_tab.addTab(self.left_panel_image_tab_web, 'DO JW.ORG')
        self.left_panel_image_tab.blockSignals(False)


        #######################################################################
        ## ADDITION PANEL - VIDEO

        class left_panel_video_icon(QLabel):
            #def mouseReleaseEvent(widget, event):
            #    widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'video_icon_hover.png').replace('\\', '/') + '); } ')
            def mousePressEvent(widget, event):
                if not self.selected_addition == 'video':
                    self.selected_addition = 'video'
                    self.show_additions()
                else:
                    self.selected_addition = False
                    self.hide_additions()
                widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'video_icon_pressed.png').replace('\\', '/') + '); } ')
            def enterEvent(widget, event):
                if not self.selected_addition == 'video':
                    widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'video_icon_hover.png').replace('\\', '/') + '); } ')
            def leaveEvent(widget, event):
                if not self.selected_addition == 'video':
                    widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'video_icon_normal.png').replace('\\', '/') + '); } ')

        self.left_panel_video_icon = left_panel_video_icon(parent=self.left_panel_icons)
        self.left_panel_video_icon.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'video_icon_normal.png').replace('\\', '/') + '); } ')

        self.left_panel_video_panel = QWidget(self.left_panel)

        self.left_panel_video_tab = QTabWidget(parent=self.left_panel_video_panel)
        self.left_panel_video_tab.setTabPosition(QTabWidget.North)
        self.left_panel_video_tab.currentChanged.connect(lambda:self.left_panel_video_tab_changed())

        self.left_panel_video_tab_songs = QWidget(parent=self)

        self.left_panel_video_song_alert = QLabel(u'Parece que não há arquivos de vídeos de cânticos importados no kihvim. Adicione os cânticos usando o botão abaixo.',parent=self.left_panel_video_tab_songs)
        self.left_panel_video_song_alert.setWordWrap(True)
        self.left_panel_video_song_alert.setStyleSheet('QLabel {padding: 50px; font-family: "Ubuntu"; font-size:12pt; color:gray; qproperty-alignment:"AlignCenter" ; }')

        self.left_panel_video_song_list = QListWidget(parent=self.left_panel_video_tab_songs)
        self.left_panel_video_song_list.setViewMode(QListView.IconMode)
        self.left_panel_video_song_list.setIconSize(QSize(48, 48))
        self.left_panel_video_song_list.setSpacing(5)
        self.left_panel_video_song_list.currentItemChanged.connect(lambda:self.left_panel_video_song_list_clicked())

        self.left_panel_video_song_add = QPushButton(parent=self.left_panel_video_tab_songs)
        self.left_panel_video_song_add.setIconSize(QSize(18, 18))
        self.left_panel_video_song_add.setIcon(QIcon(os.path.join(path_graphics, "add_icon.png")))
        self.left_panel_video_song_add.setStyleSheet('QPushButton { color:white; font-size:12px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
        self.left_panel_video_song_add.clicked.connect(lambda:self.left_panel_video_song_add_clicked())

        self.left_panel_video_song_remove = QPushButton(parent=self.left_panel_video_tab_songs)
        self.left_panel_video_song_remove.setIconSize(QSize(18, 18))
        self.left_panel_video_song_remove.setIcon(QIcon(os.path.join(path_graphics, "remove_icon.png")))
        self.left_panel_video_song_remove.setStyleSheet('QPushButton { color:white; font-size:12px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
        self.left_panel_video_song_remove.clicked.connect(lambda:self.left_panel_video_song_remove_clicked())






        self.left_panel_video_tab.blockSignals(True)
        self.left_panel_video_tab.addTab(self.left_panel_video_tab_songs, u'CÂNTICOS')
        self.left_panel_video_tab.blockSignals(False)

        self.left_panel_video_tab_local = QWidget(parent=self)

        self.left_panel_video_local_alert = QLabel(u'Parece que não há arquivos de videos disponíveis na pasta selecionada. Adicione videos usando o botão abaixo.',parent=self.left_panel_video_tab_local)
        self.left_panel_video_local_alert.setWordWrap(True)
        self.left_panel_video_local_alert.setStyleSheet('QLabel {padding: 50px; font-family: "Ubuntu"; font-size:12pt; color:gray; qproperty-alignment:"AlignCenter" ; }')

        self.left_panel_video_local_list = QListWidget(parent=self.left_panel_video_tab_local)
        self.left_panel_video_local_list.setIconSize(QSize(56*self.screen_height_proportion, 56))
        self.left_panel_video_local_list.setSpacing(5)
        self.left_panel_video_local_list.currentItemChanged.connect(lambda:self.left_panel_video_local_list_clicked())

        self.left_panel_video_add = QPushButton(parent=self.left_panel_video_tab_local)
        self.left_panel_video_add.setIconSize(QSize(18, 18))
        self.left_panel_video_add.setIcon(QIcon(os.path.join(path_graphics, "add_icon.png")))
        self.left_panel_video_add.setStyleSheet('QPushButton { color:white; font-size:12px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
        self.left_panel_video_add.clicked.connect(lambda:self.left_panel_video_add_clicked())

        class left_panel_video_path_label(QLabel):
            def enterEvent(widget, event):
                self.left_panel_video_edit.setVisible(True)
            def leaveEvent(widget, event):
                self.left_panel_video_edit.setVisible(False)

        self.left_panel_video_path_label = left_panel_video_path_label(parent=self.left_panel_video_tab_local)
        self.left_panel_video_path_label.setAlignment(Qt.AlignVCenter | Qt.AlignRight)

        self.left_panel_video_edit = QPushButton(parent=self.left_panel_video_tab_local)
        self.left_panel_video_edit.setIconSize(QSize(18, 18))
        self.left_panel_video_edit.setIcon(QIcon(os.path.join(path_graphics, "edit_icon.png")))
        self.left_panel_video_edit.setStyleSheet('QPushButton { color:white; font-size:12px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:checked { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
        self.left_panel_video_edit.clicked.connect(lambda:self.left_panel_video_edit_clicked())

        self.left_panel_video_remove = QPushButton(parent=self.left_panel_video_tab_local)
        self.left_panel_video_remove.setIconSize(QSize(18, 18))
        self.left_panel_video_remove.setIcon(QIcon(os.path.join(path_graphics, "remove_icon.png")))
        self.left_panel_video_remove.setStyleSheet('QPushButton { color:white; font-size:12px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
        self.left_panel_video_remove.clicked.connect(lambda:self.left_panel_video_remove_clicked())

        self.left_panel_video_tab.blockSignals(True)
        self.left_panel_video_tab.addTab(self.left_panel_video_tab_local, 'DO COMPUTADOR')
        self.left_panel_video_tab.blockSignals(False)

        self.left_panel_video_tab_bible = QWidget(parent=self)

        self.left_panel_video_bible_alert = QLabel(u'Parece que não há arquivos de videos da Bíblia disponíveis.',parent=self.left_panel_video_tab_bible)
        self.left_panel_video_bible_alert.setWordWrap(True)
        self.left_panel_video_bible_alert.setStyleSheet('QLabel {padding: 50px; font-family: "Ubuntu"; font-size:12pt; color:gray; qproperty-alignment:"AlignCenter" ; }')

        self.left_panel_video_bible_search = QLineEdit(parent=self.left_panel_video_tab_bible)
        self.left_panel_video_bible_search.textEdited.connect(lambda:self.populate_video_bible_books_list())

        class left_panel_video_bible_book_label(QLabel):
            def mousePressEvent(widget, event):
                self.populate_video_bible_books_list()

        self.left_panel_video_bible_book_label = left_panel_video_bible_book_label(parent=self.left_panel_video_tab_bible)
        self.left_panel_video_bible_book_label.setStyleSheet('QLabel {background-color:gray; font-family: "Ubuntu Condensed"; font-size:28px; color:white; qproperty-alignment:"AlignCenter"; }')

        class left_panel_video_bible_chapter_label(QLabel):
            def mousePressEvent(widget, event):
                self.populate_video_bible_chapters_list()

        self.left_panel_video_bible_chapter_label = left_panel_video_bible_chapter_label(parent=self.left_panel_video_tab_bible)
        self.left_panel_video_bible_chapter_label.setStyleSheet('QLabel {background-color:silver; font-family: "Ubuntu Condensed"; font-size:28px; color:white; qproperty-alignment:"AlignCenter"; }')

        class left_panel_video_bible_verse_label(QLabel):
            def mousePressEvent(widget, event):
                self.populate_video_bible_verses_list()

        self.left_panel_video_bible_verse_label = left_panel_video_bible_verse_label(parent=self.left_panel_video_tab_bible)
        self.left_panel_video_bible_verse_label.setStyleSheet('QLabel {background-color:silver; font-family: "Ubuntu Condensed"; font-size:28px; color:white; qproperty-alignment:"AlignCenter"; }')

        self.left_panel_video_bible_selected_book = False
        self.left_panel_video_bible_selected_chapter = False
        self.left_panel_video_bible_selected_verse = False

        self.left_panel_video_bible_list = QListWidget(parent=self.left_panel_video_tab_bible)
        self.left_panel_video_bible_list.setViewMode(QListView.IconMode)
        self.left_panel_video_bible_list.setSpacing(5)
        self.left_panel_video_bible_list.currentItemChanged.connect(lambda:self.left_panel_video_bible_list_clicked())

        self.left_panel_video_tab.blockSignals(True)
        self.left_panel_video_tab.addTab(self.left_panel_video_tab_bible, u'BÍBLIA')
        self.left_panel_video_tab.blockSignals(False)

        self.left_panel_video_tab_library = QWidget(parent=self)

        self.left_panel_video_library_alert = QLabel(u'Parece que não há arquivos de videos disponíveis como biblioteca.',parent=self.left_panel_video_tab_library)
        self.left_panel_video_library_alert.setWordWrap(True)
        self.left_panel_video_library_alert.setStyleSheet('QLabel {padding: 50px; font-family: "Ubuntu"; font-size:12pt; color:gray; qproperty-alignment:"AlignCenter" ; }')

        self.left_panel_video_library_search = QLineEdit(parent=self.left_panel_video_tab_library)
        self.left_panel_video_library_search.textEdited.connect(lambda:self.populate_video_library_albums_list())

        class left_panel_video_library_album_label(QLabel):
            def mousePressEvent(widget, event):
                self.populate_video_library_albums_list()

        self.left_panel_video_library_album_label = left_panel_video_library_album_label(parent=self.left_panel_video_tab_library)
        #self.left_panel_video_library_album_label.setStyleSheet('QLabel {background-color:gray; font-family: "Ubuntu Condensed"; font-size:28px; color:white; qproperty-alignment:"AlignCenter"; }')
        self.left_panel_video_library_album_label.setScaledContents(True)

        class left_panel_video_library_title_label(QLabel):
            def mousePressEvent(widget, event):
                self.populate_video_library_titles_list()

        self.left_panel_video_library_title_label = left_panel_video_library_title_label(parent=self.left_panel_video_tab_library)
        #self.left_panel_video_library_title_label.setStyleSheet('QLabel {background-color:silver; font-family: "Ubuntu Condensed"; font-size:28px; color:white; qproperty-alignment:"AlignCenter"; }')
        self.left_panel_video_library_title_label.setScaledContents(True)

        class left_panel_video_library_chapter_label(QLabel):
            def mousePressEvent(widget, event):
                self.populate_video_library_chapters_list()

        self.left_panel_video_library_chapter_label = left_panel_video_library_chapter_label(parent=self.left_panel_video_tab_library)
        self.left_panel_video_library_chapter_label.setStyleSheet('QLabel {padding:8px; background-color:silver; font-family: "Ubuntu Condensed"; font-size:16px; color:white; qproperty-alignment:"AlignLeft | AlignVCenter"; }')
        self.left_panel_video_library_chapter_label.setScaledContents(True)

        self.left_panel_video_library_selected_album = False
        self.left_panel_video_library_selected_title = False
        self.left_panel_video_library_selected_chapter = False

        self.left_panel_video_library_list = QListWidget(parent=self.left_panel_video_tab_library)
        #self.left_panel_video_library_list.setViewMode(QListView.IconMode)
        self.left_panel_video_library_list.setSpacing(5)
        self.left_panel_video_library_list.currentItemChanged.connect(lambda:self.left_panel_video_library_list_clicked())

        self.left_panel_video_tab.blockSignals(True)
        self.left_panel_video_tab.addTab(self.left_panel_video_tab_library, 'BIBLIOTECA')
        self.left_panel_video_tab.blockSignals(False)

        #######################################################################
        ## ADDITION PANEL - WEB

        class left_panel_web_icon(QLabel):
            #def mouseReleaseEvent(widget, event):
            #    widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'web_icon_hover.png').replace('\\', '/') + '); } ')
            def mousePressEvent(widget, event):
                if not self.selected_addition == 'web':
                    self.selected_addition = 'web'
                    self.show_additions()
                else:
                    self.selected_addition = False
                    self.hide_additions()
                widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'web_icon_pressed.png').replace('\\', '/') + '); } ')
            def enterEvent(widget, event):
                if not self.selected_addition == 'web':
                    widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'web_icon_hover.png').replace('\\', '/') + '); } ')
            def leaveEvent(widget, event):
                if not self.selected_addition == 'web':
                    widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'web_icon_normal.png').replace('\\', '/') + '); } ')

        self.left_panel_web_icon = left_panel_web_icon(parent=self.left_panel_icons)
        self.left_panel_web_icon.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'web_icon_normal.png').replace('\\', '/') + '); } ')

        self.left_panel_web_panel = QWidget(self.left_panel)

        self.left_panel_web_address_label = QLabel(u'ENDEREÇO',parent=self.left_panel_web_panel)
        self.left_panel_web_address_label.setStyleSheet('QLabel {font-family: "Ubuntu"; font-size:10px; color:black; qproperty-alignment:"AlignLeft" ; }')

        self.left_panel_web_address = QLineEdit(parent=self.left_panel_web_panel)
        self.left_panel_web_address.setStyleSheet('QLabel {font-family: "Ubuntu"; font-weight:bold;  font-size:20px; color:black; qproperty-alignment:"AlignLeft" ; }')

        self.left_panel_web_address_go = QPushButton(u'TESTAR', parent=self.left_panel_web_panel)
        self.left_panel_web_address_go.setStyleSheet('QPushButton { color:white; font-size:12px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 0px; border-image: url("' + os.path.join(path_graphics, "button_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 0px; border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 0px; border-image: url("' + os.path.join(path_graphics, "button_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
        self.left_panel_web_address_go.clicked.connect(lambda:self.left_panel_web_address_go_clicked())

        self.left_panel_web_webview = QWebEngineView(parent=self.left_panel_web_panel)

        #######################################################################
        ## ADDITION PANEL - CLOCK

        class left_panel_clock_icon(QLabel):
            #def mouseReleaseEvent(widget, event):
            #    widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'clock_icon_hover.png').replace('\\', '/') + '); } ')
            def mousePressEvent(widget, event):
                if not self.selected_addition == 'clock':
                    self.selected_addition = 'clock'
                    self.show_additions()
                else:
                    self.selected_addition = False
                    self.hide_additions()
                widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'clock_icon_pressed.png').replace('\\', '/') + '); } ')
            def enterEvent(widget, event):
                if not self.selected_addition == 'clock':
                    widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'clock_icon_hover.png').replace('\\', '/') + '); } ')
            def leaveEvent(widget, event):
                if not self.selected_addition == 'clock':
                    widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'clock_icon_normal.png').replace('\\', '/') + '); } ')

        self.left_panel_clock_icon = left_panel_clock_icon(parent=self.left_panel_icons)
        self.left_panel_clock_icon.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'clock_icon_normal.png').replace('\\', '/') + '); } ')

        self.left_panel_clock_panel = QWidget(self.left_panel)

        self.left_panel_clock_show_clock = QCheckBox(u'Mostrar relógio', parent=self.left_panel_clock_panel)
        #self.left_panel_clock_show_clock.clicked.connect(lambda:self.main_options_settings_enable_animations_clicked())

        self.left_panel_clock_show_song = QCheckBox(u'Mostrar título do cântico', parent=self.left_panel_clock_panel)
        #self.left_panel_clock_show_song.clicked.connect(lambda:self.main_options_settings_enable_animations_clicked())

        self.left_panel_clock_start_silent = QCheckBox(u'Iniciar mudo', parent=self.left_panel_clock_panel)
        #self.left_panel_clock_show_song.clicked.connect(lambda:self.main_options_settings_enable_animations_clicked())


        #######################################################################
        ## ADDITION PANEL - SCREENCOPY

        class left_panel_screencopy_icon(QLabel):
            def mousePressEvent(widget, event):
                if not self.selected_addition == 'screencopy':
                    self.selected_addition = 'screencopy'
                    self.show_additions()
                else:
                    self.selected_addition = False
                    self.hide_additions()
                widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'screencopy_icon_pressed.png').replace('\\', '/') + '); } ')
            def enterEvent(widget, event):
                if not self.selected_addition == 'screencopy':
                    widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'screencopy_icon_hover.png').replace('\\', '/') + '); } ')
            def leaveEvent(widget, event):
                if not self.selected_addition == 'screencopy':
                    widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'screencopy_icon_normal.png').replace('\\', '/') + '); } ')

        self.left_panel_screencopy_icon = left_panel_screencopy_icon(parent=self.left_panel_icons)
        self.left_panel_screencopy_icon.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'screencopy_icon_normal.png').replace('\\', '/') + '); } ')

        self.left_panel_screencopy_panel = QWidget(self.left_panel)

        self.left_panel_screencopy_preview_background = QLabel(self.left_panel_screencopy_panel)
        self.left_panel_screencopy_preview_background.setObjectName('left_panel_screencopy_preview_background')
        self.left_panel_screencopy_preview_background.setStyleSheet('#left_panel_screencopy_preview_background { border-top: 4px; border-right: 4px; border-bottom: 4px; border-left: 4px; border-image: url("' + os.path.join(path_graphics, "black_box.png").replace('\\', '/') + '") 4 4 4 4 stretch stretch; }')


        self.left_panel_screencopy_preview = QLabel(self.left_panel_screencopy_preview_background)
        self.left_panel_screencopy_preview.setStyleSheet('QLabel {font-family: "Ubuntu"; font-size:12pt; color:gray; qproperty-alignment:"AlignCenter" ; }')

        self.left_panel_screencopy_description = QLabel(self.left_panel_screencopy_panel)

        self.left_panel_screencopy_take_button = QPushButton(u'DEFINIR CÓPIA', self.left_panel_screencopy_panel)
        self.left_panel_screencopy_take_button.setStyleSheet('QPushButton { color:white; font-size:12px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
        self.left_panel_screencopy_take_button.clicked.connect(lambda:self.left_panel_screencopy_take_button_clicked())

        #######################################################################
        ## ADDITION PANEL - CAMERA

        class left_panel_camera_icon(QLabel):
            def mousePressEvent(widget, event):
                if not self.selected_addition == 'camera':
                    self.selected_addition = 'camera'
                    self.show_additions()
                else:
                    self.selected_addition = False
                    self.hide_additions()
                widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'camera_icon_pressed.png').replace('\\', '/') + '); } ')
            def enterEvent(widget, event):
                if not self.selected_addition == 'camera':
                    widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'camera_icon_hover.png').replace('\\', '/') + '); } ')
            def leaveEvent(widget, event):
                if not self.selected_addition == 'camera':
                    widget.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'camera_icon_normal.png').replace('\\', '/') + '); } ')

        self.left_panel_camera_icon = left_panel_camera_icon(parent=self.left_panel_icons)
        self.left_panel_camera_icon.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'camera_icon_normal.png').replace('\\', '/') + '); } ')

        self.left_panel_camera_panel = QWidget(self.left_panel)

        self.left_panel_camera_select = QComboBox(self.left_panel_camera_panel)
        self.left_panel_camera_select.activated.connect(lambda:self.left_panel_camera_select_selected())

        self.left_panel_camera_test_button = QPushButton(u'TESTAR', self.left_panel_camera_panel)
        self.left_panel_camera_test_button.setStyleSheet('QPushButton { color:white; font-size:12px; border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
        self.left_panel_camera_test_button.clicked.connect(lambda:self.left_panel_camera_test_button_clicked())

        self.left_panel_camera_preview = QLabel(self.left_panel_camera_panel)
        self.left_panel_camera_preview.setStyleSheet('QLabel {font-family: "Ubuntu"; font-size:12pt; color:gray; qproperty-alignment:"AlignCenter" ; }')

        #self.camera_instance = vlc.Instance()
        #self.left_panel_camera_preview_mediaplayer = False
        self.left_panel_camera_preview_mediaplayer = video_instance.media_player_new()

        #self.left_panel_camera_preview_mediaplayer_widget = QFrame(parent=self)
        #self.left_panel_camera_preview_mediaplayer_widget.setGeometry(0,0,self.width(),self.height())
        #self.left_panel_camera_preview_mediaplayer.set_xwindow(self.left_panel_camera_preview_mediaplayer_widget.winId())
        self.left_panel_camera_preview_mediaplayer.set_xwindow(self.left_panel_camera_preview.winId())

        self.left_panel_logo = QLabel(parent=self)
        self.left_panel_logo.setObjectName('left_panel_logo')
        self.left_panel_logo.setStyleSheet('#left_panel_logo { border-bottom: 70px; border-right: 55px; border-image: url("' + os.path.join(path_graphics, "left_panel_logo.png").replace('\\', '/') + '") 0 55 70 0 stretch stretch; }')
        self.left_panel_logo.setAttribute(Qt.WA_TransparentForMouseEvents)
        self.left_panel_logo.setVisible(False)

        self.left_panel_logo_test_button_player = video_instance.media_player_new()
        self.left_panel_logo_test_button_player.set_media(video_instance.media_new(os.path.join(path_kihvim, 'resources',  'test_song.flac')))

        class left_panel_logo_test_button(QLabel):
            def mousePressEvent(widget, event):
                self.left_panel_logo_test_button_player.stop()
                generate_effect(app.monitor.test_widget_opacity_animation, 'opacity', 500, 1.0, 0.0)
                self.left_panel_logo_test_button_player.play()


        self.left_panel_logo_test_button = left_panel_logo_test_button(parent=self)
        #self.left_panel_logo_test_button.setObjectName('left_panel_logo_test_button')
        self.left_panel_logo_test_button.setPixmap(QPixmap(os.path.join(path_graphics, "kihvim_corner_test_logo.png")))
        #self.left_panel_logo_test_button.setStyleSheet('#left_panel_logo_test_button { image: url("' + .replace('\\', '/') + '") }')
        #self.left_panel_logo_test_button.setMask(self.left_panel_logo_test_button.pixmap().mask())

        self.showMaximized()

        self.load_config()
        self.populate_audio_list()
        self.populate_list_of_images_from_folder()
        self.populate_image_list()
        self.populate_list_of_videos_from_folder()
        self.populate_video_song_list()
        self.populate_video_local_list()
        self.populate_video_bible_list()
        self.populate_video_library_list()
        self.populate_playlist()

        self.timer = QTimer(self)
        self.timer.setInterval(1000/self.settings['interface_framerate'])
        #self.connect(self.timer, Signal("timeout()"), lambda:self.update_things())
        self.timer.start()

    def keyPressEvent(self, event):
        if event.key() == Qt.Key_Space:
            if self.show_now_button.isVisible():
                self.show_now()
        if event.key() == Qt.Key_Escape:
            app.monitor.hide_widgets()
        if event.key() == Qt.Key_Backspace:
            app.monitor.hide_all()

    def closeEvent(self, event):
        self.save_config()
        app.monitor.close()
        #webserver.shutdown()

    def resizeEvent(self, event):
        self.main_background.setGeometry(0,0,self.width(),self.height())

        if self.main_options_box_is_visible:
            self.main_options_box.setGeometry(80,self.height()*.75,(self.width()*.5)-110,self.height()*.25)
        else:
            self.main_options_box.setGeometry(80,self.height()-30,(self.width()*.5)-110,30)

        self.main_options_file_icon.setGeometry(5,0,30,30)
        self.main_options_file_panel.setGeometry(5,35,((self.width()*.5)-110)-10,(self.height()*.25)-40)
        self.main_options_file_open_button.setGeometry(5,5,40,40)
        self.main_options_file_open_label.setGeometry(55,5,(self.main_options_file_panel.width()-60),40)
        self.main_options_file_save_button.setGeometry(5,55,40,40)
        self.main_options_file_save_label.setGeometry(55,55,(self.main_options_file_panel.width()-60),40)

        self.main_options_settings_icon.setGeometry(40,0,30,30)
        self.main_options_settings_panel.setGeometry(5,35,((self.width()*.5)-110)-10,(self.height()*.25)-40)
        self.main_options_settings_enable_animations.setGeometry(5,5,self.main_options_settings_panel.width()-10,20)
        self.main_options_settings_framerate.setGeometry(5,35,40,40)
        self.main_options_settings_framerate_label.setGeometry(50,35,self.main_options_settings_panel.width()-55,40)

        self.main_options_export_icon.setGeometry(75,0,30,30)
        self.main_options_export_panel.setGeometry(5,35,((self.width()*.5)-110)-10,(self.height()*.25)-40)
        self.main_options_export_profile.setGeometry(5,5,self.main_options_export_panel.width()-10,20)
        self.main_options_export_button.setGeometry(5,35,self.main_options_export_panel.width()-10,40)

        self.right_panel.setGeometry(self.width()*.75,0,self.width()*.25,self.height())
        self.right_panel_monitor_background.setGeometry(20,20,self.right_panel.width()-40,((self.right_panel.width()-40)*self.screen_width_proportion)+25)
        self.right_panel_monitor_title.setGeometry(3,3,self.right_panel_monitor_background.width()-6,25)
        self.right_panel_monitor.setGeometry(3,28,self.right_panel_monitor_background.width()-6,(self.right_panel.width()-46)*self.screen_width_proportion)
        self.right_panel_monitor_withdrawbutton.setGeometry(self.right_panel_monitor.width()*.3,self.right_panel_monitor.height()*.4,self.right_panel_monitor.width()*.4,self.right_panel_monitor.height()*.2)
        self.right_panel_monitor_player.setGeometry(20,20+self.right_panel_monitor_background.height()+10,self.right_panel_monitor_background.width(),40)
        self.right_panel_monitor_player_stopbutton.setGeometry(0,0,self.right_panel_monitor_player.height(),self.right_panel_monitor_player.height())
        self.right_panel_monitor_player_playbutton.setGeometry(self.right_panel_monitor_player.height()+1,0,self.right_panel_monitor_player.height(),self.right_panel_monitor_player.height())
        self.right_panel_monitor_player_timelinebox.setGeometry((self.right_panel_monitor_player.height()*2)+2,0,self.right_panel_monitor_player.width()-((self.right_panel_monitor_player.height()*2)+2),self.right_panel_monitor_player.height())
        self.right_panel_monitor_player_timeline.setGeometry(0,3,self.right_panel_monitor_player_timelinebox.width()-3,self.right_panel_monitor_player_timelinebox.height()-6)
        self.right_panel_monitor_prelude.setGeometry(20,20+self.right_panel_monitor_background.height()+2,self.right_panel_monitor_background.width(),56)
        self.right_panel_monitor_prelude_pausebutton.setGeometry(0,8,60,40)
        self.right_panel_monitor_prelude_volume.setGeometry(40,0,self.right_panel_monitor_prelude.height(),self.right_panel_monitor_prelude.height())
        self.right_panel_monitor_prelude_volume_label.setGeometry(0,0,self.right_panel_monitor_prelude_volume.width(),self.right_panel_monitor_prelude_volume.height())
        self.right_panel_monitor_prelude_timelinebox.setGeometry(61,8,self.right_panel_monitor_prelude.width()-102,40)
        self.right_panel_monitor_prelude_timeline.setGeometry(0,3,self.right_panel_monitor_prelude_timelinebox.width()-2,self.right_panel_monitor_prelude_timelinebox.height()-6)
        self.right_panel_monitor_prelude_nextbutton.setGeometry(self.right_panel_monitor_prelude.width()-40,8,40,40)
        self.right_panel_shownext_box.setGeometry(0,self.right_panel.height()-80-((self.right_panel.width()-46)*self.screen_width_proportion)-25-26,self.right_panel.width(),80+((self.right_panel.width()-46)*self.screen_width_proportion)+25+26)
        self.right_panel_shownext_preview_background.setGeometry(20,80,self.right_panel.width()-40,((self.right_panel.width()-40)*self.screen_width_proportion)+25)
        self.right_panel_shownext_preview_title.setGeometry(3,self.right_panel_monitor_background.height()-28,self.right_panel_monitor_background.width()-6,25)

        self.right_panel_shownext_preview.setGeometry(23,83,self.right_panel_monitor_background.width()-6,(self.right_panel.width()-46)*self.screen_width_proportion)

        self.right_panel_shownext_preview_audio.setGeometry(0,0,self.right_panel_shownext_preview.width(), self.right_panel_shownext_preview.height())
        self.right_panel_shownext_preview_audio_artwork.setGeometry(self.right_panel_shownext_preview.width()*.1,(self.right_panel_shownext_preview.height()*.5)-(self.right_panel_shownext_preview.height()*.1),self.right_panel_shownext_preview.height()*.2, self.right_panel_shownext_preview.height()*.2)
        self.right_panel_shownext_preview_audio_title.setGeometry((self.right_panel_shownext_preview.width()*.11) + (self.right_panel_shownext_preview.width()*.2),0,self.right_panel_shownext_preview.width() - ((self.right_panel_shownext_preview.width()*.21) + (self.right_panel_shownext_preview.width()*.2)), self.right_panel_shownext_preview.height())
        self.right_panel_shownext_preview_audio_artwork.setStyleSheet('QLabel {background-color: #fff; font-family: "Ubuntu Condensed"; font-size:' + str(self.right_panel_shownext_preview.height()*.09) + 'pt; font-weight:bold; color:black;}')
        self.right_panel_shownext_preview_audio_title.setStyleSheet('QLabel {background-color:none; font-family: "Ubuntu"; font-size:' + str(self.right_panel_shownext_preview.height()*.05) + 'pt; font-weight:bold; color:white}')
        self.right_panel_shownext_preview_image.setGeometry(0,0,self.right_panel_shownext_preview.width(), self.right_panel_shownext_preview.height())
        self.right_panel_shownext_preview_clock.setGeometry(0,0,self.right_panel_shownext_preview.width(), self.right_panel_shownext_preview.height())
        self.right_panel_shownext_preview_clock.setStyleSheet('QLabel {background-color: black; font-family: "Ubuntu"; font-size:' + str(self.right_panel_shownext_preview.height()*.1) + 'pt; font-weight:bold; color:white}')

        self.right_panel_shownext_button.setGeometry(0,0,self.right_panel_shownext_box.width(),80)

        self.playlist_shadow_left.setGeometry(self.width()*.75,0,10,self.height())
        if self.playlist_options_is_visible:
            self.playlist_options_panel.setGeometry(60,self.playlist_options_panel.y(),(self.width()*.5)-30,self.playlist_options_panel.height())
        else:
            self.playlist_options_panel.setGeometry((self.width()*.5)-20,0,(self.width()*.25)+20,self.height())
        self.playlist_options_panel_audio_panel.setGeometry(30,20,(self.width()*.5)-110,self.playlist_options_panel.height()-40)
        self.playlist_options_panel_audio_panel_show_lyrics.setGeometry(0,0,self.playlist_options_panel_audio_panel.width(),20)
        self.playlist_options_panel_image_panel.setGeometry(30,20,(self.width()*.5)-110,self.playlist_options_panel.height()-40)
        self.playlist_options_panel_video_panel.setGeometry(30,20,(self.width()*.5)-110,self.playlist_options_panel.height()-40)
        self.playlist_options_panel_web_panel.setGeometry(30,20,(self.width()*.5)-110,self.playlist_options_panel.height()-40)
        self.playlist_options_panel_clock_panel.setGeometry(30,20,(self.width()*.5)-110,self.playlist_options_panel.height()-40)
        self.playlist_options_panel_screencopy_panel.setGeometry(30,20,(self.width()*.5)-110,self.playlist_options_panel.height()-40)
        self.playlist_options_panel_camera_panel.setGeometry(30,20,(self.width()*.5)-110,self.playlist_options_panel.height()-40)
        self.playlist_options_panel_open_button.setGeometry(self.playlist_options_panel.x(),self.playlist_options_panel.height()-70,50,70)
        self.playlist_panel.setGeometry(self.width()*.5,0,self.width()*.25,self.height())
        self.playlist_nextarrow.setGeometry((self.width()*.5)-3,(self.height())-151,(self.width()*.25)+41,70)
        self.playlist_nextarrow_description.setGeometry(13+self.playlist_nextarrow_preview.width(),10,self.playlist_nextarrow.width()-20-13-(50*self.screen_height_proportion),50)
        self.playlist_panel_scroll.setGeometry(0,20,self.playlist_panel.width()-13,self.playlist_panel.height()-171)
        if self.playlist_panel_scroll.height() > self.playlist_panel_list.height():
            self.playlist_panel_scroll.setViewportMargins(0,self.playlist_panel_scroll.height()-self.playlist_panel_list.height()-4,0,0)
        else:
            self.playlist_panel_scroll.setViewportMargins(0,0,0,0)

        self.playlist_lastitem.setGeometry(37,self.playlist_panel.height()-20-56,self.playlist_panel.width()-57,56)

        if self.selected_addition:
            self.left_panel_addbutton.setGeometry((self.width()*.5)-183,20,196,50)
            self.left_panel.setGeometry(0,0,self.width()*.5,self.height())
        else:
            self.left_panel_addbutton.setGeometry(-196,20,196,50)
            self.left_panel.setGeometry(67-(self.width()*.5),0,self.width()*.5,self.height())
        self.left_panel_add_title.setGeometry(0,20,self.left_panel.width()-183,50)
        self.left_panel_addbutton_label.setGeometry(0,0,self.left_panel_addbutton.width(),self.left_panel_addbutton.height())

        self.left_panel_logo.setGeometry(0,0,60,self.height())
        self.left_panel_logo_test_button.setGeometry(0,self.height()-60,60,60)
        self.left_panel_icons.setGeometry(self.left_panel.width()-55,70,45,self.left_panel.height()-70)
        self.left_panel_audio_icon.setGeometry(0,0,45,45)
        self.left_panel_audio_panel.setGeometry(12,70,self.left_panel.width()-12-55,self.left_panel.height()-70)
        self.left_panel_audio_list.setGeometry(10,10,self.left_panel_audio_panel.width()-20,self.left_panel_audio_panel.height()-80)
        self.left_panel_audio_alert.setGeometry(10,10,self.left_panel_audio_panel.width()-20,self.left_panel_audio_panel.height()-80)
        self.left_panel_audio_edit.setGeometry(10+self.left_panel_audio_list.width()-80,10+self.left_panel_audio_list.height()+5,40,40)
        self.left_panel_audio_remove.setGeometry(10+self.left_panel_audio_list.width()-40,10+self.left_panel_audio_list.height()+5,40,40)
        self.left_panel_audio_download_lyrics.setGeometry(10+self.left_panel_audio_list.width()-125,10+self.left_panel_audio_list.height()+5,40,40)

        if self.left_panel_audio_list.count() > 0:
            self.left_panel_audio_add.setGeometry(10+self.left_panel_audio_list.width()-170,10+self.left_panel_audio_list.height()+5,40,40)
            #self.left_panel_audio_remove.setVisible(True)
        else:
            self.left_panel_audio_add.setGeometry(self.left_panel_audio_panel.width()*.4,self.left_panel_audio_panel.height()*.6,self.left_panel_audio_panel.width()*.2,self.left_panel_audio_panel.height()*.1)
            #self.left_panel_audio_remove.setVisible(False)

        self.left_panel_audio_edit_panel.setGeometry(10,10,self.left_panel_audio_panel.width()-20,self.left_panel_audio_panel.height()-80)
        self.left_panel_audio_edit_number_label.setGeometry(0,0,self.left_panel_audio_edit_panel.width()*.2,20)
        self.left_panel_audio_edit_number.setGeometry(0,20,self.left_panel_audio_edit_panel.width()*.2,30)
        self.left_panel_audio_edit_title_label.setGeometry(self.left_panel_audio_edit_panel.width()*.2,0,self.left_panel_audio_edit_panel.width()*.8,20)
        self.left_panel_audio_edit_title.setGeometry(self.left_panel_audio_edit_panel.width()*.2,20,self.left_panel_audio_edit_panel.width()*.8,30)
        self.left_panel_audio_edit_filepath_label.setGeometry(0,60,self.left_panel_audio_edit_panel.width(),20)
        self.left_panel_audio_edit_filepath.setGeometry(0,80,self.left_panel_audio_edit_panel.width(),30)
        self.left_panel_audio_edit_lyrics_label.setGeometry(0,120,self.left_panel_audio_edit_panel.width(),20)
        self.left_panel_audio_edit_lyrics.setGeometry(0,140,self.left_panel_audio_edit_panel.width(),self.left_panel_audio_edit_panel.height()-160)
        self.left_panel_audio_edit_lyrics_get_from_web.setGeometry(self.left_panel_audio_edit_lyrics.width()-50,self.left_panel_audio_edit_lyrics.height()-50,40,40)

        self.left_panel_image_icon.setGeometry(0,45,45,45)
        self.left_panel_image_panel.setGeometry(12,70,self.left_panel.width()-12-55,self.left_panel.height()-70)
        self.left_panel_image_tab.setGeometry(10,10,self.left_panel_image_panel.width()-20,self.left_panel_image_panel.height()-20)
        self.left_panel_image_list.setGeometry(10,10,self.left_panel_image_tab_local.width()-20,self.left_panel_image_tab_local.height()-65)
        self.left_panel_image_alert.setGeometry(10,10,self.left_panel_image_tab_local.width()-20,self.left_panel_image_tab_local.height()-80)

        self.left_panel_image_remove.setGeometry(10+self.left_panel_image_list.width()-40,10+self.left_panel_image_list.height()+5,40,40)

        if self.left_panel_image_list.count() > 0:
            self.left_panel_image_path_label.setGeometry(10,10+self.left_panel_image_list.height()+5,self.left_panel_image_list.width()-90,40)
            self.left_panel_image_add.setGeometry(10+self.left_panel_image_list.width()-85,10+self.left_panel_image_list.height()+5,40,40)
        else:
            self.left_panel_image_path_label.setGeometry(10,10+self.left_panel_image_list.height()+5,self.left_panel_image_list.width(),40)
            self.left_panel_image_add.setGeometry(self.left_panel_image_list.width()*.4,self.left_panel_image_list.height()*.6,self.left_panel_image_list.width()*.2,self.left_panel_image_list.height()*.1)
        self.left_panel_image_edit.setGeometry(self.left_panel_image_path_label.width()-40,0,40,40)
        self.left_panel_image_keyword.setGeometry(10,30,(self.left_panel_image_tab_local.width()-20)*.8,30)
        self.left_panel_image_keyword_go.setGeometry(10+((self.left_panel_image_tab_local.width()-20)*.8),30,(self.left_panel_image_tab_local.width()-20)*.2,30)
        self.left_panel_image_webview.setGeometry(10,70,self.left_panel_image_tab_local.width()-20, self.left_panel_image_tab_local.height()-80)
        self.left_panel_image_webview_warning.setGeometry(self.left_panel_image_webview.x(),self.left_panel_image_webview.y(),self.left_panel_image_webview.width(),self.left_panel_image_webview.height())
        self.left_panel_image_webview_preview.setGeometry(self.left_panel_image_webview.width()*.1,(self.left_panel_image_webview.height()-((self.left_panel_image_webview.width()*.8)*self.screen_width_proportion))*.5,self.left_panel_image_webview.width()*.8,(self.left_panel_image_webview.width()*.8)*self.screen_width_proportion)
        self.left_panel_image_webview_preview_cancel_button.setGeometry(self.left_panel_image_webview_preview.width()*.15,(self.left_panel_image_webview_preview.height()-30)*.5,self.left_panel_image_webview_preview.width()*.7,30)

        self.left_panel_video_icon.setGeometry(0,90,45,45)
        self.left_panel_video_panel.setGeometry(12,70,self.left_panel.width()-12-55,self.left_panel.height()-70)
        self.left_panel_video_tab.setGeometry(10,10,self.left_panel_video_panel.width()-20,self.left_panel_video_panel.height()-20)

        self.left_panel_video_song_list.setGeometry(10,10,self.left_panel_video_tab.width()-4-20,self.left_panel_video_tab_songs.height()-80)
        self.left_panel_video_song_alert.setGeometry(10,10,self.left_panel_video_tab.width()-4-20,self.left_panel_video_tab_songs.height()-80)
        self.left_panel_video_song_remove.setGeometry(10+self.left_panel_video_song_list.width()-40,10+self.left_panel_video_song_list.height()+5,40,40)

        if self.left_panel_video_song_list.count() > 0:
            self.left_panel_video_song_add.setGeometry(10+self.left_panel_video_song_list.width()-85,10+self.left_panel_video_song_list.height()+5,40,40)
        else:
            self.left_panel_video_song_add.setGeometry(self.left_panel_video_tab_songs.width()*.4,self.left_panel_video_tab_songs.height()*.6,self.left_panel_video_tab_songs.width()*.2,self.left_panel_video_tab_songs.height()*.1)

        self.left_panel_video_local_list.setGeometry(10,10,self.left_panel_video_tab_songs.width()-20,self.left_panel_video_tab_songs.height()-80)
        self.left_panel_video_local_alert.setGeometry(10,10,self.left_panel_video_tab_songs.width()-20,self.left_panel_video_tab_songs.height()-20)

        self.left_panel_video_remove.setGeometry(10+self.left_panel_video_local_list.width()-40,10+self.left_panel_video_local_list.height()+5,40,40)
        if self.left_panel_video_local_list.count() > 0:
            self.left_panel_video_path_label.setGeometry(10,10+self.left_panel_video_local_list.height()+5,self.left_panel_video_local_list.width()-90,40)
            self.left_panel_video_add.setGeometry(10+self.left_panel_video_local_list.width()-85,10+self.left_panel_video_local_list.height()+5,40,40)
        else:
            self.left_panel_video_path_label.setGeometry(10,10+self.left_panel_video_local_list.height()+5,self.left_panel_video_local_list.width(),40)
            self.left_panel_video_add.setGeometry(self.left_panel_video_local_list.width()*.4,self.left_panel_video_local_list.height()*.7,self.left_panel_video_local_list.width()*.2,self.left_panel_video_local_list.height()*.1)
        self.left_panel_video_edit.setGeometry(self.left_panel_video_path_label.width()-40,self.left_panel_video_path_label.y(),40,40)

        self.left_panel_video_bible_alert.setGeometry(10,10,self.left_panel_video_tab_bible.width()-20,self.left_panel_video_tab_bible.height()-20)
        self.left_panel_video_bible_search.setGeometry(10,10,self.left_panel_video_tab_songs.width()-20,48)
        self.left_panel_video_bible_book_label.setGeometry(10,10,96,48)
        self.left_panel_video_bible_chapter_label.setGeometry(111,10,48,48)
        self.left_panel_video_bible_verse_label.setGeometry(164,10,48,48)
        self.left_panel_video_bible_list.setGeometry(10,68,self.left_panel_video_tab_songs.width()-20,self.left_panel_video_tab_songs.height()-78)

        self.left_panel_video_library_alert.setGeometry(10,10,self.left_panel_video_tab_library.width()-20,self.left_panel_video_tab_library.height()-20)
        self.left_panel_video_library_search.setGeometry(10,10,self.left_panel_video_tab_songs.width()-20,48)
        self.left_panel_video_library_album_label.setGeometry(10,10,48,48)
        self.left_panel_video_library_title_label.setGeometry(68,10,48,48)
        self.left_panel_video_library_chapter_label.setGeometry(126,10,self.left_panel_video_tab_songs.width()-146,48)
        self.left_panel_video_library_list.setGeometry(10,68,self.left_panel_video_tab_songs.width()-20,self.left_panel_video_tab_songs.height()-78)

        self.left_panel_web_icon.setGeometry(0,135,45,45)
        self.left_panel_web_panel.setGeometry(12,70,self.left_panel.width()-12-55,self.left_panel.height()-70)
        self.left_panel_web_address_label.setGeometry(10,10,(self.left_panel_web_panel.width()-20)*.8,20)
        self.left_panel_web_address.setGeometry(10,30,(self.left_panel_web_panel.width()-20)*.8,30)
        self.left_panel_web_address_go.setGeometry(10+((self.left_panel_web_panel.width()-20)*.8),30,(self.left_panel_web_panel.width()-20)*.2,30)
        self.left_panel_web_webview.setGeometry(10,70,self.left_panel_web_panel.width()-20, self.left_panel_web_panel.height()-80)

        self.left_panel_clock_icon.setGeometry(0,180,45,45)
        self.left_panel_clock_panel.setGeometry(12,70,self.left_panel.width()-12-55,self.left_panel.height()-70)
        self.left_panel_clock_show_clock.setGeometry(10,10,self.left_panel_web_panel.width()-20,20)
        self.left_panel_clock_show_song.setGeometry(10,30,self.left_panel_web_panel.width()-20,20)
        self.left_panel_clock_start_silent.setGeometry(10,50,self.left_panel_web_panel.width()-20,20)

        self.left_panel_screencopy_icon.setGeometry(0,225,45,45)
        self.left_panel_screencopy_panel.setGeometry(12,70,self.left_panel.width()-12-55,self.left_panel.height()-70)
        self.left_panel_screencopy_preview_background.setGeometry(10,10,self.left_panel_screencopy_panel.width()-20,(self.left_panel_screencopy_panel.width()-20)*self.screen_width_proportion)
        self.left_panel_screencopy_preview.setGeometry(3,3,self.left_panel_screencopy_preview_background.width()-6,(self.left_panel_screencopy_preview_background.width()-6)*self.screen_width_proportion)
        self.left_panel_screencopy_description.setGeometry(10,20+self.left_panel_screencopy_preview.height(),self.left_panel_screencopy_preview.width()*.75,30)
        self.left_panel_screencopy_take_button.setGeometry(10+(self.left_panel_screencopy_preview_background.width()*.75),20+self.left_panel_screencopy_preview_background.height(),self.left_panel_screencopy_preview_background.width()*.25,30)

        self.left_panel_camera_icon.setGeometry(0,270,45,45)
        self.left_panel_camera_panel.setGeometry(12,70,self.left_panel.width()-12-55,self.left_panel.height()-70)
        self.left_panel_camera_select.setGeometry(10,10,(self.left_panel_camera_panel.width()-20)*.75,30)
        self.left_panel_camera_test_button.setGeometry(10+((self.left_panel_camera_panel.width()-20)*.75),10,(self.left_panel_camera_panel.width()-20)*.25,30)
        self.left_panel_camera_preview.setGeometry(10,50,self.left_panel_camera_panel.width()-20,(self.left_panel_camera_panel.width()-20)*self.screen_width_proportion)

    def update_things(self):
        if QDesktopWidget().screenCount() > 1:
            self.right_panel_monitor.setPixmap(QPixmap.grabWindow(QApplication.desktop().winId(), app.monitor.x(), app.monitor.y(), app.monitor.width(), app.monitor.height()))
        if app.monitor.video_mediaplayer.is_playing():
            if app.monitor.current_media[0] in ['audio', 'video']:
                self.right_panel_monitor_player_timeline.update()
                if not app.monitor.video_mediaplayer.audio_get_volume() == 100:
                    app.monitor.video_mediaplayer.audio_set_volume(100)
                if app.monitor.current_media[0] == 'video' and app.monitor.current_media[6]:
                    if app.monitor.video_mediaplayer.get_position() > app.monitor.current_media[6]/app.monitor.current_media[7]:
                        app.monitor.video_mediaplayer.stop()
            elif app.monitor.current_media[0] in ['clock']:
                self.right_panel_monitor_prelude_timeline.update()
                if self.right_panel_monitor_prelude.time_to_full_volume:
                    volume = (((datetime.datetime.now() - self.right_panel_monitor_prelude.time_to_full_volume[0]).total_seconds()) / self.right_panel_monitor_prelude.time_to_full_volume[1] ) * self.right_panel_monitor_prelude_volume.value()

                    if self.right_panel_monitor_prelude.time_to_full_volume[2]:
                        volume = self.right_panel_monitor_prelude_volume.value() - volume

                    if volume >= self.right_panel_monitor_prelude_volume.value():
                        volume = self.right_panel_monitor_prelude_volume.value()
                        self.right_panel_monitor_prelude.time_to_full_volume = False

                    elif volume <= 0:
                        if self.right_panel_monitor_prelude.time_to_full_volume[3]:
                            app.monitor.video_mediaplayer.stop()
                        else:
                            volume = 0
                        self.right_panel_monitor_prelude.time_to_full_volume = False

                    if not (app.monitor.current_media[4] or (self.right_panel_monitor_prelude_pausebutton.isChecked() and not self.right_panel_monitor_prelude_nextbutton.isEnabled())):
                        app.monitor.video_mediaplayer.audio_set_volume(int(volume))
                    if int(volume) == self.right_panel_monitor_prelude_volume.value():
                        self.right_panel_monitor_prelude_volume_label.setText('<font style="font-size:14px;"><b>' + str(self.right_panel_monitor_prelude_volume.value()) + '</b></font><br><font style="font-size:7px;">VOL</font>')
                    else:
                        self.right_panel_monitor_prelude_volume_label.setText('<font style="font-size:14px;"><b>' + str(self.right_panel_monitor_prelude_volume.value()) + '</b></font><br><font style="font-size:7px;">( ' + str(int(volume)) + ' )</font>')
                else:
                    if not self.right_panel_monitor_prelude_pausebutton.isChecked() and not app.monitor.video_mediaplayer.audio_get_volume() == self.right_panel_monitor_prelude_volume.value() and not app.monitor.current_media[4]:
                        self.right_panel_monitor_prelude_volume_changing()

                if app.monitor.current_media[4]:
                    app.monitor.video_mediaplayer.audio_set_volume(0)

        else:
            if app.monitor.current_media and app.monitor.current_media[0] in ['clock']:
                self.right_panel_monitor_player_sort_song()
                if self.right_panel_monitor_prelude.song:
                    app.monitor.media_open(self.right_panel_monitor_prelude.song[2])


                    if not self.right_panel_monitor_prelude_nextbutton.isEnabled():
                        self.right_panel_monitor_prelude_nextbutton.setEnabled(True)


                    if not self.right_panel_monitor_prelude_pausebutton.isChecked():
                        self.right_panel_monitor_prelude_crossfade_opacity()

                    app.monitor.video_mediaplayer.play()

                    app.monitor.clock_widget_title_label.setText(u'<font color:silver><small>TOCANDO AGORA:</small></font><br><fony color:gray><big>' + str(self.right_panel_monitor_prelude.song[4]) + u' 🞍 <b>' + self.right_panel_monitor_prelude.song[0] + u'</b></big></font>')

        if app.monitor.current_media and app.monitor.current_media[0] in ['clock']:#if app.monitor.clock_widget_label_opacity.opacity() > 0.0:
            app.monitor.clock_widget_label.setText(str(datetime.datetime.now().time()).split('.')[0])
        if self.selected_media and self.selected_media[0] == 'clock':#if self.right_panel_shownext_preview_clock.isVisible() == True:
            self.right_panel_shownext_preview_clock.setText(str(datetime.datetime.now().time()).split('.')[0])
        if len(self.playlist) > 0 and self.selected_media[0] == 'camera':
            self.right_panel_shownext_preview_image.setPixmap(QPixmap.grabWindow(QApplication.desktop().winId(), self.playlist_nextarrow_preview.mapToGlobal(self.playlist_nextarrow_preview.pos()).x() - self.playlist_nextarrow_preview.x(), self.playlist_nextarrow_preview.mapToGlobal(self.playlist_nextarrow_preview.pos()).y() - self.playlist_nextarrow_preview.y(), self.playlist_nextarrow_preview.width(), self.playlist_nextarrow_preview.height()).scaled(self.right_panel_shownext_preview_image.width(), self.right_panel_shownext_preview_image.height(), Qt.KeepAspectRatio, Qt.SmoothTransformation))

        if self.selected_addition == 'screencopy' and app.screencopy.selected_area:
            self.left_panel_screencopy_preview.setPixmap(QPixmap.grabWindow(QApplication.desktop().winId(), app.screencopy.selected_area[0], app.screencopy.selected_area[1], app.screencopy.selected_area[2], app.screencopy.selected_area[3]).scaled(self.left_panel_screencopy_preview.width(), self.left_panel_screencopy_preview.height(), Qt.KeepAspectRatio, Qt.SmoothTransformation))
        if len(self.playlist) > 0 and self.selected_media[0] == 'screencopy':
            self.right_panel_shownext_preview_image.setPixmap(QPixmap.grabWindow(QApplication.desktop().winId(), self.selected_media[2][0], self.selected_media[2][1], self.selected_media[2][2], self.selected_media[2][3]).scaled(self.right_panel_shownext_preview_image.width(), self.right_panel_shownext_preview_image.height(), Qt.KeepAspectRatio, Qt.SmoothTransformation))
        if app.monitor.current_media and app.monitor.current_media[0] == 'screencopy':
            app.monitor.image_widget.setPixmap(QPixmap.grabWindow(QApplication.desktop().winId(), app.monitor.current_media[2][0], app.monitor.current_media[2][1], app.monitor.current_media[2][2], app.monitor.current_media[2][3]).scaled(app.monitor.image_widget.width(), app.monitor.image_widget.height(), Qt.KeepAspectRatio, Qt.SmoothTransformation))

    def load_config(self):
        self.settings['enable_animations'] = True
        self.settings['interface_framerate'] = 40
        if os.path.isfile(os.path.join(path_user_config, 'settings')):
            settings_file = codecs.open(os.path.join(path_user_config, 'settings'), 'r', 'utf-8').read()
            if '[song ' in settings_file:
                for song in settings_file.split('[song '):
                    if song.split(']')[0].isnumeric():
                        song_number = int(song.split(']')[0])
                        song_title = ''
                        song_lyrics = ''

                        song_content = song.split(']')[1].split('[')[0]
                        if 'title=' in song_content:
                            song_title = song_content.split('title=',1)[1].split('\n')[0]
                        if 'filepath=' in song_content:
                            filepath = song_content.split('filepath=',1)[1].split('\n')[0]
                        if 'lyrics=' in song_content:
                            song_lyrics = song_content.split('lyrics={',1)[1].split('}')[0]

                        self.list_of_songs[song_number] = [song_title, song_lyrics, filepath]
            if '[video song ' in settings_file:
                for song in settings_file.split('[video song '):
                    if song.split(']')[0].isnumeric():
                        song_number = int(song.split(']')[0])
                        song_title = ''

                        song_content = song.split(']')[1].split('[')[0]
                        if 'title=' in song_content:
                            song_title = song_content.split('title=',1)[1].split('\n')[0]
                        if 'filepath=' in song_content:
                            filepath = song_content.split('filepath=',1)[1].split('\n')[0]

                        self.list_of_video_songs[song_number] = [song_title, filepath]

            if '[paths]' in settings_file:
                for line in settings_file.split('[paths]')[1].split('[')[0].split('\n'):
                    if 'videos=' in line:
                        if not line.split('=',1)[-1] == '' and os.path.isdir(line.split('=',1)[-1]):
                            self.path_for_videos = line.split('=',1)[-1]
                    if 'images=' in line:
                        if not line.split('=',1)[-1] == '' and os.path.isdir(line.split('=',1)[-1]):
                            self.path_for_images = line.split('=',1)[-1]


            if '[config]' in settings_file:
                for line in settings_file.split('[config]')[1].split('[')[0].split('\n'):
                    if 'enable_animations=' in line:
                        self.settings['enable_animations'] = bool(line.split('enable_animations=',1)[1].split('\n')[0])
                    if 'framerate=' in line:
                        self.settings['interface_framerate'] = int(line.split('framerate=',1)[1].split('\n')[0])

            self.main_options_settings_framerate.setValue(self.settings['interface_framerate'])
            self.main_options_settings_framerate_changing()
            self.main_options_settings_enable_animations.setChecked(self.settings['enable_animations'])

        if os.path.isfile(os.path.join(path_user_config, 'bible')):
            bible_file = codecs.open(os.path.join(path_user_config, 'bible'), 'r', 'utf-8').read()

            if '[verse ' in bible_file:

                for verse_content in bible_file.split('[verse '):

                    verse = verse_content.split(']')[0]
                    filepath = False
                    start = False
                    end = False

                    if 'filepath=' in verse_content:
                        filepath = verse_content.split('filepath=',1)[1].split('\n')[0]
                    if 'start=' in verse_content:
                        start = float(verse_content.split('start=',1)[1].split('\n')[0])
                    if 'end=' in verse_content:
                        end = float(verse_content.split('end=',1)[1].split('\n')[0])

                    if not verse == '' and filepath and os.path.isfile(filepath) and start and end:
                        self.list_of_bible_videos[verse] = [filepath, start, end]

        if os.path.isfile(os.path.join(path_user_config, 'library')):
            library_file = codecs.open(os.path.join(path_user_config, 'library'), 'r', 'utf-8').read()

            if '[album ' in library_file:
                for album_content in library_file.split('[album '):
                    if 'filepath=' in album_content:
                        album = album_content.rsplit(']',1)[0]
                        self.list_of_library_videos[album] = {}
                        for file_content in album_content.split('filepath='):
                            if 'title=' in file_content:

                                filepath = file_content.split('\n')[0]
                                title = file_content.split('title=')[1].split('\n')[0]
                                chapters = {}
                                for chapter in album_content.split('chapter='):
                                    if len(chapter.split(';')) > 2:
                                        name = chapter.split(';')[0]
                                        start = float(chapter.split(';')[1])
                                        end = float(chapter.split(';')[2])
                                        chapters[name] = [start, end]

                                try:
                                    video = MP4(filepath)
                                    if 'covr' in  video.tags.keys():
                                        artwork = video.tags["covr"][0]
                                        open(os.path.join(path_tmp, 'thumbnail.png'), 'wb').write(artwork)
                                except: #remover quando mutagen
                                    video_chapters_info = unicode(subprocess.Popen([ffprobe_bin,'-loglevel', 'error',  '-show_chapters', '-print_format', 'xml', filepath], startupinfo=startupinfo, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE).stdout.read(), 'utf-8')
                                    stream = '0:' + str(int(video_chapters_info.split('nb_streams="')[1].split('"')[0])-1)
                                    subprocess.call([ffmpeg_bin, '-loglevel', 'error', '-y', '-i', filepath, '-map', stream, '-c:v', 'copy', os.path.join(path_tmp, 'thumbnail.png')])

                                thumbnail = QPixmap(os.path.join(path_tmp, 'thumbnail.png'))

                                if os.path.isfile(filepath):
                                    self.list_of_library_videos[album][filepath] = [title, chapters, thumbnail]

    def save_config(self):
        settings_file = u''

        for song in self.list_of_songs.keys():
            settings_file += u'[song ' + str(song) + ']\n'
            settings_file += u'title=' + self.list_of_songs[song][0] + u'\n'
            settings_file += u'lyrics={' + self.list_of_songs[song][1] + u'}\n'
            settings_file += u'filepath=' + self.list_of_songs[song][2] + u'\n'
            settings_file += u'\n'

        for song in self.list_of_video_songs.keys():
            settings_file += u'[video song ' + str(song) + ']\n'
            settings_file += u'title=' + self.list_of_video_songs[song][0] + u'\n'
            settings_file += u'filepath=' + self.list_of_video_songs[song][1] + u'\n'
            settings_file += u'\n'

        settings_file += u'[paths]\n'
        settings_file += u'videos=' + self.path_for_videos + u'\n'
        settings_file += u'images=' + self.path_for_images + u'\n'

        settings_file += u'[config]\n'
        settings_file += u'enable_animations=' + str(self.settings['enable_animations']) + u'\n'
        settings_file += u'framerate=' + str(self.settings['interface_framerate']) + u'\n'

        if not os.path.isdir(os.path.join(path_home, '.config')):
            os.mkdir(os.path.join(path_home, '.config'))

        if not os.path.isdir(path_user_config):
            os.mkdir(path_user_config)

        if os.path.isfile(os.path.join(path_user_config, 'settings')):
            os.remove(os.path.join(path_user_config, 'settings'))
        codecs.open(os.path.join(path_user_config, 'settings'), 'w', 'utf-8').write(settings_file)

        bible_file = u''

        for verse in self.list_of_bible_videos.keys():
            bible_file += u'[verse ' + verse + u']\n'
            bible_file += u'filepath=' + self.list_of_bible_videos[verse][0] + u'\n'
            bible_file += u'start=' + str(self.list_of_bible_videos[verse][1]) + u'\n'
            bible_file += u'end=' + str(self.list_of_bible_videos[verse][2]) + u'\n'

        if os.path.isfile(os.path.join(path_user_config, 'bible')):
            os.remove(os.path.join(path_user_config, 'bible'))
        codecs.open(os.path.join(path_user_config, 'bible'), 'w', 'utf-8').write(bible_file)

        library_file = u''

        for album in self.list_of_library_videos.keys():
            library_file += u'[album ' + album + u']\n'
            for filepath in self.list_of_library_videos[album].keys():
                library_file += u'filepath=' + filepath + u'\n'
                library_file += u'title=' + self.list_of_library_videos[album][filepath][0] + u'\n'
                for chapter in self.list_of_library_videos[album][filepath][1].keys():
                    library_file += u'chapter=' + chapter + ';' + str(self.list_of_library_videos[album][filepath][1][chapter][0]) + ';' + str(self.list_of_library_videos[album][filepath][1][chapter][1]) + u'\n'

        if os.path.isfile(os.path.join(path_user_config, 'library')):
            os.remove(os.path.join(path_user_config, 'library'))
        codecs.open(os.path.join(path_user_config, 'library'), 'w', 'utf-8').write(library_file)

    ############################################################################
    ## MAIN OPTIONS

    def show_main_options(self):
        self.main_options_box_is_visible = True
        if self.settings['enable_animations']:
            generate_effect(self.main_options_box_geometry_animation, 'geometry', 500, [self.main_options_box.x(),self.main_options_box.y(),self.main_options_box.width(),self.main_options_box.height()], [80,self.height()*.75,(self.width()*.5)-110,self.height()*.25])
        else:
            self.main_options_box.setGeometry(80,self.height()*.75,(self.width()*.5)-110,self.height()*.25)

        if not self.main_options_box_selected == 'file':
            self.main_options_file_icon.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'main_options_file_icon_normal.png').replace('\\', '/') + '); } ')
            self.main_options_file_panel.setVisible(False)
        if not self.main_options_box_selected == 'settings':
            self.main_options_settings_icon.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'main_options_settings_icon_normal.png').replace('\\', '/') + '); } ')
            self.main_options_settings_panel.setVisible(False)
        if not self.main_options_box_selected == 'export':
            self.main_options_export_icon.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'main_options_export_icon_normal.png').replace('\\', '/') + '); } ')
            self.main_options_export_panel.setVisible(False)

        if self.main_options_box_selected == 'file':
            self.main_options_file_panel.setVisible(True)
        if self.main_options_box_selected == 'settings':
            self.main_options_settings_panel.setVisible(True)
        if self.main_options_box_selected == 'export':
            self.main_options_export_panel.setVisible(True)

    def hide_main_options(self):
        self.main_options_box_is_visible = False
        if self.settings['enable_animations']:
            generate_effect(self.main_options_box_geometry_animation, 'geometry', 500, [self.main_options_box.x(),self.main_options_box.y(),self.main_options_box.width(),self.main_options_box.height()], [80,self.height()-30,(self.width()*.5)-110,30])
        else:
            self.main_options_box.setGeometry(80,self.height()-30,(self.width()*.5)-110,30)

    def main_options_file_open_button_clicked(self):
        self.actual_playlist_file = QFileDialog.getOpenFileName(self, 'Selecione um arquivo de playlist para abrir', path_home, 'Playlists do Kihvim (*.kihvim)', options=self.dialog_options)[0]
        if self.actual_playlist_file:
            del self.playlist[:]
            playlist_content = codecs.open(os.path.join(self.actual_playlist_file), 'r', 'utf-8').read().split('<playlist')[1].split('>', 1)[1].split('</playlist>')[0]

            for media_line in playlist_content.split('<media '):
                if '</media>' in media_line:
                    media_type = media_line.split('type="')[1].split('"')[0]
                    final_media = [media_type]

                    if media_type == 'audio':
                        media_line_audio = media_line.split('>')[0]
                        filepath = False
                        if ' filepath="' in media_line_audio:
                            filepath = media_line_audio.split(' filepath="')[1].split('"')[0]

                        if filepath and os.path.isfile(filepath):
                            title = False
                            if ' title="' in media_line_audio:
                                title = media_line_audio.split(' title="')[1].split('"')[0]
                            else:
                                song = ID3(filepath)
                                if 'TIT2' in song.keys():
                                    title = song["TIT2"].text[0]
                            if not title:
                                title = filepath.split('/')[-1].rsplit('.')[0]

                            number = 0
                            if ' number="' in media_line_audio:
                                number = int(media_line_audio.split(' number="')[1].split('"')[0])
                            else:
                                song = ID3(filepath)
                                if 'TIT2' in song.keys():
                                    number = get_number(song["TIT2"].text[0])

                                if not number and 'TRCK' in song.keys() and (song['TRCK'].text).isnumeric():
                                    number = int(song['TRCK'].text)

                            if str(number) in title:
                                title = title.replace('_', ' ').split(str(number), 1)[1].split(' ', 1)[1]

                            show_lyrics = False
                            if ' show_lyrics="' in media_line_audio:
                                if media_line_audio.split(' show_lyrics="')[1].split('"')[0] == 'True':
                                    show_lyrics = True

                            lyrics = media_line.split('>',1)[1].split('</media>')[0]

                            duration = 60.0
                            if ' duration="' in media_line_audio:
                                duration = float(media_line_audio.split(' duration="')[1].split('"')[0])
                            else:
                                duration = MP3(filepath).info.length

                            back_to_playlist = False
                            if ' back_to_playlist="' in media_line_audio:
                                if media_line_audio.split(' back_to_playlist="')[1].split('"')[0] == 'True':
                                    back_to_playlist = True


                            icon_label = QLabel(str(number))
                            icon_label.setGeometry(0,0,50*self.screen_height_proportion,50)
                            icon_label.setAlignment(Qt.AlignCenter)
                            icon_label.setStyleSheet('QLabel {background-color:black; font-family: "Ubuntu Condensed"; font-size:28px; color:white;}')
                            preview = QPixmap(QPixmap().grabWidget(icon_label))

                            final_media += [preview, number, title, lyrics, filepath, show_lyrics, duration, back_to_playlist]
                            self.playlist.append(final_media)

                    elif media_type == 'image':
                        filepath = False
                        if ' filepath="' in media_line_audio:
                            filepath = media_line.split(' filepath="')[1].split('"')[0]

                        if filepath and os.path.isfile(filepath):
                            back_to_playlist = False
                            if ' back_to_playlist="' in media_line:
                                if media_line.split(' back_to_playlist="')[1].split('"')[0] == 'True':
                                    back_to_playlist = True

                            preview = QPixmap(os.path.join(path_tmp , filename_hash + '.png'))

                            final_media += [preview, filepath, back_to_playlist]
                            self.playlist.append(final_media)

                    elif media_type == 'video':
                        filepath = False
                        if ' filepath="' in media_line_audio:
                            filepath = media_line.split(' filepath="')[1].split('"')[0]

                        if filepath and os.path.isfile(filepath):
                            md5 = hashlib.md5()
                            md5.update(open(filepath, 'rb').read())
                            filename_hash = md5.hexdigest()
                            video = MP4(filepath)
                            if 'covr' in  video.tags.keys():
                                artwork = video.tags["covr"][0]
                                open(os.path.join(path_tmp, filepath.split('/')[-1].rsplit('.',1)[0] + '.png'), 'wb').write(artwork)
                            else:
                                subprocess.call([ffmpeg_bin, '-loglevel', 'error', '-y', '-ss', '00:10.00', '-i', filepath, '-frames:v', '1', os.path.join(path_tmp, filepath.split('/')[-1].rsplit('.',1)[0] + '.png')], startupinfo=startupinfo)
                            create_thumbnail(os.path.join(path_tmp, filepath.split('/')[-1].rsplit('.',1)[0] + '.png'), os.path.join(path_tmp , filename_hash + '.png'), 56*self.screen_height_proportion, 56)

                            preview = QPixmap(os.path.join(path_tmp , filename_hash + '.png'))
                            image = QPixmap(os.path.join(path_tmp, filepath.split('/')[-1].rsplit('.', 1)[0] + '.png'))

                            title = ''
                            if ' title="' in media_line:
                                title = media_line.split(' title="')[1].split('"')[0]
                            elif '\xa9nam' in  video.tags.keys():
                                title = video.tags['\xa9nam'][0]
                            if not title:
                                title = filepath.split('/')[-1].rsplit('.')[0]

                            duration = 60.0
                            duration_output = subprocess.Popen([ffmpeg_bin, '-i', filepath], startupinfo=startupinfo, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT).stdout.read()
                            if 'Duration:' in duration_output:
                                duration = convert_timecode_to_seconds(duration_output.split('Duration: ')[1].split(',')[0])

                            back_to_playlist = False
                            if ' back_to_playlist="' in media_line:
                                if media_line.split(' back_to_playlist="')[1].split('"')[0] == 'True':
                                    back_to_playlist = True

                            final_media += [preview, filepath, image, title, duration, back_to_playlist]
                            self.playlist.append(final_media)

                    elif media_type == 'web':
                        address = ''
                        if ' address="' in media_line:
                            address = media_line.split(' address="')[1].split('"')[0]

                        if address:
                            icon_label = QLabel()
                            icon_label.setPixmap(os.path.join(path_graphics, 'web_icon_normal.png'))
                            icon_label.setGeometry(0,0,50*self.screen_height_proportion,50)
                            icon_label.setAlignment(Qt.AlignCenter)
                            icon_label.setStyleSheet('QLabel {background-color:black;}')
                            preview = QPixmap(QPixmap().grabWidget(icon_label))
                            image = QPixmap(QPixmap().grabWidget(icon_label))

                            back_to_playlist = False
                            if ' back_to_playlist="' in media_line:
                                if media_line.split(' back_to_playlist="')[1].split('"')[0] == 'True':
                                    back_to_playlist = True

                            final_media += [preview, address, image, back_to_playlist]
                            self.playlist.append(final_media)

                    elif media_type == 'clock':
                        icon_label = QLabel()
                        icon_label.setPixmap(os.path.join(path_graphics, 'clock_icon_normal.png'))
                        icon_label.setGeometry(0,0,50*self.screen_height_proportion,50)
                        icon_label.setAlignment(Qt.AlignCenter)
                        icon_label.setStyleSheet('QLabel {background-color:black;}')
                        preview = QPixmap(QPixmap().grabWidget(icon_label))

                        back_to_playlist = False
                        if ' back_to_playlist="' in media_line:
                            if media_line.split(' back_to_playlist="')[1].split('"')[0] == 'True':
                                back_to_playlist = True

                        final_media += [preview, back_to_playlist]
                        self.playlist.append(final_media)

                    elif media_type == 'screencopy':
                        area = ''
                        if ' area="' in media_line:
                            area = media_line.split(' area="')[1].split('"')[0]

                        if area:
                            icon_label = QLabel()
                            icon_label.setPixmap(os.path.join(path_graphics, 'screencopy_icon_normal.png'))
                            icon_label.setGeometry(0,0,50*self.screen_height_proportion,50)
                            icon_label.setAlignment(Qt.AlignCenter)
                            icon_label.setStyleSheet('QLabel {background-color:black;}')
                            preview = QPixmap(QPixmap().grabWidget(icon_label))

                            back_to_playlist = False
                            if ' back_to_playlist="' in media_line:
                                if media_line.split(' back_to_playlist="')[1].split('"')[0] == 'True':
                                    back_to_playlist = True

                            final_media += [preview, area, back_to_playlist]
                            self.playlist.append(final_media)

                    elif media_type == 'camera':
                        icon_label = QLabel()
                        icon_label.setPixmap(os.path.join(path_graphics, 'camera_icon_normal.png'))
                        icon_label.setGeometry(0,0,50*self.screen_height_proportion,50)
                        icon_label.setAlignment(Qt.AlignCenter)
                        icon_label.setStyleSheet('QLabel {background-color:black;}')
                        preview = QPixmap(QPixmap().grabWidget(icon_label))

                        mediaplayer =  video_instance.media_player_new()

                        back_to_playlist = False
                        if ' back_to_playlist="' in media_line:
                            if media_line.split(' back_to_playlist="')[1].split('"')[0] == 'True':
                                back_to_playlist = True

                        final_media += [preview, mediaplayer, back_to_playlist]
                        self.playlist.append(final_media)

        self.populate_playlist()

    def main_options_file_save_button_clicked(self):
        final_playlist_file = u'<?xml version="1.0" encoding="UTF-8"?>'
        final_playlist_file += '<playlist>'

        for media in self.playlist:
            final_playlist_file += '<media type="' + media[0] + '"'

            if media[0] == 'audio':
                final_playlist_file += ' number="' + str(media[2]) + '"'
                final_playlist_file += ' title="' + media[3] + '"'
                final_playlist_file += ' filepath="' + media[5] + '"'
                final_playlist_file += ' show_lyrics="' + str(media[6]) + '"'
                final_playlist_file += ' duration="' + str(media[7]) + '"'
                final_playlist_file += ' back_to_playlist="' + str(media[-1]) + '">'
                final_playlist_file += media[4] + '</media>'

            elif media[0] == 'image':
                final_playlist_file += ' filepath="' + media[2] + '"'
                final_playlist_file += ' back_to_playlist="' + str(media[-1]) + '">'
                final_playlist_file += '</media>'

            elif media[0] == 'video':
                final_playlist_file += ' filepath="' + media[2] + '"'
                final_playlist_file += ' title="' + media[4] + '">'
                final_playlist_file += ' duration="' + str(media[5]) + '">'
                final_playlist_file += ' back_to_playlist="' + str(media[-1]) + '">'
                final_playlist_file += '</media>'

            elif media[0] == 'web':
                final_playlist_file += ' address="' + media[2] + '"'
                final_playlist_file += ' back_to_playlist="' + str(media[-1]) + '">'
                final_playlist_file += '</media>'

            elif media[0] == 'clock':
                final_playlist_file += ' back_to_playlist="' + str(media[-1]) + '">'
                final_playlist_file += '</media>'

            elif media[0] == 'screencopy':
                final_playlist_file += ' area="' + str(media[2]) + '">'
                final_playlist_file += ' back_to_playlist="' + str(media[-1]) + '">'
                final_playlist_file += '</media>'

            elif media[0] == 'camera':
                final_playlist_file += ' back_to_playlist="' + str(media[-1]) + '">'
                final_playlist_file += '</media>'

        final_playlist_file += '</playlist>'

        if not self.actual_playlist_file:
            self.actual_playlist_file = QFileDialog.getSaveFileName(self, 'Selecione um nome de arquivo para a playlist', path_home, 'Playlists do Kihvim (*.kihvim)', options=self.dialog_options)[0]

        if not self.actual_playlist_file.endswith('.kihvim'):
            self.actual_playlist_file += 'kihvim'

        codecs.open(self.actual_playlist_file, 'w', 'utf-8').write(final_playlist_file)

    def main_options_settings_enable_animations_clicked(self):
        self.settings['enable_animations'] = self.main_options_settings_enable_animations.isChecked()

    def main_options_settings_framerate_changing(self):
        if self.main_options_settings_framerate.value() > 1 :
            self.main_options_settings_framerate_label.setText(u'<small>TAXA DE ATUALIZAÇÃO</small><br>' + str(self.main_options_settings_framerate.value()) + u' vezes a cada segundo')
        else:
            self.main_options_settings_framerate_label.setText(u'<small>TAXA DE ATUALIZAÇÃO</small><br>' + str(self.main_options_settings_framerate.value()) + u' vez a cada segundo')

    def main_options_settings_framerate_changed(self):
        self.main_options_settings_framerate_changing()
        self.settings['interface_framerate'] = self.main_options_settings_framerate.value()
        self.timer.setInterval(1000/self.settings['interface_framerate'])

    def main_options_export_button_clicked(self):
        media_number = 0
        final_list = []

        final_command = ['ffmpeg', '-y']
        width = int(self.main_options_export_profile.currentText().split('x')[0])
        height = int(self.main_options_export_profile.currentText().split('x')[1].split(' ')[0])

        background = QLabel()
        background.setGeometry(0,0,width,height)
        background.setStyleSheet('QLabel {background-color:black;}')

        artwork = QLabel(u'⊡', parent=background)
        artwork.setGeometry(width*.45,height*.45,width*.1, height*.1)
        artwork.setStyleSheet('QLabel {font-family: "Ubuntu"; font-size:' + str(height*.04) + 'pt; color:white; qproperty-alignment: "AlignCenter";}')

        preview = QPixmap(QPixmap().grabWidget(background))
        preview.save(os.path.join(path_tmp , 'testmedia.png'))

        final_command.append('-loop')
        final_command.append('1')
        final_command.append('-i')
        final_command.append(os.path.join(path_tmp , 'testmedia.png'))
        final_command.append('-i')
        final_command.append(os.path.join(path_kihvim, 'resources',  'test_song.flac'))
        #final_command.append('-shortest')

        final_command.append('-vf')
        final_command.append('fade=out:0:30')

        final_command.append('-c:v')
        final_command.append('libx264')
        final_command.append('-c:a')
        final_command.append('copy')
        final_command.append('-preset')
        final_command.append('ultrafast')
        final_command.append('-qp')
        final_command.append('0')
        final_command.append('-t')
        final_command.append('2')
        final_command.append(os.path.join(path_tmp , 'media' + str('%03d'% media_number) + '.mkv'))
        subprocess.call(final_command)

        final_list.append(os.path.join(path_tmp , 'media' + str('%03d'% media_number) + '.mkv'))

        media_number += 1

        for media in self.playlist:
            final_command = ['ffmpeg', '-y']
            width = int(self.main_options_export_profile.currentText().split('x')[0])
            height = int(self.main_options_export_profile.currentText().split('x')[1].split(' ')[0])

            if media[0] == 'audio':

                background = QLabel()
                background.setGeometry(0,0,width,height)
                background.setStyleSheet('QLabel {background-color:black;}')

                artwork = QLabel(str(str(media[2])), parent=background)
                artwork.setGeometry(background.width()*.1,(background.height()*.5)-(background.height()*.1),background.height()*.2, background.height()*.2)
                artwork.setAlignment(Qt.AlignCenter)
                artwork.setStyleSheet('QLabel {background-color: #fff; font-family: "Ubuntu Condensed"; font-size:' + str(background.height()*.09) + 'pt; font-weight:bold; color:black;}')

                title = QLabel(media[3],parent=background)
                title.setAlignment(Qt.AlignVCenter | Qt.AlignLeft)
                title.setWordWrap(True)
                title.setGeometry((background.width()*.11) + (background.width()*.2),0,background.width() - ((background.width()*.21) + (background.width()*.2)), background.height())
                title.setStyleSheet('QLabel {background-color:none; font-family: "Ubuntu"; font-size:' + str(background.height()*.05) + 'pt; font-weight:bold; color:white}')

                preview = QPixmap(QPixmap().grabWidget(background))
                preview.save(os.path.join(path_tmp , 'media' + str('%03d'% media_number) + '.png'))

                final_command.append('-loop')
                final_command.append('1')
                final_command.append('-i')
                final_command.append(os.path.join(path_tmp , 'media' + str('%03d'% media_number) + '.png'))
                final_command.append('-i')
                final_command.append(media[5])
                #final_command.append('-shortest')
                final_command.append('-t')
                final_command.append(str(media[7]))
                final_command.append('-vf')
                final_command.append('fade=in:0:30')
                #final_command.append('-vf')
                #final_command.append('fade=out:' + str(int((media[7] - 1)*25)) + ':' + str(int((media[7])*25)))

                final_command.append('-c:v')
                final_command.append('libx264')
                final_command.append('-c:a')
                final_command.append('copy')
                final_command.append('-preset')
                final_command.append('ultrafast')

                final_command.append(os.path.join(path_tmp , 'media' + str('%03d'% media_number) + '.mkv'))

                final_list.append(os.path.join(path_tmp , 'media' + str('%03d'% media_number) + '.mkv'))

                subprocess.call(final_command)

            elif media[0] == 'image':
                create_thumbnail(media[2], os.path.join(path_tmp , 'media' + str('%03d'% media_number) + '.png'), width, height)

                final_command.append('-loop')
                final_command.append('1')
                final_command.append('-i')
                final_command.append(os.path.join(path_tmp , 'media' + str('%03d'% media_number) + '.png'))
                final_command.append('-i')
                final_command.append(os.path.join(path_kihvim, 'resources', 'silence.flac'))

                final_command.append('-c:v')
                final_command.append('copy')
                final_command.append('-c:a')
                final_command.append('copy')
                final_command.append('-t')
                final_command.append('5')
                final_command.append(os.path.join(path_tmp , 'media' + str('%03d'% media_number) + '.mkv'))

                final_list.append(os.path.join(path_tmp , 'media' + str('%03d'% media_number) + '.mkv'))

                subprocess.call(final_command)

            elif media[0] == 'video':
                final_command.append('-i')
                final_command.append(media[2])

                final_command.append('-c:v')
                final_command.append('copy')
                final_command.append('-c:a')
                final_command.append('copy')
                final_command.append(os.path.join(path_tmp , 'media' + str('%03d'% media_number) + '.mkv'))

                final_list.append(os.path.join(path_tmp , 'media' + str('%03d'% media_number) + '.mkv'))

                subprocess.call(final_command)

            elif media[0] == 'web':
                pass#final_playlist_file += ' address="' + media[2] + '"'
                #final_playlist_file += ' back_to_playlist="' + str(media[-1]) + '">'
                #final_playlist_file += '</media>'

            elif media[0] == 'clock':
                pass#final_playlist_file += ' back_to_playlist="' + str(media[-1]) + '">'
                #final_playlist_file += '</media>'

            elif media[0] == 'screencopy':
                pass#final_playlist_file += ' area="' + str(media[2]) + '">'
                #final_playlist_file += ' back_to_playlist="' + str(media[-1]) + '">'
                #final_playlist_file += '</media>'

            elif media[0] == 'camera':
                pass#final_playlist_file += ' back_to_playlist="' + str(media[-1]) + '">'
                #final_playlist_file += '</media>'

            #final_command.append('-c')
            #final_command.append('copy')
            #final_command.append('-bsf:v')
            #final_command.append('h264_mp4toannexb')
            #final_command.append('-f')
            #final_command.append('mpegts')
            #final_command.append(os.path.join(path_tmp , 'media' + str('%03d'% media_number) + '.ts'))

            #final_command.append('-c:v')
            #final_command.append('libx264')
            #final_command.append('-c:a')
            #final_command.append('aac')
            #final_command.append('-preset')
            #final_command.append('ultrafast')
            #final_command.append('-qp')
            #final_command.append('0')
            #final_command.append(os.path.join(path_tmp , 'media' + str('%03d'% media_number) + '.mkv'))#

            #final_list.append(os.path.join(path_tmp , 'media' + str('%03d'% media_number) + '.mkv'))

            #subprocess.call(final_command)

            media_number += 1


        concat_command = ['ffmpeg']
        concat_command.append('-y')
        for video in final_list:
            concat_command.append('-i')
            concat_command.append(video)
        concat_command.append('-filter_complex')
        filter_complex = ''
        for i in range(media_number):
            filter_complex += '[' + str(i) + ':v:0] [' + str(i) + ':a:0] '
        filter_complex += 'concat=n=' + str(media_number) + ':v=1:a=1 [v] [a]'
        concat_command.append(filter_complex)
        concat_command.append('-map')
        concat_command.append('[v]')
        concat_command.append('-map')
        concat_command.append('[a]')
        concat_command.append('-c:a')
        concat_command.append('aac')
        concat_command.append('-c:v')
        concat_command.append('libx264')
        concat_command.append('-pix_fmt')
        concat_command.append('yuv420p')
        concat_command.append(os.path.join(path_home, 'Playlist final do Kihvim.mp4'))

        subprocess.call(concat_command)


        '''if self.main_options_export_profile.currentText() == '320x180 (iPod/iPhone)':
            final_command.append('-c:a')
            final_command.append('aac')
            final_command.append('-b:a')
            final_command.append('128kb')
            final_command.append('-vcodec')
            final_command.append('mpeg4')
            final_command.append('-b:v')
            final_command.append('1200kb')
            final_command.append('-mbd')
            final_command.append('2')
            final_command.append('-flags')
            final_command.append('+trell+aic')
            final_command.append('-cmp')
            final_command.append('2')
            final_command.append('-subcmp')
            final_command.append('2')
            final_command.append('-s')
            final_command.append('320x180')
            final_command.append(os.path.join(path_tmp , 'media' + str('%03d'% media_number) + '.mp4'))
            final_list += "file '" + 'media' + str('%03d'% media_number) + '.mp4' + "'\n"

        elif self.main_options_export_profile.currentText() == '320x240 (PSP)':
            final_command.append('-b:v')
            final_command.append('300k')
            final_command.append('-s')
            final_command.append('320x240')
            final_command.append('-c:v')
            final_command.append('libxvid')
            final_command.append('-b:a')
            final_command.append('32k')
            final_command.append('-ar')
            final_command.append('24000')
            final_command.append('-acodec')
            final_command.append('aac')
            final_command.append(os.path.join(path_tmp , 'media' + str('%03d'% media_number) + '.mp4'))
            final_list += "file '" + 'media' + str('%03d'% media_number) + '.mp4' + "'\n"

        elif self.main_options_export_profile.currentText() == '720x480 (DVD player)':
            final_command.append('-target')
            final_command.append('ntsc-dvd')
            final_command.append('-ps')
            final_command.append('2000000000')
            final_command.append('-aspect')
            final_command.append('16:9')
            final_command.append(os.path.join(path_tmp , 'media' + str('%03d'% media_number) + '.mpeg'))
            final_list += "file '" + 'media' + str('%03d'% media_number) + '.mpeg' + "'\n"

        elif self.main_options_export_profile.currentText() == '1280x720 (SmartTV)':
            final_command.append('-s')
            final_command.append('hd720')
            final_command.append('-f')
            final_command.append('avi')
            final_command.append('-c:v')
            final_command.append('libx264')
            final_command.append('-crf')
            final_command.append('22')
            final_command.append('-b:v')
            final_command.append('1152k')
            final_command.append('-c:a')
            final_command.append('libmp3lame')
            final_command.append('-b:a')
            final_command.append('128k')
            final_command.append('-ac')
            final_command.append('2')
            final_command.append('-ar')
            final_command.append('44100')
            final_command.append('-pix_fmt')
            final_command.append('yuv420p')
            final_command.append(os.path.join(path_tmp , 'media' + str('%03d'% media_number) + '.mp4'))
            final_list += "file '" + 'media' + str('%03d'% media_number) + '.mp4' + "'\n"

        elif self.main_options_export_profile.currentText() == '704x480 (SmartTV)':
            final_command.append('-s')
            final_command.append('704x480')
            final_command.append('-f')
            final_command.append('avi')
            final_command.append('-c:v')
            final_command.append('libx264')
            final_command.append('-crf')
            final_command.append('22')
            final_command.append('-b:v')
            final_command.append('1152k')
            final_command.append('-c:a')
            final_command.append('libmp3lame')
            final_command.append('-b:a')
            final_command.append('128k')
            final_command.append('-ac')
            final_command.append('2')
            final_command.append('-ar')
            final_command.append('44100')
            final_command.append('-pix_fmt')
            final_command.append('yuv420p')
            final_command.append(os.path.join(path_tmp , 'media' + str('%03d'% media_number) + '.mp4'))
            final_list += "file '" + 'media' + str('%03d'% media_number) + '.mp4' + "'\n"

        final_command.append('-vf')
        final_command.append('setpts=PTS-STARTPTS, asetpts=PTS-STARTPTS')
        #final_command.append('-avoid_negative_ts')
        #final_command.append('make_zero')
        #final_command.append('-fflags')
        #final_command.append('+genpts')'''



    ############################################################################
    ## ADDITION PANEL

    def show_additions(self):
        self.left_panel_addbutton.setEnabled(False)
        self.left_panel_addbutton_label.setText(u'<font style="font-size:9px;">' + u'SELECIONE ALGO PARA' + '</font><br><font style="font-size:14px;"><b>' + u'ADICIONAR' + '</b></font>')

        if self.settings['enable_animations']:
            generate_effect(self.left_panel_geometry_animation, 'geometry', 500, [self.left_panel.x(),0,self.width()*.5,self.height()], [0,0,self.width()*.5,self.height()])
            generate_effect(self.left_panel_addbutton_geometry_animation, 'geometry', 500, [self.left_panel_addbutton.x(),self.left_panel_addbutton.y(),self.left_panel_addbutton.width(),self.left_panel_addbutton.height()], [(self.width()*.5)-183,self.left_panel_addbutton.y(),self.left_panel_addbutton.width(),self.left_panel_addbutton.height()])
        else:
            self.left_panel.setGeometry(0,0,self.width()*.5,self.height())
            self.left_panel_addbutton.setGeometry((self.width()*.5)-183,self.left_panel_addbutton.y(),self.left_panel_addbutton.width(),self.left_panel_addbutton.height())

        if not self.selected_addition == 'audio':
            self.left_panel_audio_icon.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'music_icon_normal.png').replace('\\', '/') + '); } ')
            self.left_panel_audio_panel.setVisible(False)
        if not self.selected_addition == 'image':
            self.left_panel_image_icon.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'image_icon_normal.png').replace('\\', '/') + '); } ')
            self.left_panel_image_panel.setVisible(False)
        if not self.selected_addition == 'video':
            self.left_panel_video_icon.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'video_icon_normal.png').replace('\\', '/') + '); } ')
            self.left_panel_video_panel.setVisible(False)
        if not self.selected_addition == 'clock':
            self.left_panel_clock_icon.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'clock_icon_normal.png').replace('\\', '/') + '); } ')
            self.left_panel_clock_panel.setVisible(False)
        if not self.selected_addition == 'web':
            self.left_panel_web_icon.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'web_icon_normal.png').replace('\\', '/') + '); } ')
            self.left_panel_web_panel.setVisible(False)
        if not self.selected_addition == 'screencopy':
            self.left_panel_screencopy_icon.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'screencopy_icon_normal.png').replace('\\', '/') + '); } ')
            self.left_panel_screencopy_panel.setVisible(False)
        if not self.selected_addition == 'camera':
            self.left_panel_camera_icon.setStyleSheet('QLabel { background-position: center center; background-repeat: no-repeat; background-image: url(' + os.path.join(path_graphics, 'camera_icon_normal.png').replace('\\', '/') + '); } ')
            self.left_panel_camera_panel.setVisible(False)
            if not self.left_panel_camera_preview_mediaplayer.get_media() == None:
                self.left_panel_camera_preview_mediaplayer.stop()
                self.left_panel_camera_preview_mediaplayer.get_media().release()
            #if self.left_panel_camera_preview_mediaplayer:
            #    #print self.left_panel_camera_preview_mediaplayer
            #    #self.left_panel_camera_preview_mediaplayer.get_media().release()
            #    del self.left_panel_camera_preview_mediaplayer
            #    self.left_panel_camera_preview_mediaplayer = False
            #    #self.camera_instance.release()


        if self.selected_addition == 'audio':
            self.left_panel_add_title.setText(u'CÂNTICO')
            self.left_panel_audio_panel.setVisible(True)
        self.left_panel_audio_list.setCurrentItem(None)
        if self.selected_addition == 'image':
            self.left_panel_add_title.setText(u'IMAGEM')
            self.left_panel_image_panel.setVisible(True)
            self.left_panel_image_keyword.setText('')
            self.left_panel_image_tab_changed()
        self.left_panel_image_list.setCurrentItem(None)
        if self.selected_addition == 'video':
            self.left_panel_add_title.setText(u'VÍDEO')
            self.left_panel_video_panel.setVisible(True)
            self.left_panel_video_tab_changed()
        self.left_panel_video_local_list.setCurrentItem(None)
        if self.selected_addition == 'screencopy':
            self.left_panel_add_title.setText(u'CÓPIA DE TELA')
            self.left_panel_screencopy_panel.setVisible(True)
            self.left_panel_screencopy_preview.setPixmap(None)
            self.left_panel_screencopy_preview.setText(u'Não há área para ser exibida.\nDefina uma área no botão abaixo.')
            self.left_panel_screencopy_description.setText(u'<font style="font-size:9px;color:gray;">' + u'SEM ÁREA PARA MOSTRAR' + '</font>')
            app.screencopy.selected_area = False
        if self.selected_addition == 'web':
            self.left_panel_web_address.setText('http://www.jw.org')
            self.left_panel_add_title.setText(u'PÁGINA DO JW.ORG')
            self.left_panel_web_panel.setVisible(True)
        if self.selected_addition == 'clock':
            self.left_panel_add_title.setText(u'PRELÚDIO')
            self.left_panel_clock_panel.setVisible(True)
            self.left_panel_addbutton.setEnabled(True)
            self.left_panel_addbutton_label.setText(u'<font style="font-size:14px;"><b>' + u'ADICIONAR' + '</b></font><br><font style="font-size:9px;">' + u'NA LISTA DE EXECUÇÃO' + '</font>')
        if self.selected_addition == 'camera':
            self.left_panel_add_title.setText(u'CAMERA')
            self.left_panel_camera_panel.setVisible(True)
            self.populate_camera_list()
            self.left_panel_camera_select.setCurrentIndex(-1)
            self.left_panel_camera_test_button.setEnabled(False)
            #self.left_panel_camera_preview_mediaplayer = video_instance.media_player_new()
            #self.left_panel_camera_preview_mediaplayer.set_xwindow(self.left_panel_camera_preview.winId())
            self.left_panel_camera_preview.setText(u'Não há camera selecionada.\nDefina uma camera para testar  .')
        #self.left_panel_video_local_list.setCurrentItem(None)

    def hide_additions(self):
        if self.settings['enable_animations']:
            generate_effect(self.left_panel_geometry_animation, 'geometry', 500, [self.left_panel.x(),0,self.width()*.5,self.height()], [67-(self.width()*.5),0,self.width()*.5,self.height()])
            generate_effect(self.left_panel_addbutton_geometry_animation, 'geometry', 500, [self.left_panel_addbutton.x(),self.left_panel_addbutton.y(),self.left_panel_addbutton.width(),self.left_panel_addbutton.height()], [-196,self.left_panel_addbutton.y(),self.left_panel_addbutton.width(),self.left_panel_addbutton.height()])
        else:
            self.left_panel.setGeometry(67-(self.width()*.5),0,self.width()*.5,self.height())
            self.left_panel_addbutton.setGeometry(-196,self.left_panel_addbutton.y(),self.left_panel_addbutton.width(),self.left_panel_addbutton.height())

    def left_panel_addbutton_clicked(self):
        back_to_playlist = False
        if self.selected_addition == 'audio':
            icon_label = QLabel(self.left_panel_audio_list.currentItem().toolTip())
            icon_label.setGeometry(0,0,50*self.screen_height_proportion,50)
            icon_label.setAlignment(Qt.AlignCenter)
            icon_label.setStyleSheet('QLabel {background-color:black; font-family: "Ubuntu Condensed"; font-size:28px; color:white;}')
            preview = QPixmap(QPixmap().grabWidget(icon_label))

            duration = MP3(self.list_of_songs[int(self.left_panel_audio_list.currentItem().toolTip())][2]).info.length

            self.playlist.append([  'audio',                                                                             # [0] kind
                                    preview,                                                                             # [1] preview image
                                    int(self.left_panel_audio_list.currentItem().toolTip()),                             # [2] number
                                    self.list_of_songs[int(self.left_panel_audio_list.currentItem().toolTip())][0],      # [3] title
                                    self.list_of_songs[int(self.left_panel_audio_list.currentItem().toolTip())][1],      # [4] lyrics
                                    self.list_of_songs[int(self.left_panel_audio_list.currentItem().toolTip())][2],      # [5] file path
                                    False,                                                                               # [6] show lyrics ?
                                    duration,                                                                            # [7] duration in sec
                                    back_to_playlist])                                                                   # [8] back to playlist

        elif self.selected_addition == 'image':
            if self.left_panel_image_tab.currentIndex() == 0:
                filepath = self.left_panel_image_list.currentItem().toolTip()
                md5 = hashlib.md5()
                md5.update(open(filepath, 'rb').read())
                filename_hash = md5.hexdigest()
            elif self.left_panel_image_tab.currentIndex() == 1:
                filename_hash = self.left_panel_image_webview_preview.toolTip()
                filepath = os.path.join(path_tmp , filename_hash + '.png')
            preview = QPixmap(os.path.join(path_tmp , filename_hash + '.png'))
            self.playlist.append([ 'image',                                                                              # [0] kind
                                    preview,                                                                             # [1] preview image
                                    filepath,                                                                            # [2] file path
                                    back_to_playlist])                                                                   # [3] back to playlist

        elif self.selected_addition == 'video':
            start = False
            end = False

            if self.left_panel_video_tab.currentIndex() == 0:
                filepath = self.list_of_video_songs[int(self.left_panel_video_song_list.currentItem().toolTip())][1]
                icon_label = QLabel(self.left_panel_video_song_list.currentItem().toolTip())
                icon_label.setGeometry(0,0,50*self.screen_height_proportion,50)
                icon_label.setAlignment(Qt.AlignCenter)
                icon_label.setStyleSheet('QLabel {background-color:black; font-family: "Ubuntu Condensed"; font-size:28px; color:white;}')
                preview = QPixmap(QPixmap().grabWidget(icon_label))
                title = self.list_of_video_songs[int(self.left_panel_video_song_list.currentItem().toolTip())][0]

            elif self.left_panel_video_tab.currentIndex() == 1:
                filepath = self.list_of_local_videos[self.left_panel_video_local_list.currentItem().toolTip()][1]
                md5 = hashlib.md5()
                md5.update(open(filepath, 'rb').read())
                filename_hash = md5.hexdigest()
                preview = QPixmap(os.path.join(path_tmp , filename_hash + '.png'))

                if self.list_of_local_videos[self.left_panel_video_local_list.currentItem().toolTip()][2]:
                    title = self.list_of_local_videos[self.left_panel_video_local_list.currentItem().toolTip()][2]
                else:
                    title = filepath.split('/')[-1].rsplit('.', 1)[0]

            elif self.left_panel_video_tab.currentIndex() == 2:
                verse = self.left_panel_video_bible_selected_book + ' ' + self.left_panel_video_bible_selected_chapter + ':' + self.left_panel_video_bible_selected_verse
                filepath = self.list_of_bible_videos[verse][0]

                icon_label = QLabel(verse)
                icon_label.setGeometry(0,0,50*self.screen_height_proportion,50)
                icon_label.setAlignment(Qt.AlignCenter)
                icon_label.setStyleSheet('QLabel {background-color:gray; font-family: "Ubuntu Condensed"; font-size:16px; color:white;}')
                preview = QPixmap(QPixmap().grabWidget(icon_label))

                title = verse

                start = self.list_of_bible_videos[verse][1]
                end = self.list_of_bible_videos[verse][2]
            elif self.left_panel_video_tab.currentIndex() == 3:
                filepath = self.left_panel_video_library_selected_title
                icon_label = QLabel()
                icon_label.setScaledContents(True)
                icon_label.setPixmap(self.list_of_library_videos[self.left_panel_video_library_selected_album][self.left_panel_video_library_selected_title][-1])
                icon_label.setGeometry(0,0,50*self.screen_height_proportion,50)

                preview = QPixmap(self.list_of_library_videos[self.left_panel_video_library_selected_album][self.left_panel_video_library_selected_title][-1])

                title = self.list_of_library_videos[self.left_panel_video_library_selected_album][self.left_panel_video_library_selected_title][0]

                if not self.left_panel_video_library_selected_chapter == u'all':
                    start = self.list_of_library_videos[self.left_panel_video_library_selected_album][self.left_panel_video_library_selected_title][1][self.left_panel_video_library_selected_chapter][0]
                    end = self.list_of_library_videos[self.left_panel_video_library_selected_album][self.left_panel_video_library_selected_title][1][self.left_panel_video_library_selected_chapter][1] - 1.0

            duration_output = subprocess.Popen([ffmpeg_bin, '-i', filepath], startupinfo=startupinfo, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT).stdout.read().decode()
            if 'Duration:' in duration_output:
                duration = convert_timecode_to_seconds(duration_output.split('Duration: ')[1].split(',')[0])

            subprocess.call([ffmpeg_bin, '-loglevel', 'error', '-y', '-ss', '00:10.00', '-i', filepath, '-frames:v', '1', os.path.join(path_tmp, filepath.split('/')[-1].rsplit('.', 1)[0] + '.png')], startupinfo=startupinfo)
            image = QPixmap(os.path.join(path_tmp, filepath.split('/')[-1].rsplit('.', 1)[0] + '.png'))

            self.playlist.append([ 'video',                                                                              # [0] kind
                                    preview,                                                                             # [1] preview image
                                    filepath,                                                                            # [2] file path
                                    image,                                                                               # [3] preview
                                    title,                                                                               # [4] title
                                    start,                                                                               # [5] start
                                    end,                                                                                 # [6] end
                                    duration,                                                                            # [7] duration
                                    back_to_playlist])                                                                   # [8] back to playlist

        elif self.selected_addition == 'web':
            preview = QPixmap().grabWidget(self.left_panel_web_webview, h=self.left_panel_web_webview.width()*self.screen_width_proportion).scaled(56*self.screen_height_proportion, 56)
            image = QPixmap().grabWidget(self.left_panel_web_webview, h=self.left_panel_web_webview.width()*self.screen_width_proportion)

            self.playlist.append([ 'web',                                                                                # [0] kind
                                    preview,                                                                             # [1] preview image
                                    self.left_panel_web_address.text(),                                                  # [2] address
                                    image,                                                                               # [3] preview
                                    back_to_playlist])                                                                   # [4] back to playlist
        elif self.selected_addition == 'clock':
            icon_label = QLabel()
            icon_label.setPixmap(os.path.join(path_graphics, 'clock_icon_normal.png'))
            icon_label.setGeometry(0,0,50*self.screen_height_proportion,50)
            icon_label.setAlignment(Qt.AlignCenter)
            icon_label.setStyleSheet('QLabel {background-color:black;}')
            preview = QPixmap(QPixmap().grabWidget(icon_label))

            self.playlist.append([ 'clock',                                                                              # [0] kind
                                    preview,                                                                             # [1] preview
                                    self.left_panel_clock_show_clock.isChecked(),                                        # [2] show clock
                                    self.left_panel_clock_show_song.isChecked(),                                        # [3] show song
                                    self.left_panel_clock_start_silent.isChecked(),                                      # [4] start silent
                                    back_to_playlist])                                                                   # [5] back to playlist
        elif self.selected_addition == 'screencopy':
            icon_label = QLabel()
            icon_label.setPixmap(os.path.join(path_graphics, 'screencopy_icon_normal.png'))
            icon_label.setGeometry(0,0,50*self.screen_height_proportion,50)
            icon_label.setAlignment(Qt.AlignCenter)
            icon_label.setStyleSheet('QLabel {background-color:black;}')
            preview = QPixmap(QPixmap().grabWidget(icon_label))

            self.playlist.append([ 'screencopy',                                                                         # [0] kind
                                    preview,                                                                             # [1] preview
                                    app.screencopy.selected_area,                                                        # [2] selected area
                                    back_to_playlist])                                                                   # [3] back to playlist

        elif self.selected_addition == 'camera':
            self.left_panel_camera_preview_mediaplayer.stop()
            self.left_panel_camera_preview_mediaplayer.get_media().release()
            icon_label = QLabel()
            icon_label.setPixmap(os.path.join(path_graphics, 'camera_icon_normal.png'))
            icon_label.setGeometry(0,0,50*self.screen_height_proportion,50)
            icon_label.setAlignment(Qt.AlignCenter)
            icon_label.setStyleSheet('QLabel {background-color:black;}')
            preview = QPixmap(QPixmap().grabWidget(icon_label))
            self.populate_camera_list()
            for camera in self.list_of_cameras.keys():
                if self.left_panel_camera_select.currentText() == self.list_of_cameras[camera][0]:
                    selected_camera = camera
                    break

            if selected_camera:
                mediaplayer =  video_instance.media_player_new()
                mediaplayer.set_media(video_instance.media_new('v4l2://' + selected_camera))

                self.playlist.append([ 'camera',                                                                             # [0] kind
                                        preview,                                                                             # [1] preview
                                        mediaplayer,                                                                         # [2] media player
                                        back_to_playlist])                                                                   # [3] back to playlist

        self.populate_playlist()

    ############################################################################
    ## ADDITION PANEL - AUDIO

    def populate_audio_list(self):
        self.left_panel_audio_list.clear()
        self.left_panel_audio_alert.setVisible(False)
        self.left_panel_audio_list.setVisible(False)
        self.left_panel_audio_edit.setVisible(False)
        self.left_panel_audio_remove.setVisible(False)
        self.left_panel_audio_download_lyrics.setVisible(False)
        self.left_panel_audio_edit_panel.setVisible(False)

        if len(self.list_of_songs.keys()) > 0:
            for key in sorted(self.list_of_songs.keys()):
                if os.path.isfile(self.list_of_songs[key][2]):
                    label = QLabel(str(key))
                    label.setGeometry(0,0,48,48)
                    label.setAlignment(Qt.AlignCenter)
                    label.setStyleSheet('QLabel {background-color:silver; font-family: "Ubuntu Condensed"; font-size:28px; color:white;}')
                    item = QListWidgetItem(QIcon(QPixmap().grabWidget(label)), None)
                    item.setToolTip(str(key))
                    self.left_panel_audio_list.addItem(item)

        if self.left_panel_audio_list.count() > 0:
            self.left_panel_audio_list.setVisible(True)
            self.left_panel_audio_add.setGeometry(10+self.left_panel_audio_list.width()-170,10+self.left_panel_audio_list.height()+5,40,40)
            self.left_panel_audio_edit.setVisible(True)
            self.left_panel_audio_remove.setVisible(True)
            self.left_panel_audio_download_lyrics.setVisible(True)
        else:
            self.left_panel_audio_add.setGeometry(self.left_panel_audio_panel.width()*.4,self.left_panel_audio_panel.height()*.6,self.left_panel_audio_panel.width()*.2,self.left_panel_audio_panel.height()*.1)
            self.left_panel_audio_alert.setVisible(True)

        self.left_panel_audio_list_clear_selection()

    def left_panel_audio_list_clear_selection(self):
        self.left_panel_audio_list.setCurrentRow(-1)
        self.left_panel_audio_edit.setEnabled(False)
        self.left_panel_audio_remove.setEnabled(False)

    def left_panel_audio_download_lyrics_clicked(self):
        for key in self.list_of_songs.keys():
            self.list_of_songs[key][1] = self.download_lyrics(key)

    def left_panel_audio_add_clicked(self):
        song_path_list = QFileDialog.getOpenFileNames(self, "Selecione os arquivos de audio", path_home, "Arquivos de audio (*.mp3)", options=self.dialog_options)[0]
        for song_path_file in song_path_list:
            if os.path.isfile(song_path_file):
                song = ID3(song_path_file)
                title = song["TIT2"].text[0]
                number = get_number(title)
                lyrics = ''
                if u'USLT::XXX' in song.keys():
                    lyrics = song[u'USLT::XXX'].text

                if not number and 'TRCK' in song.keys() and (song['TRCK'].text).isnumeric():
                    number = song['TRCK'].text

                if number in title:
                    title = title.replace('_', ' ').split(number, 1)[1].split(' ', 1)[1]

                if number:
                    self.list_of_songs[int(number)] = [title, lyrics, song_path_file]

        self.populate_audio_list()

    def left_panel_audio_remove_clicked(self):
        del self.list_of_songs[int(self.left_panel_audio_list.currentItem().toolTip())]
        self.populate_audio_list()

    def left_panel_audio_edit_clicked(self):
        if self.left_panel_audio_edit.isChecked():
            self.left_panel_audio_edit_panel.setVisible(True)
            self.left_panel_audio_list.setVisible(False)
            self.left_panel_audio_add.setVisible(False)
            self.left_panel_audio_edit_number.setText(self.left_panel_audio_list.currentItem().toolTip())
            self.left_panel_audio_edit_title.setText(self.list_of_songs[int(self.left_panel_audio_list.currentItem().toolTip())][0])
            self.left_panel_audio_edit_filepath.setText(self.list_of_songs[int(self.left_panel_audio_list.currentItem().toolTip())][2])
            self.left_panel_audio_edit_lyrics.setPlainText(self.list_of_songs[int(self.left_panel_audio_list.currentItem().toolTip())][1])
        else:
            self.left_panel_audio_edit_panel.setVisible(False)
            self.left_panel_audio_list.setVisible(True)
            self.left_panel_audio_add.setVisible(True)

            self.list_of_songs[int(self.left_panel_audio_list.currentItem().toolTip())][0] = self.left_panel_audio_edit_title.text()
            self.list_of_songs[int(self.left_panel_audio_list.currentItem().toolTip())][1] = self.left_panel_audio_edit_lyrics.toPlainText()

    def left_panel_audio_list_clicked(self):
        if self.left_panel_audio_list.currentItem():
            self.left_panel_add_title.setText(u'<font style="font-size:14px;"><b>' + u'CÂNTICO '+ str(self.left_panel_audio_list.currentItem().toolTip()) + '</b></font><br><font style="font-size:9px;">' + self.list_of_songs[int(self.left_panel_audio_list.currentItem().toolTip())][0] + '</font>')
            self.left_panel_addbutton_label.setText(u'<font style="font-size:14px;"><b>' + u'ADICIONAR' + '</b></font><br><font style="font-size:9px;">' + u'NA LISTA DE EXECUÇÃO' + '</font>')
            self.left_panel_addbutton.setEnabled(True)
            self.left_panel_audio_edit.setEnabled(True)
            self.left_panel_audio_remove.setEnabled(True)
            #self.playlist_options_panel_audio_panel_show_lyrics.setChecked(False)

    def left_panel_audio_edit_lyrics_get_from_web_clicked(self):
        self.left_panel_audio_edit_lyrics.setPlainText(self.download_lyrics(int(self.left_panel_audio_list.currentItem().toolTip())))

    def download_lyrics(self, song):
        media_file = MP3(self.list_of_songs[song][2])
        locale = self.list_of_songs[song][2].split('/')[-1].split('_')[1]
        if not locale in ['T']:
            locale = 'T'
        if media_file.tags and 'UFID:Default' in media_file.tags.keys():
            docid = media_file.tags['UFID:Default'].data
            lyrics_page = urllib.request.urlopen('http://wol.jw.org/wol/finder?wtlocale=' + locale + '&docid=' + docid)
            html = unicode(lyrics_page.read(), 'utf-8')
            if '"pGroup"' in html and '"closingContent"' in html:
                counter = 1
                final_text = u''
                for line in html.split('<div class="pGroup">')[1].split('<div class="closingContent">')[0].split('</p>'):
                    if '<div class="chorus">' in line:
                        final_text += '\n'

                    if 'class="sl"' in line:
                        if counter > 1:
                            final_text += '\n'
                        if '<li>' in line:
                            line = line.replace('<li>', str(counter) + '. ')
                        counter += 1
                    else:
                        final_text += '    '

                    final_text += clean_spaces(clean_xml(line)) + '\n'
        return final_text

    ############################################################################
    ## ADDITION PANEL - IMAGE

    def populate_image_list(self):
        self.left_panel_image_list.clear()
        self.left_panel_image_alert.setVisible(False)
        self.left_panel_image_list.setVisible(False)
        self.left_panel_image_remove.setVisible(False)
        self.left_panel_image_path_label.setText(u'<font style="font-size:9px;color:gray;">' + u'EXIBINDO IMAGENS DA PASTA' + '</font><br><font style="font-size:14px;color:gray;">' + self.path_for_images + '</font>')
        self.left_panel_image_edit.setVisible(False)

        list_of_images = []

        for key in self.list_of_images_from_folder.keys():
            if os.path.isfile(self.list_of_images_from_folder[key][1]):
                list_of_images.append(self.list_of_images_from_folder[key])

        for key in self.list_of_images_added.keys():
            if os.path.isfile(self.list_of_images_added[key][1]):
                list_of_images.append(self.list_of_images_added[key])

        if len(list_of_images) > 0:
            self.left_panel_image_list.setVisible(True)
            for image in list_of_images:
                item = QListWidgetItem(QIcon(image[0]), None)
                item.setToolTip(image[1])
                label = QLabel('<font style="font-size:12pt; color:black; line-height:50px;">' + image[1].split('/')[-1].rsplit('.', 1)[0] + '</font><br><font style="font-size:8pt; color:silver">' + image[1] + '</font>')
                self.left_panel_image_list.addItem(item)
                self.left_panel_image_list.setItemWidget(item, label)
            self.left_panel_image_path_label.setGeometry(10,10+self.left_panel_image_list.height()+5,self.left_panel_image_list.width()-90,40)
            self.left_panel_image_add.setGeometry(10+self.left_panel_image_list.width()-85,10+self.left_panel_image_list.height()+5,40,40)
            self.left_panel_image_remove.setVisible(True)
        else:
            self.left_panel_image_path_label.setGeometry(10,10+self.left_panel_image_list.height()+5,self.left_panel_image_list.width(),40)
            self.left_panel_image_add.setGeometry(self.left_panel_image_panel.width()*.4,self.left_panel_image_panel.height()*.6,self.left_panel_image_panel.width()*.2,self.left_panel_image_panel.height()*.1)
            self.left_panel_image_alert.setVisible(True)
        self.left_panel_image_edit.setGeometry(self.left_panel_image_path_label.width()-40,0,40,40)

        self.left_panel_image_list_clear_selection()

    def left_panel_image_list_clear_selection(self):
        self.left_panel_image_list.setCurrentRow(-1)
        self.left_panel_image_remove.setEnabled(False)

    def populate_list_of_images_from_folder(self):
        self.list_of_images_from_folder = {}
        for filename in os.listdir(self.path_for_images):
            if filename.split('.')[-1] in ['jpg', 'jpeg', 'png', 'JPG', 'PNG', 'JPEG']:
                md5 = hashlib.md5()
                md5.update(open(os.path.join(self.path_for_images, filename), 'rb').read())
                filename_hash = md5.hexdigest()
                if not os.path.isfile(os.path.join(path_tmp , filename_hash + '.png')):
                    create_thumbnail(os.path.join(self.path_for_images, filename), os.path.join(path_tmp , filename_hash + '.png'),int(56*self.screen_height_proportion), 56)
                icon = os.path.join(path_tmp , filename_hash + '.png')
                #label.setStyleSheet('QLabel { padding-left:5px; } ')
                self.list_of_images_from_folder[filename_hash] = [icon, os.path.join(self.path_for_images, filename)]

    def left_panel_image_edit_clicked(self):
        newpath = QFileDialog.getExistingDirectory(self, "Selecione uma pasta de images", path_home, options=self.dialog_options)
        if not newpath == '':
            self.path_for_images = newpath
            self.populate_list_of_images_from_folder()
            self.populate_image_list()

    def left_panel_image_add_clicked(self):
        image_path_list = QFileDialog.getOpenFileNames(self, "Selecione os arquivos de imagem", path_home, "Arquivos de imagem (*.jpg *.jpeg *.png *.pdf)", options=self.dialog_options)[0]
        for filename in image_path_list:
            if os.path.isfile(filename):
                icon = False

                filename_hash = False
                if filename.split('.')[-1] in ['jpg', 'jpeg', 'png', 'JPG', 'JPEG', 'PNG']:
                    md5 = hashlib.md5()
                    md5.update(open(filename, 'rb').read())
                    filename_hash = md5.hexdigest()
                    if not os.path.isfile(os.path.join(path_tmp , filename_hash + '.png')):
                        create_thumbnail(filename, os.path.join(path_tmp , filename_hash + '.png'), 56*self.screen_height_proportion, 56)
                    icon = os.path.join(path_tmp , filename_hash + '.png')
                elif filename.split('.')[-1] in ['pdf']:
                    args =  list.copy(gs_executable)
                    args += ['-c', '(' + filename + ') (r) file runpdfbegin pdfpagecount = quit']
                    gs_count_pages = subprocess.Popen(args, stdout=subprocess.PIPE, startupinfo=startupinfo)
                    number_of_pages = int(clean_spaces(gs_count_pages.stdout.read()))
                    page_number = QInputDialog.getInteger(self, u'Página do PDF', u'Indique o número da página', value=1, minValue=1, maxValue=number_of_pages)
                    if page_number[1] == True:
                        args =  list.copy(gs_executable)
                        args += ['-dTextAlphaBits=4','-dGraphicsAlphaBits=4','-dDOINTERPOLATE']
                        args += ['-sDEVICE=pngalpha']

                        args += ['-dFirstPage=' + str(page_number[0])]
                        args += ['-dLastPage=' + str(page_number[0])]

                        args += ['-g' + str(app.monitor.width()) + 'x' + str(app.monitor.height())]
                        #args += ['-r' + str(target_ratio_width) + 'x' + str(target_ratio_height)]
                        args += ['-sOutputFile=' + os.path.join(path_tmp, filename.split('/')[-1] + '_' + str(page_number[0])  + '.png')]
                        args += [os.path.join(destination_folder,'.' + destination_filename + '-' + str(current_page) + '.ps')]

                        subprocess.call(args, startupinfo=startupinfo)

                        if not transparency:
                            png_page = Image.open(os.path.join(path_tmp, filename.split('/')[-1] + '_' + str(page_number[0])  + '.png'))
                            alpha = png_page.convert('RGBA').split()[-1]

                            png_page_with_background = Image.new("RGBA", (int(app.monitor.width()), int(app.monitor.height())), (255, 255, 255) + (255,))
                            png_page_with_background.paste(png_page, mask=alpha)
                            png_page_with_background.save(os.path.join(path_tmp, filename.split('/')[-1] + '_' + str(page_number[0])  + '.png'))

                        md5 = hashlib.md5()
                        md5.update(open(os.path.join(path_tmp, filename.split('/')[-1] + '_' + str(page_number[0])  + '.png'), 'rb').read())
                        filename_hash = md5.hexdigest()
                        if not os.path.isfile(os.path.join(path_tmp , filename_hash + '.png')):
                            create_thumbnail(os.path.join(path_tmp, filename.split('/')[-1] + '_' + str(page_number[0])  + '.png'), os.path.join(path_tmp , filename_hash + '.png'), 56*self.screen_height_proportion, 56)
                        icon = os.path.join(path_tmp , filename_hash + '.png')

                if icon and filename_hash:
                    self.list_of_images_added[filename_hash] = [icon, filename]

        self.populate_image_list()

    def left_panel_image_list_clicked(self):
        if self.left_panel_image_list.currentItem():
            self.left_panel_add_title.setText(u'<font style="font-size:14px;"><b>' + u'IMAGEM' + '</b></font><br><font style="font-size:9px;">' + self.left_panel_image_list.currentItem().toolTip() + '</font>')
            self.left_panel_addbutton_label.setText(u'<font style="font-size:14px;"><b>' + u'ADICIONAR' + '</b></font><br><font style="font-size:9px;">' + u'NA LISTA DE EXECUÇÃO' + '</font>')
            self.left_panel_addbutton.setEnabled(True)
            self.left_panel_image_remove.setEnabled(True)

    def left_panel_image_tab_changed(self):
        if self.left_panel_image_keyword.text() == '':
            self.left_panel_image_keyword_go.setEnabled(False)
            self.left_panel_image_webview.setVisible(False)
            self.left_panel_image_webview_warning.setVisible(True)
            self.left_panel_image_webview_preview.setVisible(False)
            self.left_panel_image_webview_preview_cancel_button.setVisible(False)
        else:
            self.left_panel_image_webview_preview_cancel_button_clicked()

    def left_panel_image_keyword_go_clicked(self):
        #self.left_panel_image_webview.load(QUrl('https://www.google.com.br/search?q=' + str(self.left_panel_image_keyword.text()).replace(' ', '+') + '&as_sitesearch=jw.org&tbm=isch'))
        self.left_panel_image_webview.load(QUrl('https://duckduckgo.com/?q=' + self.left_panel_image_keyword.text().replace(' ', '+') + '&sites=jw.org&iar=images'))
        self.left_panel_image_webview_preview_cancel_button_clicked()
        #self.left_panel_image_webview.setVisible(True)
        #self.left_panel_image_webview_warning.setVisible(False)
        #self.left_panel_image_webview.show()
        #self.left_panel_add_title.setText(u'<font style="font-size:14px;"><b>' + u'IMAGEM DO JW.ORG' + '</b></font><br><font style="font-size:9px;">' + self.left_panel_web_address.text() + '</font>')
        #self.left_panel_addbutton_label.setText(u'<font style="font-size:14px;"><b>' + u'ADICIONAR' + '</b></font><br><font style="font-size:9px;">' + u'NA LISTA DE EXECUÇÃO' + '</font>')
        #self.left_panel_addbutton.setEnabled(True)

        #self.left_panel_web_webview.setZoomFactor(zoom)

    def left_panel_image_keyword_changed(self):
        if not self.left_panel_image_keyword.text() == '':
            self.left_panel_image_keyword_go.setEnabled(True)
        else:
            self.left_panel_image_keyword_go.setEnabled(False)


    def left_panel_image_keyword_load_finished(self):
        print('loading...')
        image_address = False
        page_html = False

        #self.left_panel_image_webview.page().runJavaScript("var links = document.getElementsByTagName('a');for (var i=0, len=links.length; i < len; i ++) {  links[i].target = '_self';}")

        #print(self.left_panel_image_webview.page().title())
        #print(self.left_panel_image_webview.page().url())
        #page_html = self.left_panel_image_webview.toHtml()

        if page_html:
            if '<title>Access Denied</title>' in page_html.toHtml():
                image_address = page_html.toHtml().split('"')[1].split('"')[0]
            elif len(page_html.toHtml().split('<img ')) < 3  and ' src="' in page_html.toHtml():
                image_address = page_html.toHtml().split('<img ')[1].split(' src="')[1].split('"')[0]

        if image_address:
            image = urllib.request.urlopen(image_address)
            open(os.path.join(path_tmp, 'image_from_jworg.jpg'), 'wb').write(image.read())
            md5 = hashlib.md5()
            md5.update(open(os.path.join(path_tmp, 'image_from_jworg.jpg'), 'rb').read())
            filename_hash = md5.hexdigest()

            create_thumbnail(os.path.join(path_tmp, 'image_from_jworg.jpg'), os.path.join(path_tmp, filename_hash + '.png'), int(app.monitor.width()), int(app.monitor.height()))

            self.left_panel_image_webview.setVisible(False)
            self.left_panel_image_webview_preview.setPixmap(QPixmap(os.path.join(path_tmp, filename_hash + '.png')).scaled(self.left_panel_image_webview_preview.width(), self.left_panel_image_webview_preview.height(), Qt.KeepAspectRatio, Qt.SmoothTransformation))
            self.left_panel_image_webview_preview.setToolTip(filename_hash)
            self.left_panel_add_title.setText(u'<font style="font-size:14px;"><b>' + u'IMAGEM' + '</b></font><br><font style="font-size:9px;">do jw.org (' + self.left_panel_image_keyword.text().lower() + ')</font>')
            self.left_panel_addbutton_label.setText(u'<font style="font-size:14px;"><b>' + u'ADICIONAR' + '</b></font><br><font style="font-size:9px;">' + u'NA LISTA DE EXECUÇÃO' + '</font>')
            self.left_panel_addbutton.setEnabled(True)
            self.left_panel_image_webview_preview.setVisible(True)

    def left_panel_image_webview_preview_cancel_button_clicked(self):
        self.left_panel_add_title.setText(u'IMAGEM')
        self.left_panel_addbutton.setEnabled(False)
        if self.left_panel_image_webview.page().history().canGoBack():
            self.left_panel_image_webview.back()
        self.left_panel_image_webview.setVisible(True)
        self.left_panel_image_webview_preview.setVisible(False)
        self.left_panel_image_webview_warning.setVisible(False)

    ############################################################################
    ## ADDITION PANEL - VIDEO

    def populate_video_song_list(self):
        self.left_panel_video_song_list.clear()
        self.left_panel_video_song_alert.setVisible(False)
        self.left_panel_video_song_list.setVisible(False)
        self.left_panel_video_song_remove.setVisible(False)

        existing_files = False
        if len(self.list_of_video_songs.keys()) > 0:
            for key in sorted(self.list_of_video_songs.keys()):
                if os.path.isfile(self.list_of_video_songs[key][1]):
                    if not existing_files:
                        existing_files = True
                    label = QLabel(str(key))
                    label.setGeometry(0,0,48,48)
                    label.setAlignment(Qt.AlignCenter)
                    label.setStyleSheet('QLabel {background-color:silver; font-family: "Ubuntu Condensed"; font-size:28px; color:white;}')
                    item = QListWidgetItem(QIcon(QPixmap().grabWidget(label)), None)
                    item.setToolTip(str(key))
                    self.left_panel_video_song_list.addItem(item)

        if existing_files:
            self.left_panel_video_song_list.setVisible(True)
            self.left_panel_video_song_add.setGeometry(10+self.left_panel_video_song_list.width()-85,10+self.left_panel_video_song_list.height()+5,40,40)
            self.left_panel_video_song_remove.setVisible(True)
        else:
            self.left_panel_video_song_alert.setVisible(True)
            self.left_panel_video_song_add.setGeometry(self.left_panel_video_tab_songs.width()*.4,self.left_panel_video_tab_songs.height()*.6,self.left_panel_video_tab_songs.width()*.2,self.left_panel_video_tab_songs.height()*.1)


    def left_panel_video_song_list_clicked(self):
        if self.left_panel_video_song_list.currentItem():
            self.left_panel_add_title.setText(u'<font style="font-size:14px;"><b>' + u'CÂNTICO '+ str(self.left_panel_video_song_list.currentItem().toolTip()) + '</b></font><br><font style="font-size:9px;">' + self.list_of_video_songs[int(self.left_panel_video_song_list.currentItem().toolTip())][0] + '</font>')
            self.left_panel_addbutton_label.setText(u'<font style="font-size:14px;"><b>' + u'ADICIONAR' + '</b></font><br><font style="font-size:9px;">' + u'NA LISTA DE EXECUÇÃO' + '</font>')
            self.left_panel_addbutton.setEnabled(True)
            self.left_panel_video_song_remove.setEnabled(True)

    def left_panel_video_song_add_clicked(self):
        song_path_list = QFileDialog.getOpenFileNames(self, u"Selecione os arquivos de vídeo", path_home, "Arquivos de audio (*.mp4)", options=self.dialog_options)[0]
        for song_path_file in song_path_list:
            if os.path.isfile(song_path_file):
                song = MP4(song_path_file)
                title = song_path_file.split('/')[-1]
                if '\xa9nam' in  song.tags.keys():
                    title = song.tags['\xa9nam'][0]

                number = get_number(title)

                if number:
                    if number in title:
                        title = title.replace('_', ' ').split(number, 1)[1].split(' ', 1)[1]
                    self.list_of_video_songs[int(number)] = [title, song_path_file]


        self.populate_video_song_list()

    def left_panel_video_song_remove_clicked(self):
        del self.list_of_video_songs[int(self.left_panel_video_song_list.currentItem().toolTip())]
        self.populate_video_song_list()


    def populate_video_local_list(self):
        self.left_panel_video_local_list.clear()
        self.left_panel_video_local_alert.setVisible(False)
        self.left_panel_video_local_list.setVisible(False)
        self.left_panel_video_remove.setVisible(False)
        self.left_panel_video_path_label.setText(u'<font style="font-size:9px;color:gray;">' + u'EXIBINDO VÍDEOS DA PASTA' + '</font><br><font style="font-size:14px;color:gray;">' + self.path_for_videos + '</font>')
        self.left_panel_video_edit.setVisible(False)

        self.list_of_local_videos = {}

        for key in self.list_of_videos_from_folder.keys():
            if os.path.isfile(self.list_of_videos_from_folder[key][1]):
                self.list_of_local_videos[key] = self.list_of_videos_from_folder[key]

        for key in self.list_of_videos_added.keys():
            if os.path.isfile(self.list_of_videos_added[key][1]):
                self.list_of_local_videos[key] = self.list_of_videos_added[key]

        if len(self.list_of_local_videos.keys()) > 0:
            self.left_panel_video_local_list.setVisible(True)
            for hashcode in self.list_of_local_videos.keys():
                item = QListWidgetItem(QIcon(self.list_of_local_videos[hashcode][0]), None)
                item.setToolTip(hashcode)

                if self.list_of_local_videos[hashcode][2]:
                    title = self.list_of_local_videos[hashcode][2]
                else:
                    title = self.list_of_local_videos[hashcode][1].split('/')[-1].rsplit('.', 1)[0]

                label = QLabel('<font style="font-size:12pt; color:black; line-height:50px;">' + title + '</font><br><font style="font-size:8pt; color:silver">' + self.list_of_local_videos[hashcode][1] + '</font>')
                self.left_panel_video_local_list.addItem(item)
                self.left_panel_video_local_list.setItemWidget(item, label)
            self.left_panel_video_path_label.setGeometry(10,10+self.left_panel_video_local_list.height()+5,self.left_panel_video_local_list.width()-90,40)
            self.left_panel_video_add.setGeometry(10+self.left_panel_video_local_list.width()-85,10+self.left_panel_video_local_list.height()+5,40,40)
            self.left_panel_video_remove.setVisible(True)
        else:
            self.left_panel_video_path_label.setGeometry(10,10+self.left_panel_video_local_list.height()+5,self.left_panel_video_local_list.width(),40)
            self.left_panel_video_add.setGeometry(self.left_panel_video_panel.width()*.4,self.left_panel_video_panel.height()*.6,self.left_panel_video_panel.width()*.2,self.left_panel_video_panel.height()*.1)
            self.left_panel_video_local_alert.setVisible(True)
        self.left_panel_video_edit.setGeometry(self.left_panel_video_path_label.width()-40,0,40,40)

        self.left_panel_video_local_list_clear_selection()

    def left_panel_video_tab_changed(self):
        None






    def populate_video_bible_list(self):
        self.left_panel_video_bible_alert.setVisible(False)
        self.left_panel_video_bible_list.setVisible(False)

        path_for_bible_videos = path_home
        if os.path.isfile(os.path.join(path_home, '.config', 'user-dirs.dirs')):
            filecontent = open(os.path.join(os.path.join(path_home, '.config', 'user-dirs.dirs'))).read()
            if 'XDG_VIDEOS_DIR=' in filecontent:
                if os.path.isdir(os.path.join(filecontent.split('XDG_VIDEOS_DIR=',1)[1].split('"')[1].replace('$HOME', path_home))):
                    path_for_bible_videos = os.path.join(filecontent.split('XDG_VIDEOS_DIR=',1)[1].split('"')[1].replace('$HOME', path_home))

        #self.list_of_bible_videos = {}
        list_of_bible_videos_filenames = []

        for verse in self.list_of_bible_videos.values():
            list_of_bible_videos_filenames.append(verse[0])

        for root, dirs, files in os.walk(path_for_bible_videos):
            for name in files:
                if name.endswith('.m4v'):
                    if not unicode(os.path.join(root, name), 'utf-8') in list_of_bible_videos_filenames:
                        video_chapters_info = unicode(subprocess.Popen([ffprobe_bin,'-loglevel', 'error',  '-show_chapters', '-print_format', 'xml', os.path.join(root, name)], startupinfo=startupinfo, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE).stdout.read(), 'utf-8')
                        for chapter in video_chapters_info.split('<chapter '):
                            chapter_name = False
                            start = False
                            end = False
                            if 'key="title"' in chapter and 'value="' in chapter.split('key="title"')[1]:
                                chapter_name_from_source = chapter.split('key="title"')[1].split('value="')[1].split('"')[0]
                                if ' ' in chapter_name_from_source and ':' in chapter_name_from_source:
                                    chapter_name = chapter_name_from_source
                            if 'start_time="' in chapter:
                                start = float(chapter.split('start_time="')[1].split('"')[0])
                            if 'end_time="' in chapter:
                                end = float(chapter.split('end_time="')[1].split('"')[0])
                            if chapter_name and start and end:
                                self.list_of_bible_videos[chapter_name] = [os.path.join(root, name), start, end]

        self.populate_video_bible_books_list()

        if self.left_panel_video_bible_list.count() > 0:
            self.left_panel_video_bible_list.setVisible(True)
        else:
            self.left_panel_video_bible_alert.setVisible(True)

        self.left_panel_video_bible_list_clear_selection()

    def populate_video_bible_books_list(self):
        self.left_panel_video_bible_list.clear()
        self.left_panel_video_bible_list.setIconSize(QSize(96, 48))

        self.left_panel_video_bible_selected_book = False
        self.left_panel_video_bible_selected_chapter = False
        self.left_panel_video_bible_selected_verse = False
        self.left_panel_video_bible_search.setVisible(True)
        self.left_panel_video_bible_book_label.setVisible(False)
        self.left_panel_video_bible_chapter_label.setVisible(False)
        self.left_panel_video_bible_verse_label.setVisible(False)

        list_of_books = []
        for key in self.list_of_bible_videos.keys():
            book = key.split(' ')[0]
            if not book in list_of_books:
                list_of_books.append(book)

        for book in list_of_books:
            label = QLabel(book)
            label.setGeometry(0,0,96,48)
            label.setStyleSheet('QLabel {background-color:gray; font-family: "Ubuntu Condensed"; font-size:28px; color:white;  qproperty-alignment:"AlignCenter";}')
            item = QListWidgetItem(QIcon(QPixmap().grabWidget(label)), None)
            item.setToolTip(book)
            if self.left_panel_video_bible_search.text().lower() == '' or self.left_panel_video_bible_search.text().lower() in book.lower():
                self.left_panel_video_bible_list.addItem(item)

        #self.left_panel_video_bible_list_clear_selection()

    def populate_video_bible_chapters_list(self):
        self.left_panel_video_bible_list.clear()
        self.left_panel_video_bible_list.setIconSize(QSize(48, 48))

        self.left_panel_video_bible_selected_chapter = False
        self.left_panel_video_bible_selected_verse = False

        self.left_panel_video_bible_book_label.setVisible(True)
        self.left_panel_video_bible_book_label.setText(self.left_panel_video_bible_selected_book)
        self.left_panel_video_bible_chapter_label.setVisible(False)
        self.left_panel_video_bible_verse_label.setVisible(False)

        list_of_chapters = []
        for key in self.list_of_bible_videos.keys():
            if self.left_panel_video_bible_selected_book in key:
                chapter = int(key.split(' ')[1].split(':')[0])
                if not chapter in list_of_chapters:
                    list_of_chapters.append(chapter)

        for chapter in sorted(list_of_chapters):
            label = QLabel(str(chapter))
            label.setGeometry(0,0,48,48)
            label.setAlignment(Qt.AlignCenter)
            label.setStyleSheet('QLabel {background-color:silver; font-family: "Ubuntu Condensed"; font-size:28px; color:white;}')
            item = QListWidgetItem(QIcon(QPixmap().grabWidget(label)), None)
            item.setToolTip(str(chapter))
            self.left_panel_video_bible_list.addItem(item)

    def populate_video_bible_verses_list(self):
        self.left_panel_video_bible_list.clear()
        self.left_panel_video_bible_list.setIconSize(QSize(48, 48))

        self.left_panel_video_bible_selected_verse = False

        self.left_panel_video_bible_chapter_label.setVisible(True)
        self.left_panel_video_bible_chapter_label.setText(self.left_panel_video_bible_selected_chapter)
        self.left_panel_video_bible_verse_label.setVisible(False)

        list_of_verses = []
        for key in self.list_of_bible_videos.keys():
            if self.left_panel_video_bible_selected_book in key:
                if ' ' + self.left_panel_video_bible_selected_chapter + ':' in key:
                    verse = int(key.split(':')[-1])
                    if not verse in list_of_verses:
                        list_of_verses.append(verse)

        for verse in sorted(list_of_verses):
            label = QLabel(str(verse))
            label.setGeometry(0,0,48,48)
            label.setStyleSheet('QLabel {background-color:silver; font-family: "Ubuntu Condensed"; font-size:28px; color:white; qproperty-alignment:"AlignCenter"; }')
            item = QListWidgetItem(QIcon(QPixmap().grabWidget(label)), None)
            item.setToolTip(str(verse))
            self.left_panel_video_bible_list.addItem(item)

        #self.left_panel_video_bible_list_clear_selection()

    def left_panel_video_bible_list_clicked(self):
        self.left_panel_video_library_search.setVisible(False)
        self.left_panel_add_title.setText(u'<font style="font-size:14px;"><b>' + u'VÍDEO' + '</b></font>')
        self.left_panel_addbutton_label.setText(u'<font style="font-size:9px;">' + u'SELECIONE ALGO PARA' + '</font><br><font style="font-size:14px;"><b>' + u'ADICIONAR' + '</b></font>')
        self.left_panel_addbutton.setEnabled(True)

        self.left_panel_video_bible_search.setVisible(False)
        if self.left_panel_video_bible_list.currentItem():
            if not self.left_panel_video_bible_selected_book:
                self.left_panel_video_bible_selected_book = self.left_panel_video_bible_list.currentItem().toolTip()
                self.populate_video_bible_chapters_list()
            elif not self.left_panel_video_bible_selected_chapter:
                self.left_panel_video_bible_selected_chapter = self.left_panel_video_bible_list.currentItem().toolTip()
                self.populate_video_bible_verses_list()
            else:#if not self.left_panel_video_bible_selected_verse:
                self.left_panel_video_bible_selected_verse = self.left_panel_video_bible_list.currentItem().toolTip()
                self.left_panel_video_bible_verse_label.setVisible(True)
                self.left_panel_video_bible_verse_label.setText(self.left_panel_video_bible_selected_verse)

                self.left_panel_add_title.setText(u'<font style="font-size:14px;"><b>' + u'VÍDEO' + '</b></font><br><font style="font-size:9px;">' + self.left_panel_video_bible_selected_book + u' ' + self.left_panel_video_bible_selected_chapter + u':' + self.left_panel_video_bible_selected_verse + '</font>')
                self.left_panel_addbutton_label.setText(u'<font style="font-size:14px;"><b>' + u'ADICIONAR' + '</b></font><br><font style="font-size:9px;">' + u'NA LISTA DE EXECUÇÃO' + '</font>')
                self.left_panel_addbutton.setEnabled(True)

        self.left_panel_video_bible_list_clear_selection()

    def left_panel_video_bible_list_clear_selection(self):
        #self.left_panel_video_bible_list.setCurrentRow(-1)
        self.left_panel_video_bible_list.clearSelection()

    def populate_video_library_list(self):
        self.left_panel_video_library_alert.setVisible(False)
        self.left_panel_video_library_list.setVisible(False)

        path_for_library_videos = path_home
        if os.path.isfile(os.path.join(path_home, '.config', 'user-dirs.dirs')):
            filecontent = open(os.path.join(os.path.join(path_home, '.config', 'user-dirs.dirs'))).read()
            if 'XDG_VIDEOS_DIR=' in filecontent:
                if os.path.isdir(os.path.join(filecontent.split('XDG_VIDEOS_DIR=',1)[1].split('"')[1].replace('$HOME', path_home))):
                    path_for_library_videos = os.path.join(filecontent.split('XDG_VIDEOS_DIR=',1)[1].split('"')[1].replace('$HOME', path_home))

        #self.list_of_library_videos = {}
        list_of_library_videos_filenames = []

        for album in self.list_of_library_videos.keys():
            for filename in self.list_of_library_videos[album].keys():
                list_of_library_videos_filenames.append(filename)


        for root, dirs, files in os.walk(path_for_library_videos):
            for name in files:
                if name.endswith('.m4v'):
                    if not unicode(os.path.join(root, name), 'utf-8') in list_of_library_videos_filenames:
                        video_chapters_info = unicode(subprocess.Popen([ffprobe_bin,'-loglevel', 'error', '-show_format', '-show_chapters', '-print_format', 'xml', os.path.join(root, name)], startupinfo=startupinfo, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE).stdout.read(), 'utf-8')

                        title = False
                        album = False

                        try:
                            video = MP4(os.path.join(root, name))
                            if '\xa9nam' in  video.tags.keys():
                                title = video.tags['\xa9nam'][0]
                            if '\xa9alb' in  video.tags.keys():
                                album = video.tags['\xa9alb'][0]
                            if 'covr' in  video.tags.keys():
                                artwork = video.tags["covr"][0]
                                open(os.path.join(path_tmp, 'thumbnail.png'), 'wb').write(artwork)
                        except:
                            if 'key="title"' in video_chapters_info:
                                title = video_chapters_info.split('key="title"')[1].split('value="')[1].split('"')[0]
                            if 'key="album"' in video_chapters_info:
                                album = video_chapters_info.split('key="album"')[1].split('value="')[1].split('"')[0]
                            if 'nb_streams="' in video_chapters_info:
                                stream = '0:' + str(int(video_chapters_info.split('nb_streams="')[1].split('"')[0])-1)
                                subprocess.call([ffmpeg_bin, '-loglevel', 'error', '-y', '-i', os.path.join(root, name), '-map', stream, '-c:v', 'copy', os.path.join(path_tmp, 'thumbnail.png')])

                        thumbnail = False
                        if os.path.isfile(os.path.join(path_tmp, 'thumbnail.png')):
                            thumbnail = QPixmap(os.path.join(path_tmp, 'thumbnail.png'))

                        if album and not album in self.list_of_library_videos.keys():
                            self.list_of_library_videos[album] = {}

                        chapters = {}
                        for chapter in video_chapters_info.split('<chapter '):

                            chapter_name = False
                            start = False
                            end = False

                            if 'key="title"' in chapter and 'value="' in chapter.split('key="title"')[1]:
                                chapter_name = chapter.split('key="title"')[1].split('value="')[1].split('"')[0]
                            if 'start_time="' in chapter:
                                start = float(chapter.split('start_time="')[1].split('"')[0])
                            if 'end_time="' in chapter:
                                end = float(chapter.split('end_time="')[1].split('"')[0])

                            if chapter_name and start and end:
                                chapters[chapter_name] = [start, end]

                        if title and thumbnail:
                            self.list_of_library_videos[album][unicode(os.path.join(root, name), 'utf-8')] = [title,chapters,thumbnail]

        self.populate_video_library_albums_list()

        if self.left_panel_video_library_list.count() > 0:
            self.left_panel_video_library_list.setVisible(True)
        else:
            self.left_panel_video_library_alert.setVisible(True)

        self.left_panel_video_library_list_clear_selection()

    def populate_video_library_albums_list(self):
        self.left_panel_video_library_list.clear()
        self.left_panel_video_library_list.setIconSize(QSize(96, 96))

        self.left_panel_video_library_selected_album = False
        self.left_panel_video_library_selected_title = False
        self.left_panel_video_library_selected_chapter = False
        self.left_panel_video_library_search.setVisible(True)
        self.left_panel_video_library_album_label.setVisible(False)
        self.left_panel_video_library_title_label.setVisible(False)
        self.left_panel_video_library_chapter_label.setVisible(False)

        for album in sorted(self.list_of_library_videos.keys()):
            list_filenames = []
            for filename in self.list_of_library_videos[album].keys():
                list_filenames.append([filename, self.list_of_library_videos[album][filename][-1]])
            label = QLabel()
            label.setScaledContents(True)
            label.setPixmap(sorted(list_filenames)[0][1])
            label.setGeometry(0,0,96,96)
            #label.setStyleSheet('QLabel {background-color:gray; font-family: "Ubuntu Condensed"; font-size:28px; color:white;  qproperty-alignment:"AlignCenter";}')
            item = QListWidgetItem(QIcon(QPixmap().grabWidget(label)), album)
            item.setToolTip(album)
            if self.left_panel_video_library_search.text().lower() == '' or self.left_panel_video_library_search.text().lower() in album.lower():
                self.left_panel_video_library_list.addItem(item)

        #self.left_panel_video_library_list_clear_selection()

    def populate_video_library_titles_list(self):
        self.left_panel_video_library_list.clear()
        self.left_panel_video_library_list.setIconSize(QSize(64, 64))

        self.left_panel_video_library_selected_title = False
        self.left_panel_video_library_selected_chapter = False

        self.left_panel_video_library_album_label.setVisible(True)
        self.left_panel_video_library_album_label.setPixmap(self.list_of_library_videos[self.left_panel_video_library_selected_album][sorted(self.list_of_library_videos[self.left_panel_video_library_selected_album].keys())[0]][-1])
        self.left_panel_video_library_title_label.setVisible(False)
        self.left_panel_video_library_chapter_label.setVisible(False)

        for title in sorted(self.list_of_library_videos[self.left_panel_video_library_selected_album].keys()):
            label = QLabel()
            label.setScaledContents(True)
            label.setPixmap(self.list_of_library_videos[self.left_panel_video_library_selected_album][title][-1])
            label.setGeometry(0,0,64,64)
            item = QListWidgetItem(QIcon(QPixmap().grabWidget(label)), self.list_of_library_videos[self.left_panel_video_library_selected_album][title][0])
            item.setToolTip(title)
            self.left_panel_video_library_list.addItem(item)

    def populate_video_library_chapters_list(self):
        self.left_panel_video_library_list.clear()
        self.left_panel_video_library_list.setIconSize(QSize(96, 48))

        self.left_panel_video_library_selected_chapter = False

        self.left_panel_video_library_title_label.setVisible(True)

        self.left_panel_video_library_title_label.setPixmap(self.list_of_library_videos[self.left_panel_video_library_selected_album][self.left_panel_video_library_selected_title][-1])
        self.left_panel_video_library_chapter_label.setVisible(False)

        label = QLabel(u'▬')
        label.setGeometry(0,0,48,48)
        label.setStyleSheet('QLabel {background-color:silver; font-family: "Ubuntu Condensed"; font-size:28px; color:white; qproperty-alignment:"AlignCenter"; }')
        first_item = QListWidgetItem(QIcon(QPixmap().grabWidget(label)), u'Todo o vídeo')
        first_item.setToolTip(u'all')
        self.left_panel_video_library_list.addItem(first_item)

        for value in sorted(self.list_of_library_videos[self.left_panel_video_library_selected_album][self.left_panel_video_library_selected_title][1].values()):
            for chapter in self.list_of_library_videos[self.left_panel_video_library_selected_album][self.left_panel_video_library_selected_title][1].keys():
                if value == self.list_of_library_videos[self.left_panel_video_library_selected_album][self.left_panel_video_library_selected_title][1][chapter]:
                    label = QLabel(u'🠷')
                    label.setGeometry(0,0,48,48)
                    label.setStyleSheet('QLabel {background-color:silver; font-family: "Ubuntu Condensed"; font-size:28px; color:white; qproperty-alignment:"AlignCenter"; }')
                    item = QListWidgetItem(QIcon(QPixmap().grabWidget(label)), chapter)
                    item.setToolTip(chapter)
                    self.left_panel_video_library_list.addItem(item)
                    break

    def left_panel_video_library_list_clicked(self):
        self.left_panel_video_library_search.setVisible(False)
        self.left_panel_add_title.setText(u'<font style="font-size:14px;"><b>' + u'VÍDEO' + '</b></font>')
        self.left_panel_addbutton_label.setText(u'<font style="font-size:9px;">' + u'SELECIONE ALGO PARA' + '</font><br><font style="font-size:14px;"><b>' + u'ADICIONAR' + '</b></font>')
        self.left_panel_addbutton.setEnabled(True)

        if self.left_panel_video_library_list.currentItem():
            if not self.left_panel_video_library_selected_album:
                self.left_panel_video_library_selected_album = self.left_panel_video_library_list.currentItem().toolTip()
                self.populate_video_library_titles_list()
            elif not self.left_panel_video_library_selected_title:
                self.left_panel_video_library_selected_title = self.left_panel_video_library_list.currentItem().toolTip()
                self.populate_video_library_chapters_list()
            else:#if not self.left_panel_video_library_selected_chapter:
                self.left_panel_video_library_selected_chapter = self.left_panel_video_library_list.currentItem().toolTip()
                self.left_panel_video_library_chapter_label.setVisible(True)
                self.left_panel_video_library_chapter_label.setText(self.left_panel_video_library_selected_chapter)

                self.left_panel_add_title.setText(u'<font style="font-size:14px;"><b>' + u'VÍDEO' + '</b></font><br><font style="font-size:9px;">' + self.left_panel_video_library_selected_title + '</font>')
                self.left_panel_addbutton_label.setText(u'<font style="font-size:14px;"><b>' + u'ADICIONAR' + '</b></font><br><font style="font-size:9px;">' + u'NA LISTA DE EXECUÇÃO' + '</font>')
                self.left_panel_addbutton.setEnabled(True)

        self.left_panel_video_library_list_clear_selection()

    def left_panel_video_library_list_clear_selection(self):
        #self.left_panel_video_library_list.setCurrentRow(-1)
        self.left_panel_video_library_list.clearSelection()

    def left_panel_video_local_list_clear_selection(self):
        self.left_panel_video_local_list.setCurrentRow(-1)
        self.left_panel_video_remove.setEnabled(False)

    def populate_list_of_videos_from_folder(self):
        self.list_of_videos_from_folder = {}
        for filename in os.listdir(self.path_for_videos):
            if os.path.isfile(os.path.join(self.path_for_videos, filename)) and  filename.split('.')[-1] in ['m4v', 'mp4', 'M4V', 'MP4']:
                md5 = hashlib.md5()
                md5.update(open(os.path.join(self.path_for_videos, filename), 'rb').read())
                filename_hash = md5.hexdigest()
                title = False
                if not os.path.isfile(os.path.join(path_tmp , filename_hash + '.png')):
                    video = MP4(os.path.join(self.path_for_videos, filename))


                    if not video.tags == None and 'covr' in  video.tags.keys():
                        artwork = video.tags["covr"][0]
                        open(os.path.join(path_tmp, filename + '.png'), 'wb').write(artwork)
                    else:
                        subprocess.call([ffmpeg_bin, '-loglevel', 'error', '-y', '-ss', '00:10.00', '-i', os.path.join(self.path_for_videos, filename), '-frames:v', '1', os.path.join(path_tmp, filename + '.png')], startupinfo=startupinfo)

                    create_thumbnail(os.path.join(path_tmp, filename + '.png'), os.path.join(path_tmp , filename_hash + '.png'), int(56*self.screen_height_proportion), 56)

                    if not video.tags == None and '\xa9nam' in  video.tags.keys():
                        title = video.tags['\xa9nam'][0]

                icon = os.path.join(path_tmp , filename_hash + '.png')
                #label.setStyleSheet('QLabel { padding-left:5px; } ')
                self.list_of_videos_from_folder[filename_hash] = [icon, os.path.join(self.path_for_videos, filename), title]

    def left_panel_video_edit_clicked(self):
        newpath = QFileDialog.getExistingDirectory(self, "Selecione uma pasta de images", path_home, options=self.dialog_options)
        if not newpath == '':
            self.path_for_videos = newpath
            self.populate_list_of_videos_from_folder()
            self.populate_video_local_list()

    def left_panel_video_add_clicked(self):
        image_path_list = QFileDialog.getOpenFileNames(self, "Selecione os arquivos de vídeo", path_home, "Arquivos de vídeo (*.m4v *.mp4 *.M4V *.MP4)", options=self.dialog_options)[0]
        for filename in image_path_list:
            if os.path.isfile(filename):
                icon = False
                filename_hash = False
                title = False
                if filename.split('.')[-1] in ['m4v', 'mp4', 'M4V', 'MP4']:
                    md5 = hashlib.md5()
                    md5.update(open(filename, 'rb').read())
                    filename_hash = md5.hexdigest()
                    if not os.path.isfile(os.path.join(path_tmp , filename_hash + '.png')):
                        video = MP4(filename)

                        if 'covr' in  video.tags.keys():
                            artwork = video.tags["covr"][0]
                            open(os.path.join(path_tmp, filename.split('/')[-1] + '.png'), 'wb').write(artwork)
                        else:
                            subprocess.call([ffmpeg_bin, '-loglevel', 'error', '-y', '-ss', '00:10.00', '-i', filename, '-frames:v', '1', os.path.join(path_tmp, filename.split('/')[-1] + '.png')], startupinfo=startupinfo)

                        create_thumbnail(os.path.join(path_tmp, filename.split('/')[-1] + '.png'), os.path.join(path_tmp , filename_hash + '.png'),int(56*self.screen_height_proportion), 56)

                        if '\xa9nam' in  video.tags.keys():
                            title = video.tags['\xa9nam'][0]

                    icon = os.path.join(path_tmp , filename_hash + '.png')

                if icon and filename_hash:
                    self.list_of_videos_added[filename_hash] = [icon, filename, title]

        self.populate_video_local_list()

    def left_panel_video_local_list_clicked(self):
        if self.left_panel_video_local_list.currentItem():
            self.left_panel_add_title.setText(u'<font style="font-size:14px;"><b>' + u'VÍDEO' + '</b></font><br><font style="font-size:9px;">' + self.list_of_local_videos[self.left_panel_video_local_list.currentItem().toolTip()][1] + '</font>')
            self.left_panel_addbutton_label.setText(u'<font style="font-size:14px;"><b>' + u'ADICIONAR' + '</b></font><br><font style="font-size:9px;">' + u'NA LISTA DE EXECUÇÃO' + '</font>')
            self.left_panel_addbutton.setEnabled(True)
            self.left_panel_video_remove.setEnabled(True)




    ############################################################################
    ## ADDITION PANEL - WEB

    def left_panel_web_address_go_clicked(self):
        self.left_panel_web_webview.load(QUrl(self.left_panel_web_address.text()))
        self.left_panel_web_webview.show()
        self.left_panel_add_title.setText(u'<font style="font-size:14px;"><b>' + u'PÁGINA DO JW.ORG' + '</b></font><br><font style="font-size:9px;">' + self.left_panel_web_address.text() + '</font>')
        self.left_panel_addbutton_label.setText(u'<font style="font-size:14px;"><b>' + u'ADICIONAR' + '</b></font><br><font style="font-size:9px;">' + u'NA LISTA DE EXECUÇÃO' + '</font>')
        self.left_panel_addbutton.setEnabled(True)

        #self.left_panel_web_webview.setZoomFactor(zoom)


    ############################################################################
    ## ADDITION PANEL - SCREENCOPY

    def left_panel_screencopy_take_button_clicked(self):
        app.screencopy.show()

    ############################################################################
    ## ADDITION PANEL - CAMERA

    def populate_camera_list(self):
        self.list_of_cameras = {}
        cameras_output = subprocess.Popen(['v4l2-ctl', '--list-devices'], startupinfo=startupinfo, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.STDOUT).stdout.read()

        for camera in cameras_output.replace(':\n\t', ':').split('\n'):
            if '):/' in camera:
                device = camera.split('):',1)[1].split('\n',1)[0]
                if len(self.playlist) > 0:
                    for item in self.playlist:
                        if item[0] == 'camera' and item[2] == device:
                            device = False
                            break
                if device:
                    name = camera.split('(',1)[0]
                    self.list_of_cameras[device] = [name]

        self.left_panel_camera_select.clear()

        for camera in self.list_of_cameras.keys():
            self.left_panel_camera_select.addItem(self.list_of_cameras[camera][0])

    def left_panel_camera_select_selected(self):
        self.left_panel_camera_test_button.setEnabled(True)

    def left_panel_camera_test_button_clicked(self):
        selected_camera = False

        for camera in self.list_of_cameras.keys():
            if self.left_panel_camera_select.currentText() == self.list_of_cameras[camera][0]:
                selected_camera = camera
                break

        if selected_camera:
            self.left_panel_camera_preview_mediaplayer.stop()
            self.left_panel_camera_preview_mediaplayer.set_media(video_instance.media_new('v4l2://' + selected_camera))
            self.left_panel_camera_preview_mediaplayer.play()

            self.left_panel_add_title.setText(u'<font style="font-size:14px;"><b>' + u'CAMERA' + '</b></font><br><font style="font-size:9px;">' + self.left_panel_camera_select.currentText() + '</font>')
            self.left_panel_addbutton_label.setText(u'<font style="font-size:14px;"><b>' + u'ADICIONAR' + '</b></font><br><font style="font-size:9px;">' + u'NA LISTA DE EXECUÇÃO' + '</font>')
            self.left_panel_addbutton.setEnabled(True)

    ############################################################################
    ## PLAYLIST

    def populate_playlist(self):

        for item in range(self.playlist_panel_listgrid.count()):
            child = self.playlist_panel_listgrid.takeAt(0)
            child.widget().deleteLater()

        if len(self.playlist) > 0:
            self.playlist_nextarrow.setVisible(True)
            self.right_panel_shownext_box.setStyleSheet('#right_panel_shownext_box { border-top: 75px; border-right: 25px; border-left: 25px; border-image: url("' + os.path.join(path_graphics, "shownext_box_background.png").replace('\\', '/') + '") 75 25 0 25 stretch stretch; }')
            self.right_panel_shownext_button.setText(u'MOSTRAR AGORA')

            active_list = list(reversed(self.playlist))
            first_item = active_list.pop()

            height = -4

            if first_item[0] == 'audio':
                item_description = u'<font style="font-size:14px;color:white;">' + first_item[3] + '</font><br><font style="font-size:10px;color:silver;">'
                if first_item[6]:
                    item_description += u'COM LETRA NA TELA'
                else:
                    item_description += u'SEM LETRA NA TELA'
                item_description += '</font>'
            elif first_item[0] in ['image']:
                item_description = u'<font style="font-size:14px;color:white;">' + first_item[2].split('/')[-1].rsplit('.', 1)[0] + '</font><br><font style="font-size:10px;color:silver;">' + first_item[2] + '</font>'
            elif first_item[0] in ['video']:
                item_description = u'<font style="font-size:14px;color:white;">' + first_item[4] + '</font><br><font style="font-size:10px;color:silver;">' + first_item[2] + '</font>'
            elif first_item[0] in ['web']:
                item_description = u'<font style="font-size:14px;color:white;">' + first_item[2] + '</font><br><font style="font-size:10px;color:silver;">' + first_item[2] + '</font>'
            elif first_item[0] in ['clock']:
                item_description = u'<font style="font-size:14px;color:white;">' + u'RELÓGIO' + '</font>'
            elif first_item[0] in ['screencopy']:
                item_description = u'<font style="font-size:14px;color:white;">' + u'CÓPIA DE TELA' + '</font>'
            elif first_item[0] in ['camera']:
                item_description = u'<font style="font-size:14px;color:white;">' + u'CAMERA' + '</font>'
                first_item[2].stop()
                first_item[2].set_xwindow(self.playlist_nextarrow_preview.winId())
                first_item[2].play()

            self.playlist_nextarrow_preview.setPixmap(first_item[1])
            self.playlist_nextarrow_description.setText(item_description)

            for item in active_list:

                if self.highlighted_media == self.playlist.index(item):
                    subcolor = 'silver'
                else:
                    subcolor = 'gray'

                if item[0] == 'audio':

                    item_description = u'<font style="font-size:14px;color:black;">' + item[3] + '</font><br><font style="font-size:10px;color:' + subcolor + ';">'
                    if item[6]:
                        item_description += u'COM LETRA NA TELA'
                    else:
                        item_description += u'SEM LETRA NA TELA'
                    item_description += '</font>'
                elif item[0] in ['image']:
                    item_description = u'<font style="font-size:14px;color:black;">' + item[2].split('/')[-1].rsplit('.', 1)[0] + '</font><br><font style="font-size:10px;color:' + subcolor + ';">' + item[2] + '</font>'
                elif item[0] in ['video']:
                    item_description = u'<font style="font-size:14px;color:black;">' + item[4] + '</font><br><font style="font-size:10px;color:' + subcolor + ';">' + item[2] + '</font>'
                elif item[0] in ['web']:
                    item_description = u'<font style="font-size:14px;color:black;">' + item[2] + '</font><br><font style="font-size:10px;color:' + subcolor + ';">' + item[2] + '</font>'
                elif item[0] in ['clock']:
                    item_description = u'<font style="font-size:14px;color:black;">' + u'RELÓGIO' + '</font>'
                elif item[0] in ['screencopy']:
                    item_description = u'<font style="font-size:14px;color:black;">' + u'CÓPIA DE TELA' + '</font>'
                elif item[0] in ['camera']:
                    item_description = u'<font style="font-size:14px;color:black;">' + u'CAMERA' + '</font>'

                class item_widget(QLabel):
                    #def mouseReleaseEvent(widget, event):
                    #    self.right_panel_shownext_box.setStyleSheet('#right_panel_shownext_box { border-top: 75px; border-right: 25px; border-left: 25px; border-image: url("' + os.path.join(path_graphics, "shownext_box_background_hover.png").replace('\\', '/') + '") 75 25 0 25 stretch stretch; }')
                    #def mousePressEvent(widget, event):

                    #    #self.highlight_media(lambda : self.playlist.index(item) )#, widget)
                    #    self.highlight_media(self.playlist.index(item), widget)
                    #    #    self.right_panel_shownext_box.setStyleSheet('#right_panel_shownext_box { border-top: 75px; border-right: 25px; border-left: 25px; border-image: url("' + os.path.join(path_graphics, "shownext_box_background_pressed.png").replace('\\', '/') + '") 75 25 0 25 stretch stretch; }')
                    def enterEvent(widget, event):
                        widget.item_up_button.setVisible(True)
                        widget.item_delete_button.setVisible(True)
                        widget.item_down_button.setVisible(True)
                        widget.item_to_next_button.setVisible(True)
                        widget.item_to_back_button.setVisible(True)
                    def leaveEvent(widget, event):
                        widget.item_up_button.setVisible(False)
                        widget.item_delete_button.setVisible(False)
                        widget.item_down_button.setVisible(False)
                        widget.item_to_next_button.setVisible(False)
                        widget.item_to_back_button.setVisible(False)

                item_widget = item_widget()
                #item_widget_index = self.playlist.index(item)
                item_widget.setText(item_description)
                item_widget.setObjectName('item_widget')
                if self.highlighted_media == self.playlist.index(item):
                    item_widget.setStyleSheet('#item_widget { padding-left:' + str((50*self.screen_height_proportion)+5) + 'px; border-top: 3px; border-right: 3px; border-bottom: 3px; border-left: 3px; border-image: url("' + os.path.join(path_graphics, "item_background_highlighted.png").replace('\\', '/') + '") 3 3 3 3 stretch stretch; }')
                else:
                    item_widget.setStyleSheet('#item_widget { padding-left:' + str((50*self.screen_height_proportion)+5) + 'px; border-top: 3px; border-right: 3px; border-bottom: 3px; border-left: 3px; border-image: url("' + os.path.join(path_graphics, "item_background.png").replace('\\', '/') + '") 3 3 3 3 stretch stretch; }')
                item_widget.mousePressEvent = lambda event, index = self.playlist.index(item) : self.highlight_media(index)
                #item_widget.setToolTip(str())

                item_widget.item_preview = QLabel(parent=item_widget)
                item_widget.item_preview.setScaledContents(True)
                item_widget.item_preview.setGeometry(3,3,50*self.screen_height_proportion,50)
                item_widget.item_preview.setPixmap(item[1])

                item_widget.item_up_button = QPushButton(parent=item_widget)
                item_widget.item_up_button.setStyleSheet('QPushButton { image: url(' + os.path.join(path_graphics, 'up_icon.png').replace('\\', '/') + '); border-top: 5px; border-right: 5px; border-bottom: 0px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_normal.png").replace('\\', '/') + '") 5 5 16 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 5px; border-bottom: 0px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 16 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 0px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_hover.png").replace('\\', '/') + '") 5 5 16 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 5 5 16 5 stretch stretch; outline: none; }')
                item_widget.item_up_button.setGeometry(self.playlist_panel_list.width()-60,6,30,15)
                item_widget.item_up_button.clicked.connect(lambda index = self.playlist.index(item) : self.playlist_item_up(index))
                item_widget.item_up_button.setVisible(False)

                item_widget.item_delete_button = QPushButton(parent=item_widget)
                item_widget.item_delete_button.setStyleSheet('QPushButton { image: url(' + os.path.join(path_graphics, 'delete_icon.png').replace('\\', '/') + '); border-top: 0px; border-right: 5px; border-bottom: 0px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_normal.png").replace('\\', '/') + '") 14 5 14 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 0px; border-right: 5px; border-bottom: 0px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 14 5 14 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 0px; border-right: 5px; border-bottom: 0px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_hover.png").replace('\\', '/') + '") 14 5 14 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 14 5 14 5 stretch stretch; outline: none; }')
                item_widget.item_delete_button.setGeometry(self.playlist_panel_list.width()-60,21,30,14)
                item_widget.item_delete_button.clicked.connect(lambda index = self.playlist.index(item) : self.playlist_item_delete(index))
                item_widget.item_delete_button.setVisible(False)

                item_widget.item_down_button = QPushButton(parent=item_widget)
                item_widget.item_down_button.setStyleSheet('QPushButton { image: url(' + os.path.join(path_graphics, 'down_icon.png').replace('\\', '/') + '); border-top: 0px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_normal.png").replace('\\', '/') + '") 16 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 0px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 16 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 0px; border-right: 5px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_hover.png").replace('\\', '/') + '") 16 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(path_graphics, "button_pressed.png").replace('\\', '/') + '") 16 5 5 5 stretch stretch; outline: none; }')
                item_widget.item_down_button.setGeometry(self.playlist_panel_list.width()-60,35,30,15)
                item_widget.item_down_button.clicked.connect(lambda index = self.playlist.index(item) : self.playlist_item_down(index))
                item_widget.item_down_button.setVisible(False)

                item_widget.item_to_next_button = QPushButton(parent=item_widget)
                item_widget.item_to_next_button.setStyleSheet('QPushButton { image: url(' + os.path.join(path_graphics, 'to_next_icon.png').replace('\\', '/') + '); border-top: 5px; border-right: 0px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_green_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 0px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_green_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 0px; border-bottom: 5px; border-left: 5px; border-image: url("' + os.path.join(path_graphics, "button_green_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(path_graphics, "button_green_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
                item_widget.item_to_next_button.setGeometry(((50*self.screen_height_proportion)*.5)-30+3,13,30,30)
                item_widget.item_to_next_button.clicked.connect(lambda index = self.playlist.index(item) : self.playlist_item_to_next(index))
                item_widget.item_to_next_button.setVisible(False)

                item_widget.item_to_back_button = QPushButton(parent=item_widget)
                item_widget.item_to_back_button.setStyleSheet('QPushButton { image: url(' + os.path.join(path_graphics, 'to_back_icon.png').replace('\\', '/') + '); border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 0px; border-image: url("' + os.path.join(path_graphics, "button_green_normal.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover:pressed { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 0px; border-image: url("' + os.path.join(path_graphics, "button_green_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:hover { border-top: 5px; border-right: 5px; border-bottom: 5px; border-left: 0px; border-image: url("' + os.path.join(path_graphics, "button_green_hover.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:pressed { border-image: url("' + os.path.join(path_graphics, "button_green_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; } QPushButton:checked { border-image: url("' + os.path.join(path_graphics, "button_green_pressed.png").replace('\\', '/') + '") 5 5 5 5 stretch stretch; outline: none; }')
                item_widget.item_to_back_button.setCheckable(True)
                item_widget.item_to_back_button.setGeometry(((50*self.screen_height_proportion)*.5)+3,13,30,30)
                item_widget.item_to_back_button.clicked.connect(lambda index = self.playlist.index(item) : self.playlist_item_to_back(index, item_widget.item_to_back_button.isChecked()))
                item_widget.item_to_back_button.setVisible(False)

                self.playlist_panel_listgrid.addWidget(item_widget)
                #height += 4

                if item[0] in ['camera']:
                    item[2].stop()
                    item[2].set_xwindow(item_widget.item_preview.winId())
                    item[2].play()

                height += 60

            self.playlist_panel_list.setGeometry(0,0,self.playlist_panel.width(),height)

            if self.playlist_panel_scroll.height() > self.playlist_panel_list.height():
                self.playlist_panel_scroll.setViewportMargins(0,self.playlist_panel_scroll.height()-self.playlist_panel_list.height()-4,0,0)
            else:
                self.playlist_panel_scroll.setViewportMargins(0,0,0,0)


        else:
            self.playlist_nextarrow.setVisible(False)
            self.right_panel_shownext_box.setStyleSheet('#right_panel_shownext_box { border-top: 75px; border-right: 25px; border-left: 25px; border-image: url("' + os.path.join(path_graphics, "shownext_box_background_disabled.png").replace('\\', '/') + '") 75 25 0 25 stretch stretch; }')
            self.right_panel_shownext_button.setText(u'<font style="color:silver;">NADA PARA MOSTRAR</font>')

        if self.last_item:
            self.playlist_lastitem.setVisible(True)
        else:
            self.playlist_lastitem.setVisible(False)

        if len(self.playlist) > 0:
            self.playlist_options_panel_update()
        self.update_preview()

    def playlist_item_up(self, index):
        old = self.playlist[index + 1]
        self.playlist[index + 1] = self.playlist[index]
        self.playlist[index] = old
        self.populate_playlist()

    def playlist_item_delete(self, index):
        del self.playlist[index]
        self.populate_playlist()

    def playlist_item_down(self, index):
        old = self.playlist[index - 1]
        self.playlist[index - 1] = self.playlist[index]
        self.playlist[index] = old
        self.populate_playlist()

    def playlist_item_to_next(self, index):
        self.playlist.insert(0, self.playlist.pop(index))
        self.populate_playlist()

    def playlist_item_to_back(self, index, state):
        self.playlist[index][-1] = state
        #self.playlist_item_to_next(index)
        #self.show_now_clicked()
        #self.populate_playlist()

    def highlight_media(self, index):
        self.highlighted_media = int(index)
        self.populate_playlist()

    def playlist_nextarrow_clicked(self):
        self.highlighted_media = 0
        self.playlist_options_panel_update()
        self.populate_playlist()

    def playlist_options_panel_open_button_clicked(self):
        if self.playlist_options_is_visible:
            generate_effect(self.playlist_options_panel_geometry_animation, 'geometry', 500, [self.playlist_options_panel.x(),self.playlist_options_panel.y(),self.playlist_options_panel.width(),self.playlist_options_panel.height()], [60,self.playlist_options_panel.y(),(self.width()*.5)-30,self.playlist_options_panel.height()])
            generate_effect(self.playlist_options_panel_open_button_geometry_animation, 'geometry', 500, [self.playlist_options_panel_open_button.x(),self.playlist_options_panel_open_button.y(),self.playlist_options_panel_open_button.width(),self.playlist_options_panel_open_button.height()], [60,self.playlist_options_panel_open_button.y(),self.playlist_options_panel_open_button.width(),self.playlist_options_panel_open_button.height()])
            self.playlist_options_panel_update()
        else:
            generate_effect(self.playlist_options_panel_geometry_animation, 'geometry', 500, [self.playlist_options_panel.x(),self.playlist_options_panel.y(),self.playlist_options_panel.width(),self.playlist_options_panel.height()], [(self.width()*.5)-20,0,(self.width()*.25)+20,self.height()])
            generate_effect(self.playlist_options_panel_open_button_geometry_animation, 'geometry', 500, [self.playlist_options_panel_open_button.x(),self.playlist_options_panel_open_button.y(),self.playlist_options_panel_open_button.width(),self.playlist_options_panel_open_button.height()], [(self.width()*.5)-20,self.playlist_options_panel_open_button.y(),self.playlist_options_panel_open_button.width(),self.playlist_options_panel_open_button.height()])

    def playlist_options_panel_update(self):
        if self.highlighted_media + 1 >  len(self.playlist):
            self.highlighted_media = 0

        if not self.playlist[self.highlighted_media][0] == 'audio':
            self.playlist_options_panel_audio_panel.setVisible(False)

        if not self.playlist[self.highlighted_media][0] == 'image':
            self.playlist_options_panel_image_panel.setVisible(False)

        if not self.playlist[self.highlighted_media][0] == 'video':
            self.playlist_options_panel_video_panel.setVisible(False)

        if not self.playlist[self.highlighted_media][0] == 'web':
            self.playlist_options_panel_web_panel.setVisible(False)

        if not self.playlist[self.highlighted_media][0] == 'clock':
            self.playlist_options_panel_clock_panel.setVisible(False)

        if not self.playlist[self.highlighted_media][0] == 'screencopy':
            self.playlist_options_panel_screencopy_panel.setVisible(False)

        if not self.playlist[self.highlighted_media][0] == 'camera':
            self.playlist_options_panel_camera_panel.setVisible(False)

        if self.playlist[self.highlighted_media][0] == 'audio':
            self.playlist_options_panel_audio_panel.setVisible(True)
            self.playlist_options_panel_audio_panel_show_lyrics.setChecked(self.playlist[self.highlighted_media][6])

        if self.playlist[self.highlighted_media][0] == 'image':
            self.playlist_options_panel_image_panel.setVisible(True)

        if self.playlist[self.highlighted_media][0] == 'video':
            self.playlist_options_panel_video_panel.setVisible(True)

        if self.playlist[self.highlighted_media][0] == 'web':
            self.playlist_options_panel_web_panel.setVisible(True)

        if self.playlist[self.highlighted_media][0] == 'clock':
            self.playlist_options_panel_clock_panel.setVisible(True)

        if self.playlist[self.highlighted_media][0] == 'screencopy':
            self.playlist_options_panel_screencopy_panel.setVisible(True)

        if self.playlist[self.highlighted_media][0] == 'camera':
            self.playlist_options_panel_camera_panel.setVisible(True)

    def playlist_options_panel_audio_panel_show_lyrics_clicked(self):
        self.playlist[self.highlighted_media][6] = self.playlist_options_panel_audio_panel_show_lyrics.isChecked()
        self.populate_playlist()

    ############################################################################
    ## PRELUDE player

    def right_panel_monitor_prelude_volume_changing(self):
        if not self.right_panel_monitor_prelude_pausebutton.isChecked():
            app.monitor.video_mediaplayer.audio_set_volume(self.right_panel_monitor_prelude_volume.value())
        self.right_panel_monitor_prelude_volume_label.setText('<font style="font-size:14px;"><b>' + str(self.right_panel_monitor_prelude_volume.value()) + '</b></font><br><font style="font-size:7px;">' + u'VOL' + '</font>')

    def right_panel_monitor_prelude_pausebutton_clicked(self, stop=False):
        if app.monitor.current_media[4]:
            app.monitor.current_media[4] = False
        self.right_panel_monitor_prelude_crossfade()

    def right_panel_monitor_prelude_crossfade(self,stop=False):
        if app.monitor.video_mediaplayer.audio_get_volume() > 0 or stop:
            fadeout = True

        else:
            fadeout = False

        self.right_panel_monitor_prelude.time_to_full_volume = [datetime.datetime.now(), 3.0, fadeout, stop] #is for fade out, should stop
        self.right_panel_monitor_prelude_crossfade_opacity(fadeout)

    def right_panel_monitor_prelude_crossfade_opacity(self, fadeout=False):
        if fadeout:
            generate_effect(app.monitor.clock_widget_title_label_opacity_animation, 'opacity', 3000, app.monitor.clock_widget_title_label_opacity.opacity(), 0.0)
        elif not app.monitor.current_media[4]:
            generate_effect(app.monitor.clock_widget_title_label_opacity_animation, 'opacity', 3000, app.monitor.clock_widget_title_label_opacity.opacity(), 1.0)

    def right_panel_monitor_prelude_nextbutton_clicked(self):
        self.right_panel_monitor_prelude_nextbutton.setEnabled(False)
        #if not self.right_panel_monitor_prelude_pausebutton.isChecked():
        self.right_panel_monitor_prelude_crossfade(stop=True)

    def right_panel_monitor_player_sort_song(self):
        number = self.list_of_songs.keys()[random.randint(0,len(self.list_of_songs.keys())-1)]
        self.right_panel_monitor_prelude.song = self.list_of_songs[number]
        self.right_panel_monitor_prelude.song.append(MP3(self.right_panel_monitor_prelude.song[2]).info.length)
        self.right_panel_monitor_prelude.song.append(number)

    ############################################################################
    ## PREVIEW

    def update_preview(self):
        if len(self.playlist) > 0:

            self.selected_media = self.playlist[0]
            self.right_panel_shownext_preview.setVisible(True)
            if not self.selected_media[0] == 'audio':
                self.right_panel_shownext_preview_audio.setVisible(False)
            if not self.selected_media[0] in ['image', 'video', 'web', 'screencopy','camera']:
                self.right_panel_shownext_preview_image.setPixmap(None)
            if not self.selected_media[0] in ['clock']:
                self.right_panel_shownext_preview_clock.setVisible(False)
            if not self.selected_media[0] in ['camera']:
                None #TODO

            if self.selected_media[0] == 'audio':
                self.right_panel_shownext_preview_audio_artwork.setText(str(self.selected_media[2]))
                self.right_panel_shownext_preview_audio_title.setText(self.selected_media[3])
                self.right_panel_shownext_preview_audio.setVisible(True)
            elif self.selected_media[0] == 'image':
                self.right_panel_shownext_preview_image.setPixmap(QPixmap(self.selected_media[2]).scaled(self.right_panel_shownext_preview_image.width(), self.right_panel_shownext_preview_image.height(), Qt.KeepAspectRatio, Qt.SmoothTransformation))
            elif self.selected_media[0] == 'video':
                self.right_panel_shownext_preview_image.setPixmap(self.selected_media[3].scaled(self.right_panel_shownext_preview_image.width(), self.right_panel_shownext_preview_image.height(), Qt.KeepAspectRatio, Qt.SmoothTransformation))
            elif self.selected_media[0] == 'web':
                self.right_panel_shownext_preview_image.setPixmap(self.selected_media[3].scaled(self.right_panel_shownext_preview_image.width(), self.right_panel_shownext_preview_image.height(), Qt.KeepAspectRatio, Qt.SmoothTransformation))
            elif self.selected_media[0] == 'clock':
                self.right_panel_shownext_preview_clock.setVisible(True)
            elif self.selected_media[0] == 'screencopy':
                None
            elif self.selected_media[0] == 'camera':
                None #TODO

        else:
            self.right_panel_shownext_preview.setVisible(False)

            #self.right_panel_shownext_preview

    def show_now_clicked(self):
        app.monitor.current_media = self.playlist[0]
        app.monitor.show_media()
        #if self.playlist[0][0] in ['audio']:
        #    self.right_panel_monitor_player.setVisible(False)
        if self.playlist[0][-1]:
            self.playlist.append(self.playlist.pop(0))
        else:
            del self.playlist[0]

        self.playlist_options_is_visible = False
        self.playlist_options_panel_open_button_clicked()

        self.populate_playlist()
        self.update_preview()

def generate_effect(widget, effect_type, duration, startValue, endValue):
    widget.setDuration(duration)
    if effect_type == 'geometry':
        widget.setStartValue(QRect(startValue[0],startValue[1],startValue[2],startValue[3]))
        widget.setEndValue(QRect(endValue[0],endValue[1],endValue[2],endValue[3]))
    elif effect_type == 'opacity':
        widget.setStartValue(startValue)
        widget.setEndValue(endValue)
    widget.start()

if __name__ == '__main__':
    app = QApplication(sys.argv)
    print('2')
    app.setDesktopSettingsAware(False)
    print('3')
    #app.setAttribute(Qt.AA_UseSoftwareOpenGL)
    app.setStyle("plastique")
    print('4')
    app.setApplicationName("kihvim")
    print('5')

    app.screencopy = screencopyWindow()
    print('6')
    app.monitor = monitorWindow()
    print('7')
    app.main = mainWindow()
    print('8')

    #if QDesktopWidget().screenCount() > 1:
    #    app.monitor.show()
    app.main.show()
    print('blah')
    app.monitor.hide_all()

    sys.exit(app.exec_())
