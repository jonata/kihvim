# -*- mode: python -*-

a = Analysis(['kihvim.py'],
    pathex=[],
    binaries=[
                ('vlc-3.0.6/libvlc.dll', '.' ),
                ('vlc-3.0.6/libvlccore.dll', '.' ),
                ('gs927w32.exe', '.' ),
                ( 'ffmpeg-latest-win64-static/bin/ffmpeg.exe', 'resources'),
                ( 'ffmpeg-latest-win64-static/bin/ffprobe.exe', 'resources')
    ],
    datas=[
        ('vlc-3.0.6/plugins', 'plugins' ),
        ( 'graphics/*.png', 'graphics' )
    ],
    hiddenimports=['PySide2.QtPrintSupport', 'bs4', 'urllib', 'python-vlc'],
    hookspath=[],
    runtime_hooks=[] )

pyz = PYZ(a.pure)

exe = EXE(pyz,
    a.scripts,
    exclude_binaries=True,
    name='Kihvim.exe',
    strip=False,
    upx=True,
    console=True,
    debug=True,
    icon='graphics/kihvim.ico' )

coll = COLLECT( exe,
                a.binaries,
                a.zipfiles,
                a.datas,
                strip=False,
                upx=True,
                name='Kihvim')
