import sys
from PySide2.QtWidgets import QApplication, QDialog, QLineEdit, QPushButton
from PySide2.QtCore import QUrl, Qt
from PySide2.QtWebEngineWidgets import QWebEngineView, QWebEnginePage

class Form(QDialog):
    def __init__(self, parent=None):
        super(Form, self).__init__(parent)
        self.setWindowTitle("QWebEngine test")

        class MyWebEngineView(QWebEngineView):
            def createWindow(self, windowType):
                return self

        self.webview = MyWebEngineView(parent=self)
        self.webview.setGeometry(0,0,self.width(),self.height())
        self.webview.load(QUrl('https://duckduckgo.com/?q=cat&iar=images'))

if __name__ == '__main__':
    app = QApplication(sys.argv)
    app.setAttribute(Qt.AA_UseSoftwareOpenGL)
    form = Form()
    form.show()
    sys.exit(app.exec_())
